<?php

include_once('../wp-load.php');
//include_once('/home/devplaayco/public_html/wp-load.php');
//include_once('/home/plaayco/public_html/wp-load.php');

if (!isset($_GET['chargecron'])) {
	echo 'UNPAID CHARGE CRON ARG ERROR';
	exit();
}

if (isset($_GET['chargecron']) && $_GET['chargecron'] != 'sr7ede7due') {
	echo 'UNPAID CHARGE CRON ARG CODE ERROR';
	exit();
}

$charges = malinky_activities_get_unpaid_charges();
if ($charges) {
	$coach_charges = malinky_activities_set_charge_array($charges);
	if ($coach_charges) {
		$coach_charges = malinky_activities_get_coach_card_status($coach_charges);
		if (empty($coach_charges))
			echo 'NO PAYMENT DETAILS FOR ANY COACHES';
	}
	malinky_activities_charge_coach($coach_charges);
}
?>