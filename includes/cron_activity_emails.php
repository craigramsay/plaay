<?php

include_once('../wp-load.php');
//include_once('/home/devplaayco/public_html/wp-load.php');
//include_once('/home/plaayco/public_html/wp-load.php');

if (!isset($_GET['plaaycron'])) {
	echo 'EMAIL CRON ARG ERROR';
	exit();
}

if (isset($_GET['plaaycron']) && $_GET['plaaycron'] != '3t4y4vctkn') {
	echo 'EMAIL CRON ARG CODE ERROR';
	exit();
}

$args = array(
	'post_type'         => 'activity_emails',
	'post_status'       => 'publish',
	'posts_per_page'    => -1
);

$emails = new WP_Query($args);

if ($emails->have_posts()) :
	while ($emails->have_posts() ) : $emails->the_post();
		switch (get_field('email_type')) {
			case 'downloaded-voucher-any-set-days-after':
				create_email_dvasda($post);
				break;
			case 'downloaded-voucher-by-format-unique-date':
				create_email_dvbfud($post);
				break;
			case 'downloaded-voucher-by-format-set-days-after':
				create_email_dvbfsda($post);
				break;				
			case 'all-parents-unique-date':
				create_email_apud($post);
				break;
			case 'all-parents-birthday-date':
				create_email_apbd($post);
				break;				
		}
	endwhile;
endif;

/**
 * -------------------------------------------------------------------------------------------------------------------
 * downloaded-voucher-any-set-days-after
 * This is used to email any parent that has activated a voucher and will be sent based on each voucher they activate.
 * Choose the number of days after the activation to send the email.
 * @param obj $post
 * @return array  
 * -------------------------------------------------------------------------------------------------------------------
 */
function create_email_dvasda($post)
{
	$email_vars = array();

	global $wpdb;

	$email_days = get_field('email_days');
	$email_date = date('Y-m-d', mktime(date("H"), date("i"), date("s"), date('m'), date('d') - $email_days, date('Y')));

	$voucher_details = $wpdb->get_results(
		$wpdb->prepare(
			"SELECT malinky_voucher_codes.id as voucher_id, voucher_details_activity_name, parent_id, voucher_activation_code, malinky_parents.name as recipient_name, malinky_parents.email as recipient_email FROM malinky_voucher_codes LEFT JOIN malinky_parents ON malinky_voucher_codes.parent_id = malinky_parents.id WHERE DATE(voucher_code_activated_date) = %s AND voucher_code_activated = %d AND malinky_parents.marketing_accept = %d", $email_date, 1, 1
			), ARRAY_A				
	);
	
	if (!$voucher_details) return;

	//email vars
	foreach ($voucher_details as $k => $v) {
		$email_vars[] = array(
			'recipient_email' 	=> $v['recipient_email'],
			'recipient_name' 	=> $v['recipient_name'],
			'subject' 			=> get_field('email_subject'),
			'body' 				=> wpautop($post->post_content),
			'activity_title' 	=> get_field('show_activity_name') == true ? $v['voucher_details_activity_name'] : NULL,
			'voucher_link'		=> get_field('show_voucher_link') == true ?  $v['voucher_activation_code'] . '-' . $v['voucher_id'] : NULL
		);
	}	
	
	if (!$email_vars) return;

	//send all emails
	foreach ($email_vars as $k => $v) {
		malinky_activities_activity_emails_send(
			$v['recipient_email'],
			$v['recipient_name'],
			$v['subject'],
			$v['body'],
			$v['activity_title'],
			$v['voucher_link']
		);
	}
}

/**
 * ---------------------------------------------------------------------------------------------------------------
 * downloaded-voucher-by-format-unique-date
 * This is used to email any parent that has activated a voucher and will only be sent once to that parent even if
 * they have downloaded similar vouchers.
 * Choose the format of the voucher that has been downloaded to determine the recipients.
 * @param obj $post
 * @return array  
 * ---------------------------------------------------------------------------------------------------------------
 */
function create_email_dvbfud($post)
{
	$email_vars = array();

	//check date
	$email_date = date('Y-m-d', strtotime(get_field('email_date')));
	$todays_date = date('Y-m-d');
	
	if ($email_date != $todays_date) return;

	global $wpdb;

	$parent_details = $wpdb->get_results(
		$wpdb->prepare(
			"SELECT malinky_parents.name as recipient_name, malinky_parents.email as recipient_email FROM malinky_voucher_codes LEFT JOIN malinky_parents ON malinky_voucher_codes.parent_id = malinky_parents.id WHERE voucher_details_format_term = %s AND voucher_code_activated = %d AND malinky_parents.marketing_accept = %d GROUP BY recipient_name", get_field('activity_format')->name, 1, 1
			), ARRAY_A				
	);
	
	if (!$parent_details) return;

	//email vars
	foreach ($parent_details as $k => $v) {
		$email_vars[] = array(
			'recipient_email' 	=> $v['recipient_email'],
			'recipient_name' 	=> $v['recipient_name'],
			'subject' 			=> get_field('email_subject'),
			'body' 				=> wpautop($post->post_content)
		);
	}	
	
	if (!$email_vars) return;

	//send all emails
	foreach ($email_vars as $k => $v) {
		malinky_activities_activity_emails_send(
			$v['recipient_email'],
			$v['recipient_name'],
			$v['subject'],
			$v['body']
		);
	}
}

/**
 * ---------------------------------------------------------------------------------------------------------------
 * downloaded-voucher-by-format-set-days-after
 * This is used to email any parent that has activated a voucher and will only be sent once to that parent even if
 * they have downloaded similar vouchers.
 * Choose the format of the voucher that has been downloaded to determine the recipients.
 * @param obj $post
 * @return array
 * ---------------------------------------------------------------------------------------------------------------
 */
function create_email_dvbfsda($post)
{
	$email_vars = array();

	global $wpdb;

	$email_days = get_field('email_days');
	$email_date = date('Y-m-d', mktime(date("H"), date("i"), date("s"), date('m'), date('d') - $email_days, date('Y')));

	$parent_details = $wpdb->get_results(
		$wpdb->prepare(
			"SELECT malinky_parents.name as recipient_name, malinky_parents.email as recipient_email FROM malinky_voucher_codes LEFT JOIN malinky_parents ON malinky_voucher_codes.parent_id = malinky_parents.id WHERE DATE(voucher_code_activated_date) = %s AND voucher_details_format_term = %s AND voucher_code_activated = %d AND malinky_parents.marketing_accept = %d GROUP BY recipient_name", $email_date, get_field('activity_format')->name, 1, 1
			), ARRAY_A				
	);
	
	if (!$parent_details) return;

	//email vars
	foreach ($parent_details as $k => $v) {
		$email_vars[] = array(
			'recipient_email' 	=> $v['recipient_email'],
			'recipient_name' 	=> $v['recipient_name'],
			'subject' 			=> get_field('email_subject'),
			'body' 				=> wpautop($post->post_content)
		);
	}	
	
	if (!$email_vars) return;

	//send all emails
	foreach ($email_vars as $k => $v) {
		malinky_activities_activity_emails_send(
			$v['recipient_email'],
			$v['recipient_name'],
			$v['subject'],
			$v['body']
		);
	}
}

/**
 * --------------------------------------------------------------------------------------------
 * all-parents-unique-date
 * This is used to email all parents who have signed up for a voucher.
 * They may not have activated it but will still be emailed. Choose the date to send the email.
 * @param obj $post
 * @return array  
 * --------------------------------------------------------------------------------------------
 */
function create_email_apud($post)
{
	$email_vars = array();

	//check date
	$email_date = date('Y-m-d', strtotime(get_field('email_date')));
	$todays_date = date('Y-m-d');

	if ($email_date != $todays_date) return;

	global $wpdb;

	$parent_details = $wpdb->get_results(
		$wpdb->prepare(
			"SELECT name as recipient_name, email as recipient_email FROM malinky_parents WHERE marketing_accept = %d", 1
			), ARRAY_A				
	);
	
	if (!$parent_details) return;

	//email vars
	foreach ($parent_details as $k => $v) {
		$email_vars[] = array(
			'recipient_email' 	=> $v['recipient_email'],
			'recipient_name' 	=> $v['recipient_name'],
			'subject' 			=> get_field('email_subject'),
			'body' 				=> wpautop($post->post_content)
		);
	}	
	
	if (!$email_vars) return;

	//send all emails
	foreach ($email_vars as $k => $v) {
		malinky_activities_activity_emails_send(
			$v['recipient_email'],
			$v['recipient_name'],
			$v['subject'],
			$v['body']
		);
	}	
}

/**
 * -------------------------------------------------------------------------------------------------
 * all-parents-birthday-date
 * This is used to email all parents who have signed up for a voucher.
 * They may not have activated it but will still be emailed.
 * The email date is based 3, 2 and 1 month before the birth month if the parent entered this during 
 * the voucher application process.
 * @param obj $post
 * @return array  
 * -------------------------------------------------------------------------------------------------
 */
function create_email_apbd($post)
{
	$email_vars = array();

	//get date needs to be 1st of month
	$todays_date = date('d');
	$todays_month = date('m');

	//only send on 1st of each month
	if ($todays_date != 10) return;

	global $wpdb;

	$parent_details = $wpdb->get_results(
		$wpdb->prepare(
			"SELECT name as recipient_name, email as recipient_email, child_birth_month FROM malinky_parents WHERE marketing_accept = %d AND child_birth_month BETWEEN 1 AND 12", 1
			), ARRAY_A				
	);
	
	if (!$parent_details) return;

	//email vars
	foreach ($parent_details as $k => $v) {
		if ( 	($v['child_birth_month'] + 1 == $todays_month) || 
				($v['child_birth_month'] + 2 == $todays_month) || 
				($v['child_birth_month'] + 3 == $todays_month)
		) {
			$email_vars[] = array(
				'recipient_email' 	=> $v['recipient_email'],
				'recipient_name' 	=> $v['recipient_name'],
				'subject' 			=> get_field('email_subject'),
				'body' 				=> wpautop($post->post_content)
			);
		}
	}	
	
	if (!$email_vars) return;

	//send all emails
	foreach ($email_vars as $k => $v) {
		malinky_activities_activity_emails_send(
			$v['recipient_email'],
			$v['recipient_name'],
			$v['subject'],
			$v['body']
		);
	}	
}

/**
 * ----------------------------
 * SEND EMAIL
 * @param string $recipient  
 * @param string $subject
 * @param string $body
 * @param string $activity_title OPTIONAL
 * @param string $voucher_link OPTIONAL
 * @return void   
 * ----------------------------
 */
function malinky_activities_activity_emails_send($recipient_email, $recipient_name, $subject, $body, $activity_title = NULL, $voucher_link = NULL)
{
	include_once(ABSPATH . 'wp-includes/class-phpmailer.php');

	$email_settings = get_option('malinky_activities_email_settings_options');

	if (!$email_settings) return;

	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host 		= $email_settings['settings_smtp_host'];
	$mail->SMTPAuth   	= true;
	$mail->Port       	= $email_settings['settings_smtp_port'];
	$mail->Username   	= $email_settings['settings_smtp_username'];
	$mail->Password   	= $email_settings['settings_smtp_password'];
	$mail->SMTPSecure 	= 'ssl';	

	$mail->AddAddress($recipient_email, $recipient_name);

	$mail->SetFrom(get_option('admin_email'), "PLAAY");

	$mail->IsHTML(true);

	$mail->CharSet = 'UTF-8';

	$mail->Subject = $subject;
	$mail->Body =  	'<table width="100%" bgcolor="#282828" style="padding: 0px; margin: 0px;">';
	$mail->Body .=	'<tr>';
	$mail->Body .=	'<td>';
	$mail->Body .=	'<table width="650px" align="center">';
	$mail->Body .=	'<tr>';
	$mail->Body .=	'<td style="color: #FFFFFF;"><img src="' . site_url('img/graphics/logo.png') . '" style="display: block;" alt="PLAAY.CO.UK" /></td>';
	$mail->Body .=	'<td align="right" style="color: #FFFFFF;"><img src="' . site_url('img/graphics/logo_email_strapline.png') . '" style="display: block;" alt="TRY IT. LOVE IT. PLAAY IT." /></td>';
	$mail->Body .=	'</tr>';
	$mail->Body .=	'</table>';
	$mail->Body .=	'</td>';
	$mail->Body .=	'</tr>';
	$mail->Body .=	'</table>';
	$mail->Body .=	'<table width="650px" align="center" style="color: #666666; font-family: Helvetica, Arial, sans-serif;">';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 15px;"></td></tr>';	
	$mail->Body .=	'<tr><td><p style="font-size: 20px; line-height: 18px; padding: 0px; margin: 0px;">Dear ' . $recipient_name . ',</p></td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';
	if ($activity_title) {
		$mail->Body    .= '<tr><td><p style="font-size: 14px; line-height: 18px; padding: 0px; margin: 0px;">Thanks for downloading a ' . $activity_title . ' voucher from PLAAY.</p></td></tr>';
		$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 5px;"></td></tr>';
	}
	$mail->Body .= 	'<tr><td style="font-size: 14px;">' . $body . '</td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 5px;"></td></tr>';
	if ($voucher_link) {
		$mail->Body .= '<tr><td><p style="font-size: 14px; line-height: 18px; padding:0px; margin:0px;"><a href="' . network_site_url('voucher?activation_code=' . $voucher_link) . '" style="color: #9e4381; font-weight: bold; text-decoration: none;">' . network_site_url('voucher') . '</a></p></td></tr>';
		$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';	
	}
	$mail->Body .= 	'<tr><td><p style="font-size: 14px; line-height: 18px; padding:0px; margin:0px;">Thanks,</p></td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';	
	$mail->Body .= 	'<tr><td><p style="font-size: 20px; padding: 0px; margin: 0px;">PLAAY</p></td></tr>';	
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';
	$mail->Body .= 	'<tr><td style="border-top: 2px solid #e5e5e5; padding: 0px; margin: 0px;"><p>&nbsp;</p></td></tr>';
	$mail->Body .= 	'<tr><td align="center"><p style="font-size: 20px; line-height: 18px; padding:0px; margin:0px;"><a href="' . esc_url(site_url()) . '" style=" color: #9e4381; font-weight: bold; text-decoration: none;">WWW.PLAAY.CO.UK</a></p></td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';
	$mail->Body .= 	'<tr><td><p style="font-size: 12px; line-height: 18px; padding:0px; margin:0px; text-align: center;">PLAAY. Unit 11, Hove Business Centre, Fonthill Road, Hove, East Sussex, BN3 6HA. <a href="' . esc_url(site_url('email-unsubscribe?unsubscribe=' . md5($recipient_email), 'https')) . '" style="color: #666666; text-decoration: none;">Unsubscribe</a></p></td></tr>';
	$mail->Body .= '</table>';
	
	//echo $mail->Body;

	if(!$mail->Send()) {
		error_log($mail->ErrorInfo, 0);
    }
}

//----------------------------**********************************----------------------------//
//----------------------------**********************************----------------------------//
//----------------------------**********************************----------------------------//
//----------------------------**********************************----------------------------//
//----------------------------**********************************----------------------------//
//----------------------------**********************************----------------------------//

$args_coaches = array(
	'post_type'         => 'coach_emails',
	'post_status'       => 'publish',
	'posts_per_page'    => -1
);

$emails_coaches = new WP_Query($args_coaches);

if ($emails_coaches->have_posts()) :
	while ($emails_coaches->have_posts() ) : $emails_coaches->the_post();
		switch (get_field('email_type')) {
			case 'coach-unique-date':
				create_email_cud($post);
				break;
			case 'coach-registered-set-days-after':
				create_email_crsda($post);
				break;
		}
	endwhile;
endif;

/**
 * ---------------------------------------------------------------------------------------------------------------
 * coach-unique-date
 * This is used to email all coaches on a specific date
 * @param obj $post
 * @return array  
 * ---------------------------------------------------------------------------------------------------------------
 */
function create_email_cud($post)
{
	$email_vars = array();

	//check date
	$email_date = date('Y-m-d', strtotime(get_field('email_date')));
	$todays_date = date('Y-m-d');
	
	if ($email_date != $todays_date) return;

	$args = array(
		'role' 		=> 'coach',
		'fields' 	=> array(
			'user_email',
			'display_name'
			)
	);

	$coaches = get_users($args);

	if (!$coaches) return;

	//email vars
	foreach ($coaches as $k => $v) {
		$email_vars[] = array(
			'recipient_email' 	=> $v->user_email,
			'recipient_name' 	=> $v->display_name,
			'subject' 			=> get_field('email_subject'),
			'body' 				=> wpautop($post->post_content)
		);
	}	
	
	if (!$email_vars) return;

	//send all emails
	foreach ($email_vars as $k => $v) {
		malinky_activities_coach_emails_send(
			$v['recipient_email'],
			$v['recipient_name'],
			$v['subject'],
			$v['body']
		);
	}
}

function create_email_crsda($post)
{
	$email_vars = array();

	global $wpdb;

	$email_days = get_field('email_days');
	$email_date = date('Y-m-d', mktime(date("H"), date("i"), date("s"), date('m'), date('d') - $email_days, date('Y')));

	$args = array(
		'role' 		=> 'coach',
		'fields' 	=> array(
			'user_email',
			'display_name',
			'user_registered'
			)
	);

	$coaches = get_users($args);

	if (!$coaches) return;

	//email vars
	foreach ($coaches as $k => $v) {

		if (date('Y-m-d', strtotime($v->user_registered)) == $email_date) {

			$email_vars[] = array(
				'recipient_email' 	=> $v->user_email,
				'recipient_name' 	=> $v->display_name,
				'subject' 			=> get_field('email_subject'),
				'body' 				=> wpautop($post->post_content)
			);

		}

	}	
	
	if (!$email_vars) return;

	//send all emails
	foreach ($email_vars as $k => $v) {
		malinky_activities_coach_emails_send(
			$v['recipient_email'],
			$v['recipient_name'],
			$v['subject'],
			$v['body']
		);
	}
}

/**
 * ----------------------------
 * SEND EMAIL
 * @param string $recipient  
 * @param string $subject
 * @param string $body
 * @param string $activity_title OPTIONAL
 * @param string $voucher_link OPTIONAL
 * @return void   
 * ----------------------------
 */
function malinky_activities_coach_emails_send($recipient_email, $recipient_name, $subject, $body, $activity_title = NULL, $voucher_link = NULL)
{
	include_once(ABSPATH . 'wp-includes/class-phpmailer.php');

	$email_settings = get_option('malinky_activities_email_settings_options');

	if (!$email_settings) return;

	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host 		= $email_settings['settings_smtp_host'];
	$mail->SMTPAuth   	= true;
	$mail->Port       	= $email_settings['settings_smtp_port'];
	$mail->Username   	= $email_settings['settings_smtp_username'];
	$mail->Password   	= $email_settings['settings_smtp_password'];
	$mail->SMTPSecure 	= 'ssl';	

	$mail->AddAddress($recipient_email, $recipient_name);

	$mail->SetFrom(get_option('admin_email'), "PLAAY");

	$mail->IsHTML(true);

	$mail->CharSet = 'UTF-8';
	
	$mail->Subject = $subject;
	$mail->Body =  	'<table width="100%" bgcolor="#282828" style="padding: 0px; margin: 0px;">';
	$mail->Body .=	'<tr>';
	$mail->Body .=	'<td>';
	$mail->Body .=	'<table width="650px" align="center">';
	$mail->Body .=	'<tr>';
	$mail->Body .=	'<td style="color: #FFFFFF;"><img src="' . site_url('img/graphics/logo.png') . '" style="display: block;" alt="PLAAY.CO.UK" /></td>';
	$mail->Body .=	'<td align="right" style="color: #FFFFFF;"><img src="' . site_url('img/graphics/logo_email_strapline.png') . '" style="display: block;" alt="TRY IT. LOVE IT. PLAAY IT." /></td>';
	$mail->Body .=	'</tr>';
	$mail->Body .=	'</table>';
	$mail->Body .=	'</td>';
	$mail->Body .=	'</tr>';
	$mail->Body .=	'</table>';
	$mail->Body .=	'<table width="650px" align="center" style="color: #666666; font-family: Helvetica, Arial, sans-serif;">';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 15px;"></td></tr>';	
	$mail->Body .=	'<tr><td><p style="font-size: 20px; line-height: 18px; padding: 0px; margin: 0px;">Dear ' . $recipient_name . ',</p></td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';
	if ($activity_title) {
		$mail->Body    .= '<tr><td><p style="font-size: 14px; line-height: 18px; padding: 0px; margin: 0px;">Thanks for downloading a ' . $activity_title . ' voucher from PLAAY.</p></td></tr>';
		$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 5px;"></td></tr>';
	}
	$mail->Body .= 	'<tr><td style="font-size: 14px;">' . $body . '</td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 5px;"></td></tr>';
	if ($voucher_link) {
		$mail->Body .= '<tr><td><p style="font-size: 14px; line-height: 18px; padding:0px; margin:0px;"><a href="' . network_site_url('voucher?activation_code=' . $voucher_link) . '" style="color: #9e4381; font-weight: bold; text-decoration: none;">' . network_site_url('voucher') . '</a></p></td></tr>';
		$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';	
	}
	$mail->Body .= 	'<tr><td><p style="font-size: 14px; line-height: 18px; padding:0px; margin:0px;">Thanks,</p></td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';	
	$mail->Body .= 	'<tr><td><p style="font-size: 20px; padding: 0px; margin: 0px;">PLAAY</p></td></tr>';	
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';
	$mail->Body .= 	'<tr><td style="border-top: 2px solid #e5e5e5; padding: 0px; margin: 0px;"><p>&nbsp;</p></td></tr>';
	$mail->Body .= 	'<tr><td align="center"><p style="font-size: 20px; line-height: 18px; padding:0px; margin:0px;"><a href="' . esc_url(site_url()) . '" style=" color: #9e4381; font-weight: bold; text-decoration: none;">WWW.PLAAY.CO.UK</a></p></td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';
	$mail->Body .= 	'<tr><td><p style="font-size: 12px; line-height: 18px; padding:0px; margin:0px; text-align: center;">PLAAY. Unit 11, Hove Business Centre, Fonthill Road, Hove, East Sussex, BN3 6HA.</p></td></tr>';
	$mail->Body .= '</table>';
	
	//echo $mail->Body;

	if(!$mail->Send()) {
		error_log($mail->ErrorInfo, 0);
    }
}