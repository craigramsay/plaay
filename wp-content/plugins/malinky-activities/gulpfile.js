/* ------------------------------------------------------------------------ *
 * Gulp Packages
 * ------------------------------------------------------------------------ */

var gulp        = require('gulp'); 

var concat      = require('gulp-concat');
var del         = require('del');
var minifyCSS   = require('gulp-minify-css');
var rename      = require('gulp-rename');
var runSequence = require('run-sequence');
var sass        = require('gulp-ruby-sass');
var sourcemaps  = require('gulp-sourcemaps');
var uglify      = require('gulp-uglify');

/*
https://github.com/wearefractal/gulp-concat
https://www.npmjs.com/package/gulp-concat
https://github.com/sindresorhus/del
https://www.npmjs.com/package/del
https://github.com/jonathanepollack/gulp-minify-css
https://www.npmjs.com/package/gulp-minify-css
https://github.com/hparra/gulp-rename
https://www.npmjs.com/package/gulp-rename
https://github.com/OverZealous/run-sequence
https://www.npmjs.com/package/run-sequence
https://github.com/sindresorhus/gulp-ruby-sass/tree/rw/1.0
https://www.npmjs.com/package/gulp-ruby-sass
https://github.com/floridoo/gulp-sourcemaps
https://www.npmjs.com/package/gulp-sourcemaps
https://github.com/terinjokes/gulp-uglify
https://www.npmjs.com/package/gulp-uglify
https://github.com/terinjokes/gulp-uglify/issues/56
*/





/* ------------------------------------------------------------------------ *
 * Local
 * 
 * gulp
 *
 * Compile SASS and create sourcemap.
 * Sourcemap is stored in sourcemaps folder.
 * Sourcemap is linked into correct folder structure from developer tools.
 * ------------------------------------------------------------------------ */

/**
 * Compile our SASS.
 * Doesn't support globs hence the return sass rather than gulp.src.
 *
 * sourceRoot sets the path where the source files are hosted relative to the source map.
 * This makes things appear in the correct folders when viewing through developer tools.
 */
gulp.task('sass', function() {
    return sass('sass', { sourcemap: true, style: 'expanded' })
    .on('error', function (err) {
      console.error('SASS Error - ', err.message);
   	})
    .pipe(sourcemaps.write('sourcemaps', {includeContent: false, sourceRoot: '../sass'}))
    .pipe(gulp.dest('./'));
});


/**
 * Watch files for changes.
 */
gulp.task('watch', function() {
    gulp.watch('sass/**/*.scss', ['sass']);
});


/**
 * Set up default (local) task.
 */
//gulp.task('default', ['sass', 'watch']);





/* ------------------------------------------------------------------------ *
 * Dev
 * 
 * gulp dev
 *
 * Move all applicable files and folders.
 * This includes all js for debugging with sourcemaps.
 * Concat and minify JS to scripts.js.
 * ------------------------------------------------------------------------ */

/**
 * Delete all contents of dev folder.
 */
gulp.task('dev-clean', function (cb) {
    del('dev/*', cb);
});


/**
  * Move root .php files.
  */
gulp.task('dev-move-files', function() {
    return gulp.src('*.php')
        .pipe(gulp.dest('dev'));
});


/**
  * Move root directories and their contents.
  * Move js and SASS to be used with root maps on dev.
  */
gulp.task('dev-move-dir', function() {
    return gulp.src(['admin/**', 'css/**', 'form-fields/**', 'forms/**', 'includes/**', 'js/**', 'templates/**'], { base: './'} )
        .pipe(gulp.dest('dev'));
});


/**
 * Concat (rename) and minify our JS.
 *
 * sourceRoot sets the path where the source files are hosted relative to the source map.
 * This makes things appear in the correct folders when viewing through developer tools.
 */
gulp.task('dev-scripts', function() {
    return gulp.src(['js/main.js', 'js/jquery.validate.js', 'js/activity-validate.js', 'js/postcode-search-validate.js', 'js/voucher-validate.js', 'js/voucher-application-validate.js', 'js/email-unsubscribe-validate.js'])
		.pipe(sourcemaps.init())
		.pipe(concat('malinky-activities-scripts.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('../sourcemaps', {includeContent: false, sourceRoot: '../js'}))
        .pipe(gulp.dest('dev/js'));
});


/**
 * Minify and rename activities-ajax.js
 */
gulp.task('dev-scripts-others', function() {
    return gulp.src(['js/activities-ajax.js', 'js/social-api.js'])
        .pipe(sourcemaps.init())
        .pipe(rename(function (path) {
            path.extname = ".min.js"
        }))
        .pipe(uglify())
        .pipe(sourcemaps.write('../sourcemaps', {includeContent: false, sourceRoot: '../js'}))
        .pipe(gulp.dest('dev/js'));
});


/**
 * Set up dev task.
 */
gulp.task('dev', function() {
  	runSequence('dev-clean', 
                'dev-move-files', 
                'dev-move-dir',  
                'dev-scripts', 
                'dev-scripts-others'
            );
})





/* ------------------------------------------------------------------------ *
 * Prod
 * 
 * gulp prod
 *
 * Move all applicable files and folders.
 * Compress CSS.
 * Concat and minify JS to scripts.js
 * ------------------------------------------------------------------------ */

/**
 * Delete all contents of prod folder.
 */
gulp.task('prod-clean', function (cb) {
    del('prod/*', cb);
});


/**
  * Move root .php files.
  */
gulp.task('prod-move-files', function() {
    return gulp.src('*.php')
        .pipe(gulp.dest('prod'));
});


/**
  * Move root directories and their contents.
  * Not js as we just need minified version as no sourcemaps are used in prod.
  */
gulp.task('prod-move-dir', function() {
    return gulp.src(['admin/**', 'css/**', 'form-fields/**', 'forms/**', 'includes/**', 'templates/**'], { base: './'} )
        .pipe(gulp.dest('prod'));
});


/**
 * Concat (rename) and minify our JS.
 */
gulp.task('prod-scripts', function() {
    return gulp.src(['js/main.js', 'js/jquery.validate.js', 'js/activity-validate.js', 'js/postcode-search-validate.js', 'js/voucher-validate.js', 'js/voucher-application-validate.js', 'js/email-unsubscribe-validate.js'])
        .pipe(concat('malinky-activities-scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('prod/js'));
});


/**
 * Minify and rename activities-ajax.js
 */
gulp.task('prod-scripts-others', function() {
    return gulp.src(['js/activities-ajax.js', 'js/social-api.js'])
        .pipe(rename(function (path) {
            path.extname = ".min.js"
        }))
        .pipe(uglify())
        .pipe(gulp.dest('prod/js'));
});


/**
 * Move two already minifed js files.
 */
gulp.task('prod-scripts-cards', function() {
    return gulp.src(['js/card-details.min.js', 'js/jquery.payment.min.js'])
        .pipe(gulp.dest('prod/js'));
});


/**
 * Set up prod task.
 */
gulp.task('prod', function() {
    runSequence('prod-clean', 
                'prod-move-files', 
                'prod-move-dir', 
                'prod-scripts', 
                'prod-scripts-others', 
                'prod-scripts-cards'
            );
})