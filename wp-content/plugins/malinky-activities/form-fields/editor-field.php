<?php
$editor = array(
	'media_buttons' => false,
	'textarea_rows' => 8,
	'quicktags' => false,
	'tinymce' => array(
		'plugins' => 'paste',
		'paste_auto_cleanup_on_paste' => true,
		'paste_remove_styles' => true,
		'paste_text_sticky' => true,
		'paste_text_sticky_default' => true,				
		'paste_retain_style_properties' => "none",
		'paste_strip_class_attributes' => true,
		'theme_advanced_buttons1' => 'bold,italic,|,bullist,numlist,|,link,|,undo,redo,|,|,code',
		'theme_advanced_buttons2' => '',
		'theme_advanced_buttons3' => '',
		'theme_advanced_buttons4' => '',
		'content_css' => get_stylesheet_directory_uri() . '/css/malinky-editor-styles.css' 
	),
);
wp_editor(isset($field['value']) ? esc_textarea($field['value']) : '', $key, $editor);