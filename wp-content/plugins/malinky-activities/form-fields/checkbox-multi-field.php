<?php
foreach($field['options'] as $option_key => $option_value) { ?>
	<div class="checkbox_block">
	<label for="<?php esc_attr_e($option_key); ?>"><?php esc_html_e($option_value); ?></label>
	<input type="checkbox" name="<?php echo esc_attr($key); ?>[]" id="<?php echo esc_attr($option_key); ?>" value="<?php echo esc_attr($option_key); ?>" <?php if (!empty($field['value'])) { foreach($field['value'] as $array_checkbox_value) { checked($array_checkbox_value, $option_key); } } ?> />
	</div>
<?php } ?>
<?php if (!empty($field['description'])) : ?><small class="description"><?php echo esc_html($field['description']); ?></small><?php endif; ?>
