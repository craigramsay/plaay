<select name="<?php echo esc_attr($key); ?>" id="<?php echo esc_attr($key); ?>">
<option value="please_choose">Please Choose</option>
<?php
foreach($field['options'] as $option_key => $option_value) { ?>
	<option value="<?php echo esc_attr($option_key); ?>" <?php if (isset($field['value'])) { selected($field['value'], $option_key); } ?>><?php echo esc_html($option_value); ?></option>
<?php } ?>
</select>
<?php if (!empty($field['description'])) : ?><small class="description"><?php echo esc_html($field['description']); ?></small><?php endif; ?>