<div class="checkbox_block">
<input type="checkbox" name="<?php echo esc_attr($key); ?>" id="<?php echo esc_attr($key); ?>" value="1" <?php if (isset($field['value'])) { checked($field['value'], 1); } ?> class="single_checkbox" />
</div>
<?php if (!empty($field['description'])) : ?><small class="description"><?php echo esc_html($field['description']); ?></small><?php endif; ?>
