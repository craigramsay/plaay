<?php
/**
 * Plugin Name: Malinky Activities
 * Plugin URI: 
 * Description: Activities CMS
 * Version: 1.1
 * Author: Malinky
 * Author URI: http://www.malinkymedia.com
 * Copyright: 2013 Craig Ramsay
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

if (! defined('ABSPATH')) exit; // Exit if accessed directly

class Malinky_Activities {

	function __construct()
	{
		define('MALINKY_ACTIVITIES_PLUGIN_DIR', plugin_dir_path(__FILE__));
		define('MALINKY_ACTIVITIES_PLUGIN_URL', plugins_url(basename(plugin_dir_path(__FILE__))));

		include('includes/malinky-activities-activities-ajax.php');		
		include('includes/malinky-activities-activity-functions.php');
		include('includes/malinky-activities-custom-post-types.php');
		include('includes/malinky-activities-emails.php');
		include('includes/malinky-activities-form-fields.php');
		include('includes/malinky-activities-form-processing.php');
		include('includes/malinky-activities-generic-functions.php');
		include('includes/malinky-activities-postcode-functions.php');
		include('includes/malinky-activities-shortcodes.php');
		include('includes/malinky-activities-stripe-functions.php');
		include('includes/malinky-activities-template.php');
		include('includes/malinky-activities-voucher-functions.php');
		include('includes/malinky-activities-coach-logo-functions.php');
		include('includes/stripe-lib/Stripe.php');
		include('includes/malinky-activities-rich-snippets-seo.php');

		if (is_admin()) {
			include_once('admin/malinky-activities-settings.php');
			include_once('admin/malinky-activities-meta-boxes.php');
			include_once('admin/malinky-activities-admin-screens.php');
			include_once('admin/malinky-activities-admin-parents-page.php');
			include_once('admin/malinky-activities-admin-vouchers-page.php');
			include_once('admin/malinky-activities-admin-voucher-page.php');
			include_once('admin/malinky-activities-admin-invoices-page.php');
			include_once('admin/malinky-activities-admin-invoice-page.php');
			include_once('admin/malinky-activities-admin-unpaid-invoices-page.php');
			add_action('admin_enqueue_scripts', array($this, 'malinky_activities_admin_scripts'));
		}

		//init classes
		$this->post_types = new Malinky_Activities_Custom_Post_Types();

		// Activation - works with symlinks
		register_activation_hook(basename(dirname(__FILE__)) . '/' . basename(__FILE__), array($this->post_types, 'register_post_types'), 10);
		register_activation_hook(basename(dirname(__FILE__)) . '/' . basename(__FILE__), create_function('', "include_once('includes/malinky-activities-install.php');"), 10);
		register_activation_hook(basename(dirname(__FILE__)) . '/' . basename(__FILE__), 'flush_rewrite_rules', 15);

		include('includes/fpdf.php');

		add_action('wp_enqueue_scripts', array($this, 'malinky_activities_frontend_scripts'));
	}


	function malinky_activities_frontend_scripts()
	{	

		if ( WP_ENV == 'local' ) {

			//LOAD MAIN.JS
			wp_register_script('malinky_activities_main_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/main.js', array('jquery'), NULL, true);
			wp_enqueue_script('malinky_activities_main_js');

			//LOAD VALIDATE.JS USED ON MOST PAGES FRONT END AND COACH CMS
			wp_register_script( 'malinky_activities_validate_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/jquery.validate.js', array('jquery'), NULL, true);
			wp_enqueue_script('malinky_activities_validate_js');

			if (is_page( 'add-activity') || is_page( 'edit-activity')) {
				wp_register_script('malinky_activities_activity_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/activity-validate.js', array('jquery', 'malinky_activities_validate_js'), NULL, true);
				wp_enqueue_script('malinky_activities_activity_js');
			}

			if (is_page('home') || is_page( 'activities')) {			
				wp_register_script('malinky_activities_postcode_search_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/postcode-search-validate.js', array('jquery', 'malinky_activities_validate_js'), NULL, true);
				wp_enqueue_script('malinky_activities_postcode_search_js');
			}

			if (is_page('voucher')) {
				wp_register_script('malinky_activities_voucher_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/voucher-validate.js', array('jquery', 'malinky_activities_validate_js'), NULL, true);
				wp_enqueue_script('malinky_activities_voucher_js');
				wp_register_script('malinky_activities_voucher_social_api_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/social-api.js', false, NULL, true);
				wp_enqueue_script('malinky_activities_voucher_social_api_js');			
			}	

			if (is_page('voucher-application')) {		
				wp_register_script('malinky_activities_voucher_application_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/voucher-application-validate.js', array('jquery', 'malinky_activities_validate_js'), NULL, true);
				wp_enqueue_script('malinky_activities_voucher_application_js');
			}				
			
			if (is_page('email-unsubscribe')) {			
				wp_register_script('malinky_activities_email_unsubscribe_voucher_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/email-unsubscribe-validate.js', array('jquery', 'malinky_activities_validate_js'), NULL, true);
				wp_enqueue_script('malinky_activities_email_unsubscribe_voucher_js');
			}

			if (is_page('activities')) {
				wp_enqueue_script('malinky-activities-activities-ajax-handle', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/activities-ajax.js', array('jquery' ));
				wp_localize_script( 'malinky-activities-activities-ajax-handle', 'MalinkyActivitiesActivitiesAjax', array('ajaxurl' => admin_url('admin-ajax.php')));			
			}

		}

		if ( WP_ENV == 'dev' || WP_ENV == 'prod' ) {

			/*
			 * 'main.js', 'jquery.validate.js', 'activity-validate.js', 'postcode-search-validate.js', 'voucher-validate.js', 'voucher-application-validate.js', 'email-unsubscribe-validate.js', 'social-api.js'
			 */
			wp_register_script( 'malinky-activities-scripts-min-js',
								MALINKY_ACTIVITIES_PLUGIN_URL . '/js/malinky-activities-scripts.min.js',
								array( 'jquery' ),
								NULL,
								true
			);
			wp_enqueue_script( 'malinky-activities-scripts-min-js' );

			if (is_page('voucher')) {
				wp_register_script('malinky_activities_voucher_social_api_min_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/social-api.min.js', false, NULL, true);
				wp_enqueue_script('malinky_activities_voucher_social_api_min_js');			
			}	

			if (is_page('activities')) {
				wp_enqueue_script('malinky-activities-activities-ajax-handle', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/activities-ajax.min.js', array('jquery' ));
				wp_localize_script( 'malinky-activities-activities-ajax-handle', 'MalinkyActivitiesActivitiesAjax', array('ajaxurl' => admin_url('admin-ajax.php')));			
			}

		}

		if (is_page('payment-details')) {
			wp_enqueue_script('stripejs', 'https://js.stripe.com/v2/', '', '2.0', true );
			wp_register_script('malinky_activities_payment_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/jquery.payment.min.js', array('stripejs'), NULL, true);	
			wp_enqueue_script('malinky_activities_payment_js');	
			wp_register_script('malinky_activities_card_details_js', MALINKY_ACTIVITIES_PLUGIN_URL . '/js/card-details.min.js', array('stripejs', 'malinky_activities_payment_js', 'jquery'), NULL, true);
			
			$stripe_published_key = get_option('malinky_activities_stripe_settings_options');
			//Live Stripe Key
			if ($stripe_published_key['settings_stripe_live']) {

				wp_localize_script(	'malinky_activities_card_details_js',
									'stripe_published_key',
									array('stripe_published_key' => $stripe_published_key['settings_stripe_live_published_key'])
								);

			//Test Stripe Key
			} else {

				wp_localize_script(	'malinky_activities_card_details_js',
									'stripe_published_key',
									array('stripe_published_key' => $stripe_published_key['settings_stripe_test_published_key'])
								);
				
			}

			wp_enqueue_script('malinky_activities_card_details_js');
		}

	}

	function malinky_activities_admin_scripts()
	{
		wp_register_style('malinky_activities_admin_css', MALINKY_ACTIVITIES_PLUGIN_URL . '/css/malinky-admin.css', '', NULL);
		wp_enqueue_style('malinky_activities_admin_css');
	}
}
$malinky_activities = new Malinky_Activities();