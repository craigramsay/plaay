<?php
if (!empty($filters_output)) {
	foreach($filters_output as $term => $term_data) {
		echo '<nav class="sidebar_block" role="navigation">';
		echo '<h4 class="sidebar_heading">' . str_replace('_', ' ', $term_data[0]['taxonomy_label']) . '</h4>';
		foreach ($term_data as $key => $value) {
			echo '<div class="activities_filter_checkbox_block">';
				echo '<input type="checkbox" name="filter_' . $term . '" id="' . $term . '_' . strtolower($value['term_name']) . '" value="' . $value['term_id'] . '"' . checked($value['term_active'], $value['term_id'], false) . ' />';		
				echo '<label for="' . $term . '_' . strtolower($value['term_name']) . '">' . $value['term_name'] . ' (' . $value['term_count'] . ')</label>';
			echo '</div>';
		}
		echo '</nav>';
	}
} else {
	echo '<p>There are no results.</p>';
}