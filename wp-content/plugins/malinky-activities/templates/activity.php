<?php
/**
 * The Template for displaying all single activities
 */

get_header(); ?>

	<main role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="col">
					<div class="col_item col_item_full">
						<?php $distance = 0; ?>
						<?php if (isset($_SESSION['postcode'])) {
							$distance = parent_coach_distance($post->ID, $_SESSION['postcode']);
						} ?>
					</div>
				</div><!-- .col -->

				<div class="col" itemscope itemtype="http://schema.org/Product">
					<div class="col_item col_item_7_10 medium-col_item_half small-col_item_full">
						<h1 itemprop="name"><?php the_title(); ?></h1>
						<span itemprop="description"><?php echo get_post_meta($post->ID, '_activity_description', true); ?></span>
						<?php
						$additional_information = get_post_meta($post->ID, '_activity_additional_information', true);
						if ($additional_information != '') { ?>
							<h4>Additional Information</h4>
							<?php echo $additional_information; ?>
						<?php }
						$active_coach_payment_status = get_user_meta($post->post_author, '_coach_stripe_payment_status', true);
						if ( $active_coach_payment_status && !empty($distance) ) { ?>
							<a href="<?php echo wp_nonce_url(add_query_arg(array('activity_id' => $post->ID), site_url('voucher-application')),'malinky_voucher_application_nonce_action', 'voucher_application_nce'); ?>" class="button full_width activity_normal_button">Voucher Application</a>
						<?php } ?>
					</div><!--
					--><div class="col_item col_item_3_10 medium-col_item_half small-col_item_full">
						<div class="col">
							<div class="col_item col_item--align_center col_item--margin_bottom_20 col_item_full">
								<?php
								echo wp_get_attachment_image(get_user_meta($post->post_author, '_coach_logo', true), 'coach-logo');
								?>
							</div>								
							<div class="col_item col_item_full">
								<div class="sidebar_block">
									<h4 class="sidebar_heading">Location</h4>
									<ul class="bulleted_list">
										<li><?php echo esc_html(get_post_meta($post->ID, '_activity_town', true)) . ', ' . esc_html(the_activity_county($post, false)); ?></li>
										<?php if (!empty($distance)) { ?>
										<li><?php echo $distance . ' miles'; ?></li>
										<li><?php echo coach_will_travel_distance($post->ID, $distance); ?></li>
										<?php } ?>
										<?php if (empty($distance)) { ?>
											<li>Before you can download a voucher please enter a postcode to ensure this activity is within a suitable distance from your location.</li>
										<?php } ?>
									</ul>
								</div>
							</div>
							<div class="col_item col_item_full">
								<div class="sidebar_block">
									<h4 class="sidebar_heading">Sport or Activity Types</h4>
									<ul class="bulleted_list">
										<li><?php esc_html(the_activity_format($post)); ?></li>
										<?php foreach (the_activity_category($post, false) as $k => $v) { ?>
											<li><?php echo $v; ?></li>
										<?php } ?>
									</ul>
								</div>
							</div>								
							<div class="col_item col_item_full">
								<div class="sidebar_block">
									<h4 class="sidebar_heading">Age Group of Child</h4>
									<ul class="ticked_list">
										<?php foreach (the_activity_age_group($post, false) as $k => $v) { ?>
											<li><?php echo $v; ?></li>
										<?php } ?>
									</ul>
								</div>
							</div>	
							<div class="col_item col_item_full">
								<div class="sidebar_block">								
									<h4 class="sidebar_heading">Voucher Types</h4>
									<ul class="ticked_list">
										<?php foreach (the_activity_voucher($post, false) as $k => $v) { ?>
											<li itemprop="offers" itemscope itemtype="http://schema.org/Offer">
												<?php echo $v; ?>
												<meta itemprop="price" content="0.00" />
												<meta itemprop="priceCurrency" content="GBP" />
												<link itemprop="availability" href="http://schema.org/InStock" content="in_stock" />
											</li>
										<?php } ?>
									</ul>
								</div>
							</div>
							<div class="col_item col_item--margin_bottom_20 col_item_full activity_mobile_button">
							<?php if ( $active_coach_payment_status && !empty($distance) ) { ?>
							<a href="<?php echo wp_nonce_url(add_query_arg(array('activity_id' => $post->ID), site_url('voucher-application', 'https')),'malinky_voucher_application_nonce_action', 'voucher_application_nce'); ?>" class="button full_width">Voucher Application</a>
							<?php } ?>
							</div>
							<div class="col_item col_item_full">
								<h4 class="sidebar_heading">Share</h4>
								<!-- Go to www.addthis.com/dashboard to customize your tools -->
								<div class="addthis_sharing_toolbox"></div>
							</div>																					
						</div><!-- .col nested -->
					</div>					
				</div><!-- .col -->

			</article><!-- #post -->
		<?php endwhile; // end of the loop. ?>
	</main>

<?php get_footer(); ?>