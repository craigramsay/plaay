<table class="coach_table">
	<thead>
		<tr>
			<th class="coach_invoice_th_stripe_charge_id">Charge ID</th>
			<th class="coach_invoice_th_stripe_date"><a href="<?php echo get_query_var('orderby') == 'date_desc' ? add_query_arg(array('orderby' => 'date_asc')) : add_query_arg(array('orderby' => 'date_desc')); ?>"<?php echo get_query_var('orderby') == 'date_desc' ? 'class="sort_desc"' : '' ?><?php echo get_query_var('orderby') == 'date_asc' ? 'class="sort_asc"' : '' ?><?php echo get_query_var('orderby') == '' ? 'class="sort_desc"' : '' ?>>Date</a></th>
			<th class="coach_invoice_th_stripe_amount"><a href="<?php echo get_query_var('orderby') == 'amount_desc' ? add_query_arg(array('orderby' => 'amount_asc')) : add_query_arg(array('orderby' => 'amount_desc')); ?>"<?php echo get_query_var('orderby') == 'amount_desc' ? 'class="sort_desc"' : '' ?><?php echo get_query_var('orderby') == 'amount_asc' ? 'class="sort_asc"' : '' ?>>Amount</a></th>
		</tr>
	</thead>
	<tbody>
		<?php if (!$invoices) : ?>
			<tr>
				<td colspan="6"><?php esc_html_e('You don\'t have any invoices.'); ?></td>
			</tr>
		<?php else : ?>
			<?php foreach ($invoices as $invoice) : ?>
				<tr><td class="coach_invoice_stripe_charge_id"><a href="<?php echo esc_url(add_query_arg(array('charge' => $invoice->stripe_charge_id), site_url('coach/invoice'))); ?>"><?php esc_html_e($invoice->stripe_charge_id); ?></a></td>
					<td class="coach_invoice_stripe_date"><?php esc_html_e(date('d/m/Y', strtotime($invoice->stripe_charge_created))); ?></td>
					<td class="coach_invoice_stripe_amount">&pound;<?php esc_html_e(number_format($invoice->stripe_charge_amount/100, 2)); ?></td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>