<?php
/**
 * Pagination - Show numbered pagination for catalog pages.
 */
if (!defined('ABSPATH')) exit(); // Exit if accessed directly

if ($max_num_pages <= 1)
	return;
?>
<div class="col">
	<div class="col_item col_item_full">
		<div id="activities_pagination">
		<?php
			echo paginate_links(array(
				'base' 			=> str_replace(999999999, '%#%', get_pagenum_link(999999999)),
				'format' 		=> '',
				'current' 		=> max(1, $page),
				'total' 		=> $max_num_pages,
				'prev_text' 	=> '<',
				'next_text' 	=> '>',
				'type'			=> 'plain',
				'end_size'		=> 3,
				'mid_size'		=> 3
			));
		?>
		</div>
	</div>
</div><!-- .col nested -->