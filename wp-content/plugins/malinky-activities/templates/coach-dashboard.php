<?php
global $current_user;
get_currentuserinfo();
if (in_array('coach', (array) $current_user->roles)) :
    $args = array(
        'menu'              => 'Coach Menu',
        'container_id'      => 'menu_coach_dashboard',
        'menu_class'		=> 'list_no_style'
    );
    wp_nav_menu($args);
endif; ?>