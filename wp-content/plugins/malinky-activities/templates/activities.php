<?php if (isset($offset) && isset($displayed_posts) && isset($total_posts)) { ?>
	<h3 class="activities"><?php echo 'Showing Activities ' . ($offset + 1) . ' to ' . ($offset + $displayed_posts) . ' of ' . $total_posts; ?></h3>
<?php } ?>

<!--<div id="activity_per_page" class="clearfix">
	<div class="col col_span_10_10">
		<select name="activity_per_page_options" id="activity_per_page_options">
			<option value="<?php esc_attr_e(1); ?>" <?php //echo isset($posts_per_page_selected) ? selected($posts_per_page_selected, 1) : ''; ?>>1</option>
			<option value="<?php esc_attr_e(3); ?>" <?php //echo isset($posts_per_page_selected) ? selected($posts_per_page_selected, 3) : ''; ?>>3</option>
			<option value="<?php esc_attr_e(10); ?>" <?php //echo isset($posts_per_page_selected) ? selected($posts_per_page_selected, 10) : ''; ?>>10</option>
		</select>
	</div>
</div>-->

<?php if (isset($error)) {

	echo $error;

	?>

	<br /><br />
	<div class="col">
		<div class="col_item col_item_full">
			<div id="activity_distance">
				<select name="activity_distance_options" id="activity_distance_options">
					<option value="<?php esc_attr_e(5); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 5) : ''; ?>>5 Miles</option>
					<option value="<?php esc_attr_e(10); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 10) : ''; ?>>10 Miles</option>
					<option value="<?php esc_attr_e(15); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 15) : ''; ?>>15 Miles</option>
					<option value="<?php esc_attr_e(20); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 20) : ''; ?>>20 Miles</option>
					<option value="<?php esc_attr_e(25); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 25) : ''; ?>>25 Miles</option>
					<option value="<?php esc_attr_e(30); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 30) : ''; ?>>30 Miles</option>
					<option value="<?php esc_attr_e(40); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 40) : ''; ?>>40 Miles</option>
					<option value="<?php esc_attr_e(50); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 50) : ''; ?>>50 Miles</option>
					<option value="<?php esc_attr_e(100); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 100) : ''; ?>>100 Miles</option>
				</select>
			</div>
		</div>
	</div><!-- .col -->

	<?php

} elseif (!isset($activities_output)) {

	esc_html_e('There are no activities listed.');

} else { ?>

	<div id="activity_sorting">

		<div class="col">
			<div class="col_item col_item_half">
				<div id="activity_distance">
					<select name="activity_distance_options" id="activity_distance_options">
						<option value="<?php esc_attr_e(5); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 5) : ''; ?>>5 Miles</option>
						<option value="<?php esc_attr_e(10); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 10) : ''; ?>>10 Miles</option>
						<option value="<?php esc_attr_e(15); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 15) : ''; ?>>15 Miles</option>
						<option value="<?php esc_attr_e(20); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 20) : ''; ?>>20 Miles</option>
						<option value="<?php esc_attr_e(25); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 25) : ''; ?>>25 Miles</option>
						<option value="<?php esc_attr_e(30); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 30) : ''; ?>>30 Miles</option>
						<option value="<?php esc_attr_e(40); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 40) : ''; ?>>40 Miles</option>
						<option value="<?php esc_attr_e(50); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 50) : ''; ?>>50 Miles</option>
						<option value="<?php esc_attr_e(100); ?>" <?php echo isset($distance_selected) ? selected($distance_selected, 100) : ''; ?>>100 Miles</option>
					</select>
				</div>
			</div><!--
			--><div class="col_item col_item_half">
				<div id="activity_sort">
					<select name="activity_sort_options" id="activity_sort_options">
						<option value="<?php esc_attr_e('distance_nearest'); ?>" <?php echo isset($orderby_selected) ? selected($orderby_selected, 'distance_nearest') : ''; ?>>Nearest First</option>
						<option value="<?php esc_attr_e('activity_az'); ?>" <?php echo isset($orderby_selected) ? selected($orderby_selected, 'activity_az') : ''; ?>>Activity A-Z</option>
						<option value="<?php esc_attr_e('activity_za'); ?>" <?php echo isset($orderby_selected) ? selected($orderby_selected, 'activity_za') : ''; ?>>Activity Z-A</option>
					</select>
				</div>
			</div>
		</div><!-- .col -->

	</div><!-- #activity_sorting -->
	
	<?php echo $activities_output; ?>
	<?php get_malinky_activities_template('activities-pagination.php', array('max_num_pages' => $max_num_pages, 'page' => $page)); ?>

	<h4 class="sidebar_heading">Share</h4>
	<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
		<a class="addthis_button_facebook"></a>
    	<a class="addthis_button_twitter"></a>
    	<a class="addthis_button_google_plusone_share"></a>
    	<a class="addthis_button_pinterest_share"></a>
    	<a class="addthis_button_linkedin"></a>
    	<a class="addthis_button_compact"></a>
	</div>	

<?php } ?>
