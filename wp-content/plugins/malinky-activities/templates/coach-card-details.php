<?php
if (isset($card_details_exist) || isset($card_expiry_date)) { ?>
	<fieldset id="fieldset_activity_login_details" class="activity_fieldset">
		<legend>Payment Status</legend>
		<?php
		if (isset($card_details_exist))
			echo '<p>' . esc_html($card_details_exist) . '</p>';
		if (isset($card_details_exist))
			echo '<p><span class="bold_text">Current Card Expiry Date</span> ' . esc_html($card_expiry_date) . '</p>';
		?>	
	</fieldset>
<?php }
if (isset($errors) && is_wp_error($errors)) {
	$errors = $errors->errors;
	if (!empty($errors)) {
		echo '<p class="coach_message_error_permanent">' . $errors['malinky_activities_stripe_error'][0] . '</p>';
	}
} ?>

<div class="col">
	<div class="col_item col_item_full">

		<form action="<?php echo esc_url(site_url('coach/payment-details')); ?>" method="POST" id="card_details_form" role="form">
			<span id="payment_errors"></span>
			<fieldset id="fieldset_activity_login_details" class="activity_fieldset">
				<legend>Card Details</legend>
				<div class="col">
					<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full"> 
						<label for="cc_num">Card Number <small>(required)</small></label>
					</div><!--
					--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
						<input type="text" size="20" class="cc_num" id="cc_num" autocomplete="off" />
					</div><!--
					--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
						<div class="field_error_icon"></div>          
					</div>
				</div>
				<div class="col">
					<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full"> 
						<label for="cvc_num">CVC <small>(required)</small></label>
					</div><!--
					--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
						<input type="text" size="4" class="cvc_num" id="cvc_num" autocomplete="off" />
					</div><!--
					--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
						<div class="field_error_icon"></div>          
					</div>
				</div>	
				<div class="col">
					<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full"> 
						<label for="cc_exp">Expiration (MM/YYYY) <small>(required)</small></label>
					</div><!--
					--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
						<input type="text" size="4" class="cc_exp" id="cc_exp" autocomplete="off" />
					</div><!--
					--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
						<div class="field_error_icon"></div>          
					</div>
				</div>	
			</fieldset>
			<?php wp_nonce_field( 'malinky_activities_card_details_form', 'malinky_activities_card_details_form_nonce' ); ?>
			<input type="submit" id="card_submit" name="card_submit" class="button full_width" />
		</form>

	</div>
</div>	