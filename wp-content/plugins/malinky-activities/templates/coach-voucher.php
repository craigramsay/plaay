<?php if (isset($errors) && is_wp_error($errors)) {

	$errors = $errors->errors;
	echo '<p class="coach_message_error">' . $errors['coach_voucher_error'][0] . '</p>';

} else { ?>

	<div class="col">
		<div class="col_item col_item_half medium-col_item_full small-col_item_full">

			<fieldset>
				<legend>Voucher Details</legend>
				<p><span class="bold_text">Activity</span> <?php esc_html_e($voucher->voucher_details_activity_name); ?></p>
				<p><span class="bold_text">Code</span> <?php esc_html_e($voucher->voucher_code) ;?></p>
				<p><span class="bold_text">Activated</span> <?php esc_html_e(date('d/m/Y', strtotime($voucher->voucher_code_activated_date)));?>
				<p><span class="bold_text">Format</span> <?php esc_html_e($voucher->voucher_details_format_term); ?></p>
				<p><span class="bold_text">Age Group of Child</span> <?php esc_html_e($voucher->voucher_details_age_group_term); ?></p>
				<p><span class="bold_text">Sport or Activity Type</span> <?php esc_html_e($voucher->voucher_details_category_term); ?></p>
				<p><span class="bold_text">Voucher Type</span> <?php esc_html_e($voucher->voucher_details_voucher_term); ?></p>
			</fieldset>

		</div><!--
		--><div class="col_item col_item_half medium-col_item_full small-col_item_full">
	
			<div class="col">
				<div class="col_item col_item_full">
			
					<fieldset>
						<legend>Voucher Holder Details</legend>
							<p><span class="bold_text">Name</span> <?php esc_html_e($parent->name);?></p>
							<p><span class="bold_text">Email</span> <?php esc_html_e($parent->email);?></p>
							<p><span class="bold_text">Phone</span> <?php esc_html_e($parent->phone);?></p>
					</fieldset>
	
				</div><!--
				--><div class="col_item col--align_right col_item_full">

				<?php
				switch ($voucher->voucher_mark_attended) {
					case false :
						?>
						<p><a href="<?php echo wp_nonce_url(add_query_arg(array('action' => 'mark_attended', 'voucher_id' => $voucher->id), site_url('coach/voucher')),'malinky_voucher_coach_voucher_mark_attended_action', 'mark_attended_nce'); ?>" class="button">Mark Attented</a></p>
						<?php
						break;
					case true :
						?>
						<p><a href="<?php echo wp_nonce_url(add_query_arg(array('action' => 'mark_unattended', 'voucher_id' => $voucher->id), site_url('coach/voucher')),'malinky_voucher_coach_voucher_mark_attended_action', 'mark_attended_nce'); ?>" class="button">Mark Unattented</a></p>
						<?php
						break;											
				} ?>

				</div>
			</div>

		</div><!--
		--><div class="col_item col_item_full">

			<fieldset>
				<legend>Voucher Charges</legend>
			<p><span class="bold_text">Cost</span> &pound;<?php esc_html_e(number_format($voucher->voucher_cost/100, 2)); ?></p>
			<?php 
			if ($stripe_charge) {
				if ($stripe_charge->stripe_charge_id) { ?>
					<p><span class="bold_text">Invoice No.</span> <?php esc_html_e($stripe_charge->stripe_charge_id); ?></p>
					<p><span class="bold_text">Invoice Date</span> <?php esc_html_e(date('d/m/Y', strtotime($stripe_charge->stripe_charge_created))); ?></p>
					<p><a href="<?php echo add_query_arg(array('charge' => $stripe_charge->stripe_charge_id), 'invoice') ?>">View Invoice</a></p>
				<?php }
			} ?>
			</fieldset>

		</div>

	</div>

<?php }