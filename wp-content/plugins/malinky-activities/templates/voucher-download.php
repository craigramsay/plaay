<?php get_header(); ?>
	<main role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h1><?php the_title(); ?></h1>
			<div class="col">
				<div class="col_item col_item_full">
					<p>You can download or view and print your voucher by clicking the buttons below. Your voucher has also been emailed to you. Next, contact the provider to book yourself in with them.</p>
					<div class="col">
						<div class="col_item col_item_half small-col_item_full">
							<a href="<?php echo wp_nonce_url(add_query_arg(array('activation_code' => $voucher_activation_code . '-' . $voucher_id, 'type' => 'download'), site_url('download-voucher')), 'download_voucher_nonce_action_' . $voucher_activation_code . '_' . $voucher_id, 'dwnl_nce'); ?>" class="voucher_download_link">Download Voucher</a>
						</div><!--
						--><div class="col_item col_item_half small-col_item_full">
							<a href="<?php echo wp_nonce_url(add_query_arg(array('activation_code' => $voucher_activation_code . '-' . $voucher_id, 'type' => 'view'), site_url('download-voucher')), 'download_voucher_nonce_action_' . $voucher_activation_code . '_' . $voucher_id, 'dwnl_nce'); ?>" target="_blank" class="voucher_download_link">View Voucher</a>							
						</div>
					</div><!-- .col nested -->
				</div><!--
				--><div class="col_item col_item_full">
					<h3 class="heading_1">Show the love and spread the word</h3>
					<p>We hope you and your child enjoy the activity you’ve chosen, please show us some love and spread the word by telling all of your friends below and we’ll donate up to £1 to charity and put you into a prize draw to win fantastic prizes for you and your child.</p>
					<div class="col">
						<div class="col_item col_item--align_right medium-col_item--align_center small-col_item--align_center col_item_half medium-col_item_full small-col_item_full">
							<!-- Place the tag where you want the button to render -->
							<a href="" class="voucher_download_social_link_facebook">
								<img src="<?php echo get_template_directory_uri() . '/img/fb.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/fb@2x.png'; ?>" alt="Facebook" class="voucher_download_social" />
							</a>
							<a href="https://twitter.com/intent/tweet
									?url=<?php echo $encoded_social_link_url; ?>
									&text=<?php echo $encoded_social_link_text; ?>
									&related=plaayuk" 
									class="voucher_download_social_link_twitter">
								<img src="<?php echo get_template_directory_uri() . '/img/twitter.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/twitter@2x.png'; ?>" alt="Twitter" class="voucher_download_social" />
							</a>	
							<div class="voucher_download_social_link_googleplus g-interactivepost"
							    data-clientid="709168221143-2elblnidmdokim8v9lrqe6qai0os630l.apps.googleusercontent.com"
							    data-contenturl="<?php echo $unencoded_social_link_url; ?>"
							    data-cookiepolicy="single_host_origin"
							    data-prefilltext="<?php echo $unencoded_social_link_text; ?>. <?php echo $unencoded_social_link_description; ?>"
							    data-calltoactionlabel="DOWNLOAD"
							    data-calltoactionurl="<?php echo $unencoded_social_link_url; ?>">   
								<img src="<?php echo get_template_directory_uri() . '/img/google+.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/google+@2x.png'; ?>" alt="Google Plus" class="voucher_download_social" />
							</div>
						</div><!--
						--><div class="col_item col_item--align_left medium-col_item--align_center small-col_item--align_center col_item_half medium-col_item_full small-col_item_full">
							<a href="http://www.pinterest.com/pin/create/button/?url=<?php echo $encoded_social_link_url; ?>&media=<?php echo urlencode(get_template_directory_uri() . '/img/logo_facebook_og.jpg'); ?>&description=<?php echo $encoded_social_link_text; ?>" data-pin-do="buttonPin" data-pin-config="above" class="voucher_download_social_link_pinterest" target="_blank">
						        <img src="<?php echo get_template_directory_uri() . '/img/pinterest.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/pinterest@2x.png'; ?>" alt="Pinterest" class="voucher_download_social" />
						    </a>
							<a href="http://www.linkedin.com/shareArticle?
							mini=true&
							url=<?php echo $encoded_social_link_url; ?>&
							title=<?php echo $encoded_social_link_text; ?>&
							summary=<?php echo $encoded_social_link_description; ?>&
							source=PLAAY" 
							class="voucher_download_social_link_linkedin" 
							target="_blank">
								<img src="<?php echo get_template_directory_uri() . '/img/linkedin.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/linkedin@2x.png'; ?>" alt="Linked In" class="voucher_download_social" />
							</a>
							<a href="mailto:?subject=<?php echo $unencoded_social_link_text; ?>&body=<?php echo $unencoded_social_link_description . ' <' . $unencoded_social_link_url . '>'; ?>" class="voucher_download_social_link_email">
								<img src="<?php echo get_template_directory_uri() . '/img/gmail.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/gmail@2x.png'; ?>" alt="Mail" class="voucher_download_social" />					
							</a>
						</div>
					</div><!-- .col nested -->	
				</div>
			</div><!-- .col -->							
		</article>
	</main>

<!-- FACEBOOK LIKE OVERLAY -->
<div class="facebook_api_like">
	<div class="facebook_api_like_position">
		<a href="" class="facebook_api_like_close">x</a>
 		<iframe class="facebook_api_like_iframe" allowtransparency="true" frameborder="0" scrolling="no" src="//www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/plaayuk&width=360&height=300&colorscheme=light&show_faces=true&stream=false&show_border=false"></iframe>
	</div>
</div>

<?php if ( WP_ENV == 'local' || WP_ENV == 'dev' ) { ?>

	<script>
	//FACEBOOK js SDK
	window.fbAsyncInit = function() {
		FB.init({
			appId     	: '847550535289911', //dev
			xfbml      	: true,
			version    	: 'v2.2'
		});

		jQuery(document).ready(function($) {

			$('.voucher_download_social_link_facebook').click(function() {

				FB.login(function(response) {
					if (response.status === 'connected') {
						plaay_facebook_share();
					} else {
		    			alert('You can\'t post to Facebook without accepting.');
		  			}
				});

		        event.preventDefault();

		    });

		    function plaay_facebook_share()
		    {

			 	FB.ui({
					method: 'share_open_graph',
					action_type: 'plaayvouchertest:download', //dev
					action_properties: JSON.stringify({
						//object: "<?php echo $unencoded_social_link_url; ?>",
						free_voucher: "<?php echo $unencoded_social_link_url; ?>", //dev and live
						'fb:explicitly_shared' : true
					})
			    }, function(response){
			    	//could perform action on closure, success, cancellation or error
			    	$('.facebook_api_like').show();
	    	    	
	    	    	$('.facebook_api_like_close, #wrap_coach').click(function() {
			    		$('.facebook_api_like').hide();
			    		event.preventDefault();
			    		$('#wrap_coach').off('click');
			    		$('.facebook_api_like_close').off('click');
		    		});


			    });

			 }

	    });					

	};
	</script>

<?php } elseif ( WP_ENV == 'prod' ) { ?>
	
	<script>
	//FACEBOOK js SDK
	window.fbAsyncInit = function() {
		FB.init({
			appId    	: '839001169478181', //live
			xfbml      	: true,
			version    	: 'v2.2'
		});

		jQuery(document).ready(function($) {

			$('.voucher_download_social_link_facebook').click(function() {

				FB.login(function(response) {
					if (response.status === 'connected') {
						plaay_facebook_share();
					} else {
		    			alert('You can\'t post to Facebook without accepting.');
		  			}
				});

		        event.preventDefault();

		    });

		    function plaay_facebook_share()
		    {

			 	FB.ui({
					method: 'share_open_graph',
					action_type: 'plaayvoucher:download', //live
					action_type: 'og.likes', //live
					action_properties: JSON.stringify({
						//object: "<?php echo $unencoded_social_link_url; ?>",
						free_voucher: "<?php echo $unencoded_social_link_url; ?>", //dev and live
						'fb:explicitly_shared' : true
					})
			    }, function(response){
			    	//could perform action on closure, success, cancellation or error
			    	$('.facebook_api_like').show();
	    	    	
	    	    	$('.facebook_api_like_close, #wrap_coach').click(function() {
			    		$('.facebook_api_like').hide();
			    		event.preventDefault();
			    		$('#wrap_coach').off('click');
			    		$('.facebook_api_like_close').off('click');
		    		});


			    });

			 }

	    });					

	};
	</script>

<?php } ?>

<?php get_footer();