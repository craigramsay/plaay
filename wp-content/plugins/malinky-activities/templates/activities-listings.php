<div class="col col--margin_bottom_20">
	<div class="col_item col_item_full">
		<h3 class="activities_name"><a href="<?php the_permalink(); ?>"><?php esc_html_e(the_title()); ?></a></h3>
	</div><!--
	--><div class="col_item col_item_7_10 small-col_item_full">
		<div class="activities_description">
			<?php echo truncate_words(get_post_meta($post->ID, '_activity_description', true), 250); ?>
		</div>
	</div><!--
	--><div class="col_item col_item_3_10 small-col_item_full activities_vertical_seperator">
		<ul class="bulleted_list">
			<li><?php if (isset($post->temp_distance_output)) { esc_html_e($post->temp_distance_output); } else { echo '-'; } ?></li>
			<li><?php esc_html(the_activity_format($post)); ?></li>
		</ul>
		<a href="<?php the_permalink(); ?>" class="button full_width margin_top_25">More Info</a>
	</div>
	<div class="col_item col_item_full"><hr></div>
</div><!-- .col -->