<table class="coach_table">
	<thead>
		<tr>
			<th class="coach_voucher_th_code"><a href="<?php echo get_query_var('orderby') == 'code_desc' ? add_query_arg(array('orderby' => 'code_asc')) : add_query_arg(array('orderby' => 'code_desc')); ?>"<?php echo get_query_var('orderby') == 'code_desc' ? 'class="sort_desc"' : '' ?><?php echo get_query_var('orderby') == 'code_asc' ? 'class="sort_asc"' : '' ?>>Voucher Code</a></th>
			<th class="coach_voucher_th_activity large-hide"><a href="<?php echo get_query_var('orderby') == 'activity_desc' ? add_query_arg(array('orderby' => 'activity_asc')) : add_query_arg(array('orderby' => 'activity_desc')); ?>"<?php echo get_query_var('orderby') == 'activity_desc' ? 'class="sort_desc"' : '' ?><?php echo get_query_var('orderby') == 'activity_asc' ? 'class="sort_asc"' : '' ?>>Activity</a></th>
			<th class="coach_voucher_th_activated"><a href="<?php echo get_query_var('orderby') == 'date_desc' ? add_query_arg(array('orderby' => 'date_asc')) : add_query_arg(array('orderby' => 'date_desc')); ?>"<?php echo get_query_var('orderby') == 'date_desc' ? 'class="sort_desc"' : '' ?><?php echo get_query_var('orderby') == 'date_asc' ? 'class="sort_asc"' : '' ?><?php echo get_query_var('orderby') == '' ? 'class="sort_desc"' : '' ?>>Date Activated</a></th>
			<th class="coach_voucher_th_paid"><a href="<?php echo get_query_var('orderby') == 'paid_desc' ? add_query_arg(array('orderby' => 'paid_asc')) : add_query_arg(array('orderby' => 'paid_desc')); ?>"<?php echo get_query_var('orderby') == 'paid_desc' ? 'class="sort_desc"' : '' ?><?php echo get_query_var('orderby') == 'paid_asc' ? 'class="sort_asc"' : '' ?>>Paid</a></th>
			<th class="coach_voucher_th_mark_attended large-hide"><a href="<?php echo get_query_var('orderby') == 'attended_desc' ? add_query_arg(array('orderby' => 'attended_asc')) : add_query_arg(array('orderby' => 'attended_desc')); ?>"<?php echo get_query_var('orderby') == 'attended_desc' ? 'class="sort_desc"' : '' ?><?php echo get_query_var('orderby') == 'attended_asc' ? 'class="sort_asc"' : '' ?>>Attended</a></th>
		</tr>
	</thead>
	<tbody>
		<?php if (!$vouchers) : ?>
			<tr>
				<td colspan="6"><?php esc_html_e('You don\'t have any downloaded vouchers.'); ?></td>
			</tr>
		<?php else : ?>
			<?php foreach ($vouchers as $voucher) : ?>
				<tr>
					<td class="coach_voucher_code"><a href="<?php echo esc_url(add_query_arg(array('voucher_id' => $voucher->id), site_url('coach/voucher'))); ?>"><?php esc_html_e($voucher->voucher_code); ?></a></td>
					<td class="coach_voucher_activity large-hide"><?php esc_html_e($voucher->voucher_details_activity_name); ?></td>
					<td class="coach_voucher_activated"><?php esc_html_e(date('d/m/Y', strtotime($voucher->voucher_code_activated_date))); ?></td>
					<td class="coach_voucher_voucher"><?php if ($voucher->stripe_charge_id) echo 'Y'; ?></td>			
					<td class="coach_voucher_mark_attended large-hide"><?php esc_html_e(mark_attended_status($voucher->id)); ?></td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>