<?php if (isset($errors) && is_wp_error($errors)) {

	$errors = $errors->errors;
	echo '<p class="coach_message_error">' . $errors['coach_invoice_error'][0] . '</p>';

} else { ?>

	<table class="coach_invoice">
		<tr>
			<td class="extra_padding_bottom">
			PLAAY<br />
			Unit 11<br />
			Hove Business Centre<br />
			Fonthill Road<br />
			Hove<br />East Sussex<br />
			BN3 6HA
			</td>
			<td colspan="2" class="td_nested extra_padding_bottom">
				<table>
					<tr><td><span class="bold_text">Invoice No.</span></td> <td><?php esc_html_e($invoice[0]->stripe_charge_id); ?></td></tr>
					<tr><td><span class="bold_text">Date</span></td> <td><?php esc_html_e(date('d/m/Y', strtotime($invoice[0]->stripe_charge_created))); ?></td></tr>
				</table>
			</td>
		</tr>
		<tr><td class="bold_text">Invoice To</td></tr>
		<tr>
			<td class="extra_padding_bottom">
			<?php echo get_user_meta(get_current_user_id(), '_coach_company_name', true); ?><br />
			<?php echo get_user_meta(get_current_user_id(), '_coach_address', true); ?><br />
			<?php echo get_user_meta(get_current_user_id(), '_coach_town', true); ?><br />
			<?php echo ucwords(get_user_meta(get_current_user_id(), '_coach_county', true)); ?><br />
			<?php echo get_user_meta(get_current_user_id(), '_coach_postcode', true); ?>
			</td>
		</tr>
		<tr><td class="bold_text">Code</td><td class="bold_text">Date Activated</td><td class="bold_text">Cost</td></tr>
		<?php foreach ($invoice as $k => $v) { ?>
			<tr>
				<td><a href="<?php echo esc_url(add_query_arg(array('voucher_id' => $invoice[$k]->voucher_id), site_url('coach/voucher'))); ?>"><?php esc_html_e($invoice[$k]->voucher_code); ?></a></td>
				<td><?php esc_html_e(date('d/m/Y', strtotime($invoice[$k]->voucher_code_activated_date))); ?></td>
				<td>&pound;<?php esc_html_e(number_format($invoice[$k]->voucher_cost/100, 2)); ?></td>
			</tr>
		<?php } ?>
		<tr><td class="extra_padding_top"></td><td class="extra_padding_top"><span class="bold_text">Total:</span> </td><td class="coach_invoice_total extra_padding_top">&pound;<?php esc_html_e(number_format($invoice[0]->stripe_charge_amount/100, 2)); ?></td></tr>
	</table>

	<a href="javascript:window.print()" class="button print_button">Print</a>

<?php }
