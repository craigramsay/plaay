<table class="coach_table">
	<thead>
		<tr>
			<th class="coach_activity_th_name"><a href="<?php echo get_query_var('orderby') == 'activity_desc' ? add_query_arg(array('orderby' => 'activity_asc')) : add_query_arg(array('orderby' => 'activity_desc')); ?>"<?php echo get_query_var('orderby') == 'activity_desc' ? 'class="sort_desc"' : '' ?><?php echo get_query_var('orderby') == 'activity_asc' ? 'class="sort_asc"' : '' ?><?php echo get_query_var('orderby') == '' ? 'class="sort_desc"' : '' ?>>Activity</a></th>
			<th class="coach_activity_th_format large-hide"><?php esc_html_e('Format'); ?></th>
			<th class="coach_activity_th_ages large-hide"><?php esc_html_e('Age Groups'); ?></th>
			<th class="coach_activity_th_categories large-hide"><?php esc_html_e('Sport or Activity Types'); ?></th>
			<th class="coach_activity_th_vouchers large-hide"><?php esc_html_e('Voucher Types'); ?></th>
			<th class="coach_activity_th_status"><?php esc_html_e('Status'); ?></th>
			<th class="coach_activity_th_actions"><?php esc_html_e('Actions'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php if (!$activities) : ?>
			<tr>
				<td colspan="6"><?php esc_html_e('You don\'t have any activities.'); ?></td>
			</tr>
		<?php else : ?>
			<?php foreach ($activities as $activity) : ?>
				<tr>

					<td class="coach_activity_name"><a href="<?php echo esc_url(add_query_arg(array('activity_id' => $activity->ID), site_url('coach/edit-activity'))); ?>"><?php esc_html_e($activity->post_title); ?></a></td>
					<td class="coach_activity_format large-hide"><?php the_activity_format($activity); ?></td>
					<td class="coach_activity_ages large-hide"><?php the_activity_age_group($activity); ?></td>
					<td class="coach_activity_categories large-hide"><?php the_activity_category($activity); ?></td>
					<td class="coach_activity_vouchers large-hide"><?php the_activity_voucher($activity); ?></td>
					<td class="coach_activity_status"><?php the_activity_status($activity); ?></td>
					<td class="coach_activity_actions">
						<?php
						$actions = array();
						switch ($activity->post_status) {
							case 'publish' :
								$actions['mark_inactive'] = array( 'label' => 'Mark Inactive', 'nonce' => true );
								$actions['edit'] = array('label' => 'Edit', 'nonce' => false, 'different_uri' => 'edit-activity');
								break;
							case 'pending' :
								$actions['mark_active'] = array( 'label' => 'Mark Active', 'nonce' => true );
								$actions['edit'] = array('label' => 'Edit', 'nonce' => false, 'different_uri' => 'edit-activity');
								break;											
						}

						$actions['delete'] = array( 'label' => 'Delete', 'nonce' => true );

						foreach ( $actions as $action => $value ) {
							if ($action == 'edit') {
								$action_url = add_query_arg(array('activity_id' => $activity->ID ), $value['different_uri']);
							} else {
								$action_url = add_query_arg(array('action' => $action, 'activity_id' => $activity->ID));
							}
							if ( $value['nonce'] )
								$action_url = wp_nonce_url( $action_url, 'malinky_activities_coach_activities_actions' );
							$button = $action == 'delete' ? ' button red' : '';
							echo '<p class="coach_table_actions"><a href="' . $action_url . '" class="coach_activities_actions">' . $value['label'] . '</a></p>';
						}
						?>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>
<?php get_malinky_activities_template('coach-pagination.php', array('max_num_pages' => $max_num_pages)); ?>