<?php
/**
 * ------------------------------
 * GET EXISTING ACTIVITY FOR FORM
 * @param obj $activity
 * @param array $activity_meta 
 * @return array
 * ------------------------------
 */
function malinky_activities_get_activity_form_fields($activity, $activity_meta)
{
	$form_fields = malinky_activities_set_activity_form_fields();			

	foreach ($form_fields as $fieldset => $fields) {
		foreach ($fields as $key => $field) {
			$property = '_' . $key;
			$term_property = 'malinky_' . str_replace('activity', 'activities', $key);
			switch ($key) {
				case 'activity_name' :
					$values[$fieldset][$key] = sanitize_text_field($activity->post_title);
				break;
				case 'activity_format' :
					$values[$fieldset][$key] = get_the_terms($activity->ID, $term_property);
					$values[$fieldset][$key] = $values[$fieldset][$key][0]->slug;
				break;				
				case 'activity_category' :
					$terms = get_the_terms($activity->ID, $term_property);
					$count_terms = count($terms);
					for ($count = 0; $count < $count_terms; $count++) {
						$values[$fieldset][$key][] = $terms[$count]->slug;	
					}
				break;	
				case 'activity_voucher' :
					$terms = get_the_terms($activity->ID, $term_property);
					$count_terms = count($terms);
					for ($count = 0; $count < $count_terms; $count++) {
						$values[$fieldset][$key][] = $terms[$count]->slug;	
					}

				break;	
				case 'activity_age_group' :
					$terms = get_the_terms($activity->ID, $term_property);
					$count_terms = count($terms);
					for ($count = 0; $count < $count_terms; $count++) {
						$values[$fieldset][$key][] = $terms[$count]->slug;	
					}
				break;	
				case 'activity_county' :
					$values[$fieldset][$key] = get_the_terms($activity->ID, $term_property);
					$values[$fieldset][$key] = $values[$fieldset][$key][0]->slug;
				break;																
				case 'activity_description' :
					$values[$fieldset][$key] = strip_tags(get_post_meta($activity->ID, $property, true), '<strong><p><br><em><ul><ol><li>');
				break;
				case 'activity_additional_information' :
					$values[$fieldset][$key] = strip_tags(get_post_meta($activity->ID, $property, true), '<strong><p><br><em><ul><ol><li>');
				break;
				case 'activity_active' :
					$values[$fieldset][$key] = sanitize_text_field($activity->post_status);
					if ($values[$fieldset][$key] == 'publish')
						$values[$fieldset][$key] = '1';
				break;								
				default:
					$values[$fieldset][$key] = sanitize_text_field(get_post_meta($activity->ID, $property, true));
				break;			
			}
			$form_fields[$fieldset][$key]['value'] = $values[$fieldset][$key];
		}
	}	
	return $form_fields;
}

/**
 * ---------------------------------
 * PROCESS FORM AND ADD NEW ACTIVITY
 * @return void
 * ---------------------------------
 */
function malinky_activities_new_activity()
{
	//check form submit and nonce
	if ((empty($_POST['submit_activity'])) || (!isset($_POST['malinky_activities_add_activity_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_activities_add_activity_form_nonce'], 'malinky_activities_add_activity_form')))
		wp_die(__('There was a fatal error with your form submission'));

	$values = malinky_activities_get_posted_form_fields('malinky_activities_set_activity_form_fields', true);

	if ($values['activity_publish']['activity_active'] == '1') {
		$post_status = 'publish';
	} else {
		$post_status = 'pending';
	}

	$activity_data = array(
		'post_title'     	=> $values['activity_details']['activity_name'],
		'post_status'    	=> $post_status,
		'post_type'      	=> 'activities',
		'comment_status' 	=> 'closed'
	);  

	$activity_id = wp_insert_post($activity_data);
	wp_set_object_terms($activity_id, $values['activity_details']['activity_format'], 'malinky_activities_format', false);
	wp_set_object_terms($activity_id, $values['activity_details']['activity_category'], 'malinky_activities_category', false);
	wp_set_object_terms($activity_id, $values['activity_location']['activity_county'], 'malinky_activities_county', false);
	wp_set_object_terms($activity_id, $values['activity_details']['activity_voucher'], 'malinky_activities_voucher', false);
	wp_set_object_terms($activity_id, $values['activity_details']['activity_age_group'], 'malinky_activities_age_group', false);
	update_post_meta($activity_id, '_activity_description', $values['activity_details']['activity_description']);
	update_post_meta($activity_id, '_activity_additional_information', $values['activity_details']['activity_additional_information']);
	update_post_meta($activity_id, '_activity_distance_travelled', $values['activity_location']['activity_distance_travelled']);
	update_post_meta($activity_id, '_activity_address', $values['activity_location']['activity_address']);
	update_post_meta($activity_id, '_activity_town', $values['activity_location']['activity_town']);
	update_post_meta($activity_id, '_activity_postcode', preg_replace('/\s+/', '', $values['activity_location']['activity_postcode']));
	//update_post_meta($activity_id, '_activity_active', $values['activity_details']['activity_active']);

	if ($activity_id > 0) {
		add_activity_postcode($values['activity_location']['activity_postcode']);
		wp_safe_redirect('activities?action=activity_added&activity_id=' . $activity_id);
		exit();
	}
}

/**
 * --------------------------------
 * PROCESS FORM AND UPDATE ACTIVITY
 * @param int $activity_id
 * @return void  
 * --------------------------------
 */
function malinky_activities_update_activity($activity_id)
{
	if ((empty($_POST['update_activity'])) || (!isset($_POST['malinky_activities_edit_activity_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_activities_edit_activity_form_nonce'], 'malinky_activities_edit_activity_form')))
		wp_die(__('There was a fatal error with your form submission'));

	$values = malinky_activities_get_posted_form_fields('malinky_activities_set_activity_form_fields', true);

	if ($values['activity_publish']['activity_active'] == '1') {
		$post_status = 'publish';
	} else {
		$post_status = 'pending';
	}

	$activity_data = array(
		'ID'			=> $activity_id,
		'post_title'	=> $values['activity_details']['activity_name'],
		'post_status'	=> $post_status
	);

	wp_update_post($activity_data);
	wp_set_object_terms($activity_id, $values['activity_details']['activity_format'], 'malinky_activities_format', false);
	wp_set_object_terms($activity_id, $values['activity_details']['activity_category'], 'malinky_activities_category', false);
	wp_set_object_terms($activity_id, $values['activity_location']['activity_county'], 'malinky_activities_county', false);
	wp_set_object_terms($activity_id, $values['activity_details']['activity_voucher'], 'malinky_activities_voucher', false);
	wp_set_object_terms($activity_id, $values['activity_details']['activity_age_group'], 'malinky_activities_age_group', false);
	update_post_meta($activity_id, '_activity_description', $values['activity_details']['activity_description']);
	update_post_meta($activity_id, '_activity_additional_information', $values['activity_details']['activity_additional_information']);
	update_post_meta($activity_id, '_activity_distance_travelled', $values['activity_location']['activity_distance_travelled']);
	update_post_meta($activity_id, '_activity_address', $values['activity_location']['activity_address']);
	update_post_meta($activity_id, '_activity_town', $values['activity_location']['activity_town']);
	update_post_meta($activity_id, '_activity_postcode', preg_replace('/\s+/', '', $values['activity_location']['activity_postcode']));	
	//update_post_meta($activity_id, '_activity_active', $values['activity_details']['activity_active']);

	if ($activity_id > 0) {
		add_activity_postcode($values['activity_location']['activity_postcode']);
		wp_safe_redirect('activities?action=activity_updated&activity_id=' . $activity_id);
		exit();
	}
}

/**
 * ------------------------------
 * GET THE LIST OF ACTIVITY TERMS
 * @param string $taxonomy
 * @param bool $slug_key (optional)
 * @return array|void
 * ------------------------------
 */
function get_activity_terms($taxonomy, $slug_key = true) 
{
	$terms = get_terms($taxonomy, array(
		'orderby'       => 'name',
	    'order'         => 'ASC',
	    'hide_empty'    => false,
	) );
	$options = array();
	
	if (is_wp_error($terms))
		return;

	if ($slug_key) {
		foreach ($terms as $term) {
			$options[$term->slug] = $term->name;
		}
	} else {
		foreach ($terms as $term) {
			$options[] = $term->name;
		}
	}

	return $options;	
}

/**
 * -----------------------------------------------
 * GET THE LIST OF ACTIVITY TERMS FOR A POST BY ID
 * @param int|obj $post_id
 * @param string $taxonomy
 * @param string $return_type
 * @return array
 * -----------------------------------------------
 */
function get_post_activity_terms($post_id, $taxonomy, $return_key = 'slug') 
{
	if(!$post_id)
		return;

	$post = get_post($post_id);
	if ($post->post_type !== 'activities')
		return;

	$terms = wp_get_post_terms($post->ID, $taxonomy);
	$options = array();
	
	if (is_wp_error($terms))
		return;

	foreach ($terms as $term) {
		if ($return_key == 'term_id') {
			$options[$term->term_id] = $term->name;
		} elseif ($return_key == 'name') {
			$options[$term->name] = $term->name;
		} else {
			$options[$term->slug] = $term->name;		
		}
	}
	return $options;
}

/**
 * -------------------------------
 * GET A SINGLE TERM_ID BY POST_ID
 * @param int $activity_id
 * @param string $taxonomy
 * @return int
 * -------------------------------
 */
function get_single_term_id($activity_id, $taxonomy)
{
	$term = wp_get_post_terms($activity_id, $taxonomy);
	
	if ($term) {
		return $term[0]->term_id;
	} else {
		return false;
	}
}

/**
 * ---------------------------------
 * GET A SINGLE TERM_NAME BY POST_ID
 * @param int $activity_id
 * @param string $taxonomy
 * @return int
 * ---------------------------------
 */
function get_single_term_name_by_post_id($activity_id, $taxonomy)
{
	$term = wp_get_post_terms($activity_id, $taxonomy);

	if ($term) {
		return $term[0]->name;
	} else {
		return false;
	}
}

/**
 * -------------------------
 * CHECK FOR ACTIVE ACTIVITY
 * @param int $activity_id
 * @return obj|NULL
 * -------------------------
 */
function malinky_activities_active_activity($activity_id)
{
	$args = array(
		'post_type'     => 'activities',
		'post_status'   => 'publish',
		'p'				=> $activity_id
	);
	$activity = get_posts($args);

	return $activity;
}

/**
 * --------------------------------------------------
 * GET TERM ID's AND NAME's FOR USE IN ACTIVITIES FILTERS
 * @param array $activities
 * @param string $taxonomy
 * @param int $active_term
 * @return array   
 * --------------------------------------------------
 */
function filter_get_terms($activities, $taxonomy, $active_term = NULL)
{
	$filters = array();

	foreach($activities as $activity) {
		//get taxonomy label as no longer set to category, format etc /MALINKYHACK
		$tax_object_for_label = get_taxonomy($taxonomy);
		$tax_object_for_label = get_taxonomy_labels($tax_object_for_label);
		//get all post terms for an activity taxonomy
		$post_term = wp_get_post_terms($activity->ID, $taxonomy);
		//count number of terms per activity
		$count_post_terms = count($post_term);
		//loop through number of terms assinging to new array with a count as key reference and the term_id and name
		for ($count_terms = 0; $count_terms < $count_post_terms; $count_terms++) {
			$filters[$activity->ID][$count_terms]['term_id'] 			= $post_term[$count_terms]->term_id;
			$filters[$activity->ID][$count_terms]['term_name'] 			= $post_term[$count_terms]->name;
			$filters[$activity->ID][$count_terms]['taxonomy_label'] 	= $tax_object_for_label->singular_name;

			//check if count has started for particular term_id
			if(empty($count[$post_term[$count_terms]->term_id]))
				$count[$post_term[$count_terms]->term_id] = 0;
			//add 1 to count of term_id each time it is found
			$count[$post_term[$count_terms]->term_id]++;
			if ($post_term[$count_terms]->term_id == $active_term) {
				$filters[$activity->ID][$count_terms]['term_active'] = $active_term;	
			} else {
				$filters[$activity->ID][$count_terms]['term_active'] = NULL;	
			}		
		}
	}
	
	if (!$filters) return;

	//remove top level of array that used activity->ID
	$filters = call_user_func_array('array_merge', $filters);
	//remove duplicates
	$no_duplicates_filters = array_intersect_key($filters , array_unique(array_map('serialize' , $filters)));
	$filters = array_values($no_duplicates_filters);
	//compare the unqiue filters term_ids with the count term_ids and add to array for updated count
	foreach ($filters as $key => $value) {
		if (array_key_exists($value['term_id'], $count)) {
			$filters[$key]['term_count'] = $count[$value['term_id']];
		}
	}
	//sort my term name or ages
	if ($taxonomy == 'malinky_activities_age_group') {
		usort($filters, 'compare_term_name_ages');	
	} else {
		usort($filters, 'compare_term_names');
	}
	return $filters;
}

function compare_term_names($a, $b)
{
    if ($a['term_name'] == $b['term_name']) {
        return 0;
    }

    return ($a['term_name'] < $b['term_name']) ? -1 : 1;
}

function compare_term_name_ages($a, $b)
{
    if ($a['term_name'] == $b['term_name']) {
        return 0;
    }

    $a['term_name'] = explode('-', $a['term_name']);
	$b['term_name'] = explode('-', $b['term_name']);

    return ($a['term_name'] < $b['term_name']) ? -1 : 1;
}