<?php
/**
 * ----------------------------
 * SEND PARENT ACTIVATION EMAIL
 * @param string $parent_email  
 * @param int $voucher_id 
 * @return void   
 * ----------------------------
 */
function malinky_activities_parent_activation_email($parent_email, $parent_name, $voucher_id)
{
	global $wpdb;

	$parent_activation_code = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT parent_activation_code FROM malinky_voucher_codes WHERE id = %d", $voucher_id
		)			
	);

	$parent_activation_code = $parent_activation_code . '-' . $voucher_id;	

	$recipient_email = $parent_email;
	$recipient_name = $parent_name;

	$subject = 'Please Confirm Your Email Address and Download Your Voucher';

	$body = '<p>Thank you for applying for a voucher with ' . get_bloginfo('name') . '.</p><p>Please click on the link below to confirm your email address and download your voucher.</p><p><a href="' . network_site_url('confirm-email?activation_code=' . $parent_activation_code) . '">' . network_site_url('confirm-email?activation_code=' . $parent_activation_code) . '</a>';

	malinky_activities_emails_send($recipient_email, $recipient_name, $subject, $body, false);
}

/**
 * -------------------------
 * SEND PARENT VOUCHER EMAIL
 * @param id $voucher_id
 * @return string   
 * -------------------------
 */
function malinky_activities_voucher_email($voucher_id)
{
	global $wpdb;

	$parent_id = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT parent_id FROM malinky_voucher_codes WHERE id = %d", $voucher_id
		)	
	);

	$parent_email = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT email FROM malinky_parents WHERE id = %d", $parent_id
		)			
	);

	$parent_name = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT name FROM malinky_parents WHERE id = %d", $parent_id
		)			
	);	

	$recipient_email = $parent_email;
	$recipient_name = $parent_name;

	$subject = 'PLAAY Voucher';

	$body = '<p>Thanks for downloading a voucher from ' . get_bloginfo('name') . '. Please see attached.</p>';

	malinky_activities_emails_send($recipient_email, $recipient_name, $subject, $body, false, true, $voucher_id);
}

/**
 * ------------------------
 * SEND COACH VOUCHER EMAIL
 * @param id $voucher_id
 * @return string   
 * ------------------------
 */
function malinky_activities_voucher_coach_email($voucher_id)
{
	global $wpdb;

	$coach_id = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT coach_id FROM malinky_voucher_codes WHERE id = %d", $voucher_id
		)	
	);

	$coach = get_userdata($coach_id);

	$recipient_email = $coach->user_login;
	$recipient_name = $coach->display_name;

	$subject = 'PLAAY Voucher Downloaded';

	$body = '<p>A voucher has been downloaded from ' . get_bloginfo('name') . '. Please see attached or login to your account.</p>';

	malinky_activities_emails_send($recipient_email, $recipient_name, $subject, $body, false, true, $voucher_id);
}

/**
 * -----------------------------------------------------------------------------------------------------------------
 * SEND ADMIN AN EMAIL AS A CHARGE HAS FAILED FOR A COACH OR A NON CARD RELATED ISSUE WHEN COACH ADDING CARD DETAILS
 * @param string $subject
 * @param int $coach_id 
 * @param string $status 
 * @param string $error_page 
 * @param string $exception 
 * @param string $error_type 
 * @param string $error_message 
 * @param string $error_date 
 * @return void   
 * -----------------------------------------------------------------------------------------------------------------
 */
function malinky_activities_coach_stripe_failure_email_admin($subject, $coach_id, $status, $error_page, $exception, $error_type, $error_message, $error_date)
{
	include_once(ABSPATH . 'wp-includes/class-phpmailer.php');

	$email_settings = get_option('malinky_activities_email_settings_options');

	if (!$email_settings) return;

	$coach = get_userdata($coach_id);

	if (!$coach) return;

	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host 		= $email_settings['settings_smtp_host'];
	$mail->SMTPAuth   	= true;
	$mail->Port       	= $email_settings['settings_smtp_port'];
	$mail->Username   	= $email_settings['settings_smtp_username'];
	$mail->Password   	= $email_settings['settings_smtp_password'];
	$mail->SMTPSecure 	= 'ssl';	

	$mail->AddAddress(get_option('admin_email'), "PLAAY");

	$mail->SetFrom(get_option('admin_email'), "PLAAY");

	$mail->IsHTML(true);

	$mail->Subject = $subject;
	$mail->Body    = '<p>There has been an error during the charging of a coach. Please check the details below to guide you in rectifying the issue.</p>';
	$mail->Body    .= '<p>Coach: ' . get_user_meta($coach_id, '_coach_company_name', true) . '</p>';
	$mail->Body    .= '<p>Coach Email: ' . $coach->user_login . '</p>';
	$mail->Body    .= '<p>Error Status: ' . $status . '</p>';
	$mail->Body    .= '<p>Error Page: ' . $error_page . '</p>';
	$mail->Body    .= '<p>Exception Class: ' . $exception . '</p>';
	$mail->Body    .= '<p>Error Type: ' . $error_type . '</p>';
	$mail->Body    .= '<p>Error Message: ' . $error_message . '</p>';

	if(!$mail->Send()) {
		error_log($mail->ErrorInfo, 0);
    }
}

/**
 * -----------------------------------------------------------------------
 * SEND COACH AN EMAIL AS A CHARGE HAS FAILED AND ITS A CARD RELATED ERROR
 * @param string $subject 
 * @param int $coach_id
 * @param string $status 
 * @param string $error_message
 * @param string $main_body 
 * @return void   
 * -----------------------------------------------------------------------
 */
function malinky_activities_coach_stripe_failure_email_coach($subject, $coach_id, $status, $error_message, $main_body)
{
	include_once(ABSPATH . 'wp-includes/class-phpmailer.php');

	$email_settings = get_option('malinky_activities_email_settings_options');

	if (!$email_settings) return;

	$coach = get_userdata($coach_id);

	if (!$coach) return;

	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host 		= $email_settings['settings_smtp_host'];
	$mail->SMTPAuth   	= true;
	$mail->Port       	= $email_settings['settings_smtp_port'];
	$mail->Username   	= $email_settings['settings_smtp_username'];
	$mail->Password   	= $email_settings['settings_smtp_password'];
	$mail->SMTPSecure 	= 'ssl';	

	$mail->AddAddress($coach->user_login, $coach->first_name . ' ' . $coach->last_name);

	$mail->SetFrom(get_option('admin_email'), "PLAAY");

	$mail->IsHTML(true);

	$mail->Subject 	= $subject;
	$mail->Body    	= $main_body;
	$mail->Body    .= '<p>Coach: ' . get_user_meta($coach_id, '_coach_company_name', true) . '</p>';
	$mail->Body    .= '<p>Coach Email: ' . $coach->user_login . '</p>';
	$mail->Body    .= '<p>Error Status: ' . $status . '</p>';
	$mail->Body    .= '<p>Error Message: ' . $error_message . '</p>';

	if(!$mail->Send()) {
		error_log($mail->ErrorInfo, 0);
    }
}

/**
 * -----------------------------------------------------
 * SEND COACH AN EMAIL CONFIRMING THEY HAVE BEEN CHARGED
 * @param int $coach_id  
 * @param str $stripe_charge_id 
 * @param int $stripe_charge_amount
 * @return void   
 * -----------------------------------------------------
 */
function malinky_activities_charge_coach_success_email_coach($coach_id, $stripe_charge_id, $stripe_charge_amount)
{
	include_once(ABSPATH . 'wp-includes/class-phpmailer.php');

	$email_settings = get_option('malinky_activities_email_settings_options');

	if (!$email_settings) return;

	$coach = get_userdata($coach_id);

	if (!$coach) return;
	
	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host 		= $email_settings['settings_smtp_host'];
	$mail->SMTPAuth   	= true;
	$mail->Port       	= $email_settings['settings_smtp_port'];
	$mail->Username   	= $email_settings['settings_smtp_username'];
	$mail->Password   	= $email_settings['settings_smtp_password'];
	$mail->SMTPSecure 	= 'ssl';	

	$mail->AddAddress($coach->user_login, $coach->first_name . ' ' . $coach->last_name);

	$mail->SetFrom(get_option('admin_email'), "PLAAY");

	$mail->IsHTML(true);

	$mail->Subject 	= 'PLAAY Charge';
	$mail->Body    	= '<p>You have been billed for vouchers that have been downloaded from ' . get_bloginfo('name') . '.</p><p>For more information on the individual vouchers that have been downloaded login to your acccount.';
	$mail->Body    .= '<p>Transaction ID: ' . $stripe_charge_id . '</p>';
	$mail->Body    .= '<p>Transaction Amount: &pound;' . number_format(($stripe_charge_amount/100), 2) . '</p>';
	$mail->Body    .= '<p>Thanks</p>';

	if(!$mail->Send()) {
		error_log($mail->ErrorInfo, 0);
    }
}

/**
 * ----------------------------------
 * SEND EMAIL
 * @param string $recipient  
 * @param string $subject
 * @param string $body
 * @param bool $attachment OPTIONAL
 * @param int $attachment_id OPTIONAL 
 * @return void   
 * ----------------------------------
 */
function malinky_activities_emails_send($recipient_email, $recipient_name, $subject, $body, $unsubscribe = true, $attachment = NULL, $attachment_id = NULL)
{
	include_once(ABSPATH . 'wp-includes/class-phpmailer.php');

	$email_settings = get_option('malinky_activities_email_settings_options');

	if (!$email_settings) return;

	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host 		= $email_settings['settings_smtp_host'];
	$mail->SMTPAuth   	= true;
	$mail->Port       	= $email_settings['settings_smtp_port'];
	$mail->Username   	= $email_settings['settings_smtp_username'];
	$mail->Password   	= $email_settings['settings_smtp_password'];
	$mail->SMTPSecure 	= 'ssl';	

	$mail->AddAddress($recipient_email, $recipient_name);

	$mail->SetFrom(get_option('admin_email'), "PLAAY");

	if ($attachment) {
		$voucher = malinky_activities_generate_voucher($attachment_id);
		$mail->addStringAttachment($voucher, 'voucher.pdf', 'base64', 'application/pdf');
	}

	$mail->IsHTML(true);

	$mail->Subject = $subject;
	$mail->Body =  	'<table width="100%" bgcolor="#282828" style="padding: 0px; margin: 0px;">';
	$mail->Body .=	'<tr>';
	$mail->Body .=	'<td>';
	$mail->Body .=	'<table width="650px" align="center">';
	$mail->Body .=	'<tr>';
	$mail->Body .=	'<td style="color: #FFFFFF;"><img src="' . get_template_directory_uri() . '/img/logo.png' . '" style="display: block;" alt="PLAAY.CO.UK" /></td>';
	$mail->Body .=	'<td align="right" style="color: #FFFFFF;"><img src="' . get_template_directory_uri() . '/img/logo_email_strapline.png' . '" style="display: block;" alt="TRY IT. LOVE IT. PLAAY IT." /></td>';
	$mail->Body .=	'</tr>';
	$mail->Body .=	'</table>';
	$mail->Body .=	'</td>';
	$mail->Body .=	'</tr>';
	$mail->Body .=	'</table>';
	$mail->Body .=	'<table width="650px" align="center" style="color: #666666; font-family: Helvetica, Arial, sans-serif;">';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 15px;"></td></tr>';	
	$mail->Body .=	'<tr><td><p style="font-size: 20px; line-height: 18px; padding: 0px; margin: 0px;">Dear ' . $recipient_name . ',</p></td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';
	$mail->Body .= 	'<tr><td style="font-size: 14px;">' . $body . '</td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 5px;"></td></tr>';
	$mail->Body .= 	'<tr><td><p style="font-size: 14px; line-height: 18px; padding:0px; margin:0px;">Thanks,</p></td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';	
	$mail->Body .= 	'<tr><td><p style="font-size: 20px; padding: 0px; margin: 0px;">PLAAY</p></td></tr>';	
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';
	$mail->Body .= 	'<tr><td style="border-top: 2px solid #e5e5e5; padding: 0px; margin: 0px;"><p>&nbsp;</p></td></tr>';
	$mail->Body .= 	'<tr><td align="center"><p style="font-size: 20px; line-height: 18px; padding:0px; margin:0px;"><a href="' . esc_url(site_url()) . '" style=" color: #9e4381; font-weight: bold; text-decoration: none;">WWW.PLAAY.CO.UK</a></p></td></tr>';
	$mail->Body .=	'<tr><td style="margin: 0; padding: 0; height: 10px;"></td></tr>';
	$mail->Body .= 	'<tr><td><p style="font-size: 12px; line-height: 18px; padding:0px; margin:0px; text-align: center;">PLAAY. 1 King Street, Ipswich, IP1 1AR.';
	if ($unsubscribe) {
		$mail->Body .= 	'<a href="' . esc_url(site_url('email-unsubscribe?unsubscribe=' . md5($recipient_email))) . '" style="color: #666666; text-decoration: none;">Unsubscribe</a>';
	}
	$mail->Body .= '</p></td></tr>';
	$mail->Body .= '</table>';
	
	//echo $mail->Body;

	if(!$mail->Send()) {
		error_log($mail->ErrorInfo, 0);
    }
}