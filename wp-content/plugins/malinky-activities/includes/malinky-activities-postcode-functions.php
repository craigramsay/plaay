<?php
/**
 * ---------------------------
 * ADD ACTIVITY POSTCODE TO DB
 * @param string $postcode
 * @return TODO    
 * ---------------------------
 */
function add_activity_postcode($postcode)
{
	$postcode = preg_replace('/\s+/', '', $postcode);

	global $wpdb;

	$postcode_exists = $wpdb->get_results(
					$wpdb->prepare(
					"SELECT postcode FROM malinky_postcodes WHERE postcode = %s", $postcode
					), ARRAY_A				
				);

	if ($wpdb->num_rows > 0)
		return;

	$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $postcode . '&sensor=false';
	$data = json_decode(file_get_contents($url));
	$latitude = $data->results[0]->geometry->location->lat;
	$longitude = $data->results[0]->geometry->location->lng;

	$wpdb->insert( 
		'malinky_postcodes', 
		array( 
			'postcode' 	=> $postcode,
			'latitude' 	=> $latitude,
			'longitude' => $longitude
		), 
		array( 
			'%s', 
			'%s',
			'%s',
		) 
	);

	if (!$wpdb->insert_id) {
		echo 'INSERT POSTCODE ERROR';
		return;
	}
}

/**
 * ---------------------------
 * GET POSTCODES WITHIN RADIUS
 * @param string $postcode
 * @param int $distance
 * @return array
 * ---------------------------
 */
function get_postcodes($postcode, $distance = 25)
{
	$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $postcode . '&sensor=false';

	$data = json_decode(file_get_contents($url));

	if (empty($data->results)) {
		throw new Exception('<h3 class="activities">There was an error processing your postcode.</h3><p>The postcode was valid but can\'t be located in the UK. Please try again.</p>');
	}

	global $wpdb;

	$postcodes = $wpdb->get_results(
					$wpdb->prepare(
					"SELECT postcode, ( 3959 * acos( cos( radians(%s) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(%s) ) + sin( radians(%s) ) * sin( radians( latitude ) ) ) ) AS distance FROM malinky_postcodes HAVING distance < $distance ORDER BY distance", $data->results[0]->geometry->location->lat, $data->results[0]->geometry->location->lng, $data->results[0]->geometry->location->lat
					), ARRAY_A				
				);

	if (empty($postcodes)) {
		throw new Exception('<h3 class="activities">Activities</h3>There are no activities within the specified distance from your postcode. Please try again by increasing the distance.');
	}
	
	return $postcodes;
}

/**
 * -----------------------------------------------
 * GET POST IDS WITHIN RADIUS IN ORDER OF DISTANCE
 * @param array $postcodes
 * @return array 
 * -----------------------------------------------
 */
function get_activities_by_distance($postcodes)
{
	global $wpdb;

	$postcode_postids = array();
	$postids = array();
	
	$my_wp_postmeta = $wpdb->prefix . 'postmeta';

	$sprintf_digits = implode(', ', array_fill(0, count($postcodes), '%s'));
	foreach ($postcodes as $key => $value) {
		$postcode_order[$key] = '\'' . $value . '\'';
	}
	$postcode_order = implode(', ', $postcode_order);
	$postcode_order = 'meta_value, ' . $postcode_order;
	$postcode_postids = $wpdb->get_results(
					$wpdb->prepare(
					"SELECT post_id FROM $my_wp_postmeta WHERE meta_value IN ($sprintf_digits) ORDER BY FIELD ($postcode_order)", $postcodes
					), ARRAY_A				
				);

	if (empty($postcode_postids)) return $postcode_postids;

	foreach ($postcode_postids as $key => $value) {
		$postids[] = $value['post_id'];
	}
	
	return $postids;
}