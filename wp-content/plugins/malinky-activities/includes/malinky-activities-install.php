<?php

if (! defined('ABSPATH')) exit; // Exit if accessed directly

class Malinky_Activities_Install {

	public function __construct() {
		$this->init_user_roles();
	}

	public function init_user_roles() {
		global $wp_roles;

		if (class_exists('WP_Roles') && ! isset($wp_roles))
			$wp_roles = new WP_Roles();

		//add manage_activities role to administrator
		if (is_object($wp_roles)) {
			$wp_roles->add_cap('administrator', 'manage_activities');
			$wp_roles->add_cap('administrator', 'manage_activity_emails');
			$wp_roles->add_cap('administrator', 'manage_invoices');
		}
	}
	
}
new Malinky_Activities_Install();