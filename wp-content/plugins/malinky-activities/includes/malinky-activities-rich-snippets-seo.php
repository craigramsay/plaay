<?php
/**
 * --------------------------------
 * ADD OPEN GRAPH AND TWITTER CARDS
 * --------------------------------
 */
add_action('wp_head', 'malinky_activities_rich_snippets');
function malinky_activities_rich_snippets()
{
	if (!is_admin() && tree() != 'coach') {
		global $post;
		//Activities listings
		if (is_page('activities')) {
			//FACEBOOK / PINTEREST
			$og_title = get_post_meta($post->ID, '_yoast_wpseo_title', true);
			$og_description = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true);
			?>
			<meta property="og:site_name" content="<?php bloginfo('name'); ?>. <?php bloginfo('description'); ?>" />
			<meta property="og:type" content="product.group" />			
			<meta property="og:url" content="<?php the_permalink(); ?>" />			
			<meta property="og:title" content="<?php echo $og_title; ?>" />
			<meta property="og:description" content="<?php echo $og_description; ?>" />
			<meta property="og:image" content="<?php echo get_template_directory_uri() . '/img/logo_facebook_og.jpg'; ?>" />
  			<meta property="og:image:width" content="1200" />
  			<meta property="og:image:height" content="630" />			
			<?php
			//TWITTER CARD
			?>
			<meta name="twitter:site" content="@plaayuk">
			<meta name="twitter:card" content="summary">
			<meta name="twitter:title" content="<?php echo $og_title; ?>">
			<meta name="twitter:description" content="<?php echo $og_description; ?>">
			<meta name="twitter:image" content="<?php echo get_template_directory_uri() . '/img/logo_twitter_card.jpg'; ?>">
			<meta name="twitter:image:width" content="600" />
			<meta name="twitter:image:height" content="600" />
			<?php			
		//Activity listing
		} elseif (is_singular('activities')) {
			//FACEBOOK / PINTEREST
			?>
			<meta property="og:site_name" content="<?php bloginfo('name'); ?>. <?php bloginfo('description'); ?>" />
			<!--<meta property="og:type" content="plaayvouchertest:free_voucher" />-->
			<meta property="og:type" content="plaayvoucher:free_voucher" />
			<meta property="og:url" content="<?php the_permalink(); ?>" />			
			<meta property="og:title" content="<?php echo get_the_title() . ' - PLAAY. ' . the_activity_format($post, false, true) . '.'; ?>" />
			<meta property="og:description" content="<?php foreach (the_activity_voucher($post, false) as $k => $v) { echo $v . '. '; } ?><?php echo truncate_words(strip_tags(get_post_meta($post->ID, '_activity_description', true)), 140); ?>" />
			<meta property="og:image" content="<?php echo get_template_directory_uri() . '/img/logo_facebook_og.jpg'; ?>" />
  			<meta property="og:image:width" content="1200" />
  			<meta property="og:image:height" content="630" />
  			<meta property="og:price:amount" content="0.00">
  			<meta property="og:price:currency" content="GBP">  
  			<meta property="og:availability" content="instock">  			
			<?php
			//TWITTER CARD
			?>
			<meta name="twitter:site" content="@plaayuk">
			<meta name="twitter:card" content="product">
			<meta name="twitter:title" content="<?php echo get_the_title() . ' - PLAAY. ' . the_activity_format($post, false, true) . '.'; ?>">
			<meta name="twitter:description" content="<?php foreach (the_activity_voucher($post, false) as $k => $v) { echo $v . '. '; } ?><?php echo truncate_words(strip_tags(get_post_meta($post->ID, '_activity_description', true)), 140); ?>">
			<meta name="twitter:image" content="<?php echo get_template_directory_uri() . '/img/logo_twitter_card.jpg'; ?>">
			<meta name="twitter:image:width" content="600" />
			<meta name="twitter:image:height" content="600" />
			<meta name="twitter:data1" content="£0.00">
			<meta name="twitter:label1" content="PRICE">
			<meta name="twitter:data2" content="In Stock">
			<meta name="twitter:label2" content="AVAILABILITY">	
			<?php
		//Set tags to the activity voucher that has been downloaded so share boxes populate with correct data (particular facebook custom story)
		//Same as is_singular('activities') tagging above
		} elseif (is_page('voucher')) {
			global $wpdb;
			//Need to set up new $post using the correct activity id which is taken from the voucher id in the url
			$og_voucher_exploded_activation_code = malinky_activities_validate_activation_code_id($_GET['activation_code']);
			$og_voucher_id = $og_voucher_exploded_activation_code['voucher_id'];
			//get the activity id
			$og_activity_id = $wpdb->get_var(
				$wpdb->prepare(
					"SELECT activity_id FROM malinky_voucher_codes WHERE id = %d", $og_voucher_id
				)			
			);
			$og_post = get_post($og_activity_id);
			//FACEBOOK / PINTEREST
			?>
			<meta property="og:site_name" content="<?php bloginfo('name'); ?>. <?php bloginfo('description'); ?>" />
			<meta property="og:type" content="product" />
			<meta property="og:url" content="<?php the_permalink(); ?>" />			
			<meta property="og:title" content="<?php echo get_the_title($og_post->ID) . ' - PLAAY. ' . the_activity_format($og_post, false, true) . '.'; ?>" />
			<meta property="og:description" content="<?php foreach (the_activity_voucher($og_post, false) as $k => $v) { echo $v . '. '; } ?><?php echo truncate_words(strip_tags(get_post_meta($og_post->ID, '_activity_description', true)), 140); ?>" />
			<meta property="og:image" content="<?php echo get_template_directory_uri() . '/img/logo_facebook_og.jpg'; ?>" />
  			<meta property="og:image:width" content="1200" />
  			<meta property="og:image:height" content="630" />
  			<meta property="og:price:amount" content="0.00">
  			<meta property="og:price:currency" content="GBP">  
  			<meta property="og:availability" content="instock">  			
			<?php
			//TWITTER CARD
			?>
			<meta name="twitter:site" content="@plaayuk">
			<meta name="twitter:card" content="product">
			<meta name="twitter:title" content="<?php echo get_the_title($og_post->ID) . ' - PLAAY. ' . the_activity_format($og_post, false, true) . '.'; ?>">
			<meta name="twitter:description" content="<?php foreach (the_activity_voucher($og_post, false) as $k => $v) { echo $v . '. '; } ?><?php echo truncate_words(strip_tags(get_post_meta($og_post->ID, '_activity_description', true)), 140); ?>">
			<meta name="twitter:image" content="<?php echo get_template_directory_uri() . '/img/logo_twitter_card.jpg'; ?>">
			<meta name="twitter:image:width" content="600" />
			<meta name="twitter:image:height" content="600" />
			<meta name="twitter:data1" content="£0.00">
			<meta name="twitter:label1" content="PRICE">
			<meta name="twitter:data2" content="In Stock">
			<meta name="twitter:label2" content="AVAILABILITY">	
			<?php
		//Other front end pages
		} else {
			//FACEBOOK / PINTEREST
			$og_title = get_post_meta($post->ID, '_yoast_wpseo_title', true);
			$og_description = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true);
			?>
			<meta property="og:site_name" content="<?php bloginfo('name'); ?>. <?php bloginfo('description'); ?>" />
			<meta property="og:type" content="website" />
			<meta property="og:url" content="<?php the_permalink(); ?>" />
			<meta property="og:title" content="<?php echo $og_title; ?>" />
			<meta property="og:description" content="<?php echo $og_description; ?>" />
			<meta property="og:image" content="<?php echo get_template_directory_uri() . '/img/logo_facebook_og.jpg'; ?>" />
  			<meta property="og:image:width" content="1200" />
  			<meta property="og:image:height" content="630" />	
			<?php
			//TWITTER CARD
			?>
			<meta name="twitter:site" content="@plaayuk">
			<meta name="twitter:card" content="summary">
			<meta name="twitter:title" content="<?php echo $og_title; ?>">
			<meta name="twitter:description" content="<?php echo $og_description; ?>">
			<meta name="twitter:image" content="<?php echo get_template_directory_uri() . '/img/logo_twitter_card.jpg'; ?>">
			<meta name="twitter:image:width" content="600" />
			<meta name="twitter:image:height" content="600" />
			<?php			
		}
	}
}

/**
 * ------------------------------------
 * AMEND META TITLE ON ACTIVITY LISTING
 * @param string $metadesc
 * @return string   
 * ------------------------------------
 */
add_filter('wpseo_title', 'malinky_activities_meta_title');
function malinky_activities_meta_title($title)
{
	if (!is_admin()) {
		if (is_singular('activities')) {
			global $post;
			return get_the_title() . ' - PLAAY. ' . the_activity_format($post, false, true) . '.';
		} else {
			return $title;
		}
	}

}

/**
 * -----------------------------------------
 * AMEND META DESCRIPTION ON ACTIVITY LISTING
 * @param string $metadesc
 * @return string   
 * -----------------------------------------
 */
add_filter('wpseo_metadesc', 'malinky_activities_meta_description');
function malinky_activities_meta_description($metadesc)
{
	if (!is_admin()) {
		if (is_singular('activities')) {
			global $post;
			foreach (the_activity_voucher($post, false) as $k => $v) {
				$metadesc .= $v . '. ';
			}
			return $metadesc . truncate_words(strip_tags(get_post_meta($post->ID, '_activity_description', true)), 140);
		} else {
			return $metadesc;
		}
	}

}