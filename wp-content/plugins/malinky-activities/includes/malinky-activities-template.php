<?php

function get_malinky_activities_template($template_name, $args = array())
{
	if ($args && is_array($args))
		extract($args);

	include(MALINKY_ACTIVITIES_PLUGIN_DIR . 'templates/' . $template_name);
}

function get_malinky_activities_template_part($template_name)
{
	$template = MALINKY_ACTIVITIES_PLUGIN_DIR . 'templates/' . $template_name;
	if ($template)
		load_template($template, false);
}

/**
 * ----------------------
 * OUTPUT ACTIVITY FORMAT
 * @param obj $post
 * @param bool $echo
 * @return array|string 
 * ----------------------
 */
function the_activity_format($post = null, $echo = true, $singular = false)
{
	$job_types = '';

	if ($job_type = get_the_activity_format($post)) {
		if (!$echo) {
			foreach ($job_type as $key => $value) {
				$job_types[] = $job_type[$key]->name;
			}
			if (!$singular) {
				return $job_types;
			} else {
				return $job_types[0];
			}			
		}

		foreach ($job_type as $key => $value) {
			$job_types .= $job_type[$key]->name;
		}
		echo $job_types;
	}
}

/**
 * -------------------
 * GET ACTIVITY FORMAT
 * @param obj $post
 * @return array|bool 
 * --------------------
 */
function get_the_activity_format($post = null)
{
	$post = get_post($post);
	if ($post->post_type !== 'activities')
		return;

	$types = wp_get_post_terms($post->ID, 'malinky_activities_format');

	if ($types) {
		return $types;
	} else {
		return false;
	}
}

/**
 * ------------------------
 * OUTPUT ACTIVITY CATEGORY
 * @param obj $post
 * @param bool $echo
 * @return array|string 
 * ------------------------
 */
function the_activity_category($post = null, $echo = true, $singular = false)
{
	$job_types = '';

	if ($job_type = get_the_activity_category($post)) {
		if (!$echo) {
			foreach ($job_type as $key => $value) {
				$job_types[] = $job_type[$key]->name;
			}
			if (!$singular) {
				return $job_types;
			} else {
				return $job_types[0];
			}			
		}

		foreach ($job_type as $key => $value) {
			$job_types .= $job_type[$key]->name . '<br />';
		}
		echo $job_types;
	}
}

/**
 * ---------------------
 * GET ACTIVITY CATEGORY
 * @param obj $post
 * @return array|bool 
 * ---------------------
 */
function get_the_activity_category($post = null)
{
	$post = get_post($post);
	if ($post->post_type !== 'activities')
		return;

	$types = wp_get_post_terms($post->ID, 'malinky_activities_category');

	if ($types) {
		return $types;
	} else {
		return false;
	}
}

/**
 * -----------------------
 * OUTPUT ACTIVITY VOUCHER
 * @param obj $post
 * @param bool $echo
 * @return array|string 
 * -----------------------
 */
function the_activity_voucher($post = null, $echo = true, $singular = false)
{
	$job_types = '';

	if ($job_type = get_the_activity_voucher($post)) {
		if (!$echo) {
			foreach ($job_type as $key => $value) {
				$job_types[] = $job_type[$key]->name;
			}
			if (!$singular) {
				return $job_types;
			} else {
				return $job_types[0];
			}			
		}

		foreach ($job_type as $key => $value) {
			$job_types .= $job_type[$key]->name . '<br />';
		}
		echo $job_types;
	}		
}

/**
 * --------------------
 * GET ACTIVITY VOUCHER
 * @param obj $post
 * @return array|bool 
 * --------------------
 */
function get_the_activity_voucher($post = null)
{
	$post = get_post($post);
	if ($post->post_type !== 'activities')
		return;

	$types = wp_get_post_terms($post->ID, 'malinky_activities_voucher');

	if ($types) {
		return $types;
	} else {
		return false;
	}
}

/**
 * -------------------------
 * OUTPUT ACTIVITY AGE GROUP
 * @param obj $post
 * @param bool $echo
 * @return array|string
 * -------------------------
 */
function the_activity_age_group($post = null, $echo = true, $singular = false)
{
	$job_types = '';
	$job_type_string = '';

	if ($job_type = get_the_activity_age_group($post)) {
		
		foreach ($job_type as $key => $value) {
			$job_types[] = $job_type[$key]->name;
		}
		usort($job_types, "compare_ages");

		if (!$echo) {
			if (!$singular) {
				return $job_types;
			} else {
				return $job_types[0];
			}
		}

		foreach ($job_types as $key => $value) {
			$job_type_string .= $value . '<br />';
		}
		echo $job_type_string;
	}		
}

/**
 * ----------------------
 * GET ACTIVITY AGE GROUP
 * @param obj $post
 * @return array|bool 
 * ----------------------
 */
function get_the_activity_age_group($post = null)
{
	$post = get_post($post);
	if ($post->post_type !== 'activities')
		return;

	$types = wp_get_post_terms($post->ID, 'malinky_activities_age_group');

	if ($types) {
		return $types;
	} else {
		return false;
	}
}

/**
 * -------------------------------------
 * COMPARE AND ORDER ACTIVITY AGE GROUPS
 * @param string $a
 * @param string $b
 * @return array
 * -------------------------------------
 */
function compare_ages($a, $b)
{
	$a_part = explode('-', $a);
	$b_part = explode('-', $b);
	//echo 'A ' . $a_part[0] . ' B ' . $b_part[0] . '<br />';
    if ($a_part == $b_part) {
        return 0;
    }
    return ($a_part[0] < $b_part[0]) ? -1 : 1;
}

/**
 * ----------------------
 * OUTPUT ACTIVITY COUNTY
 * @param obj $post
 * @param bool $echo
 * @return array|string
 * ----------------------
 */
function the_activity_county($post = null, $echo = true)
{
	if ($job_type = get_the_activity_county($post)) {
		if (!$echo)
			return $job_type->name;

		echo $job_type->name;
		
	}
		
}

/**
 * -------------------
 * GET ACTIVITY COUNTY
 * @param obj $post
 * @return array|bool 
 * -------------------
 */
function get_the_activity_county($post = null)
{
	$post = get_post($post);
	if ($post->post_type !== 'activities')
		return;

	$types = wp_get_post_terms($post->ID, 'malinky_activities_county');

	if ($types)
		$type = current($types);
	else
		$type = false;

	return $type;
}

/**
 * ----------------------
 * OUTPUT ACTIVITY STATUS
 * @param obj $post
 * @return string
 * ----------------------
 */
function the_activity_status( $post = null )
{
	echo get_the_activity_status( $post );
}

/**
 * -------------------
 * GET ACTIVITY STATUS
 * @param obj $post
 * @return array|bool 
 * -------------------
 */
function get_the_activity_status( $post = null )
{
	$post = get_post( $post );

	$status = $post->post_status;

	if ( $status == 'publish' )
		$status = __( 'Active', 'job_manager' );
	elseif ( $status == 'pending' )
		$status = __( 'Inactive', 'job_manager' );

	return $status;
}

/**
 * -----------------------
 * GET NICE POSTCODE
 * @param string $postcode
 * @return string
 * -----------------------
 */
function get_front_end_postcode($postcode = NULL)
{
	$postcode = preg_replace('/\s+/', '', $postcode);
	return strlen($postcode) == 7 ? strtoupper(substr_replace($postcode, ' ', 4, 0)) : strtoupper(substr_replace($postcode, ' ', 3, 0));
}

/**
 * --------------------------
 * GET SINGLE TERM NAME BY ID
 * @param int $term_id
 * @param string $taxonomy
 * @return string
 * --------------------------
 */
function get_single_term_name($term_id, $taxonomy)
{
	$term = get_term($term_id, $taxonomy);

	if (is_wp_error($term))
		return;	

	return $term->name;
}

/**
 * ------------------------------
 * GET ATTENTED STATUS OF VOUCHER
 * @param int $voucher_id
 * @return bool
 * ------------------------------
 */
function mark_attended_status($voucher_id)
{
	global $wpdb;

	$mark_attended = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT voucher_mark_attended FROM malinky_voucher_codes WHERE id = %d", $voucher_id
		)			
	);

	if ($mark_attended)
		return 'Y';

	if (!$mark_attended)
		return 'N';
}

/**
 * -------------------------------------
 * GET DISTANCE BETWEEN PARENT AND COACH
 * @param int $activity_id
 * @param string $postcode
 * @return bool
 * -------------------------------------
 */
function parent_coach_distance($activity_id, $parent_postcode)
{
	if(!preg_match("~^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$~i", $parent_postcode)) return;

	$activity_postcode = get_post_meta($activity_id, '_activity_postcode', true);

	$parent_coord = get_postcode_lat_lng($parent_postcode);
	$activity_coord = get_postcode_lat_lng($activity_postcode);

	$coach_distance_travelled = get_post_meta($activity_id, '_activity_distance_travelled', true);

	return number_format(lat_lng_distance($parent_coord['lat'], $parent_coord['lng'], $activity_coord['lat'], $activity_coord['lng'], 'M'), 1);
}

/**
 * -----------------------
 * GET LAT LNG OF POSTCODE
 * @param string $postcode
 * @return array
 * -----------------------
 */
function get_postcode_lat_lng($postcode)
{
	$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $postcode . '&sensor=false';
	$data = json_decode(file_get_contents($url));
	$coord['lat'] = $data->results[0]->geometry->location->lat;
	$coord['lng'] = $data->results[0]->geometry->location->lng;
	return $coord;
}

/**
 * --------------------------------
 * GET DISTANCE BETWEEN COORDINATES
 * @param int $lat1
 * @param int $lng1
 * @param int $lat2
 * @param int $lng2
 * @param str $unit   
 * @return int
 * --------------------------------
 */

function lat_lng_distance($lat1, $lng1, $lat2, $lng2, $unit)
{
	$theta = $lng1 - $lng2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);

	if ($unit == "K") {
		return ($miles * 1.609344);
	} else if ($unit == "N") {
  		return ($miles * 0.8684);
	} else {
   		return $miles;
  	}
}

/**
 * --------------------------------------------------------------------
 * CHECK DISTANCE BETWEEN PARENT, ACTIVITY AND COACH DISTANCE TRAVELLED
 * @param int $activity_id
 * @param int $parent_coach_distance
 * @return array
 * --------------------------------------------------------------------
 */
function coach_will_travel_distance($activity_id, $parent_coach_distance)
{
	$coach_distance_travelled = get_post_meta($activity_id, '_activity_distance_travelled', true);
	if ($coach_distance_travelled == 0) {
		return 'Travel to the venue';
	} elseif ( ($coach_distance_travelled != 0) && ($coach_distance_travelled < round($parent_coach_distance)) ) {
		return 'The coach won\'t travel this distance';
	} else {
		return 'The coach will travel to you';
	}
}

/**
 * ---------------------------
 * INSERT POSTCODE SEARCH FORM
 * @return void
 * ---------------------------
 */
function postcode_search_form()
{
    //echo '<a href="" id="unset_postcode" onclick="return false;">Remove Postcode</a>'; ?>
    <form id="postcode_search_form" method="post" action="<?php echo esc_url( site_url( 'process-postcode-search' ) ); ?>" role="form">
    	<?php
    	global $post;
    	if (isset($_SESSION['postcode']) && is_page('activities')) { ?>
    		<p class="postcode_search_distance_message">Activities within <span class="postcode_search_distance"><?php echo $_SESSION['distance_selected']; ?></span> miles of the following postcode</p>
    		<p class="postcode_search_distance_message_medium">Activities within <span class="postcode_search_distance"><?php echo $_SESSION['distance_selected']; ?></span> miles of</span></p>
    		<p class="postcode_search_distance_message_small"><span class="postcode_search_distance"><?php echo $_SESSION['distance_selected']; ?></span> miles of</span></p>
    	<?php } elseif (isset($_SESSION['postcode']) && is_singular('activities')) { ?>
    		<p class="postcode_search_distance_message">Activity within <span class="postcode_search_distance"><?php echo $_SESSION['distance_selected']; ?></span> miles of the following postcode</p>
    		<p class="postcode_search_distance_message_medium">Activity within <span class="postcode_search_distance"><?php echo $_SESSION['distance_selected']; ?></span> miles of</span></p>
    		<p class="postcode_search_distance_message_small"><span class="postcode_search_distance"><?php echo $_SESSION['distance_selected']; ?></span> miles of</span></p>    	
    	<?php } else { ?>
    		<p class="postcode_search_distance_message">Search</p>
    		<p class="postcode_search_distance_message_medium">Search</p>
    		<p class="postcode_search_distance_message_small">Search</p>
    	<?php } ?>
        <input type="text" name="home_postcode_search" id="home_postcode_search" placeholder="Enter Postcode..." value="<?php echo isset($_SESSION['postcode']) ? esc_attr_e(get_front_end_postcode($_SESSION['postcode'])) : ''; ?>" />
        <?php wp_nonce_field( 'malinky_activities_postcode_search', 'malinky_activities_postcode_search_nonce' ); ?>
        <input type="submit" name="submit_postcode_search" class="button_postcode_search" value="" />
    </form>
<?php }

/**
 * --------------------------------
 * INSERT HOME POSTCODE SEARCH FORM
 * @return void
 * --------------------------------
 */
function postcode_search_form_home()
{ ?>
    <form id="malinky_postcode_search_form_home" method="post" action="<?php echo esc_url( site_url( 'process-postcode-search' ) ); ?>" role="form">
        <input type="text" name="home_postcode_search" id="home_postcode_search" placeholder="Enter Postcode..." value="<?php echo isset($_SESSION['postcode']) ? esc_attr_e(get_front_end_postcode($_SESSION['postcode'])) : ''; ?>" />
        <?php wp_nonce_field( 'malinky_activities_postcode_search', 'malinky_activities_postcode_search_nonce' ); ?>
        <input type="submit" name="submit_postcode_search" class="button_home" value="" />
    </form>
<?php }

/**
 * -------------------------------
 * INSERT 404 POSTCODE SEARCH FORM
 * @return void
 * -------------------------------
 */
function postcode_search_form_404()
{
	?>
    <form id="postcode_search_form" method="post" action="<?php echo esc_url( site_url( 'process-postcode-search' ) ); ?>" role="form">
        <input type="text" name="home_postcode_search" id="home_postcode_search" placeholder="Enter Postcode..." value="<?php echo isset($_SESSION['postcode']) ? esc_attr_e(get_front_end_postcode($_SESSION['postcode'])) : ''; ?>" />
        <?php wp_nonce_field( 'malinky_activities_postcode_search', 'malinky_activities_postcode_search_nonce' ); ?>
        <input type="submit" name="submit_postcode_search" class="button_postcode_search" value="" />
    </form>
<?php }

/**
 * -------------------------------
 * GET THE HOMEPAGE STRAPLINE TO USE IN NO POSTCODE ERROR MESSAGES AND OTHER PLACES.
 * @return str
 * -------------------------------
 */
function get_homepage_strapline()
{
	$malinky_home_page = get_page_by_path('home');

	if ($malinky_home_page)
		$malinky_home_page_id = $malinky_home_page->ID;

	return get_field('home_strapline', $malinky_home_page_id);

}