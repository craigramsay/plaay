<?php
//both wp_ajax_nopriv and wp_ajax are added to the action name malinky-activities-ajax-submit
//the action function is then called as 2nd parameter
//malinky-activities-ajax-submit is used in the js file as the ajax action
add_action('wp_ajax_nopriv_malinky-activities-ajax-submit', 'malinky_activities_ajax_submit');
add_action('wp_ajax_malinky-activities-ajax-submit', 'malinky_activities_ajax_submit');
 
add_action('wp_ajax_nopriv_malinky-activities-ajax-unset-postcode', 'malinky_activities_ajax_unset_postcode');
add_action('wp_ajax_malinky-activities-ajax-unset-postcode', 'malinky_activities_ajax_unset_postcode');
 
function malinky_activities_ajax_unset_postcode()
{
	if (isset($_SESSION['postcode'])) {
		unset($_SESSION['postcode']);
		unset($_SESSION['distance_selected']);
		$result['redirect'] = site_url('activities');
		echo json_encode($result); exit();
	}
	
}

function malinky_activities_ajax_submit()
{   
	$errors = new WP_Error();
	
	if (isset($_SESSION['postcode']))
		$postcode = $_SESSION['postcode'];

	try {

		if (isset($postcode)) {
			$distance_selected = absint($_POST['distance']);
			$_SESSION['distance_selected'] = absint($_POST['distance']);
			$postcodes = get_postcodes($postcode, $distance_selected);
		}

	} catch ( Exception $e ) {
		$errors->add('malinky_activities_postcode_error', $e->getMessage());
		if ($errors->get_error_code()) {
			$error_messages = $errors->errors;
		} else {
			$error_messages = NULL;
		}
	}	

	//ajax page load results with valid postcode
	if (isset($postcodes) && isset($_SESSION['postcode'])) {
		
		foreach ($postcodes as $key => $value) {
			$postcodes_only[] = $postcodes[$key]['postcode'];
		}

		//get activities will return results but at this point we dont know if the activity is published
		//or it could be picking up old activity psotcode from the malinky postcodes_table that has now been deleted
		$activity_ids = get_activities_by_distance($postcodes_only);

		//remove activity_ids where the coach distance travelled is further away than the parent wishes to travel
		foreach($activity_ids as $activity_key => $id) {
			$coach_distance_travelled = get_post_meta($id, '_activity_distance_travelled', true);
			$coach_distance_travelled_activity_postcode = get_post_meta($id, '_activity_postcode', true);
			foreach ($postcodes as $postcodes_key => $postcodes_value) {
				if ($postcodes_value['postcode'] == $coach_distance_travelled_activity_postcode) {
					if ( ($coach_distance_travelled != 0) && ($coach_distance_travelled < round($postcodes_value['distance'])) ) {
						unset($activity_ids[$activity_key]);
					}
				}
			} 
		}

		//remove activities from a coach who hasn't set up payment details yet
		$banned_coach_ids = array();
		foreach ($activity_ids as $key => $activity_id) {
			$banned_coach_ids[] = get_post_field('post_author', $activity_id);
		}

		//get unique coach ids
		$banned_coach_ids = array_unique($banned_coach_ids);
		$banned_coach_ids = array_values($banned_coach_ids);
		
		//loop through coach ids if _coach_stripe_payment_status is true unset them. The query uses author__not_in to remove banned coaches
		foreach ($banned_coach_ids as $key => $banned_coach_id) {
			$banned_coach_payment_status = get_user_meta($banned_coach_id, '_coach_stripe_payment_status', true);
			if ($banned_coach_payment_status) {
				unset($banned_coach_ids[$key]);
			}
		}

		switch ($_POST['orderby']) {
			case 'activity_az' :
				$orderby_query = 'title';
				$orderby_selected = 'activity_az';
				break;
			case 'activity_za' :
				$orderby_query = 'title';
				$orderby_selected = 'activity_za';
				break;			
			case 'distance_nearest' :
				$orderby_query = 'post__in';
				$orderby_selected = 'distance_nearest';							
			default:
				$orderby = NULL;
		}
		switch ($_POST['order']) {
			case 'asc' :
				$order_query = 'asc';
				$order_selected = 'asc';
				break;
			case 'desc' :
				$order_query = 'desc';
				$order_selected = 'desc';
				break;
			default:
				$order_query = NULL;
		}		

		$posts_per_page = absint($_POST['posts_per_page']);
		
		$args = array(
			'post_type'         => 'activities',
			'post_status'       => 'publish',
			'post__in'			=> $activity_ids,
			'orderby'           => isset($orderby_query) ? $orderby_query : 'post__in',
			'order'             => isset($order_query) ? $order_query : 'asc',
			'posts_per_page'    => $posts_per_page,
			'offset'            => (max(1, absint($_POST['page'])) - 1) * $posts_per_page,	
			'author__not_in'	=> $banned_coach_ids,
			'meta_query'		=> array(
										array(
											'key'		=> '_activity_postcode',
											'value'		=> $postcodes_only,
											'compare'	=> 'IN'
											)
										)	
		);

		$filters_args = array(
			'post_type'         => 'activities',
			'post_status'       => 'publish',
			'post__in'			=> $activity_ids,
			'posts_per_page'    => -1,	
			'author__not_in'	=> $banned_coach_ids,
			'meta_query'		=> array(
										array(
											'key'		=> '_activity_postcode',
											'value'		=> $postcodes_only,
											'compare'	=> 'IN'
											)
										)
		);	

		if (isset($_POST['filter_format'])) {
			$args['tax_query'][] = array(
									'taxonomy' 	=> 'malinky_activities_format',
									'field' 	=> 'term_id',
									'terms' 	=> $_POST['filter_format']
								);
			$filters_args['tax_query'][] = array(
									'taxonomy' 	=> 'malinky_activities_format',
									'field' 	=> 'term_id',
									'terms' 	=> $_POST['filter_format']
								);
		}
		if (isset($_POST['filter_age_group'])) {
			$args['tax_query'][] = array(
									'taxonomy' 	=> 'malinky_activities_age_group',
									'field' 	=> 'term_id',
									'terms' 	=> $_POST['filter_age_group']
								);
			$filters_args['tax_query'][] = array(
									'taxonomy' 	=> 'malinky_activities_age_group',
									'field' 	=> 'term_id',
									'terms' 	=> $_POST['filter_age_group']
								);											

		}		
		if (isset($_POST['filter_category'])) {
			$args['tax_query'][] = array(
									'taxonomy' 	=> 'malinky_activities_category',
									'field' 	=> 'term_id',
									'terms' 	=> $_POST['filter_category']
								);
			$filters_args['tax_query'][] = array(
									'taxonomy' 	=> 'malinky_activities_category',
									'field' 	=> 'term_id',
									'terms' 	=> $_POST['filter_category']
								);
		}
		if (isset($_POST['filter_voucher'])) {
			$args['tax_query'][] = array(
									'taxonomy' 	=> 'malinky_activities_voucher',
									'field' 	=> 'term_id',
									'terms' 	=> $_POST['filter_voucher']
								);
			$filters_args['tax_query'][] = array(
									'taxonomy' 	=> 'malinky_activities_voucher',
									'field' 	=> 'term_id',
									'terms' 	=> $_POST['filter_voucher']
								);
		}

		$filters = new WP_Query;

		if (isset($_POST['filter_format'])) {
			$filters_output['format'] = filter_get_terms($filters->query($filters_args), 'malinky_activities_format', absint($_POST['filter_format']));
		} else {
			$filters_output['format'] = filter_get_terms($filters->query($filters_args), 'malinky_activities_format');
		}

		if (isset($_POST['filter_age_group'])) {
			$filters_output['age_group'] = filter_get_terms($filters->query($filters_args), 'malinky_activities_age_group', absint($_POST['filter_age_group']));
		} else {
			$filters_output['age_group'] = filter_get_terms($filters->query($filters_args), 'malinky_activities_age_group');
		}

		if (isset($_POST['filter_category'])) {
			$filters_output['category'] = filter_get_terms($filters->query($filters_args), 'malinky_activities_category', absint($_POST['filter_category']));
		} else {
			$filters_output['category'] = filter_get_terms($filters->query($filters_args), 'malinky_activities_category');
		}

		if (isset($_POST['filter_voucher'])) {
			$filters_output['voucher'] = filter_get_terms($filters->query($filters_args), 'malinky_activities_voucher', absint($_POST['filter_voucher']));
		} else {
			$filters_output['voucher'] = filter_get_terms($filters->query($filters_args), 'malinky_activities_voucher');			
		}

		//removes all values if filters are empty
		$filters_output = array_filter($filters_output);
		
		$activities = new WP_Query($args);

		//final error check as there could be no posts found in the query
		//based on banned coaches or using old activities from postcode table
		//need to trigger error and ob_start and ob_get_clean
		try {
			if (empty($activities->post_count))
				throw new Exception('<h3 class="activities">Activities</h3>There are no activities within the specified distance from your postcode. Please try again by increasing the distance.');
		} catch ( Exception $e ) {
			$errors->add('malinky_activities_postcode_error', $e->getMessage());
			if ($errors->get_error_code()) {
				$error_messages = $errors->errors;
			} else {
				$error_messages = NULL;
			}
			ob_start(); ?>
			<nav id="sidebar_content" role="sidebar">
				<div id="activities_filter">
					<h3 class="activities">Refine Results</h3>
					<?php get_malinky_activities_template('activities-filter.php', array('filters_output' => $filters_output)); ?>
				</div>
			</nav>
			<?php $result['activities_filter'] = ob_get_clean();
			ob_start(); ?>
			<div id="activities" data-orderby="<?php echo esc_attr('distance_nearest'); ?>" data-order="<?php echo esc_attr('asc'); ?>" data-posts_per_page="<?php echo esc_attr(10); ?>" data-page="<?php echo esc_attr(0); ?>">
				<?php get_malinky_activities_template('activities.php', array('orderby_selected' => 'distance_nearest', 'distance_selected' => $distance_selected, 'posts_per_page_selected' => 10, 'error' => $error_messages['malinky_activities_postcode_error'][0])); ?>
			</div>
			<?php $result['activities'] = ob_get_clean();
			echo json_encode( $result );
			exit();
		}

		ob_start();

		if ($activities->have_posts()) :
			while ($activities->have_posts() ) : $activities->the_post();
				if (isset($postcodes) && isset($_SESSION['postcode'])) {
					$activity_postcode = get_post_meta($activities->post->ID, '_activity_postcode', true);
					foreach ($postcodes as $key => $value) {
						if ($value['postcode'] == $activity_postcode) {
							$distance = number_format($value['distance'], 1) . ' miles';
						}
					} 
				}		
				$activities->post->temp_distance_output = $distance;
				get_malinky_activities_template_part('activities-listings.php');
			endwhile;
		endif;

		wp_reset_postdata();

		$activities_output = ob_get_clean();

		$total_posts 		= $activities->found_posts;
		$displayed_posts 	= $activities->post_count;
		$offset  = (max(1, absint($_POST['page'])) - 1) * $posts_per_page;		

		ob_start(); ?>

		<nav id="sidebar_content" role="sidebar">
			<div id="activities_filter">
				<h3 class="activities">Refine Results</h3>
				<?php get_malinky_activities_template('activities-filter.php', array('filters_output' => $filters_output)); ?>
			</div>
		</nav>

		<?php $result['activities_filter'] = ob_get_clean();

		ob_start(); ?>

		<div id="activities" data-orderby="<?php echo esc_attr(isset($orderby_selected) ? $orderby_selected : 'post__in'); ?>" data-order="<?php echo esc_attr(isset($order_selected) ? $order_selected : 'asc'); ?>" data-posts_per_page="<?php echo esc_attr($posts_per_page); ?>" data-page="<?php echo esc_attr(absint($_POST['page'])); ?>">
			<?php get_malinky_activities_template('activities.php', array('activities_output' => $activities_output, 'postcodes' => $postcodes, 'max_num_pages' => $activities->max_num_pages, 'orderby_selected' => $orderby_selected, 'distance_selected' => $distance_selected, 'posts_per_page_selected' => $posts_per_page, 'page' => absint($_POST['page']), 'total_posts' => $total_posts, 'offset' => $offset, 'displayed_posts' => $displayed_posts)); ?>
		</div>

		<?php $result['activities'] = ob_get_clean();

	//ajax page load no results as problem with postcode
	} elseif (!isset($postcodes) && isset($_SESSION['postcode'])) {

		ob_start(); ?>

		<div id="activities_filter">
			<h3 class="activities">Refine Results</h3>
			<p>There are no results.</p>
		</div>	

		<?php $result['activities_filter'] = ob_get_clean();
		
		ob_start(); ?>

		<div id="activities" data-orderby="<?php echo esc_attr('distance_nearest'); ?>" data-order="<?php echo esc_attr('asc'); ?>" data-posts_per_page="<?php echo esc_attr(10); ?>" data-page="<?php echo esc_attr(0); ?>">
			<?php get_malinky_activities_template('activities.php', array('orderby_selected' => 'distance_nearest', 'distance_selected' => $distance_selected, 'posts_per_page_selected' => 10, 'error' => $error_messages['malinky_activities_postcode_error'][0])); ?>
		</div>

		<?php $result['activities'] = ob_get_clean();

	//no results shown as no postcode has been entered
	} else {

		ob_start(); ?>

		<div id="activities_filter">
			<h3 class="activities">Refine Results</h3>
			<p>There are no results.</p>
		</div>

		<?php $result['activities_filter'] = ob_get_clean();

		ob_start(); ?>

		<div id="activities">
			<?php get_malinky_activities_template('activities.php', array('orderby_selected' => 'distance_nearest', 'distance_selected' => 25, 'posts_per_page_selected' => 10, 'error' => '<h3 class="activities">Please enter a postcode</h3><p>Welcome to PLAAY.CO.UK. ' . get_homepage_strapline() . ' Enter a postcode to search.</p>')); ?>
		</div>

		<?php $result['activities'] = ob_get_clean();

	}	
	
	echo json_encode( $result );

	exit();
}