<?php
/**
 * ------------------------
 * ERROR MESSAGES
 * @param string $error_key
 * @return array  
 * ------------------------
 */
function malinky_activities_error_messages($error_key)
{
    $error_messages = array(
        'activity_name_error'                   => 'Please enter an activity name.',
        'activity_format_error'                 => 'Please choose a session format.',
        'activity_category_error'               => 'Please choose atleast one category.',
        'activity_voucher_error'                => 'Please choose atleast one voucher.',
        'activity_age_group_error'              => 'Please choose atleast one age group.',       
        'activity_description_error'            => 'Please enter an activity description.',     
        'activity_distance_travelled_error'     => 'Please choose a distance travelled.',
        'activity_address_error'                => 'Please enter your address.',
        'activity_town_error'                   => 'Please enter your town.',
        'activity_county_error'                 => 'Please choose a county.',       
        'activity_postcode_error'               => 'Please enter a valid postcode.',
        'parent_name_error'                     => 'Please enter your name.',
        'parent_county_error'                   => 'Please choose a county.',
        'parent_postcode_error'                 => 'Please enter a valid postcode.',
        'parent_email_error'                    => 'Please enter a valid email address.',
        'parent_phone_error'                    => 'Please enter a valid phone number.',
        'terms_and_conditions_error'            => 'Please accept our terms and conditions.',
        'voucher_application_category_error'    => 'Please select one category.',
        'voucher_application_voucher_error'     => 'Please select one voucher.',
        'voucher_application_age_group_error'   => 'Please select one age group.',
        'voucher_application_age_group_error'   => 'Please select one age group.',
        'child_birth_month_error'               => 'Please choose a birth month.',
        'coach_logo_error'                      => 'Please upload a file of the specified format and size.'
    );
    return $error_messages[$error_key];
}

/**
 * -------------
 * SESSION STUFF
 * -------------
 */
add_action('init', 'myStartSession', 1);
add_action('wp_logout', 'myEndSession');
add_action('wp_login', 'myEndSession');

function myStartSession() {
    if(!session_id()) {
        session_start();
    }
}

function myEndSession() {
    session_destroy();
}

/**
 * -----------------------------------------
 * CREATE OWN PAGINATION LINKS AS USING AJAX
 * -----------------------------------------
 */
add_filter('paginate_links', 'malinky_activities_paginate_links');
function malinky_activities_paginate_links($link) {
	$page = preg_replace('/[^0-9]/', '', $link);
	$link = site_url('activities/page/') . $page;
	return $link;
}

/**
 * ------------------------------------------------------------
 * CREATE LINK TO SINGLE ACTIVITY TEMPLATE IN PLUG IN DIRECTORY
 * ------------------------------------------------------------
 */
add_filter('single_template', 'malinky_activities_activities_post_type_single_template');
function malinky_activities_activities_post_type_single_template($single_template)
{
     global $post;
     if ($post->post_type == 'activities') {
          $single_template = MALINKY_ACTIVITIES_PLUGIN_DIR . 'templates/activity.php';
     }
     return $single_template;
}

/**
 * --------------------------------------
 * EMAIL UNSUBSCRIBE
 * @return TODO  
 * --------------------------------------
 */
function malinky_activities_email_unsubscribe_process()
{
    $errors = new WP_Error();

    //check form submit and nonce
    if ((empty($_POST['submit_email'])) || (!isset($_POST['malinky_email_unsubscribe_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_email_unsubscribe_form_nonce'], 'malinky_email_unsubscribe_form_nonce_action')))
        wp_die(__('Security Error'));

    $parent_email = sanitize_email($_POST['parent_email']);
    $parent_email_hashed = md5($parent_email);

    $unsubscribe_code = $_POST['unsubscribe_code'];
    if (!preg_match('/^[a-f0-9]{32}$/', $unsubscribe_code)) wp_safe_redirect(site_url());

    if ($parent_email_hashed != $unsubscribe_code) {
        $errors->add('email_unsubscribe_error', 'There has been a problem validating your email address. Please try again.');
        return $errors;
    }

    global $wpdb;

    $unsubscribe_success = $wpdb->update( 
        'malinky_parents',
        array(
            'marketing_accept' => false
        ), 
        array('email' => $parent_email), 
        '%d',
        '%s'
    );

    return;

}