<?php
/**
 * ---------------------------------
 * GET ALL UNPIAD CHARGES FOR STRIPE
 * @return arr   
 * ---------------------------------
 */
function malinky_activities_get_unpaid_charges()
{
	$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
	//Live Stripe Key
	if ($stripe_secret_key['settings_stripe_live']) {

		$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
		Stripe::setApiKey($stripe_secret_key['settings_stripe_live_secret_key']);
	
	//Test Stripe Key
	} else {

		$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
		Stripe::setApiKey($stripe_secret_key['settings_stripe_test_secret_key']);

	}

	global $wpdb;

	$charges = $wpdb->get_results(
	"SELECT malinky_stripe_payments.voucher_id, malinky_voucher_codes.coach_id, malinky_voucher_codes.voucher_cost FROM malinky_stripe_payments LEFT JOIN malinky_voucher_codes ON malinky_stripe_payments.voucher_id = malinky_voucher_codes.id WHERE (stripe_charge_id IS NULL OR stripe_charge_id = '') AND (stripe_error_type IS NULL OR stripe_error_type = '') AND malinky_voucher_codes.voucher_code_activated = TRUE", ARRAY_A		
	);

	print_r($charges);

	return $charges;	
}

/**
 * ----------------------------------------------
 * GET ALL FAILED / CARD ERROR CHARGES FOR STRIPE
 * @return arr   
 * ----------------------------------------------
 */
function malinky_activities_get_card_error_charges()
{
	$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
	//Live Stripe Key
	if ($stripe_secret_key['settings_stripe_live']) {

		$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
		Stripe::setApiKey($stripe_secret_key['settings_stripe_live_secret_key']);
	
	//Test Stripe Key
	} else {

		$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
		Stripe::setApiKey($stripe_secret_key['settings_stripe_test_secret_key']);

	}

	global $wpdb;

	$charges = $wpdb->get_results(
	"SELECT malinky_stripe_payments.voucher_id, malinky_stripe_payments.stripe_error_created, malinky_voucher_codes.coach_id, malinky_voucher_codes.voucher_cost FROM malinky_stripe_payments LEFT JOIN malinky_voucher_codes ON malinky_stripe_payments.voucher_id = malinky_voucher_codes.id WHERE (stripe_charge_id IS NULL OR stripe_charge_id = '') AND stripe_error_type = 'card_error' AND malinky_voucher_codes.voucher_code_activated = TRUE", ARRAY_A		
	);

	print_r($charges);

	return $charges;
}

/**
 * ----------------------------------------
 * SET UP THE CHARGE ARRAY READY FOR STRIPE
 * @param arr $charges 
 * @return arr  
 * ----------------------------------------
 */
function malinky_activities_set_charge_array($charges)
{
	
	//get the coach_ids from array
	foreach ($charges as $key => $value) {
		$coach_ids[] = $value['coach_id'];
	}

	//get unique coach_ids
	$coach_ids = array_unique($coach_ids);
	$coach_ids = array_values($coach_ids);

	//start new array and add the coach_ids
	foreach ($coach_ids as $key => $value) {
		$coach_charges[] = array('coach_id' => $value);
	}

	//loop through new final array that just contains the unique coach ids to be charged
	foreach ($coach_charges as $key => $coach_array) {
		//compare to original array with all voucher ids to be charged
		foreach ($charges as $charges_key => $charges_array) {
			//if the coach_ids match
			if ($coach_array['coach_id'] == $charges_array['coach_id']) {
				//test if array key for cost exists as a running total is created
				if (array_key_exists('charge_amount', $coach_charges[$key])) {
					$coach_charges[$key]['charge_amount'] += $charges_array['voucher_cost'];	
				} else {
					$coach_charges[$key]['charge_amount'] = $charges_array['voucher_cost'];	
				}
				//add another array of all voucher_ids
				$coach_charges[$key]['voucher_ids'][] = $charges_array['voucher_id'];
				//check if we're being passed in previously declined $charges array
				if (isset($charges_array['stripe_error_created'])) {
					$coach_charges[$key]['stripe_error_created'] = $charges_array['stripe_error_created'];
				}
			}
		}
	}

	print_r($coach_charges);

	return $coach_charges;
}

/**
 * ----------------------------------------------------------------
 * GET THE COACH CARD STATUS IS CARD SET UP AND HASNT BEEN DECLINED
 * @param arr $coach_charges 
 * @return arr  
 * ----------------------------------------------------------------
 */
function malinky_activities_get_coach_card_status($coach_charges)
{
	foreach ($coach_charges as $key => $coach_array) {
		//get two coach card details indicators
		$coach_stripe_id = get_user_meta($coach_array['coach_id'], '_coach_stripe_id', true);
		$coach_stripe_payment_status = get_user_meta($coach_array['coach_id'], '_coach_stripe_payment_status', true);
		//if either is missing remove coach for charging
		if ($coach_stripe_id && $coach_stripe_payment_status) {
			$coach_charges[$key]['coach_stripe_id'] = $coach_stripe_id;
		} else {
			unset($coach_charges[$key]);
		}
	}

	return $coach_charges;
}

/**
 * -------------------------
 * CHARGE COACHES
 * @param arr $coach_charges
 * @return void   
 * -------------------------
 */
function malinky_activities_charge_coach($coach_charges)
{
	$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
	//Live Stripe Key
	if ($stripe_secret_key['settings_stripe_live']) {

		$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
		Stripe::setApiKey($stripe_secret_key['settings_stripe_live_secret_key']);
	
	//Test Stripe Key
	} else {

		$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
		Stripe::setApiKey($stripe_secret_key['settings_stripe_test_secret_key']);

	}

	global $wpdb;

	foreach ($coach_charges as $key => $coach_array) {

		try {
		
			$charge = Stripe_Charge::create(array(
				'amount' => ($coach_array['charge_amount']),
				'currency' => 'gbp',
				'customer' => $coach_array['coach_stripe_id'],
				'description' => 'PLAAY Downloaded Voucher Charge'
			));

			//update each line in stripe payments that corresponds to the coaches voucher id
			//remember the payment is the total for the transaction not per voucher
			foreach($coach_array['voucher_ids'] as $voucher_id_keys => $voucher_id_array ) {

				$update_charges = $wpdb->update( 
					'malinky_stripe_payments', 
					array( 
						'stripe_charge_id' 		=> $charge->id,
						'stripe_charge_amount' 	=> $charge->amount,
						'stripe_charge_created' => date("Y-m-d H:i:s", $charge->created),
						'stripe_error_type' 	=> '',
						'stripe_error_code' 	=> '',
						'stripe_error_message' 	=> '',
						'stripe_error_created' 	=> '',
					), 
					array('voucher_id' => $voucher_id_array), 
					array( 
						'%s',
						'%d',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
					), 
					array('%d') 
				);

			}

			//send an email to the coach confirming the successful charge
			malinky_activities_charge_coach_success_email_coach($coach_array['coach_id'], $charge->id, $charge->amount);

		} catch (Stripe_CardError $e) {

			//if we're dealing with a previously declined charge
			if (isset($coach_array['stripe_error_created'])) {

				$date_difference = time() - strtotime($coach_array['stripe_error_created']);
				$day_difference = $date_difference / 86400;
				//if failure is within 7 days of orginal failure
				if (floor($day_difference < 7)) {

					//update stripe_payments with error code
					malinky_activities_charge_failure_repeated_card_error(
						$coach_array['voucher_ids'],
						$err['type'],
						$err['code'],
						$err['message']
					);

					//send admin email
					malinky_activities_coach_stripe_failure_email_admin(
						'Coach Charge - Card Error',
						$coach_array['coach_id'],
						'Coach to update card details',
						'stripe_charge_cron',
						'Stripe_CardError',
						$err['type'],
						$err['message'],
						date('d/m/Y - G:i')	
					);

					//send coach email to update card details
					malinky_activities_coach_stripe_failure_email_coach(
						'PLAAY Charge - Card Error',
						$coach_array['coach_id'],
						'Please check card details',
						$err['message'],
						'<p>There has been an error during the charging of your card for downloaded vouchers. Please check the details below to guide you in rectifying the issue. We will attempt to charge your card again automatically.</p>'
					);					

				//if failure is outside of 7 days then need to also deactivate the listings
				} else {

					//update stripe_payments with error code
					malinky_activities_charge_failure_repeated_card_error(
						$coach_array['voucher_ids'],
						$err['type'],
						$err['code'],
						$err['message']
					);

					//send admin email
					malinky_activities_coach_stripe_failure_email_admin(
						'Coach Charge - Final Card Error',
						$coach_array['coach_id'],
						'Coach to update card details. Coach listings have been disabled',
						'stripe_charge_cron',
						'Stripe_CardError',
						$err['type'],
						$err['message'],
						date('d/m/Y - G:i')	
					);
					
					//send coach email to update card details
					malinky_activities_coach_stripe_failure_email_coach(
						'PLAAY Charge - Final Card Error',
						$coach_array['coach_id'],
						'Please check card details.',
						$err['message'],
						'<p>There has been another error during the charging of your card for vouchers downloaded from PLAAY. Your listings have now been disabled. Please login to your account and update your card details. This will automatically activate your listings and attempt to recharge your new card during the next billing cycle.</p>'
					);

					//deactivate listings by removing stripe payment status from user meta
					malinky_activities_coach_stripe_deactivate_listings($coach_array['coach_id']);

				}

			//if its a first time declined charge				
			} else {

				//update stripe_payments with error code
				malinky_activities_charge_failure_card_error(
					$coach_array['voucher_ids'],
					$err['type'],
					$err['code'],
					$err['message']
				);

				//send admin email
				malinky_activities_coach_stripe_failure_email_admin(
					'Coach Charge - Card Error',
					$coach_array['coach_id'],
					'Coach to update card details.',
					'stripe_charge_cron',
					'Stripe_CardError',
					$err['type'],
					$err['message'],
					date('d/m/Y - G:i')	
				);

				//send coach email to update card details
				malinky_activities_coach_stripe_failure_email_coach(
					'PLAAY Charge - Card Error',
					$coach_array['coach_id'],
					'Please check card details.',
					$err['message'],
					'<p>There has been an error during the charging of your card for downloaded vouchers. Please check the details below to guide you in rectifying the issue. We will attempt to charge your card again automatically.</p>'
				);

			}

		} catch (Stripe_InvalidRequestError $e) {

			//Something wrong with the code, incorrect parameters, admin must action. Payments will be retried at later date as no stripe_id is added
			$body = $e->getJsonBody();
			$err = $body['error'];
			error_log(
				'Stripe stripe_charge_cron - ' . 
				'Error Type : ' . $err['type'] . 
				' - Error Message : ' . $err['message'] . 
				' - Stripe Exception : Stripe_InvalidRequestError'
			);

			malinky_activities_coach_stripe_failure_email_admin(
				'Coach Charge - Request Error',
				$coach_array['coach_id'],
				'Urgent action required',
				'stripe_charge_cron',
				'Stripe_InvalidRequestError',
				$err['type'],
				$err['message'],
				date('d/m/Y - G:i')	
			);

			/*THIS BLOCK NEEDS REMOVING USED FOR TESTING AS CANT TEST CARD ERRORS ON PAYMENT PROPERLY*/
			/*---------------------------------------------------------------------------------------*/
			
			/*if (isset($coach_array['stripe_error_created'])) {

				$date_difference = time() - strtotime($coach_array['stripe_error_created']);
				$day_difference = $date_difference / 86400;
				if (floor($day_difference < 7)) {

					malinky_activities_charge_failure_repeated_card_error(
						$coach_array['voucher_ids'],
						$err['type'],
						$err['code'],
						$err['message']
					);

					malinky_activities_coach_stripe_failure_email_admin(
						'Coach Charge - Card Error',
						$coach_array['coach_id'],
						'Coach to update card details',
						'stripe_charge_cron',
						'Stripe_CardError',
						$err['type'],
						$err['message'],
						date('d/m/Y - G:i')	
					);

					malinky_activities_coach_stripe_failure_email_coach(
						'PLAAY Charge - Card Error',
						$coach_array['coach_id'],
						'Please check card details',
						$err['message'],
						'<p>There has been an error during the charging of your card for downloaded vouchers. Please check the details below to guide you in rectifying the issue. We will attempt to charge your card again automatically.</p>'
					);					

				} else {

					malinky_activities_charge_failure_repeated_card_error(
						$coach_array['voucher_ids'],
						$err['type'],
						$err['code'],
						$err['message']
					);

					malinky_activities_coach_stripe_failure_email_admin(
						'Coach Charge - Final Card Error',
						$coach_array['coach_id'],
						'Coach to update card details. Coach listings have been disabled',
						'stripe_charge_cron',
						'Stripe_CardError',
						$err['type'],
						$err['message'],
						date('d/m/Y - G:i')	
					);

					malinky_activities_coach_stripe_failure_email_coach(
						'PLAAY Charge - Final Card Error',
						$coach_array['coach_id'],
						'Please check card details.',
						$err['message'],
						'<p>There has been another error during the charging of your card for vouchers downloaded from PLAAY. Your listings have now been disabled. Please login to your account and update your card details. This will automatically activate your listings and attempt to recharge your new card during the next billing cycle..</p>'
					);

					malinky_activities_coach_stripe_deactivate_listings($coach_array['coach_id']);

				}
				
			} else {

				malinky_activities_charge_failure_card_error(
					$coach_array['voucher_ids'],
					$err['type'],
					$err['code'],
					$err['message']
				);

				malinky_activities_coach_stripe_failure_email_admin(
					'Coach Charge - Card Error',
					$coach_array['coach_id'],
					'Coach to update card details.',
					'stripe_charge_cron',
					'Stripe_CardError',
					$err['type'],
					$err['message'],
					date('d/m/Y - G:i')	
				);

				malinky_activities_coach_stripe_failure_email_coach(
					'PLAAY Charge - Card Error',
					$coach_array['coach_id'],
					'Please check card details.',
					$err['message'],
					'<p>There has been an error during the charging of your card for downloaded vouchers. Please check the details below to guide you in rectifying the issue. We will attempt to charge your card again automatically.</p>'
				);

			}*/
			
			/*THIS ABOVE BLOCK NEEDS REMOVING USED FOR TESTING AS CANT TEST CARD ERRORS ON PAYMENT PROPERLY*/
			/*---------------------------------------------------------------------------------------*/

		} catch (Stripe_AuthenticationError $e) {

			//Something wrong with API keys, admin must action. Payments will be retried at later date as no stripe_id is added
			$body = $e->getJsonBody();
			$err = $body['error'];
			error_log(
				'Stripe stripe_charge_cron - ' . 
				'Error Type : ' . $err['type'] . 
				' - Error Message : ' . $err['message'] . 
				' - Stripe Exception : Stripe_AuthenticationError'
			);

			malinky_activities_coach_stripe_failure_email_admin(
				'Coach Charge - Authetication Error',
				$coach_array['coach_id'],
				'Urgent action required',
				'stripe_charge_cron',
				'Stripe_AuthenticationError',
				$err['type'],
				$err['message'],
				date('d/m/Y - G:i')	
			);

		} catch (Stripe_ApiConnectionError $e) {

			//Something wrong with Stripe API server. Can't action. Payments will be retried at later date as no stripe_id is added
			$body = $e->getJsonBody();
			$err = $body['error'];
			error_log(
				'Stripe stripe_charge_cron - ' . 
				'Error Type : ' . $err['type'] . 
				' - Error Message : ' . $err['message'] . 
				' - Stripe Exception : Stripe_ApiConnectionError'
			);

			malinky_activities_coach_stripe_failure_email_admin(
				'Coach Charge - Stripe Server Error',
				$coach_array['coach_id'],
				'No action required',
				'stripe_charge_cron',
				'Stripe_ApiConnectionError',
				$err['type'],
				$err['message'],
				date('d/m/Y - G:i')	
			);

		} catch (Stripe_ApiError $e) {

			//Something wrong with Stripe API server. Can't action. Payments will be retried at later date as no stripe_id is added
			$body = $e->getJsonBody();
			$err = $body['error'];
			error_log(
				'Stripe stripe_charge_cron - ' . 
				'Error Type : ' . $err['type'] . 
				' - Error Message : ' . $err['message'] . 
				' - Stripe Exception : Stripe_ApiError'
			);

			malinky_activities_coach_stripe_failure_email_admin(
				'Coach Charge - Stripe Server Error',
				$coach_array['coach_id'],
				'No action required',
				'stripe_charge_cron',
				'Stripe_ApiError',
				$err['type'],
				$err['message'],
				date('d/m/Y - G:i')	
			);			
	
		} catch (Stripe_Error $e) {

			//Generic Stripe error but everything else should be caught by now.
			$body = $e->getJsonBody();
			$err = $body['error'];
			error_log(
				'Stripe stripe_charge_cron - ' . 
				'Error Type : ' . $err['type'] . 
				' - Error Message : ' . $err['message'] . 
				' - Stripe Exception : Stripe_Error'
			);

			malinky_activities_coach_stripe_failure_email_admin(
				'Coach Charge - Stripe Other Error',
				$coach_array['coach_id'],
				'Temporary error',
				'stripe_charge_cron',
				'Stripe_Error',
				$err['type'],
				$err['message'],
				date('d/m/Y - G:i')	
			);

		}		

	}
}

/**
 * -----------------------------------------------------------------------------------------
 * FOR THE FIRST CARD FAILURE UPDATE STRIPE PAYMENTS WITH CARD ERROR AGAINST EACH VOUCHER ID
 * @param arr $voucher_ids
 * @param str $card_error_type
 * @param str $card_error_code
 * @param str $card_error_message
 * @return void   
 * -----------------------------------------------------------------------------------------
 */
function malinky_activities_charge_failure_card_error($voucher_ids, $card_error_type, $card_error_code, $card_error_message)
{
	global $wpdb;

	$current_date = date("Y-m-d H:i:s");

	foreach ($voucher_ids as $key => $voucher_id) {

		$update_failed_stripe_payments = $wpdb->update( 
			'malinky_stripe_payments', 
			array( 
				'stripe_error_type' 	=> $card_error_type,
				'stripe_error_code' 	=> $card_error_code,
				'stripe_error_message' 	=> $card_error_message,
				'stripe_error_created' 	=> $current_date
			), 
			array('voucher_id' => $voucher_id), 
			array( 
				'%s',
				'%s',
				'%s',
				'%s'
			), 
			array('%d') 
		);

	}
}

/**
 * ------------------------------------------------------------------------------------------------------------
 * FOR A REPEATED CARD FAILURE UPDATE STRIPE PAYMENTS WITH NEW CARD ERROR MESSAGES BUT LEAVE ERROR CREATED DATE
 * @param arr $voucher_ids
 * @param str $card_error_type
 * @param str $card_error_code
 * @param str $card_error_message
 * @return void   
 * ------------------------------------------------------------------------------------------------------------
 */
function malinky_activities_charge_failure_repeated_card_error($voucher_ids, $card_error_type, $card_error_code, $card_error_message)
{
	global $wpdb;

	foreach ($voucher_ids as $key => $voucher_id) {

		$update_failed_stripe_payments = $wpdb->update( 
			'malinky_stripe_payments', 
			array( 
				'stripe_error_type' 	=> $card_error_type,
				'stripe_error_code' 	=> $card_error_code,
				'stripe_error_message' 	=> $card_error_message,
			), 
			array('voucher_id' => $voucher_id), 
			array( 
				'%s',
				'%s',
				'%s',
			), 
			array('%d') 
		);

	}
}

/**
 * -----------------------------------------------------------------------------------------------------------
 * FOR A FINAL FAILED CHARGE AFTER 7 DAYS DISABLE ALL COACHES LISTINGS BY CHANGING PAYMENT STATUS IN USER META
 * @param arr $coach_array
 * @return void   
 * -----------------------------------------------------------------------------------------------------------
 */
function malinky_activities_coach_stripe_deactivate_listings($coach_id)
{
	update_user_meta($coach_id, '_coach_stripe_payment_status', false);
}

/**
 * ------------------------------------------------------------------------------------
 * REMOVE ALL CARD ERRORS FOR A COACH AS THEY MUST HAVE JUST UPDATED THEIR CARD DETAILS
 * @param int $coach_id
 * @return void   
 * ------------------------------------------------------------------------------------
 */
function malinky_activities_coach_remove_card_errors($coach_id)
{
	global $wpdb;

	$voucher_ids = $wpdb->get_results(
	"SELECT id FROM malinky_voucher_codes WHERE coach_id = $coach_id", ARRAY_A		
	);

	if (isset($voucher_ids)) {

		foreach ($voucher_ids as $key => $voucher_id) {

			$update_stripe_payments = $wpdb->update( 
				'malinky_stripe_payments', 
				array( 
					'stripe_error_type' 	=> '',
					'stripe_error_code' 	=> '',
					'stripe_error_message' 	=> '',
					'stripe_error_created' 	=> ''
				), 
				array('voucher_id' => $voucher_id['id']), 
				array( 
					'%s',
					'%s',
					'%s',
					'%s',
				), 
				array('%d') 
			);

		}

	}
}

/**
 * ------------------------------------------------
 * GET THE EXPIRY DATE FOR THE COACHES CURRENT CARD
 * @param int $coach_id
 * @return string
 * ------------------------------------------------
 */
function malinky_activities_get_card_expiry($coach_id) {
	$coach_stripe_id = get_user_meta($coach_id, '_coach_stripe_id', true);
	$coach_stripe_account = Stripe_Customer::retrieve($coach_stripe_id);
	$coach_default_card = $coach_stripe_account->default_card;
	$coach_default_card = $coach_stripe_account->cards->retrieve($coach_default_card);
	return $coach_default_card->exp_month . '/' . $coach_default_card->exp_year;
}