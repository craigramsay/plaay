<?php

class Malinky_Activities_Custom_Post_Types {

	public function __construct() {
		add_action('init', array($this, 'register_post_types'));
	}

	public function register_post_types() {
		
		$admin_capability = 'manage_activities';

	    /**
		 * Taxonomy - Activity Category
		 */
		$singular  = 'Sport or Activity Type';
		$plural    = 'Sport or Activity Types';

		$rewrite     = array(
			'slug'       	=> 'activity-category',
			'with_front' 	=> false,
			'hierarchical'	=> false
		);

	    $args = array(
	    	'label' 				=> $plural,
	        'labels' => array(
	            'name' 							=> $plural,
	            'singular_name' 				=> $singular,
	            'menu_name'						=> 'Sport or Activity Types',
	           	'all_items' 					=> sprintf('All %s', $plural),
	            'edit_item' 					=> sprintf('Edit %s', $singular),
				'view_item' 					=> sprintf('View %s', $singular),	
	            'update_item' 					=> sprintf('Update %s', $singular),				           	
	            'add_new_item' 					=> sprintf('Add New %s', $singular),
	            'new_item_name' 				=> sprintf('New %s Name',  $singular),
				'parent_item' 					=> sprintf('Parent %s', $singular),
	            'parent_item_colon' 			=> sprintf('Parent %s:', $singular),
	            'search_items' 					=> sprintf('Search %s', $plural),
	            'popular_items' 				=> sprintf('Popular %s', $plural),
	            'separate_items_with_commas' 	=> sprintf('Seperate %s with commas', $plural),
	            'add_or_remove_items' 			=> sprintf('Add or remove %s', $plural),
	            'add_or_remove_items' 			=> sprintf('Choose from the most used %s', $plural),
	            'no_found' 						=> sprintf('No %s found', $plural),   
	    	),
			'public'				=> true,
	        'show_ui' 				=> true,
			'show_in_nav_menus' 	=> false,	        			
	        'hierarchical' 			=> true,
	        'update_count_callback' => '_update_post_term_count',
	        'query_var' 			=> true,	        
	        'rewrite' 				=> $rewrite,
	        'capabilities'			=> array(
	        	'manage_terms' 		=> $admin_capability,
	        	'edit_terms' 		=> $admin_capability,
	        	'delete_terms' 		=> $admin_capability,
	        	'assign_terms' 		=> $admin_capability,
	       ),
	        
	   	);

	   	register_taxonomy('malinky_activities_category', 'activities', $args);

	    /**
		 * Taxonomy - Voucher
		 */
		$singular  = 'Voucher Type';
		$plural    = 'Voucher Types';

		$rewrite     = array(
			'slug'       	=> 'activity-voucher',
			'with_front' 	=> false,
			'hierarchical'	=> false
		);

	    $args = array(
	    	'label' 				=> $plural,
	        'labels' => array(
	            'name' 							=> $plural,
	            'singular_name' 				=> $singular,
	          	'menu_name'						=> 'Voucher Types',
	           	'all_items' 					=> sprintf('All %s', $plural),
	            'edit_item' 					=> sprintf('Edit %s', $singular),
				'view_item' 					=> sprintf('View %s', $singular),	
	            'update_item' 					=> sprintf('Update %s', $singular),				           	
	            'add_new_item' 					=> sprintf('Add New %s', $singular),
	            'new_item_name' 				=> sprintf('New %s Name',  $singular),
				'parent_item' 					=> sprintf('Parent %s', $singular),
	            'parent_item_colon' 			=> sprintf('Parent %s:', $singular),
	            'search_items' 					=> sprintf('Search %s', $plural),
	            'popular_items' 				=> sprintf('Popular %s', $plural),
	            'separate_items_with_commas' 	=> sprintf('Seperate %s with commas', $plural),
	            'add_or_remove_items' 			=> sprintf('Add or remove %s', $plural),
	            'add_or_remove_items' 			=> sprintf('Choose from the most used %s', $plural),
	            'no_found' 						=> sprintf('No %s found', $plural),   
	    	),
			'public'				=> true,
	        'show_ui' 				=> true,
			'show_in_nav_menus' 	=> false,	        			
	        'hierarchical' 			=> true,
	        'update_count_callback' => '_update_post_term_count',
	        'query_var' 			=> true,	        
	        'rewrite' 				=> $rewrite,
	        'capabilities'			=> array(
	        	'manage_terms' 		=> $admin_capability,
	        	'edit_terms' 		=> $admin_capability,
	        	'delete_terms' 		=> $admin_capability,
	        	'assign_terms' 		=> $admin_capability,
	       ),
	        
	   	);

	   	register_taxonomy('malinky_activities_voucher', 'activities', $args);

	    /**
		 * Taxonomy - Age Group
		 */
		$singular  = 'Age Group of Child';
		$plural    = 'Age Group of Child';

		$rewrite     = array(
			'slug'       	=> 'activity-age-group',
			'with_front' 	=> false,
			'hierarchical'	=> false
		);

	    $args = array(
	    	'label' 				=> $plural,
	        'labels' => array(
	            'name' 							=> $plural,
	            'singular_name' 				=> $singular,
	          	'menu_name'						=> 'Age Groups',	            
	           	'all_items' 					=> sprintf('All %s', $plural),
	            'edit_item' 					=> sprintf('Edit %s', $singular),
				'view_item' 					=> sprintf('View %s', $singular),	
	            'update_item' 					=> sprintf('Update %s', $singular),				           	
	            'add_new_item' 					=> sprintf('Add New %s', $singular),
	            'new_item_name' 				=> sprintf('New %s Name',  $singular),
				'parent_item' 					=> sprintf('Parent %s', $singular),
	            'parent_item_colon' 			=> sprintf('Parent %s:', $singular),
	            'search_items' 					=> sprintf('Search %s', $plural),
	            'popular_items' 				=> sprintf('Popular %s', $plural),
	            'separate_items_with_commas' 	=> sprintf('Seperate %s with commas', $plural),
	            'add_or_remove_items' 			=> sprintf('Add or remove %s', $plural),
	            'add_or_remove_items' 			=> sprintf('Choose from the most used %s', $plural),
	            'no_found' 						=> sprintf('No %s found', $plural),   
	    	),
			'public'				=> true,
	        'show_ui' 				=> true,
			'show_in_nav_menus' 	=> false,	        			
	        'hierarchical' 			=> true,
	        'update_count_callback' => '_update_post_term_count',
	        'query_var' 			=> true,	        
	        'rewrite' 				=> $rewrite,
	        'capabilities'			=> array(
	        	'manage_terms' 		=> $admin_capability,
	        	'edit_terms' 		=> $admin_capability,
	        	'delete_terms' 		=> $admin_capability,
	        	'assign_terms' 		=> $admin_capability,
	       ),
	        
	   	);

	   	register_taxonomy('malinky_activities_age_group', 'activities', $args);	   	

	    /**
		 * Taxonomy - County
		 */
		$singular  = 'County';
		$plural    = 'Counties';

		$rewrite     = array(
			'slug'       	=> 'activity-county',
			'with_front' 	=> false,
			'hierarchical'	=> false
		);

	    $args = array(
	    	'label' 				=> $plural,
	        'labels' => array(
	            'name' 							=> $plural,
	            'singular_name' 				=> $singular,
	           	'all_items' 					=> sprintf('All %s', $plural),
	            'edit_item' 					=> sprintf('Edit %s', $singular),
				'view_item' 					=> sprintf('View %s', $singular),	
	            'update_item' 					=> sprintf('Update %s', $singular),				           	
	            'add_new_item' 					=> sprintf('Add New %s', $singular),
	            'new_item_name' 				=> sprintf('New %s Name',  $singular),
				'parent_item' 					=> sprintf('Parent %s', $singular),
	            'parent_item_colon' 			=> sprintf('Parent %s:', $singular),
	            'search_items' 					=> sprintf('Search %s', $plural),
	            'popular_items' 				=> sprintf('Popular %s', $plural),
	            'separate_items_with_commas' 	=> sprintf('Seperate %s with commas', $plural),
	            'add_or_remove_items' 			=> sprintf('Add or remove %s', $plural),
	            'add_or_remove_items' 			=> sprintf('Choose from the most used %s', $plural),
	            'no_found' 						=> sprintf('No %s found', $plural),   
	    	),
			'public'				=> true,
	        'show_ui' 				=> true,
			'show_in_nav_menus' 	=> false,	        			
	        'hierarchical' 			=> true,
	        'update_count_callback' => '_update_post_term_count',
	        'query_var' 			=> true,	        
	        'rewrite' 				=> $rewrite,
	        'capabilities'			=> array(
	        	'manage_terms' 		=> $admin_capability,
	        	'edit_terms' 		=> $admin_capability,
	        	'delete_terms' 		=> $admin_capability,
	        	'assign_terms' 		=> $admin_capability,
	       ),
	        
	   	);

	   	register_taxonomy('malinky_activities_county', 'activities', $args);	

		/**
		 * Taxonomy - Activity Format
		 */
		$singular  = 'Session Format';
		$plural    = 'Session Formats';

		$rewrite     = array(
			'slug'       	=> 'activity-format',
			'with_front' 	=> false,
			'hierarchical'	=> false
		);

	    $args = array(
	    	'label' 				=> $plural,
	        'labels' => array(
	            'name' 							=> $plural,
	            'singular_name' 				=> $singular,
	            'menu_name'						=> 'Session Formats',
	           	'all_items' 					=> sprintf('All %s', $plural),
	            'edit_item' 					=> sprintf('Edit %s', $singular),
				'view_item' 					=> sprintf('View %s', $singular),	
	            'update_item' 					=> sprintf('Update %s', $singular),				           	
	            'add_new_item' 					=> sprintf('Add New %s', $singular),
	            'new_item_name' 				=> sprintf('New %s Name',  $singular),
				'parent_item' 					=> sprintf('Parent %s', $singular),
	            'parent_item_colon' 			=> sprintf('Parent %s:', $singular),
	            'search_items' 					=> sprintf('Search %s', $plural),
	            'popular_items' 				=> sprintf('Popular %s', $plural),
	            'separate_items_with_commas' 	=> sprintf('Seperate %s with commas', $plural),
	            'add_or_remove_items' 			=> sprintf('Add or remove %s', $plural),
	            'add_or_remove_items' 			=> sprintf('Choose from the most used %s', $plural),
	            'no_found' 						=> sprintf('No %s found', $plural),   
	    	),
			'public'				=> true,
	        'show_ui' 				=> true,
			'show_in_nav_menus' 	=> false,	        			
	        'hierarchical' 			=> true,
	        'update_count_callback' => '_update_post_term_count',
	        'query_var' 			=> true,	        
	        'rewrite' 				=> $rewrite,
	        'capabilities'			=> array(
	        	'manage_terms' 		=> $admin_capability,
	        	'edit_terms' 		=> $admin_capability,
	        	'delete_terms' 		=> $admin_capability,
	        	'assign_terms' 		=> $admin_capability,
	       ),
	        
	   	);

	   	register_taxonomy('malinky_activities_format', 'activities', $args);

	    /**
		 * Post types
		 * activities
		 */
		$singular  = 'Activity';
		$plural    = 'Activities';

		$rewrite     = array(
			'slug'       => 'activity',
			'with_front' => false,
			'feeds'      => false,
			'pages'      => false
		);

		$args = array(
			'labels' => array(
				'name' 					=> $plural,
				'singular_name' 		=> $singular,
				'menu_name'             => $plural,
				'all_items'             => sprintf('All %s', $plural),
				'add_new' 				=> 'Add New',
				'add_new_item' 			=> sprintf('Add %s', $singular),
				'edit' 					=> 'Edit',
				'edit_item' 			=> sprintf('Edit %s', $singular),
				'new_item' 				=> sprintf('New %s', $singular),
				'view' 					=> sprintf('View %s', $singular),
				'view_item' 			=> sprintf('View %s', $singular),
				'search_items' 			=> sprintf('Search %s', $plural),
				'not_found' 			=> sprintf('No %s found', $plural),
				'not_found_in_trash' 	=> sprintf('No %s found in trash', $plural),
				'parent' 				=> sprintf('Parent %s', $singular)
			),
			'description' => 'This is where you can create and manage' . $plural . '.',
			'public' 				=> true,
			'exclude_from_search' 	=> false,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'show_in_nav_menus' 	=> false,
			'show_in_menu'			=> true,
			'show_in_admin_bar'		=> true,
			'menu_icon'				=> 'dashicons-video-alt3',
			'capability_type' 		=> 'post',
			'capabilities' => array(
				'edit_post' 			=> $admin_capability,
				'read_post' 			=> $admin_capability,
				'delete_post' 			=> $admin_capability,
				'edit_posts' 			=> $admin_capability,
				'edit_others_posts' 	=> $admin_capability,
				'publish_posts' 		=> $admin_capability,
				'read_private_posts'	=> $admin_capability,						
			),
			'hierarchical' 			=> false,
			'supports' 				=> array('title', 'custom-fields', 'author'),
			'has_archive' 			=> false,
			'rewrite' 				=> $rewrite,
			'query_var' 			=> true,
			'can_export'			=> true
		);
		
		register_post_type('activities', $args);

	    /**
		 * Post types
		 * activity_emails
		 */
		$admin_capability = 'manage_activity_emails';

		$singular  = 'Activity Email';
		$plural    = 'Activity Emails';

		$rewrite     = array(
			'slug'       => 'activity-email',
			'with_front' => false,
			'feeds'      => false,
			'pages'      => false
		);

		$args = array(
			'labels' => array(
				'name' 					=> $plural,
				'singular_name' 		=> $singular,
				'menu_name'             => $plural,
				'all_items'             => sprintf('All %s', $plural),
				'add_new' 				=> 'Add New',
				'add_new_item' 			=> sprintf('Add %s', $singular),
				'edit' 					=> 'Edit',
				'edit_item' 			=> sprintf('Edit %s', $singular),
				'new_item' 				=> sprintf('New %s', $singular),
				'view' 					=> sprintf('View %s', $singular),
				'view_item' 			=> sprintf('View %s', $singular),
				'search_items' 			=> sprintf('Search %s', $plural),
				'not_found' 			=> sprintf('No %s found', $plural),
				'not_found_in_trash' 	=> sprintf('No %s found in trash', $plural),
				'parent' 				=> sprintf('Parent %s', $singular)
			),
			'description' => 'This is where you can create and manage' . $plural . '.',
			'public' 				=> true,
			'exclude_from_search' 	=> false,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'show_in_nav_menus' 	=> false,
			'show_in_menu'			=> true,
			'show_in_admin_bar'		=> true,
			'menu_icon'				=> 'dashicons-email-alt',
			'capability_type' 		=> 'post',
			'capabilities' => array(
				'edit_post' 			=> $admin_capability,
				'read_post' 			=> $admin_capability,
				'delete_post' 			=> $admin_capability,
				'edit_posts' 			=> $admin_capability,
				'edit_others_posts' 	=> $admin_capability,
				'publish_posts' 		=> $admin_capability,
				'read_private_posts'	=> $admin_capability,						
			),
			'hierarchical' 			=> false,
			'has_archive' 			=> false,
			'rewrite' 				=> $rewrite,
			'query_var' 			=> true,
			'can_export'			=> true
		);
		
		register_post_type('activity_emails', $args);

		/**
		 * Post types
		 * coach_emails
		 */
		$admin_capability = 'manage_activity_emails';

		$singular  = 'Coach Email';
		$plural    = 'Coach Emails';

		$rewrite     = array(
			'slug'       => 'coach-email',
			'with_front' => false,
			'feeds'      => false,
			'pages'      => false
		);

		$args = array(
			'labels' => array(
				'name' 					=> $plural,
				'singular_name' 		=> $singular,
				'menu_name'             => $plural,
				'all_items'             => sprintf('All %s', $plural),
				'add_new' 				=> 'Add New',
				'add_new_item' 			=> sprintf('Add %s', $singular),
				'edit' 					=> 'Edit',
				'edit_item' 			=> sprintf('Edit %s', $singular),
				'new_item' 				=> sprintf('New %s', $singular),
				'view' 					=> sprintf('View %s', $singular),
				'view_item' 			=> sprintf('View %s', $singular),
				'search_items' 			=> sprintf('Search %s', $plural),
				'not_found' 			=> sprintf('No %s found', $plural),
				'not_found_in_trash' 	=> sprintf('No %s found in trash', $plural),
				'parent' 				=> sprintf('Parent %s', $singular)
			),
			'description' => 'This is where you can create and manage' . $plural . '.',
			'public' 				=> true,
			'exclude_from_search' 	=> false,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'show_in_nav_menus' 	=> false,
			'show_in_menu'			=> true,
			'show_in_admin_bar'		=> true,
			'menu_icon'				=> 'dashicons-email-alt',
			'capability_type' 		=> 'post',
			'capabilities' => array(
				'edit_post' 			=> $admin_capability,
				'read_post' 			=> $admin_capability,
				'delete_post' 			=> $admin_capability,
				'edit_posts' 			=> $admin_capability,
				'edit_others_posts' 	=> $admin_capability,
				'publish_posts' 		=> $admin_capability,
				'read_private_posts'	=> $admin_capability,						
			),
			'hierarchical' 			=> false,
			'has_archive' 			=> false,
			'rewrite' 				=> $rewrite,
			'query_var' 			=> true,
			'can_export'			=> true
		);
		
		register_post_type('coach_emails', $args);		

	}

}