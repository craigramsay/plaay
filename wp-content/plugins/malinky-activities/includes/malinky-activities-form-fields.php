<?php
/**
 * -------------------------------------
 * GENERATE THE HTML FOR EACH FORM FIELD
 * @param string $template_name
 * @param array $args
 * @return void
 * -------------------------------------
 */
function malinky_activities_get_form_field_template($template_name, $args = array()) {
	if ($args && is_array($args))
		extract($args);

	include(MALINKY_ACTIVITIES_PLUGIN_DIR . 'form-fields/' . $template_name);
}

/**
 * ------------------------------------------
 * GENERATE COACH CREATE ACTIVITY FORM FIELDS
 * @return array
 * ------------------------------------------
 */
function malinky_activities_set_activity_form_fields()
{
	$form_fields = array(
		'activity_details' => array(
			'activity_name' => array(
				'type'				=> 'text',
				'label'       		=> 'Activity Name',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'text_field'
			),			
			'activity_format' => array(
				'type'				=> 'dropdown',
				'label'       		=> 'Session Format',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('please_choose', 'county'),
				'placeholder' 		=> '',
				'description'		=> 'Choose one session format. You can add multiple activities each with a different format if required.',
				'options'			=> get_activity_terms('malinky_activities_format'),
				'sanitize_type'		=> 'text_field'
			),
			'activity_age_group' => array(
				'type'				=> 'checkbox-multi',
				'label'       		=> 'Age Group of Child',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('checkbox'),
				'placeholder' 		=> '',
				'description'		=> 'Choose one or multiple child age groups that this activity applies to.',
				'options'			=> get_activity_terms('malinky_activities_age_group', false),
				'post_return_array'	=> true,
				'sanitize_type'		=> 'text_field'
			),							
			'activity_category' => array(
				'type'				=> 'checkbox-multi',
				'label'       		=> 'Sport or Activity Type',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('checkbox'),
				'placeholder' 		=> '',
				'description'		=> 'Choose one or multiple sport or activity types that this activity applies to.',
				'options'			=> get_activity_terms('malinky_activities_category'),
				'post_return_array'	=> true,
				'sanitize_type'		=> 'text_field'
			),
			'activity_voucher' => array(
				'type'				=> 'checkbox-multi',
				'label'       		=> 'Voucher Type',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('checkbox'),
				'placeholder' 		=> '',
				'description'		=> 'Choose one or multiple vouchers that are available for this activity.',
				'options'			=> get_activity_terms('malinky_activities_voucher'),
				'post_return_array'	=> true,
				'sanitize_type'		=> 'text_field'
			),		
			'activity_description' => array(
				'type'				=> 'editor',
				'label'       		=> 'Description',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'text_area_field'
			),
			'activity_additional_information' => array(
				'type'				=> 'editor',
				'label'       		=> 'Additional Information',
				'label_type'		=> '(optional)',
				'validation_rules' 	=> array(),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'text_area_field'
			),
		),
		'activity_location' => array(
			'activity_distance_travelled' => array(
				'type'				=> 'dropdown',
				'label'       		=> 'Distance Travelled',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('please_choose', 'integer'),
				'placeholder' 		=> '',	
				'options'			=> array('0' => 'Not Applicable', '5' => '5 Miles', '10' => '10 Miles', '15' => '15 Miles', '20' => '20 Miles', '25' => '25 Miles', '30' => '30 Miles', '40' => '40 Miles', '50' => '50 Miles', '100' => '100 Miles'),
				'sanitize_type'		=> 'text_field'
			),								
			'activity_address' => array(
				'type'				=> 'text',
				'label'       		=> 'Address',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'text_field'
			),
			'activity_town' => array(
				'type'				=> 'text',
				'label'       		=> 'Town',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'text_field'
			),
			'activity_county' => array(
				'type'				=> 'dropdown',
				'label'       		=> 'County',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('please_choose', 'county'),
				'placeholder' 		=> '',
				'options'			=> get_activity_terms('malinky_activities_county'),
				'sanitize_type'		=> 'text_field'
			),				
			'activity_postcode' => array(
				'type'				=> 'text',
				'label'       		=> 'Postcode',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'postcode'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'text_field'
			),
		),
		'activity_publish' => array(	
			'activity_active' => array(
				'type'				=> 'checkbox',
				'label'       		=> 'Active',
				'label_type'		=> '(optional)',
				'validation_rules' 	=> array(),	
				'placeholder' 		=> '',
				'description'		=> '',
				'sanitize_type'		=> 'text_field'
			)	
		)

	);
	return $form_fields;
}

/**
 * ----------------------------------------
 * GENERATE VOUCHER APPLICATION FORM FIELDS
 * @param int $activity_id
 * @return array
 * ---------------------------------------- 
 */
function malinky_activities_set_voucher_application_form_fields($activity_id)
{
	$form_fields = array(
		'personal_details' => array(
			'parent_name' => array(
				'type'				=> 'text',
				'label'       		=> 'Name',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'text_field'
			),
			'parent_county' => array(
				'type'				=> 'dropdown',
				'label'       		=> 'County',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('please_choose', 'county'),
				'placeholder' 		=> '',
				'options'			=> malinky_register_get_counties(),
				'sanitize_type'		=> 'text_field'
			),
			'parent_email' => array(
				'type'				=> 'text',
				'label'       		=> 'Email',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'email'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'email_field'
			),				
			'parent_phone' => array(
				'type'				=> 'text',
				'label'       		=> 'Phone',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'phone'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'text_field'
			),
		),
		'voucher_options' => array(
			'voucher_application_category' => array(
				'type'				=> 'checkbox-multi',
				'label'       		=> 'Sport or Activity Type',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('checkbox_max_1'),
				'placeholder' 		=> '',
				'options'			=> get_post_activity_terms($activity_id, 'malinky_activities_category', 'name'),
				'post_return_array'	=> true,
				'sanitize_type'		=> 'text_field'
			),
			'voucher_application_voucher' => array(
				'type'				=> 'checkbox-multi',
				'label'       		=> 'Voucher Type',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('checkbox_max_1'),
				'placeholder' 		=> '',
				'options'			=> get_post_activity_terms($activity_id, 'malinky_activities_voucher', 'name'),
				'post_return_array'	=> true,
				'sanitize_type'		=> 'text_field'
			),			
			'voucher_application_age_group' => array(
				'type'				=> 'checkbox-multi',
				'label'       		=> 'Age Group of Child',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('checkbox_max_1'),
				'placeholder' 		=> '',
				'options'			=> get_post_activity_terms($activity_id, 'malinky_activities_age_group', 'name'),
				'post_return_array'	=> true,
				'sanitize_type'		=> 'text_field'
			),
		),
		'child_information' => array(				
			'child_birth_month' => array(
				'type'				=> 'dropdown',
				'label'       		=> 'Child Birth Month',
				'label_type'		=> '(optional)',
				'validation_rules' 	=> array('integer'),
				'placeholder' 		=> '',
				'options'			=> array('1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April', '5' => 'May', '6' => 'June', '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'), 
				'sanitize_type'		=> 'text_field'
			)
		),
		'terms' => array(				
			'terms_and_conditions' => array(
				'type'				=> 'checkbox',
				'label'       		=> 'Terms &amp; Conditions',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('checkbox'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'text_field'
			),
			'marketing_accept' => array(
				'type'				=> 'checkbox',
				'label'       		=> 'PLAAY Benefits',
				'label_type'		=> '(optional)',
				'validation_rules' 	=> '',
				'placeholder' 		=> '',
				'description'		=> 'Tick the box above to unsubscribe',
				'sanitize_type'		=> 'text_field'
			)			
		)

	);
	return $form_fields;
}

/**
 * ---------------------------------
 * GENERATE VIEW VOUCHER FORM FIELDS
 * @return array  
 * ---------------------------------
 */
function malinky_activities_set_email_voucher_form_fields()
{
	$form_fields = array(
		'parent_details' => array(
			'parent_email' => array(
				'type'				=> 'text',	
				'label'       		=> 'Email',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'email'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'email_field'
			)
		)

	);
	return $form_fields;
}

/**
 * --------------------------------------
 * GENERATE EMAIL UNSUBSCRIBE FORM FIELDS
 * @return array  
 * --------------------------------------
 */
function malinky_activities_set_email_unsubscribe_form_fields()
{
	$form_fields = array(
		'parent_details' => array(
			'parent_email' => array(
				'type'				=> 'text',
				'label'       		=> 'Email',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'email'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'email_field'
			)
		)

	);
	return $form_fields;
}

/**
 * --------------------------------------
 * GENERATE EMAIL UNSUBSCRIBE FORM FIELDS
 * @return array  
 * --------------------------------------
 */
function malinky_activities_set_coach_logo_form_fields()
{
	$form_fields = array(
		'coach_details' => array(
			'coach_logo' => array(
				'type'				=> 'file',
				'label'       		=> 'Logo',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('file_image'),
				'placeholder' 		=> '',
				'sanitize_type'		=> 'file_field',
				'description'		=> 'File type .jpg, .gif, .png. Max file size 2MB.'
			)
		)

	);
	return $form_fields;
}