<?php
/**
 * --------------------------------
 * VOUCHER APPLICATION PROCESS FORM
 * @param int $activity_id
 * @return TODO 
 * --------------------------------
 */
function malinky_activities_voucher_application($activity_id)
{
	$errors = new WP_Error();

	//check form submit and nonce
	if ((empty($_POST['submit_voucher_application'])) || (!isset($_POST['malinky_voucher_application_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_voucher_application_form_nonce'], 'malinky_voucher_application_form_nonce_action')))
		wp_die(__('Security Error'));

	$values = malinky_activities_get_posted_form_fields('malinky_activities_set_voucher_application_form_fields', true);
	$db_values = malinky_activities_posted_form_fields_values($values);

	global $wpdb;

	//opposite set of marketing
	$db_values['marketing_accept'] = $db_values['marketing_accept'] == '' ? 1 : 0;

	$parent_exists = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT email FROM malinky_parents WHERE email = %s", $db_values['parent_email']
		)
	);
	
	if (isset($parent_exists)) {
		$parent_updated = $wpdb->update( 
			'malinky_parents', 
			array( 
				'name' 					=> $db_values['parent_name'],
				'county' 				=> $db_values['parent_county'],
				'phone' 				=> $db_values['parent_phone'],
				'child_birth_month'		=> $db_values['child_birth_month'],
				'terms_and_conditions' 	=> $db_values['terms_and_conditions'],
				'marketing_accept'	 	=> $db_values['marketing_accept']
			), 
			array('email' => $db_values['parent_email']), 
			array( 
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%d',
			), 
			array('%s') 
		);
		//use false as values could remain the same, multiple vouchers for a parent
		//if checked for return value of num rows count would return 0 in the above case
		if ($parent_updated === false) {
			$errors->add('parent_voucher_application_error', 'You have previously applied for a voucher and there has been a problem updating your contact details. Please try again.');
			return $errors;
		}
	} else {
		$wpdb->insert(
			'malinky_parents',
			array(
				'name' 					=> $db_values['parent_name'],
				'county' 				=> $db_values['parent_county'],
				'email' 				=> $db_values['parent_email'],
				'phone' 				=> $db_values['parent_phone'],
				'child_birth_month'		=> $db_values['child_birth_month'],
				'terms_and_conditions' 	=> $db_values['terms_and_conditions'],
				'marketing_accept' 		=> $db_values['marketing_accept']
			),
			array(
				'%s',
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%d'
			)
		);
		if (!$wpdb->insert_id) {
			$errors->add('parent_voucher_application_error', 'There was a problem adding your details. Please try again.');
			return $errors;
		}
	}

	$parent_id = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT id FROM malinky_parents WHERE email = %s", $db_values['parent_email']
		)			
	);

	$coach_id = get_post($activity_id);
	$coach_id = $coach_id->post_author;

	if (isset($parent_id)) {
		$wpdb->insert(
			'malinky_voucher_codes',
			array(
				'coach_id' 							=> $coach_id,
				'parent_id' 						=> $parent_id,
				'parent_activation_code'  			=> malinky_activities_generate_activation_code(),
				'parent_activated'					=> false,
				'activity_id'						=> $activity_id,
				'voucher_cost'						=> malinky_activities_set_voucher_cost(get_single_term_id($activity_id, 'malinky_activities_format')),
				'voucher_details_activity_name' 	=> get_the_title($activity_id),
				'voucher_details_format_term' 		=> get_single_term_name_by_post_id($activity_id, 'malinky_activities_format'),
				'voucher_details_category_term'		=> $db_values['voucher_application_category'][0],
				'voucher_details_voucher_term'		=> $db_values['voucher_application_voucher'][0],
				'voucher_details_age_group_term'	=> $db_values['voucher_application_age_group'][0]
			),
			array(
				'%d',
				'%d',
				'%s',
				'%d',
				'%d',
				'%f',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s'
			)
		);
	} else {
		$errors->add('parent_voucher_application_error', 'There was a problem finding your details. Please try again.');	
		return $errors;	
	}

	if (!$wpdb->insert_id) {
		$errors->add('parent_voucher_application_error', 'There was a problem adding your details. Please try again.');
		return $errors;
	} else {
		malinky_activities_parent_activation_email($db_values['parent_email'], $db_values['parent_name'], $wpdb->insert_id);
		wp_safe_redirect('thanks?message="successful_voucher_application"');
	}
}

/**
 * ------------------------
 * GENERATE ACTIVATION CODE
 * @return string  
 * ------------------------
 */
function malinky_activities_generate_activation_code()
{
    mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
    $charid = strtoupper(md5(uniqid(rand(), true)));
    $hyphen = chr(45);// "-"
    $uuid = substr($charid, 0, 8) . $hyphen
        	. substr($charid, 8, 4) . $hyphen
        	. substr($charid,12, 4) . $hyphen
        	. substr($charid,16, 4) . $hyphen
        	. substr($charid,20,12);
    return $uuid;
}

/**
 * -------------------------------------
 * ACTIVATE PARENT
 * @param int $activity_id
 * @param string $parent_activation_code 
 * @return bool|void  
 * -------------------------------------
 */
function malinky_activities_activate_parent($activity_id, $parent_activation_code)
{
	global $wpdb;
	$errors = new WP_Error();

	$is_parent_activated = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT parent_activated FROM malinky_voucher_codes WHERE id = %d", $activity_id
		)			
	);

	if ($is_parent_activated)
		return true;

	$parent_activated = $wpdb->update( 
		'malinky_voucher_codes',
		array(
			'parent_activated'			=> true,
			'parent_activated_date'		=> current_time('mysql', 1),
		), 
		array('id' => $activity_id, 'parent_activation_code' => $parent_activation_code), 
		array( 
			'%d',
			'%s',
			'%d'
		), 
		array('%d', '%s') 
	);

	if ($parent_activated === false || $parent_activated == 0) {
		wp_safe_redirect('thanks?message="unsuccessful_activate_visitor"');
		return false;
	} else {
		return true;
	}
}

/**
 * --------------------------------------------
 * SET VOUCHER CODE AND VOUCHER ACTIVATION CODE
 * @param int $activity_id
 * @param string $parent_activation_code 
 * @return bool   
 * --------------------------------------------
 */
function malinky_activities_set_voucher_code($activity_id, $parent_activation_code)
{
	global $wpdb;

	$is_set_voucher_activation_code = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT voucher_activation_code FROM malinky_voucher_codes WHERE id = %d AND parent_activation_code = %s", $activity_id, $parent_activation_code
		)			
	);
	
	if (isset($is_set_voucher_activation_code))
		return true;

	$coach_id = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT coach_id FROM malinky_voucher_codes WHERE id = %d", $activity_id
		)			
	);

	$voucher_code = $wpdb->update( 
		'malinky_voucher_codes',
		array(
			'voucher_activation_code'	=> malinky_activities_generate_activation_code(),
			'voucher_code_activated'	=> false,
			'voucher_code'				=> malinky_activities_generate_voucher_code($coach_id)
		), 
		array('id' => $activity_id, 'parent_activation_code' => $parent_activation_code), 
		array( 
			'%s',
			'%d',
			'%s'
		), 
		array('%d', '%s') 
	);

	if ($voucher_code === false || $voucher_code == 0) {
		wp_safe_redirect('thanks?message="unsuccessful_create_voucher_code"');	
		return false;	
	} else {
		return true;
	}
}

/**
 * ---------------------
 * GENERATE VOUCHER CODE
 * @param int $coach_id 
 * @return string   
 * ---------------------
 */
function malinky_activities_generate_voucher_code($coach_id)
{
	$voucher_code_prefix = malinky_activities_generate_voucher_code_prefix($coach_id);
	$voucher_code_suffix = malinky_activities_generate_voucher_code_suffix($coach_id);
	return $coach_id . '-' . $voucher_code_prefix . $voucher_code_suffix;
}

/**
 * --------------------------------------------------------------
 * GET THE COACH COMPANY NAME TO GENERATE THE VOUCHER CODE PREFIX
 * @param int $coach_id 
 * @return string 
 * --------------------------------------------------------------
 */
function malinky_activities_generate_voucher_code_prefix($coach_id)
{
	$company_name = get_user_meta($coach_id, '_coach_company_name', true);
	$company_name = preg_replace('/\s+/', '', $company_name);
	return strtoupper(substr($company_name, 0, 6));
}

/**
 * --------------------------------------------------------------------------------
 * GET THE MOST RECENT VOUCHER CODE GENERATED FOR A COACH AND RETURN THE NEW SUFFIX
 * @param int $coach_id 
 * @return string 
 * --------------------------------------------------------------------------------
 */
function malinky_activities_generate_voucher_code_suffix($coach_id)
{
	global $wpdb;

	$recent_voucher_codes = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT voucher_code FROM malinky_voucher_codes WHERE coach_id = %d AND voucher_activation_code != 'NULL' ORDER BY parent_activated_date DESC LIMIT 1", $coach_id
		)				
	);
	
	if (!$recent_voucher_codes) {
		return '000001'; //exit();
	} else {
		$existing_voucher_code_suffix = substr($recent_voucher_codes, -6, 6);
		return str_pad(++$existing_voucher_code_suffix, 6, '0', STR_PAD_LEFT); //exit();
	}
}

/**
 * -------------------------------------------------
 * VALIDATE ACTIVATION CODE WITH VOUCHER ID APPENDED
 * @param string $activation_code 
 * @return array
 * -------------------------------------------------
 */
function malinky_activities_validate_activation_code_id($activation_code)
{
	$voucher_id = explode('-', $activation_code);
	$voucher_id = end($voucher_id);
	$activation_code = explode('-', $activation_code, -1);
	$activation_code = implode('-', $activation_code);
	
	if (!is_numeric($voucher_id)) {
		wp_safe_redirect('thanks?message="unsuccessful_validate_activation_code"');	
	}

	switch(true) {
		case (strlen($activation_code) != 36):
			wp_safe_redirect('thanks?message="unsuccessful_validate_activation_code"');	
			break;
		case (substr($activation_code, 8, 1) != '-'):
			wp_safe_redirect('thanks?message="unsuccessful_validate_activation_code"');	
			break;	
		case (substr($activation_code, 13, 1) != '-'):
			wp_safe_redirect('thanks?message="unsuccessful_validate_activation_code"');	
			break;	
		case (substr($activation_code, 18, 1) != '-'):
			wp_safe_redirect('thanks?message="unsuccessful_validate_activation_code"');	
			break;	
		case (substr($activation_code, 23, 1) != '-'):
			wp_safe_redirect('thanks?message="unsuccessful_validate_activation_code"');	
			break;											
	}

	$exploded_activation_code['voucher_id'] = $voucher_id;
	$exploded_activation_code['activation_code'] = $activation_code;

	return $exploded_activation_code;
}

/**
 * ---------------------------
 * GET VOUCHER ACTIVATION CODE
 * @param int $voucher_id 
 * @param bool $echo 
 * @return string 
 * ---------------------------
 */
function malinky_activities_get_voucher_activation_code($voucher_id, $echo = false)
{
	global $wpdb;

	$voucher_activation_code = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT voucher_activation_code FROM malinky_voucher_codes WHERE id = %d", $voucher_id
		)			
	);

	if (!$voucher_activation_code)
		wp_die('Fatal Error');

	if ($echo) {
		echo $voucher_activation_code;
		return;
	}

	return $voucher_activation_code;
}

/**
 * --------------------------------------
 * ACTIVATE THE VOUCHER ONE TIME ONLY
 * @param int $voucher_id 
 * @param string $voucher_activation_code 
 * @return TODO  
 * --------------------------------------
 */
function malinky_activities_activate_voucher($voucher_id, $voucher_activation_code)
{
	$errors = new WP_Error();

	//check form submit and nonce
	if ((empty($_POST['submit_voucher'])) || (!isset($_POST['malinky_voucher_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_voucher_form_nonce'], 'malinky_voucher_form_nonce_action')))
		wp_die(__('Security Error'));

	$parent_email = sanitize_email($_POST['parent_email']);

	global $wpdb;

	$parent_id = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT id FROM malinky_parents WHERE email = %s", $parent_email
		)			
	);

	$parent_id_voucher = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT parent_id FROM malinky_voucher_codes WHERE id = %d AND voucher_activation_code = '%s'", $voucher_id, $voucher_activation_code
		)			
	);	

	$is_voucher_code_activated = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT voucher_code_activated FROM malinky_voucher_codes WHERE id = %d AND voucher_activation_code = '%s'", $voucher_id, $voucher_activation_code
		)			
	);	


	if( (isset($parent_id) && isset($parent_id_voucher)) && ($parent_id == $parent_id_voucher) )  {

		if (!$is_voucher_code_activated) {
			$activate_voucher_code = $wpdb->update( 
				'malinky_voucher_codes',
				array(
					'voucher_code_activated'		=> true,
					'voucher_code_activated_date'	=> current_time('mysql', 1),			
				), 
				array('id' => $voucher_id, 'voucher_activation_code' => $voucher_activation_code, 'parent_id' => $parent_id), 
				array( 
					'%d',
					'%s',
				), 
				array('%d', '%s', '%d') 
			);

			if ($activate_voucher_code === false || $activate_voucher_code == 0) {
				$errors->add('voucher_code_activation_error', 'There has been a problem activating your voucher. Please try again.');
				return $errors;
			} else {
				//add voucher_id into Stripe payments table, if no insert stop execution
				if (!malinky_activities_set_voucher_for_stripe_payment($voucher_id)) {
					//unset the activation above as parent needs to re enter details to make sure Stripe payments table is updated
					$activate_voucher_code = $wpdb->update( 
						'malinky_voucher_codes',
						array(
							'voucher_code_activated' 		=> false,
							'voucher_code_activated_date' 	=> ''
						), 
						array('id' => $voucher_id, 'voucher_activation_code' => $voucher_activation_code, 'parent_id' => $parent_id), 
						array( 
							'%d',
						), 
						array('%d', '%s', '%d') 
					);
					$errors->add('voucher_code_activation_error', 'There has been a problem activating your voucher. Please try again.');
					return $errors;
				}
				//first time success email the coach
				malinky_activities_voucher_coach_email($voucher_id);
				return $voucher_id;
			}
		} else {
			return $voucher_id;
		}

	} else {
		$errors->add('voucher_code_activation_error', 'The email address you have entered is incorrect for this voucher. Please try again.');
		return $errors;
	}
}

/**
 * --------------------------------------------
 * SET VOUCHER CODE ID IN STRIPE PAYMENTS TABLE
 * @param int $voucher_id 
 * @return bool   
 * --------------------------------------------
 */
function malinky_activities_set_voucher_for_stripe_payment($voucher_id)
{
	global $wpdb;

	if(!is_numeric($voucher_id))
		return false;

	$voucher_id_exists = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT voucher_id FROM malinky_stripe_payments WHERE voucher_id = %d", $voucher_id
		)			
	);	

	if (!isset($voucher_id_exists)) {

		$wpdb->insert(
			'malinky_stripe_payments',
			array(
				'voucher_id' => $voucher_id
			),
			array(
				'%d'
			)
		);
		if (!$wpdb->insert_id) {
			return false;
		}
	}
	return true;
}

/**
 * ------------------------------------------
 * SET VOUCHER COST FOR DB FROM SETTINGS
 * @param int $voucher_details_format_term_id 
 * @return int   
 * ------------------------------------------
 */
function malinky_activities_set_voucher_cost($voucher_details_format_term_id)
{		
	$voucher_format = get_term($voucher_details_format_term_id, 'malinky_activities_format');
	$voucher_format_cost = get_option('malinky_activities_format_settings_options');
	$voucher_cost = $voucher_format_cost['settings_format_' . str_replace('-', '_', $voucher_format->slug)];
	return $voucher_cost;
}

/**
 * ----------------
 * GENERATE VOUCHER
 * @param string $voucher_code 
 * @return void   
 * ----------------
 */
function malinky_activities_generate_voucher($voucher_id, $output = 'string')
{
	global $wpdb;

	$voucher = $wpdb->get_row(
		$wpdb->prepare(
		"SELECT * FROM malinky_voucher_codes WHERE id = %d AND voucher_code_activated = TRUE", $voucher_id
		)			
	);

	if (!isset($voucher)) {
		//throw new Exception('There has been a problem accessing the voucher information. Please try again.');
	}

	$parent = $wpdb->get_row(
		$wpdb->prepare(
		"SELECT * FROM malinky_parents WHERE id = %d", $voucher->parent_id
		)			
	);	

	if (!isset($parent)) {
		//throw new Exception('There has been a problem accessing the voucher information. Please try again.');
	}

	$coach = get_userdata($voucher->coach_id);

	if (!isset($coach)) {
		//throw new Exception('There has been a problem accessing the voucher information. Please try again.');
	}

	$pdf = new FPDF();
	$pdf->AddPage();

	$pdf->SetDrawColor(229, 229, 229);
	$pdf->SetLineWidth(0.5);

	$pdf->Image(get_template_directory_uri() . '/img/logo_dark_voucher.jpg',10,10,-300);

	$pdf->SetXY(10, 12);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(190, 10, $voucher->voucher_code, 0 , 1, 'R');

	$pdf->Line(10, 24, 200, 24);

	$pdf->SetXY(10, 26);

	$pdf->SetFontSize(11);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(25, 10, 'Activity:');
	$pdf->SetFont('');
	$pdf->Cell(30, 10, $voucher->voucher_details_activity_name);
	$pdf->Ln(6);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(25, 10, 'Code:');
	$pdf->SetFont('');
	$pdf->Cell(30, 10, $voucher->voucher_code);
	$pdf->Ln(6);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(25, 10, 'Format:');	
	$pdf->SetFont('');
	$pdf->Cell(30, 10, $voucher->voucher_details_format_term);
	$pdf->Ln(6);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(25, 10, 'Age Group:');	
	$pdf->SetFont('');	
	$pdf->Cell(30, 10, $voucher->voucher_details_age_group_term);
	$pdf->Ln(6);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(25, 10, 'Type:');	
	$pdf->SetFont('');	
	$pdf->Cell(30, 10, $voucher->voucher_details_category_term);
	$pdf->Ln(6);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(25, 10, 'Voucher:');	
	$pdf->SetFont('');	
	$pdf->Cell(30, 10, $voucher->voucher_details_voucher_term);
	$pdf->Ln(6);

	$pdf->SetXY(10, 66);

	$pdf->SetFont('Arial', 'B', 13);
	$pdf->Cell(30, 10, 'Voucher Holder');
	$pdf->Ln();

	$pdf->SetFontSize(11);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, 'Name:');	
	$pdf->SetFont('');	
	$pdf->Cell(40, 10, $parent->name);
	$pdf->Ln(6);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, 'Email:');	
	$pdf->SetFont('');	
	$pdf->Cell(40, 10, $parent->email);
	$pdf->Ln(6);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, 'Phone:');	
	$pdf->SetFont('');	
	$pdf->Cell(40, 10, $parent->phone);
	$pdf->Ln(6);

	$pdf->Line(10, 100, 200, 100);

	$pdf->SetXY(10, 100);

	$pdf->SetFont('Arial', 'B', 8);
	$pdf->Cell(0, 10, 'Terms and Conditions:');	
	$pdf->Ln(10);			

	$pdf->SetFont('Arial', '', 8);
	$pdf->Write(4, "1. All people use the plaay.co.uk website at their own discretion, the promoter gives no guarantee of quality of provision of activity providers but will remove providers if they deem them as unsuitable for the website\n2. The voucher entitles one child to the offer as detailed on the voucher at the participating activity provider listed on the website. Other individual terms and conditions may apply to specific activity providers, please refer to the individual terms and conditions listed under each activity provider on their website after downloading the voucher for full details. It is always advisable to reconfirm these with your chosen activity provider before setting out from home.\n3. All lessons must be pre-booked by telephone, email or acceptable method of communication as deemed by the activity provider\n4. This offer is only open to children not already regularly attending the classes of the chosen activity provider\n5. Some individuals may be required (or may wish) to take out their own private insurance cover for certain activities and some activities may require a certain level of fitness or ability. Please check with the activity provider in advance.\n6. This offer is obtained upon presentation of the voucher at the venue.\n7. Offer valid until the date on the voucher, subject to the activity providers normal schedule, unless otherwise stated. Please check times prior to booking. \n8. The offer may not be valid on Bank Holiday weekends or during special events unless at the discretion of the activity provider.\n9. The offer may not be used in conjunction with any other discount voucher, offer or concessionary rate offered by the activity provider.\n10. All information and prices detailed on the plaay.co.uk website are subject to change throughout the promotion and are correct at time of going live.\n11. The activity provider reserves the right not to redeem or accept photocopied, damaged, forged or defaced vouchers and reserve the right to refuse admission where these conditions have not been adhered to. Only one free lesson is permitted per activity provider, per visit and is subject to availability.\n12. The promoter will make no reimbursement for unused vouchers.\n13. There is no cash alternative in lieu of vouchers or listed savings. This voucher has no cash value and cannot be resold on to any other party.\n14. The promoter accepts no responsibility for loss of items, injuries, or accidents that may occur during visits or whilst taking part in any of the activities.\n15. Voucher holders should check with activity providers that they are fully qualified in their activity to standard acceptable levels to work with children, that they are child protection and first aid qualified, that they are fully insured and anyone working with their child has been DBS checked. They should also check that the above are in date, which should be within at least the last two years.\n16. This offer is as listed on the voucher per child not per voucher, any more than this is up to the discretion of the activity provider.\n17. Individuals taking part in any of the activities contract directly with the centre and as such take part in the activities at their own risk.\n18. This offer is subject to availability and activity providers have the right to refuse to honour in extreme circumstances at which point the parent should find an alternative activity provider through the plaay.co.uk website.\n19. Voucher holders must hand in their voucher upon arrival at the venue in order to ensure the free lessons are honoured. Failure to do so will result in the lessons being charged at the full prevailing rate.\n20. Some individuals will be required (or may wish) to take out their own private insurance cover for certain activities and some activities may require a certain level of fitness or ability. Please check with the centre in advance.\n21. Participation in the promotion implies acceptance to the Terms and Conditions as set out here.\n22. The promoter reserves the right to withdraw, amend or replace any individual activity or offer without prior notification. Any queries, please contact the promoter: Sport & Activity Professionals, UNIT 11, HOVE BUSINESS CENTRE, FONTHILL ROAD, HOVE, EAST SUSSEX, BN3 6HA \n");
	$pdf->Ln(6);
	$pdf->SetXY(120, 26);
			

	$pdf->SetXY(120, 26);

	$pdf->SetFont('Arial', 'B', 13);	
	$pdf->Cell(40, 10, 'Coach');
	$pdf->Ln();

	$pdf->SetFontSize(11);		

	$pdf->SetXY(120, 36);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, 'Name:');
	$pdf->SetFont('');
	$pdf->Cell(60, 10, get_user_meta($coach->ID, 'first_name', true) . ' ' . get_user_meta($coach->ID, 'last_name', true));
	$pdf->Ln(6);

	$pdf->SetXY(120, 42);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, 'Company:');
	$pdf->SetFont('');
	$pdf->Cell(60, 10, get_user_meta($coach->ID, '_coach_company_name', true));
	$pdf->Ln(6);

	$pdf->SetXY(120, 48);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, 'Address:');
	$pdf->SetFont('');
	$pdf->Cell(60, 10, get_user_meta($coach->ID, '_coach_address', true));
	$pdf->Ln(6);

	$pdf->SetXY(120, 54);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, '');
	$pdf->SetFont('');
	$pdf->Cell(60, 10, get_user_meta($coach->ID, '_coach_town', true));
	$pdf->Ln(6);

	$pdf->SetXY(120, 60);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, '');
	$pdf->SetFont('');
	$pdf->Cell(60, 10, get_user_meta($coach->ID, '_coach_county', true));
	$pdf->Ln(6);

	$pdf->SetXY(120, 66);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, '');
	$pdf->SetFont('');
	$pdf->Cell(60, 10, get_user_meta($coach->ID, '_coach_postcode', true));
	$pdf->Ln(6);

	$pdf->SetXY(120, 72);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, 'Email:');
	$pdf->SetFont('');
	$pdf->Cell(60, 10, $coach->user_email);
	$pdf->Ln(6);

	$pdf->SetXY(120, 78);

	$pdf->SetFont('Arial', 'B');
	$pdf->Cell(30, 10, 'Phone');
	$pdf->SetFont('');
	$pdf->Cell(60, 10, get_user_meta($coach->ID, '_coach_phone', true));
	$pdf->Ln(6);									

	if ($output == 'string') {
		return $pdf->Output('voucher.pdf', 'S');	
	}
	if ($output == 'download') {
		return $pdf->Output('voucher.pdf', 'D');	
	}
	if ($output == 'view') {
		return $pdf->Output('voucher.pdf', 'I');	
	}		
}