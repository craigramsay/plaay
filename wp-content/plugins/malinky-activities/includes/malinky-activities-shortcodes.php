<?php

if (! defined('ABSPATH')) exit; // Exit if accessed directly

/**
 * ----------------------------------------------------------
 * *****************COACH BACKEND SHORTCODES*****************
 * ----------------------------------------------------------
 */

/**
 * ------------------------------------------
 * COACH ADD ACTIVITY FORM coach/add-activity
 * ------------------------------------------
 */
add_shortcode('malinky-activities-new-activity-form', 'malinky_activities_new_activity_form_shortcode');

function malinky_activities_new_activity_form_shortcode()
{
	include(MALINKY_ACTIVITIES_PLUGIN_DIR . '/forms/malinky-activities-new-activity-form.php');
	return;
}

/**
 * -----------------------------------------------------------
 * COACH EDIT ACTIVITY FORM coach/edit-activity?activity_id=XX
 * -----------------------------------------------------------
 */
add_shortcode('malinky-activities-edit-activity-form', 'malinky_activities_edit_activity_form_shortcode');

function malinky_activities_edit_activity_form_shortcode()
{
	include(MALINKY_ACTIVITIES_PLUGIN_DIR . '/forms/malinky-activities-edit-activity-form.php');
	return;	
}

/**
 * ---------------------------------
 * COACH ACTIVITIES coach/activities
 * ---------------------------------
 */
add_shortcode('malinky-activities-coach-activities', 'malinky_activities_coach_activities_shortcode');

function malinky_activities_coach_activities_shortcode()
{
	if (!empty($_REQUEST['action']) && !empty($_REQUEST['activity_id'])) {
		$action = sanitize_title($_REQUEST['action']);
		$activity_id = absint($_REQUEST['activity_id']);
		$activity = get_post($activity_id);

		if ($activity->post_author != get_current_user_id())
			wp_die(__('Security Error'));

		switch ($action) {
			case 'activity_added' :
				$coach_activities_message = sprintf('%s has been added.', $activity->post_title );
				break;
			case 'activity_updated' :
				$coach_activities_message = sprintf('%s has been updated.', $activity->post_title );
				break;
		}
	}

	if (!empty( $_REQUEST['action']) && !empty($_REQUEST['_wpnonce']) && wp_verify_nonce($_REQUEST['_wpnonce'], 'malinky_activities_coach_activities_actions')) {

		$action = sanitize_title( $_REQUEST['action'] );
		$activity_id = absint( $_REQUEST['activity_id'] );

		$activity = get_post($activity_id);

		if ($activity->post_author != get_current_user_id())
			wp_die(__('Security Error'));

		switch ($action) {
			case 'mark_active' :
				if ($activity->post_status == 'publish')
					$coach_activities_message = sprintf('%s is already active.', $activity->post_title);

				$args = array(
					'ID'           => $activity_id,
					'post_status' => 'publish'
				);
			  	wp_update_post( $args );
			  	$coach_activities_message = sprintf('%s has been set to active.', $activity->post_title);
				break;
			case 'mark_inactive' :
				if ($activity->post_status == 'pending')
					$coach_activities_message = sprintf('%s is already inactive.', $activity->post_title);
				$args = array(
					'ID'           => $activity_id,
					'post_status' => 'pending'
				);
			  	wp_update_post( $args );
			  	$coach_activities_message = sprintf('%s has been set to inactive.', $activity->post_title);
				break;
			case 'delete' :
				wp_trash_post($activity_id);
				break;
		}

	}

	global $wp_query;

	if (isset($wp_query->query_vars['orderby'])) {
		$orderby = sanitize_title($wp_query->query_vars['orderby']);
		$order_bits = explode('_', $orderby);
		$orderby = $order_bits[0];
		$order = $order_bits[1];
		switch ($orderby) {
			case 'activity' :
				$orderby = 'title';
				break;
			/*case 'status' :
				$orderby = 'post_status';
				break;*/
			default:
				$orderby = NULL;
		}
		switch ($order) {
			case 'asc' :
				$order = 'asc';
				break;
			case 'desc' :
				$order = 'desc';
				break;
			default:
				$order = NULL;
		}		
	}

	$posts_per_page = 10;

	$args = array(
		'post_type'         => 'activities',
		'post_status'       => array('publish', 'pending'),
		'orderby'           => isset($orderby) ? $orderby : 'title',
		'order'             => isset($order) ? $order : 'asc',
		'posts_per_page'    => $posts_per_page,
		'offset'            => (max(1, get_query_var('paged')) - 1) * $posts_per_page,		
		'author'            => get_current_user_id()	
	);

	/*if (isset($wp_query->query_vars['coach_category'])) {
		$category = sanitize_title($wp_query->query_vars['coach_category']);
		$args['tax_query'] = array(
			array(
				'taxonomy' 	=> 'malinky_activities_category',
				'field' 	=> 'slug',
				'terms' 	=> $category
			)
		);
	}*/

	$activities = new WP_Query;

	ob_start();

	if (isset($coach_activities_message))
		echo '<p class="coach_message_success">' . esc_html($coach_activities_message) . '</p>';

	get_malinky_activities_template('coach-activities.php', array('activities' => $activities->query($args), 'max_num_pages' => $activities->max_num_pages));

	return ob_get_clean();
}

/**
 * -----------------------------
 * COACH VOUCHERS coach/vouchers
 * -----------------------------
 */
add_shortcode('malinky-activities-coach-vouchers', 'malinky_activities_coach_vouchers_shortcode');

function malinky_activities_coach_vouchers_shortcode()
{
	global $wpdb;

	if (isset($_GET['orderby'])) {
		$orderby = sanitize_title($_GET['orderby']);
		$order_bits = explode('_', $orderby);
		$orderby = $order_bits[0];
		$order = $order_bits[1];
		switch ($orderby) {
			case 'activity' :
				$orderby = 'malinky_voucher_codes.voucher_details_activity_name';
				break;
			case 'code' :
				$orderby = 'malinky_voucher_codes.voucher_code';
				break;	
			case 'date' :
				$orderby = 'malinky_voucher_codes.voucher_code_activated_date';
				break;
			case 'paid' :
				$orderby = 'malinky_stripe_payments.voucher_id';
				break;					
			case 'attended' :
				$orderby = 'malinky_voucher_codes.voucher_mark_attended';
				break;												
			default:
				$orderby = NULL;
		}
		switch ($order) {
			case 'asc' :
				$order = 'asc';
				break;
			case 'desc' :
				$order = 'desc';
				break;
			default:
				$order = NULL;
		}		
	} else {
		$orderby = 'malinky_voucher_codes.voucher_code_activated_date';
		$order = 'desc';
	}

	$vouchers = $wpdb->get_results(
		$wpdb->prepare(
		"SELECT malinky_voucher_codes.*, malinky_parents.name, malinky_stripe_payments.stripe_charge_id FROM malinky_voucher_codes LEFT JOIN malinky_parents ON malinky_voucher_codes.parent_id = malinky_parents.id LEFT JOIN malinky_stripe_payments ON malinky_voucher_codes.id = malinky_stripe_payments.voucher_id WHERE coach_id = %d AND voucher_code_activated = TRUE ORDER BY $orderby $order", get_current_user_id()
		)			
	);

	ob_start();
	get_malinky_activities_template('coach-vouchers.php', array('vouchers' => $vouchers));
	return ob_get_clean();	
}

/**
 * -----------------------------------------
 * COACH VOUCHER coach/voucher?voucher_id=XX
 * -----------------------------------------
 */
add_shortcode('malinky-activities-coach-voucher', 'malinky_activities_coach_voucher_shortcode');

function malinky_activities_coach_voucher_shortcode()
{
	$voucher_id = absint($_GET['voucher_id']);

	$errors = new WP_Error();
	global $wpdb;

	try {

		$voucher = $wpdb->get_row(
			$wpdb->prepare(
			"SELECT * FROM malinky_voucher_codes WHERE id = %d AND coach_id = %d AND voucher_code_activated = TRUE", $voucher_id, get_current_user_id()
			)			
		);
		
		if (!isset($voucher)) {
			throw new Exception('There has been a problem accessing the voucher information. Please try again.');
		}

		$parent = $wpdb->get_row(
			$wpdb->prepare(
			"SELECT * FROM malinky_parents WHERE id = %d", $voucher->parent_id
			)			
		);	

		if (!isset($parent)) {
			throw new Exception('There has been a problem accessing the voucher information. Please try again.');
		}

		$stripe_charge = $wpdb->get_row(
			$wpdb->prepare(
			"SELECT *  FROM malinky_stripe_payments WHERE voucher_id = %d", $voucher_id
			)			
		);

		//check for any page actions
		if (!empty( $_REQUEST['action']) && !empty($_REQUEST['mark_attended_nce']) && wp_verify_nonce($_REQUEST['mark_attended_nce'], 'malinky_voucher_coach_voucher_mark_attended_action')) {

			$action = sanitize_title( $_REQUEST['action'] );

			if ($voucher->coach_id != get_current_user_id())
				wp_die(__('Security Error'));

			switch ($action) {
				case 'mark_attended' :
					if ($voucher->voucher_mark_attended == true)
						$coach_voucher_message = sprintf('%s is already marked as attended.', $activity->post_title);

					$voucher_mark_attended = $wpdb->update( 
						'malinky_voucher_codes',
						array(
							'voucher_mark_attended' => true
						), 
						array('id' => $voucher_id, 'coach_id' => $voucher->coach_id), 
						array( 
							'%d'
						), 
						array('%d', '%d') 
					);

					if ($voucher_mark_attended === false || $voucher_mark_attended == 0) {
						throw new Exception('There has been a problem marking the voucher as attended. Please try again.');
					} else {
						//update for page reload
						$voucher->voucher_mark_attended = true;
						$coach_activities_message = sprintf('%s has been marked as attended.', $voucher->voucher_details_activity_name);	
					}
					break;
				case 'mark_unattended' :
					if ($voucher->voucher_mark_attended == false)
						$coach_activities_message = sprintf('%s is already marked as unattended.', $voucher->voucher_details_activity_name);

					$voucher_mark_attended = $wpdb->update( 
						'malinky_voucher_codes',
						array(
							'voucher_mark_attended' => false
						), 
						array('id' => $voucher_id, 'coach_id' => $voucher->coach_id), 
						array( 
							'%d'
						), 
						array('%d', '%d') 
					);

					if ($voucher_mark_attended === false || $voucher_mark_attended == 0) {
						throw new Exception('There has been a problem marking the voucher as unattended. Please try again.');
					} else {
						//update for page reload
						$voucher->voucher_mark_attended = false;
						$coach_activities_message = sprintf('%s has been marked as unattended.', $voucher->voucher_details_activity_name );
					}
					break;
			}

		}

		ob_start();

		if (isset($coach_activities_message))
			echo '<p class="coach_message_success">' . esc_html($coach_activities_message) . '</p>';

		get_malinky_activities_template('coach-voucher.php', array('voucher' => $voucher, 'parent' => $parent, 'stripe_charge' => $stripe_charge));
		return ob_get_clean();	

	} catch (Exception $e) {

		$errors->add('coach_voucher_error', $e->getMessage());

		ob_start();
		get_malinky_activities_template('coach-voucher.php', array('errors' => $errors));
		return ob_get_clean();

	}
}

/**
 * -------------------------------------
 * COACH CARD DETAILS coach/card-details
 * -------------------------------------
 */
add_shortcode('malinky-activities-coach-payment-details', 'malinky_activities_coach_payment_details_shortcode');

function malinky_activities_coach_payment_details_shortcode()
{
	$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
	//Live Stripe Key
	if ($stripe_secret_key['settings_stripe_live']) {

		$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
		Stripe::setApiKey($stripe_secret_key['settings_stripe_live_secret_key']);
	
	//Test Stripe Key
	} else {

		$stripe_secret_key = get_option('malinky_activities_stripe_settings_options');
		Stripe::setApiKey($stripe_secret_key['settings_stripe_test_secret_key']);

	}

	global $current_user;
    get_currentuserinfo();

	$errors = new WP_Error();
	$card_details_exist = NULL;
	$card_expiry_date = NULL;

	try {

	    //check if coach already has a stripe_id WP
		$coach_stripe_id = get_user_meta(get_current_user_id(), '_coach_stripe_id', true);
		$coach_stripe_payment_status = get_user_meta(get_current_user_id(), '_coach_stripe_payment_status', true);
		//if coach has a stripe_id check the card was saved correctly and return message to them
		if ($coach_stripe_id) {
			$stripe_customer = Stripe_Customer::retrieve($coach_stripe_id);
			$stripe_customer_card_count = $stripe_customer->cards->total_count;
			if ($stripe_customer_card_count == 1 && $coach_stripe_payment_status) {
				$card_details_exist = 'You have already submitted your card details and your payment status is active. You can amend the card used by by completing the form below.';
				$card_expiry_date = malinky_activities_get_card_expiry(get_current_user_id());
			} elseif ($stripe_customer_card_count == 1 && !$coach_stripe_payment_status) {
				$card_details_exist = 'You have previously submitted your card details. However, there has been a problem charging your card and you will have recieved an email explaining this. Your card details should be updated by completing the form below.';
			}
		}

		if (isset($_POST['stripeToken'])) {

			if (!isset($_POST['malinky_activities_card_details_form_nonce']) || !wp_verify_nonce($_POST['malinky_activities_card_details_form_nonce'], 'malinky_activities_card_details_form'))
					throw new Exception('We apologise but there was a problem processing your payment details. Please try again. If the problem persists please contact PLAAY.');

			$token = $_POST['stripeToken'];

			//if stripe customer exists in WP
	    	if ($coach_stripe_id) {
				
				$stripe_customer = Stripe_Customer::retrieve($coach_stripe_id);
	    		$stripe_customer->card = $token;
	    		$stripe_customer->description = get_user_meta(get_current_user_id(), '_coach_company_name', true);
				$stripe_customer->email = $current_user->user_login;
	    		$stripe_customer->save();
	    		update_user_meta(get_current_user_id(), '_coach_stripe_payment_status', true);

	    		//remove all card errors from stripe_payments as customer may have been previously disabled, runs even if no errors
	    		malinky_activities_coach_remove_card_errors(get_current_user_id());
	    		
	    		$coach_activities_message = 'Your payment details have been added / updated.';

	    	//if new stripe customer
	    	} else {

				$customer = Stripe_Customer::create(array(
					'card' => $token,
					'description' => get_user_meta(get_current_user_id(), '_coach_company_name', true),
					'email' => $current_user->user_login
				));

				//update the stripe user into WP however the card/token could have failed at this point, see error handling below
				update_user_meta(get_current_user_id(), '_coach_stripe_id', $customer->id);

				//customer and card have been added proceed with updating other DB tables
				if ($customer->id && $customer->cards->total_count == 1) {
					update_user_meta(get_current_user_id(), '_coach_stripe_payment_status', true);
					$coach_activities_message = 'Your payment details have been added.';
				//this could be thrown if the card fails but REMEMBER the customer is still added so no other DB tables are updated	
				} else {
					throw new Exception('We apologise but there was a problem processing your payment details. Please try again. If the problem persists please contact us.');
				}
			}

		}

	} catch (Stripe_CardError $e) {

		//Card error. Don't email admin as coach will see error on screen to rectify and enter correct card details
		$body = $e->getJsonBody();
		$err = $body['error'];
		error_log(
			'Stripe stripe_charge_cron - ' . 
			'Error Type : ' . $err['type'] . 
			' - Error Code : ' . $err['code']  . 
			' - Error Message : ' . $err['message']. 
			' - Stripe Exception : Stripe_CardError'
		);

		$error = 'We apologise but there was a problem with your card details. The error returned was as follows: ' . $err['message'] . ' Please try again.';
		$errors->add('malinky_activities_stripe_error', $error);

	} catch (Stripe_InvalidRequestError $e) {

		//Something wrong with the code, incorrect parameters, admin must action.
		$body = $e->getJsonBody();
		$err = $body['error'];
		error_log(
			'Stripe card-details.php - ' . 
			'Error Type : ' . $err['type'] . 
			' - Error Message : ' . $err['message'] . 
			' - Stripe Exception : Stripe_InvalidRequestError'
		);

		malinky_activities_coach_stripe_failure_email_admin(
			'Coach Add Card Details - Request Error',
			get_current_user_id(),
			'Urgent action required',
			'card-details.php',
			'Stripe_InvalidRequestError',
			$err['type'],
			$err['message'],
			date('d/m/Y - G:i')	
		);

		$error = 'We apologise but there was a problem with your request. PLAAY have been informed to rectify the issue as quickly as possible. Stripe_InvalidRequestError';
		$errors->add('malinky_activities_stripe_error', $error);

	} catch (Stripe_AuthenticationError $e) {

		//Something wrong with API keys, admin must action.
		$body = $e->getJsonBody();
		$err = $body['error'];
		error_log(
			'Stripe card-details.php - ' . 
			'Error Type : ' . $err['type'] . 
			' - Error Message : ' . $err['message'] . 
			' - Stripe Exception : Stripe_AuthenticationError'
		);

		malinky_activities_coach_stripe_failure_email_admin(
			'Coach Add Card Details - Authetication Error',
			get_current_user_id(),
			'Urgent action required',
			'card-details.php',
			'Stripe_AuthenticationError',
			$err['type'],
			$err['message'],
			date('d/m/Y - G:i')	
		);

		$error = 'We apologise but there was a problem with your request. PLAAY have been informed to rectify the issue as quickly as possible. Stripe_InvalidRequestError';
		$errors->add('malinky_activities_stripe_error', $error);

	} catch (Stripe_ApiConnectionError $e) {

		//Something wrong with Stripe API server. Can't action.
		$body = $e->getJsonBody();
		$err = $body['error'];
		error_log(
			'Stripe card-details.php - ' . 
			'Error Type : ' . $err['type'] . 
			' - Error Message : ' . $err['message'] . 
			' - Stripe Exception : Stripe_ApiConnectionError'
		);

		malinky_activities_coach_stripe_failure_email_admin(
			'Coach Add Card Details - Stripe Server Error',
			get_current_user_id(),
			'No action required',
			'card-details.php',
			'Stripe_ApiConnectionError',
			$err['type'],
			$err['message'],
			date('d/m/Y - G:i')	
		);

		$error = 'We apologise but there was a problem connecting to the payment system. Please try again. Stripe_ApiConnectionError';
		$errors->add('malinky_activities_stripe_error', $error);

	} catch (Stripe_ApiError $e) {

		//Something wrong with Stripe API server. Can't action.
		$body = $e->getJsonBody();
		$err = $body['error'];
		error_log(
			'Stripe card-details.php - ' . 
			'Error Type : ' . $err['type'] . 
			' - Error Message : ' . $err['message'] . 
			' - Stripe Exception : Stripe_ApiError'
		);

		malinky_activities_coach_stripe_failure_email_admin(
			'Coach Add Card Details - Stripe Server Error',
			get_current_user_id(),
			'No action required',
			'card-details.php',
			'Stripe_ApiError',
			$err['type'],
			$err['message'],
			date('d/m/Y - G:i')	
		);			

		$error = 'We apologise but there was a problem connecting to the payment system. Please try again. Stripe_ApiError';
		$errors->add('malinky_activities_stripe_error', $error);

	} catch (Stripe_Error $e) {

		//Generic Stripe error but everything else should be caught by now.
		$body = $e->getJsonBody();
		$err = $body['error'];
		error_log(
			'Stripe card-details.php - ' . 
			'Error Type : ' . $err['type'] . 
			' - Error Message : ' . $err['message'] . 
			' - Stripe Exception : Stripe_Error'
		);

		malinky_activities_coach_stripe_failure_email_admin(
			'Coach Add Card Details - Stripe Other Error',
			get_current_user_id(),
			'Temporary error',
			'card-details.php',
			'Stripe_Error',
			$err['type'],
			$err['message'],
			date('d/m/Y - G:i')	
		);

		$error = 'We apologise but there was a problem connecting to the payment system. Please try again. Stripe_Error';
		$errors->add('malinky_activities_stripe_error', $error);		

	} catch (Exception $e) {

		error_log(
			'Stripe card-details.php - ' . 
			'Error Type : ' . $e->getCode() . 
			' - Error Message : ' . $e->getMessage() . 
			' - Stripe Exception : Exception'
		);

		malinky_activities_coach_stripe_failure_email_admin(
			'Coach Add Card Details - Other Error',
			get_current_user_id(),
			'Temporary error',
			'card-details.php',
			'Exception',
			$e->getCode(),
			$e->getMessage(),
			date('d/m/Y - G:i')	
		);

		$errors->add('malinky_activities_stripe_error', $e->getMessage());

	}

	ob_start();
	if (isset($coach_activities_message))
		echo '<p class="coach_message_success">' . esc_html($coach_activities_message) . '</p>';
	get_malinky_activities_template('coach-card-details.php', array('card_details_exist' => $card_details_exist, 'card_expiry_date' => $card_expiry_date, 'errors' => $errors));
	return ob_get_clean();	
}

/**
 * -------------------------------------
 * COACH CARD DETAILS coach/invoices
 * -------------------------------------
 */
add_shortcode('malinky-activities-coach-invoices', 'malinky_activities_coach_invoices_shortcode');

function malinky_activities_coach_invoices_shortcode()
{     
	global $wpdb;

	global $current_user;
    get_currentuserinfo();

	$invoices = NULL;
	$new_voucher_ids = NULL;

	if (isset($_GET['orderby'])) {
		$orderby = sanitize_title($_GET['orderby']);
		$order_bits = explode('_', $orderby);
		$orderby = $order_bits[0];
		$order = $order_bits[1];
		switch ($orderby) {
			case 'date' :
				$orderby = 'stripe_charge_created';
				break;
			case 'amount' :
				$orderby = 'stripe_charge_amount';
				break;																
			default:
				$orderby = NULL;
		}
		switch ($order) {
			case 'asc' :
				$order = 'asc';
				break;
			case 'desc' :
				$order = 'desc';
				break;
			default:
				$order = NULL;
		}		
	} else {
		$orderby = 'stripe_charge_created';
		$order = 'desc';
	}

	$voucher_ids = $wpdb->get_results(
		$wpdb->prepare(
		"SELECT id FROM malinky_voucher_codes WHERE coach_id = %d", get_current_user_id()
		), ARRAY_N		
	);

	foreach ($voucher_ids as $voucher_id) {
		$new_voucher_ids[] = $voucher_id[0];
	}

	if ($new_voucher_ids) {
	$invoices = $wpdb->get_results(
		$wpdb->prepare(
		"SELECT DISTINCT stripe_charge_id, stripe_charge_amount, stripe_charge_created FROM malinky_stripe_payments WHERE stripe_charge_id != %s AND voucher_id IN (" . implode(',', $new_voucher_ids) . ") ORDER BY $orderby $order", ''
		)		
	);
	}

	ob_start();
	get_malinky_activities_template('coach-invoices.php', array('invoices' => $invoices));
	return ob_get_clean();
}

/**
 * ------------------------------------------
 * COACH CARD DETAILS coach/invoice?charge=XX
 * ------------------------------------------
 */
add_shortcode('malinky-activities-coach-invoice', 'malinky_activities_coach_invoice_shortcode');

function malinky_activities_coach_invoice_shortcode()
{
	global $wpdb;
	$errors = new WP_Error();

	global $current_user;
    get_currentuserinfo();

	$charge_id = NULL;

	if (isset($_GET['charge']))
		$charge_id = $_GET['charge'];

	if (!preg_match('/^(ch_)[a-zA-Z0-9]{24}+$/', $charge_id)) {
		wp_die(__('You can\'t view this invoice.'));
	}

	try {

		$invoice = $wpdb->get_results(
			$wpdb->prepare(
			"SELECT malinky_stripe_payments.id, malinky_stripe_payments.voucher_id, malinky_stripe_payments.stripe_charge_id, malinky_stripe_payments.stripe_charge_amount, malinky_stripe_payments.stripe_charge_created, malinky_voucher_codes.voucher_code_activated_date, malinky_voucher_codes.voucher_code, malinky_voucher_codes.voucher_cost FROM malinky_stripe_payments LEFT JOIN malinky_voucher_codes ON malinky_stripe_payments.voucher_id = malinky_voucher_codes.id WHERE stripe_charge_id = %s AND malinky_voucher_codes.coach_id = " . get_current_user_id() . "", $charge_id
			)			
		);
		
		if (!isset($invoice) || $invoice == NULL) {
			throw new Exception('There has been a problem accessing the invoice information. Please try again.');
		}

		ob_start();
		get_malinky_activities_template('coach-invoice.php', array('invoice' => $invoice));
		return ob_get_clean();

	} catch (Exception $e) {

			$errors->add('coach_invoice_error', $e->getMessage());

			ob_start();
			get_malinky_activities_template('coach-invoice.php', array('errors' => $errors));
			return ob_get_clean();

	}
}

/**
 * ---------------
 * COACH DASHBOARD
 * ---------------
 */
add_shortcode('malinky-activities-coach-dashboard', 'malinky_activities_coach_dashboard_shortcode');

function malinky_activities_coach_dashboard_shortcode()
{
	ob_start();

	if (isset($_GET['action'])) {
		if ($_GET['action'] == 'profile_updated') {
			$coach_activities_message = 'Your profile has been updated.';
		} elseif ($_GET['action'] == 'logo_uploaded') {
			$coach_activities_message = 'Your logo has been uploaded.';
		}
	}

    //check if coach has set up payment details
	$coach_stripe_id = get_user_meta(get_current_user_id(), '_coach_stripe_id', true);
	$coach_stripe_payment_status = get_user_meta(get_current_user_id(), '_coach_stripe_payment_status', true);
	//if coach has a stripe_id check the card was saved correctly and return message to them
	if (!$coach_stripe_id || !$coach_stripe_payment_status) {
		echo '<p class="coach_message_error_permanent">Please visit the <a href="' . site_url('coach/payment-details') . '">Payment Details</a> page to enter your card details. You can add activities but they will not be listed on the website until this has been completed. Thanks.</p>';
	}

	if (isset($coach_activities_message))
		echo '<p class="coach_message_success">' . esc_html($coach_activities_message) . '</p>';

	get_malinky_activities_template('coach-dashboard.php');

	return ob_get_clean();
}

/**
 * ----------
 * COACH LOGO
 * ----------
 */
add_shortcode('malinky-activities-coach-logo', 'malinky_activities_coach_logo_shortcode');

function malinky_activities_coach_logo_shortcode()
{
	include(MALINKY_ACTIVITIES_PLUGIN_DIR . '/forms/malinky-activities-coach-logo-form.php');
	return;
}

/**
 * ----------------------------------------------------------------
 * *****************ACTIVITIES FRONTEND SHORTCODES*****************
 * ----------------------------------------------------------------
 */

/**
 * ------------------------------------------------
 * PROCESS POSTCODE SEARCH /process-postcode-search
 * ------------------------------------------------
 */
add_shortcode('malinky-activities-process-postcode-search', 'malinky_activities_process_postcode_search_shortcode');

function malinky_activities_process_postcode_search_shortcode()
{
	if (isset($_POST['submit_postcode_search'])) {
		
		if (!isset($_POST['malinky_activities_postcode_search_nonce']) || !wp_verify_nonce($_POST['malinky_activities_postcode_search_nonce'], 'malinky_activities_postcode_search')) {

			//throw new Exception('There was an error processing your postcode. Please try again.');
			unset($_SESSION['postcode']);
			wp_safe_redirect('activities');
			exit();

		}

		$postcode = preg_replace('/\s+/', '', $_POST['home_postcode_search']);
		
		if(!preg_match("~^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$~i", $postcode)) {
			
			//throw new Exception('Please enter a valid UK postcode.');
			wp_safe_redirect('activities');
			exit();

		}
		
		$_SESSION['postcode'] = $postcode;
		wp_safe_redirect('activities');

	}
}

/**
 * ---------------------------
 * VIEW ACTIVITIES /activities
 * ---------------------------
 */
add_shortcode('malinky-activities-activities', 'malinky_activities_activities_shortcode');

function malinky_activities_activities_shortcode()
{	
	$errors = new WP_Error();

	$distance_selected = $_SESSION['distance_selected'];

	try {
		if (isset($_SESSION['postcode']))
			$postcodes = get_postcodes($_SESSION['postcode'], $distance_selected);
	} catch ( Exception $e ) {
		$errors->add('malinky_activities_postcode_error', $e->getMessage());
		if ($errors->get_error_code()) {
			$error_messages = $errors->errors;
		} else {
			$error_messages = NULL;
		}
	}

	//normal first page load results with valid postcode
	if (isset($postcodes) && isset($_SESSION['postcode'])) {		

		foreach ($postcodes as $key => $value) {
			$postcodes_only[] = $postcodes[$key]['postcode'];
		}

		//get activities will return results but at this point we dont know if the activity is published
		//or it could be picking up old activity postcode from the malinky postcodes_table that has now been deleted
		$activity_ids = get_activities_by_distance($postcodes_only);

		//remove activity_ids where the coach distance travelled is further away than the parent wishes to travel
		foreach($activity_ids as $activity_key => $id) {
			$coach_distance_travelled = get_post_meta($id, '_activity_distance_travelled', true);
			$coach_distance_travelled_activity_postcode = get_post_meta($id, '_activity_postcode', true);
			foreach ($postcodes as $postcodes_key => $postcodes_value) {
				if ($postcodes_value['postcode'] == $coach_distance_travelled_activity_postcode) {
					if ( ($coach_distance_travelled != 0) && ($coach_distance_travelled < round($postcodes_value['distance'])) ) {
						unset($activity_ids[$activity_key]);
					}
				}
			} 
		}

		//remove activities from a coach who hasn't set up payment details yet
		$banned_coach_ids = array();
		foreach ($activity_ids as $key => $activity_id) {
			$banned_coach_ids[] = get_post_field('post_author', $activity_id);
		}

		//get unique coach ids
		$banned_coach_ids = array_unique($banned_coach_ids);
		$banned_coach_ids = array_values($banned_coach_ids);
		
		//loop through coach ids if _coach_stripe_payment_status is true unset them. The query uses author__not_in to remove banned coaches
		foreach ($banned_coach_ids as $key => $banned_coach_id) {
			$banned_coach_payment_status = get_user_meta($banned_coach_id, '_coach_stripe_payment_status', true);
			if ($banned_coach_payment_status) {
				unset($banned_coach_ids[$key]);
			}
		}

		$posts_per_page = 10;

		$args = array(
			'post_type'         => 'activities',
			'post_status'       => 'publish',
			'post__in'			=> $activity_ids,
			'orderby'           => 'post__in',
			'order'             => 'asc',
			'posts_per_page'    => $posts_per_page,
			'offset'            => (max(1, get_query_var('paged')) - 1) * $posts_per_page,
			'author__not_in'	=> $banned_coach_ids,
			'meta_query'		=> array(
										array(
											'key'		=> '_activity_postcode',
											'value'		=> $postcodes_only,
											'compare'	=> 'IN'
											)
										)
		);

		$filters_args = array(
			'post_type'         => 'activities',
			'post_status'       => 'publish',
			'post__in'			=> $activity_ids,
			'posts_per_page'    => -1,
			'author__not_in'	=> $banned_coach_ids,
			'meta_query'		=> array(
										array(
											'key'		=> '_activity_postcode',
											'value'		=> $postcodes_only,
											'compare'	=> 'IN'
											)
										)
		);	

		$filters = new WP_Query;

		$filters_output['format'] 		= filter_get_terms($filters->query($filters_args), 'malinky_activities_format');
		$filters_output['age_group'] 	= filter_get_terms($filters->query($filters_args), 'malinky_activities_age_group');		
		$filters_output['category'] 	= filter_get_terms($filters->query($filters_args), 'malinky_activities_category');
		$filters_output['voucher'] 		= filter_get_terms($filters->query($filters_args), 'malinky_activities_voucher');

		//removes all values if filters are empty
		$filters_output = array_filter($filters_output);

		$activities = new WP_Query($args);

		//final error check as there could be no posts found in the query
		//based on banned coaches or using old activities from postcode table
		//need to trigger error and ob_start and ob_get_clean
		try {
			if (empty($activities->post_count))
				throw new Exception('<h3 class="activities">Activities</h3>There are no activities within the specified distance from your postcode. Please try again by increasing the distance.');
		} catch ( Exception $e ) {
			$errors->add('malinky_activities_postcode_error', $e->getMessage());
			if ($errors->get_error_code()) {
				$error_messages = $errors->errors;
			} else {
				$error_messages = NULL;
			}
			ob_start(); ?>
			<div class="col">
				<div class="col_item col_item_3_10">
					<div id="activities_filter">
						<h3 class="activities">Refine Results</h3>
						<p>There are no results.</p>
					</div>
				</div><!--
				--><div class="col_item col_item_7_10">
					<div id="activities" data-orderby="<?php echo esc_attr('distance_nearest'); ?>" data-order="<?php echo esc_attr('asc'); ?>" data-posts_per_page="<?php echo esc_attr(10); ?>" data-page="<?php echo esc_attr(0); ?>">
						<?php get_malinky_activities_template('activities.php', array('orderby_selected' => 'distance_nearest', 'distance_selected' => $distance_selected, 'posts_per_page_selected' => 10, 'error' => $error_messages['malinky_activities_postcode_error'][0])); ?>
					</div>
				</div>
			</div><!-- .col -->
			<?php return ob_get_clean();
		}

		ob_start();

		if ($activities->have_posts()) :
			while ($activities->have_posts() ) : $activities->the_post();
				if (isset($postcodes) && isset($_SESSION['postcode'])) {
					$activity_postcode = get_post_meta($activities->post->ID, '_activity_postcode', true);
					foreach ($postcodes as $key => $value) {
						if ($value['postcode'] == $activity_postcode) {
							$distance = number_format($value['distance'], 1) . ' miles';
						}
					} 
				}		
				$activities->post->temp_distance_output = $distance;
				get_malinky_activities_template_part('activities-listings.php');
			endwhile;
		endif;

		wp_reset_postdata();

		$activities_output = ob_get_clean();

		$total_posts 		= $activities->found_posts;
		$displayed_posts 	= $activities->post_count;

		ob_start(); ?>

		<div class="col">
			<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">

				<a href="" class="sidebar_mobile_content_button button full_width col_item--margin_bottom_20">Refine Results</a>

				<nav id="sidebar_content" role="sidebar">
					<div id="activities_filter">
						<h3 class="activities">Refine Results</h3>
						<?php get_malinky_activities_template('activities-filter.php', array('filters_output' => $filters_output)); ?>
					</div>
				</nav>

			</div><!--
			--><div class="col_item col_item_7_10 medium-col_item_full small-col_item_full">
				<div id="activities" data-orderby="<?php echo esc_attr('distance_nearest'); ?>" data-order="<?php echo esc_attr('asc'); ?>" data-posts_per_page="<?php echo esc_attr($posts_per_page); ?>" data-page="<?php echo esc_attr(0); ?>">
					<?php get_malinky_activities_template('activities.php', array('activities_output' => $activities_output, 'postcodes' => $postcodes, 'max_num_pages' => $activities->max_num_pages, 'orderby_selected' => 'distance_nearest', 'distance_selected' => $distance_selected, 'posts_per_page_selected' => $posts_per_page, 'page' => 0, 'total_posts' => $total_posts, 'offset' => 0, 'displayed_posts' => $displayed_posts)); ?>
				</div>
			</div>
		</div><!-- .col -->

		<?php return ob_get_clean();

	//normal first page load no results as problem with postcode
	} elseif (!isset($postcodes) && isset($_SESSION['postcode'])) {

		ob_start(); ?>

		<div class="col">
			<div class="col_item col_item_3_10">
				<div id="activities_filter">
					<h3 class="activities">Refine Results</h3>
					<p>There are no results.</p>
				</div>
			</div><!--
			--><div class="col_item col_item_7_10">
				<div id="activities" data-orderby="<?php echo esc_attr('distance_nearest'); ?>" data-order="<?php echo esc_attr('asc'); ?>" data-posts_per_page="<?php echo esc_attr(10); ?>" data-page="<?php echo esc_attr(0); ?>">
					<?php get_malinky_activities_template('activities.php', array('orderby_selected' => 'distance_nearest', 'distance_selected' => $distance_selected, 'posts_per_page_selected' => 10, 'error' => $error_messages['malinky_activities_postcode_error'][0])); ?>
				</div>
			</div>
		</div><!-- .col -->

		<?php return ob_get_clean();

	}

	//no results shown as no postcode has been entered
	else {

		ob_start(); ?>

		<div class="col">
			<div class="col_item col_item_3_10">
				<div id="activities_filter">
					<h3 class="activities">Refine Results</h3>
					<p>There are no results.</p>
				</div>
			</div><!--
			--><div class="col_item col_item_7_10">
				<div id="activities">
					<?php get_malinky_activities_template('activities.php', array('orderby_selected' => 'distance_nearest', 'distance_selected' => $distance_selected, 'posts_per_page_selected' => 10, 'error' => '<h3 class="activities">Please enter a postcode</h3><p>Welcome to PLAAY.CO.UK. ' . get_homepage_strapline() . ' Enter a postcode to search.</p>')); ?>
				</div>
			</div>
		</div><!-- .col -->

		<?php return ob_get_clean();

	}

}

/**
 * ----------------------------------------
 * CREATE VOUCHER FORM /voucher-application
 * ----------------------------------------
 */
add_shortcode('malinky-activities-voucher-application-form', 'malinky_activities_voucher_application_form_shortcode');

function malinky_activities_voucher_application_form_shortcode()
{
	include(MALINKY_ACTIVITIES_PLUGIN_DIR . '/forms/malinky-activities-voucher-application-form.php');
	return;
}

/**
 * --------------------------------------------
 * ACTIVATE PARENT EMAIL ADDRESS /confirm-email
 * --------------------------------------------
 */
add_shortcode('malinky-activities-activate-parent', 'malinky_activities_activate_parent_shortcode');

function malinky_activities_activate_parent_shortcode()
{
	if (is_page('confirm-email') && isset($_GET['activation_code'])) {

		$parent_exploded_activation_code = malinky_activities_validate_activation_code_id($_GET['activation_code']);
		$voucher_id = $parent_exploded_activation_code['voucher_id'];
		$parent_activation_code = $parent_exploded_activation_code['activation_code'];

		if (is_array($parent_exploded_activation_code)) {
			if (malinky_activities_activate_parent($voucher_id, $parent_activation_code)) {
				if (malinky_activities_set_voucher_code($voucher_id, $parent_activation_code)) {
					wp_safe_redirect(add_query_arg(array('activation_code' => malinky_activities_get_voucher_activation_code($voucher_id) . '-' . $voucher_id), site_url('voucher')));
				}
			}
		}

	} else {
		wp_die('Security Error');
	}
}

/**
 * --------------------
 * GET VOUCHER /voucher
 * --------------------
 */
add_shortcode('malinky-activities-voucher', 'malinky_activities_voucher_shortcode');

function malinky_activities_voucher_shortcode()
{
	if (is_page('voucher') && isset($_GET['activation_code'])) {

		global $wpdb;

		//test voucher code and split into code and voucher id
		$voucher_exploded_activation_code = malinky_activities_validate_activation_code_id($_GET['activation_code']);
		$voucher_id = $voucher_exploded_activation_code['voucher_id'];
		$voucher_activation_code = $voucher_exploded_activation_code['activation_code'];

		//get the activity id which is used to make a link for the social share after viewing voucher
		//if the activity is no longer live then share website homepage
		$social_link_activity_id = $wpdb->get_var(
			$wpdb->prepare(
			"SELECT activity_id FROM malinky_voucher_codes WHERE id = %d", $voucher_id
			)
		);

		$social_link_post = '';
		$social_link_description = '';

		$social_link_post = get_post($social_link_activity_id);

		//get the url to the activity that has been downloaded
		if (isset($social_link_activity_id)) {

			//twitter, linkedin, pinterest, email encoded url
			$encoded_social_link_url = get_post_status($social_link_activity_id) == 'publish' ? urlencode(str_replace('https://', 'http://', get_permalink($social_link_activity_id))) : urlencode(site_url('', 'http'));
			//facebook, googleplus unencoded url
			$unencoded_social_link_url = get_post_status($social_link_activity_id) == 'publish' ? str_replace('https://', 'http://', get_permalink($social_link_activity_id)) : site_url('', 'http');

			//twitter, linkedin, pinterest encoded text
			$encoded_social_link_text 	= 'Visit PLAAY to download a ' . urlencode(ucwords(get_the_title($social_link_post->ID)) . ' Voucher.');
			//googleplus, email unencoded text
			$unencoded_social_link_text = 'Visit PLAAY to download a ' . ucwords(get_the_title($social_link_post->ID)) . ' Voucher.';

			$social_link_description 		.= the_activity_format($social_link_post, false, true) . '. ';
			foreach (the_activity_voucher($social_link_post, false) as $k => $v) {
				$social_link_description 	.= $v . '. ';
			}
			$social_link_description 		.= truncate_words(strip_tags(get_post_meta($social_link_post->ID, '_activity_description', true)), 140);

			//linkedin encoded description
			$encoded_social_link_description 	= urlencode($social_link_description);
			//googleplus, email unencoded description
			$unencoded_social_link_description	= $social_link_description;

		} else {

			//twitter, linkedin, pinterest, email encoded url
			$encoded_social_link_url = urlencode(site_url('', 'http'));
			//facebook, googleplus unencoded url
			$unencoded_social_link_url = site_url('', 'http');

			//twitter, linkedin, pinterest encoded text
			$encoded_social_link_text 	= get_homepage_strapline();
			//googleplus, email unencoded text
			$unencoded_social_link_text = get_homepage_strapline();

			//linkedin encoded description
			$encoded_social_link_description 	= '';
			//googleplus, email unencoded description
			$unencoded_social_link_description	= '';

		}
		
		include(MALINKY_ACTIVITIES_PLUGIN_DIR . '/forms/malinky-activities-voucher-form.php');
		return;

	} else {
		wp_die('Security Error');
	}
}

/**
 * ----------------------------------
 * DOWNLOAD VOUCHER /download-voucher
 * ----------------------------------
 */
add_shortcode('malinky-activities-download-voucher', 'malinky_activities_download_voucher_shortcode');

function malinky_activities_download_voucher_shortcode()
{
	if (is_page('download-voucher') && isset($_GET['activation_code'])) {

		$voucher_exploded_activation_code = malinky_activities_validate_activation_code_id($_GET['activation_code']);
		$voucher_id = $voucher_exploded_activation_code['voucher_id'];
		$voucher_activation_code = $voucher_exploded_activation_code['activation_code'];

		//check nonce
		if (!isset($_GET['dwnl_nce']) || (!wp_verify_nonce($_GET['dwnl_nce'], 'download_voucher_nonce_action_' . $voucher_activation_code . '_' . $voucher_id)))
			wp_die(__('Security Error'));

		if ($_GET['type'] != 'download' && $_GET['type'] != 'view')
			wp_die(__('Security Error'));
		
		$view_download = $_GET['type'];

		malinky_activities_generate_voucher($voucher_id, $view_download);

	} else {
		wp_die('Security Error');
	}
}

/**
 * --------------------
 * THANKS /thanks
 * --------------------
 */
add_shortcode('malinky-activities-thanks', 'malinky_activities_thanks_shortcode');

function malinky_activities_thanks_shortcode()
{
	if (isset($_GET['message'])) {
		$message = $_GET['message'];
		switch ($message) {
			case ('successful_voucher_application'):
				$title = 'Thanks';
				$message = 'Thanks for your voucher application. Please check your email for details on how to download your voucher.';
				$message_class = '';
				break;
			case ('unsuccessful_validate_activation_code'):
				$title = 'We\'re Sorry';
				$message = 'We apologise there has been a problem with your activation code. Please try again.';
				$message_class = 'coach_message_error_permanent';
				break;
			case ('unsuccessful_activate_visitor'):
				$title = 'We\'re Sorry';
				$message = 'We apologise there has been a problem activating your account. Please try again.';
				$message_class = 'coach_message_error_permanent';
				break;
			case ('unsuccessful_create_voucher_code'):
				$title = 'We\'re Sorry';
				$message = 'We apologise there has been a problem creating your voucher code. Please try again.';
				$message_class = 'coach_message_error_permanent';
				break;				
			default: 
				$title = 'We\'re Sorry';
				$message = 'Please use the navigation to find what you\'re looking for.';
				$message_class = 'coach_message_error_permanent';
		}
	} else {
		wp_die('Security Error');
	}
	?>

	<?php get_header(); ?>

	<main role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="col">
				<div class="col_item col_item_full">
					<h1><?php esc_html_e($title); ?></h1>
					<p class="<?php echo $message_class; ?>"><?php esc_html_e($message); ?></p>
				</div><!-- .col_item -->
			</div><!-- .col -->

		</article>
	</main>

	<?php get_footer();
}

/**
 * --------------------------------------------
 * EMAIL UNSUBSCRIBE /email-unsubscribe
 * --------------------------------------------
 */
add_shortcode('malinky-activities-email-unsubscribe-form', 'malinky_activities_email_unsubscribe_shortcode');

function malinky_activities_email_unsubscribe_shortcode()
{
	if (is_page('email-unsubscribe') && isset($_GET['unsubscribe'])) {

		$unsubscribe_code = $_GET['unsubscribe'];

		if (!preg_match('/^[a-f0-9]{32}$/', $unsubscribe_code)) wp_safe_redirect(site_url());

		include(MALINKY_ACTIVITIES_PLUGIN_DIR . '/forms/malinky-activities-email-unsubscribe-form.php');
		return;

	} else {
		wp_safe_redirect(site_url());
	}
}