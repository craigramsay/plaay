<?php
function malinky_activities_coach_logo_upload($file = array())
{
	$errors = new WP_Error();

	global $current_user;
    get_currentuserinfo();

	require_once(ABSPATH . 'wp-admin/includes/admin.php');

	//sanitizing file name, check extension and move to the uploads directory
	$file_return = wp_handle_upload($file, array('test_form' => false));
    
    //if there is a problem return error
    if(isset($file_return['error']) ||isset( $file_return['upload_error_handler'])) {
       	
       	$errors->add('coach_logo_move_error', 'There has been a problem uploading your logo. Please try again.');
		return $errors;

    } else {

    	//get path to uploaded file in wp_handle_upload
		$filename = $file_return['file'];
        
        $attachment = array(
            'post_mime_type' 	=> $file_return['type'],
            'post_title' 		=> preg_replace( '/\.[^.]+$/', '', basename($filename)),
            'post_content' 		=> '',
            'post_status' 		=> 'inherit',
            'guid' 				=> $file_return['url']
        );
        
        //insert attachment as post and into media library
        $attachment_id = wp_insert_attachment($attachment, $file_return['url']);

        //attach meta data and create thumnails of the image
        require_once (ABSPATH . 'wp-admin/includes/image.php');
		$attach_data = wp_generate_attachment_metadata($attachment_id, $filename);
  		wp_update_attachment_metadata($attachment_id,  $attach_data);

  		update_user_meta($current_user->ID, '_coach_logo', $attachment_id);
    }

    return false;
}