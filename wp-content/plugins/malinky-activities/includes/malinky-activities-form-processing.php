<?php
/*
 * --------------------------------------------------------------------------------------------------------------------------------
 * GET POSTED FORM FIELDS
 * IF RETURN_VALUES FALSE THESE VALUES ARE ADDED TO THE MAIN ARRAY GENERATED IN APPLICABLE SET FORM FIELDS
 * IF RETURN_VALUES TRUE THESE ARE RETURNED JUST AS VALUES AND USED FOR PROCESSING
 * @param array $form_fields
 * @param bool $return_values
 * @param int $activity_id
 * @return array
 * --------------------------------------------------------------------------------------------------------------------------------
 */
function malinky_activities_get_posted_form_fields($form_fields, $return_values = false, $activity_id = NULL)
{
	$form_fields = call_user_func($form_fields, $activity_id);

	foreach ($form_fields as $fieldset => $fields) {
		foreach ($fields as $key => $field) {
			if (isset($form_fields[$fieldset][$key]['post_return_array']) && isset($_POST[$key])) {
				foreach ($_POST[$key] as $key_array) {
					$values[$fieldset][$key][] = $key_array;
				}
			} else {
				$values[$fieldset][$key] = isset($_POST[$key]) ? stripslashes($_POST[$key]) : '';
			}
			switch ($form_fields[$fieldset][$key]['sanitize_type']) {
				case 'email_field' :
					$values[$fieldset][$key] = sanitize_email($values[$fieldset][$key]);
				break;	
				case 'text_area_field' :
					$values[$fieldset][$key] = strip_tags($values[$fieldset][$key], '<strong><p><br><em><ul><ol><li>');
				break;		
				case 'file_field' :
					$values[$fieldset][$key] = sanitize_file_name($_FILES[$key]['name']);
				break;											
				default:
					if (is_array($values[$fieldset][$key])) {
						$values[$fieldset][$key] = array_map('sanitize_text_field', $values[$fieldset][$key]);
					} else {
						$values[$fieldset][$key] = sanitize_text_field($values[$fieldset][$key]);
					}
				break;			
			}
			$form_fields[$fieldset][$key]['value'] = $values[$fieldset][$key];
		}
	}
	
	if ($return_values == true)
		return $values;

	return $form_fields;
}

/**
 * -----------------------------------------------------------------------
 * GET THE VALUES AS ONE ARRAY FROM THE FORM FIELDS. FIELDSETS ARE REMOVED
 * @param array $form_fields
 * @return array
 * -----------------------------------------------------------------------
 */
function malinky_activities_posted_form_fields_values($form_fields)
{
	foreach ($form_fields as $fieldset => $fields) {
		foreach ($fields as $key => $value) {
			$db_values[$key] = $value;
		}
	}

	return $db_values;
}

/**
 * -------------------------
 * FORM VALIDATION
 * @param array $form_fields
 * @return obj  
 * -------------------------
 */
function malinky_activities_validate_form_fields($form_fields) {

	$errors = new WP_Error();

	foreach ($form_fields as $fieldset => $fields) {
		foreach ($fields as $key => $field) {
			if (is_array($field['validation_rules'])) {
				foreach ($field['validation_rules'] as $rule) {
					switch ($rule) {
						case 'required':
							if (empty($form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}
						break;	
						case 'please_choose':
							if ($form_fields[$fieldset][$key]['value'] == 'please_choose') {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}	
						break;							
						case 'email':
							if (!is_email($form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}
						break;																															
						case 'county':
							if (!preg_match('/^[a-z\-]+$/', $form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}	
						break;
						case 'integer':
							if ($form_fields[$fieldset][$key]['value'] != 'please_choose' && !is_numeric($form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}	
						break;						
						case 'postcode':
							if ($form_fields[$fieldset][$key]['value'] != '' && !preg_match("~^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$~i", $form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}
						break;
						case 'phone':
							if (!empty($form_fields[$fieldset][$key]['value']) && !preg_match('/[0-9 ]+$/', $form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}	
						break;	
						case 'checkbox':							
							if (!$form_fields[$fieldset][$key]['value']) {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}
						break;	
						case 'checkbox_max_1':	
							if (!$form_fields[$fieldset][$key]['value'] || count($form_fields[$fieldset][$key]['value']) > 1) {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}
						break;	
						case 'file_image':
							$file_ext = wp_check_filetype($_FILES[$key]['name']);
							$file_ext_white_list = array('jpg', 'jpeg', 'png', 'gif');
							$file_mime_white_list = array('image/jpg', 'image/jpeg', 'image/png', 'image/gif');
							if (!in_array($file_ext['ext'], $file_ext_white_list)) {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}
							if (!in_array($file_ext['type'], $file_mime_white_list)) {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));
							}
							if ($_FILES[$key]['size'] > '2000000') {
								$errors->add($key . '_error', malinky_activities_error_messages($key . '_error'));	
							}
						break;																				
					}
				}					
			}
		}
	}

	if ($errors->get_error_code())
		return $errors;

}