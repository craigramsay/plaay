<?php
global $current_user;
get_currentuserinfo();

$form_fields = malinky_activities_set_coach_logo_form_fields();

if (!empty($_POST['submit_logo'])) {

	$form_fields = malinky_activities_get_posted_form_fields('malinky_activities_set_coach_logo_form_fields');
	$errors = malinky_activities_validate_form_fields($form_fields);
	if (is_wp_error($errors)) {
		$error_messages = $errors->errors;
	} else {
		$coach_logo = malinky_activities_coach_logo_upload($_FILES['coach_logo']);
		if (!is_wp_error($coach_logo)) {
			wp_safe_redirect('coach?action=logo_uploaded');
			exit();
		}
	}

}

$current_logo = get_user_meta($current_user->ID, '_coach_logo', true);

if (isset($coach_logo) && is_wp_error($coach_logo))
	$process_error_messages = $coach_logo->errors;

$form_action = site_url('coach/logo');

get_header('coach'); ?>

<main role="main">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<h1><?php the_title(); ?></h1>

		<?php if (isset($process_error_messages))
			echo '<p class="coach_message_error">' . $process_error_messages['coach_logo_move_error'][0] . '</p>'; ?>		

		<div class="col">
			<div class="col_item col_item_full">					

				<form id="malinky_voucher_form" action="<?php echo esc_url($form_action); ?>" method="post" role="form" enctype="multipart/form-data">

					<fieldset id="fieldset_coach_logo_exists">
						<legend>Logo Status</legend>
						<?php
						if (!empty($current_logo)) {
							echo '<p>You have already uploaded a logo. This will be displayed on your activity pages.</p>';
						} else {
							echo '<p>Please upload a logo to be displayed your activity pages.</p>';
						}
						?>	
					</fieldset>

					<fieldset id="fieldset_voucher" class="voucher_fieldset">
						<legend>Company Logo</legend>
						<?php foreach ($form_fields['coach_details'] as $key => $field) { ?>
						<div class="col">
							<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
								<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
							</div><!--
							--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
								 <?php malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
								 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
							</div><!--
							--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
								<div class="field_error_icon"></div>					
							</div>
						</div><!-- .col -->
						<?php } ?>
					</fieldset>

					<?php wp_nonce_field( 'malinky_coach_logo_form_nonce_action', 'malinky_coach_logo_form_nonce' ); ?>
					<input type="submit" name="submit_logo" class="button full_width" value="<?php esc_attr_e('Upload Logo'); ?>" />

				</form>
			</div><!-- .col_item -->
		</div><!-- .col -->

	</article>
</main>

<?php get_footer(); ?>