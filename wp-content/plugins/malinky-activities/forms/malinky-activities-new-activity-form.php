<?php
$form_fields = malinky_activities_set_activity_form_fields();

if (!empty($_POST['submit_activity'])) {

	$form_fields = malinky_activities_get_posted_form_fields('malinky_activities_set_activity_form_fields');
	$errors = malinky_activities_validate_form_fields($form_fields);
	if (is_wp_error($errors)) {
		$error_messages = $errors->errors;
	} else {
		malinky_activities_new_activity();
	}

}

$form_action = site_url('coach/add-activity');

get_header('coach'); ?>

<main role="main">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<h1><?php the_title(); ?></h1>

		<div class="col">
			<div class="col_item col_item_full">

				<form id="malinky_activity_form" action="<?php echo esc_url($form_action); ?>"method="post" role="form">

					<fieldset id="fieldset_activity_login_details" class="activity_fieldset">
						<legend>Activity Details</legend>
						<p class="coach_message">Enter an activity name, choose one session format and choose multiple child age groups, sport or activity types and voucher types that are applicable.</p>
						<?php foreach ($form_fields['activity_details'] as $key => $field) { ?>
							<div class="col form_fields_2 <?php echo $key == 'activity_voucher' ? 'activity_voucher_parent_js': ''; ?>">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									<?php
									//used to sort activity vouhchers into activity formats uses code from checkbox-multi-field.php
									if ($key == 'activity_voucher') {
										//get the main activity formats
										$voucher_format = get_activity_terms('malinky_activities_format');
										foreach ($voucher_format as $voucher_format_key => $voucher_format_value) {
											//echo '<p class="activity_form_voucher_format_heading">' . $voucher_format_value . '</p>';
											foreach($field['options'] as $option_key => $option_value) {
												if (strpos($option_key, $voucher_format_key) !== false) { ?>
												<div class="checkbox_block">
													<label for="<?php esc_attr_e($option_key); ?>"><?php esc_html_e($option_value); ?></label>
													<input type="checkbox" name="<?php echo esc_attr($key); ?>[]" id="<?php echo esc_attr($option_key); ?>" value="<?php echo esc_attr($option_key); ?>" <?php if (!empty($field['value'])) { foreach($field['value'] as $array_checkbox_value) { checked($array_checkbox_value, $option_key); } } ?> />
												</div>
												<?php }
											}
										}
										if (!empty($field['description'])) : ?><small class="description"><?php echo esc_html($field['description']); ?></small><?php endif;
									} elseif ($key == 'activity_age_group') {
										//sort age groups
										usort($field['options'], "compare_ages");
										//set age groups to be the keys also
										$field['options'] = array_combine($field['options'], $field['options']);
										malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) );
									} else {
										malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) );
									}
									if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->
						<?php } ?>
					</fieldset>

					<fieldset id="fieldset_activity_login_details" class="activity_fieldset">
						<legend>Activity Location</legend>
						<p class="coach_message">If the activity is held at a venue, choose Not Applicable from the dropdown and enter the venues address. If you travel to the voucher holder select the distance you are prepared to travel and enter your business address.</p>
						<?php foreach ($form_fields['activity_location'] as $key => $field) { ?>
							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">			
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									 <?php malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
									 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div>
						<?php } ?>
					</fieldset>	

					<fieldset id="fieldset_activity_login_details" class="activity_fieldset">
						<legend>Activity Active</legend>
						<p class="coach_message">Please choose whether the activity is to be shown on the website.</p>
						<?php foreach ($form_fields['activity_publish'] as $key => $field) { ?>
							<div class="col">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									 <?php malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
									 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div>
						<?php } ?>
					</fieldset>												
						
					<?php
					//wp registration error
					if (isset($error_messages['activity_fail'][0])) echo '<p class="error">' . $error_messages['activity_fail'][0] . '</p>';
					?>

					<?php wp_nonce_field( 'malinky_activities_add_activity_form', 'malinky_activities_add_activity_form_nonce' ); ?>
					<input type="submit" name="submit_activity" class="button full_width" value="<?php esc_attr_e('Add Activity'); ?>" />

				</form>
			</div><!-- .col_item -->
		</div><!-- .col -->

	</article>
</main>

<?php get_footer('coach'); ?>