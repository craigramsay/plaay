<?php
$form_fields = malinky_activities_set_email_voucher_form_fields();

if (!empty($_POST['submit_voucher'])) {

	$form_fields = malinky_activities_get_posted_form_fields('malinky_activities_set_email_voucher_form_fields');
	$errors = malinky_activities_validate_form_fields($form_fields);
	if (is_wp_error($errors)) {
		$error_messages = $errors->errors;
	} else {
		/*
		Created in the shortcode
		$voucher_id
		$voucher_activation_code
		$social_link_post (downloaded activity $post)
		$encoded_social_link_url
		$unencoded_social_link_url
		$encoded_social_link_text
		$unencoded_social_link_text
		Required in voucher_download.php
		*/
		$voucher_id = malinky_activities_activate_voucher($voucher_id, $voucher_activation_code);
		if (!is_wp_error($voucher_id)) {
			malinky_activities_voucher_email($voucher_id);
			get_malinky_activities_template('voucher-download.php', array('voucher_id' => $voucher_id, 'voucher_activation_code' => $voucher_activation_code, 'social_link_post' => $social_link_post, 'encoded_social_link_url' => $encoded_social_link_url, 'unencoded_social_link_url' => $unencoded_social_link_url, 'encoded_social_link_text' => $encoded_social_link_text, 'unencoded_social_link_text' => $unencoded_social_link_text, 'encoded_social_link_description' => $encoded_social_link_description, 'unencoded_social_link_description' => $unencoded_social_link_description));
			exit();
		}
	}

}

if (isset($voucher_id) && is_wp_error($voucher_id))
	$process_error_messages = $voucher_id->errors;

$form_activity_nonce = $_GET['activation_code'];
$form_action = site_url('voucher?activation_code=' . $form_activity_nonce);

get_header(); ?>

<main role="main">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<h1><?php the_title(); ?></h1>
		<p class="form_intro">Please enter the email address used when applying for your voucher and click the Get Voucher button.</p>

		<?php if (isset($process_error_messages))
			echo '<p class="coach_message_error">' . $process_error_messages['voucher_code_activation_error'][0] . '</p>'; ?>		

		<div class="col">
			<div class="col_item col_item_full">					

				<form id="malinky_voucher_form" action="<?php echo esc_url($form_action); ?>" method="post" role="form">

					<fieldset id="fieldset_voucher" class="voucher_fieldset">
						<legend>Email Address</legend>
						<?php foreach ($form_fields['parent_details'] as $key => $field) { ?>
						<div class="col">
							<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
								<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
							</div><!--
							--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
								 <?php malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
								 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
							</div><!--
							--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
								<div class="field_error_icon"></div>					
							</div>
						</div><!-- .col -->
						<?php } ?>
					</fieldset>

					<?php wp_nonce_field( 'malinky_voucher_form_nonce_action', 'malinky_voucher_form_nonce' ); ?>
					<input type="submit" name="submit_voucher" class="button full_width" value="<?php esc_attr_e('Get Voucher'); ?>" />

				</form>
			</div><!-- .col_item -->
		</div><!-- .col -->

	</article>
</main>

<?php get_footer(); ?>