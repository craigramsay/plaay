<?php
if ((!isset($_GET['voucher_application_nce'])) || (!wp_verify_nonce($_GET['voucher_application_nce'], 'malinky_voucher_application_nonce_action')))
	wp_safe_redirect(site_url('', 'http'));

if (!malinky_activities_active_activity(absint($_GET['activity_id'])))
	wp_safe_redirect(site_url('', 'http'));

$form_fields = malinky_activities_set_voucher_application_form_fields($_GET['activity_id']);

if (!empty($_POST['submit_voucher_application'])) {

	$form_fields = malinky_activities_get_posted_form_fields('malinky_activities_set_voucher_application_form_fields', false, $_GET['activity_id']);
	$errors = malinky_activities_validate_form_fields($form_fields);
	if (is_wp_error($errors)) {
		$error_messages = $errors->errors;
	} else {
		$process_errors = malinky_activities_voucher_application(absint($_GET['activity_id']));
		if (is_wp_error($process_errors)) {
			$process_error_messages = $process_errors->errors;
		}
	}

}

$form_activity_id = $_GET['activity_id'];
$form_activity_nonce = $_GET['voucher_application_nce'];
$form_action = site_url('voucher-application?activity_id=' . $form_activity_id . '&voucher_application_nce=' . $form_activity_nonce);

?>

<?php get_header(); ?>

<main role="main">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<h1><?php the_title(); ?></h1>
		<p class="form_intro">Please fill in the important details below so that we can send you your voucher, to make sure we send you to the right place and verify that your request is genuine and you are a human, not a computer programme.</p>

		<?php if (isset($process_error_messages))
			echo '<p class="coach_message_error">' . $process_error_messages['parent_voucher_application_error'][0] . '</p>'; ?>

		<div class="col">
			<div class="col_item col_item_full">

				<form id="malinky_voucher_application_form" action="<?php echo esc_url($form_action); ?>" method="post" role="form">

					<fieldset id="fieldset_voucher_application_personal_details" class="voucher_application_fieldset">
						<legend>Personal Details</legend>
						<?php foreach ($form_fields['personal_details'] as $key => $field) { ?>
							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									 <?php malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
									 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->
						<?php } ?>
					</fieldset>
					<fieldset id="fieldset_voucher_application_options" class="voucher_application_fieldset">
						<legend>Voucher Options</legend>
						<?php foreach ($form_fields['voucher_options'] as $key => $field) { ?>
							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									<?php if ($key == 'voucher_application_age_group') {
										//sort age groups
										usort($field['options'], "compare_ages");
										//set age groups to be the keys also
										$field['options'] = array_combine($field['options'], $field['options']);
									} ?>
								 	<?php malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
								 	<?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->
						<?php } ?>
					</fieldset>
					<fieldset id="fieldset_voucher_application_options" class="voucher_application_fieldset">
						<legend>Child Information</legend>
						<?php foreach ($form_fields['child_information'] as $key => $field) { ?>
							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									 <?php malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
									 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->
						<?php } ?>
					</fieldset>						
					<fieldset id="fieldset_voucher_application_terms" class="voucher_application_fieldset">
						<legend>Terms and Conditions</legend>
						<?php foreach ($form_fields['terms'] as $key => $field) { ?>
							<div class="col">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">			
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									 <?php malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
									 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->
						<?php } ?>
					</fieldset>												
						
					<?php
					//wp registration error
					if (isset($error_messages['activity_fail'][0])) echo '<p class="error">' . $error_messages['activity_fail'][0] . '</p>';
					?>

					<?php wp_nonce_field( 'malinky_voucher_application_form_nonce_action', 'malinky_voucher_application_form_nonce' ); ?>
					<input type="submit" name="submit_voucher_application" class="button full_width" value="<?php esc_attr_e('Submit'); ?>" />

				</form>
			</div><!-- .col_item -->
		</div><!-- .col -->

	</article>
</main>

<?php get_footer(); ?>