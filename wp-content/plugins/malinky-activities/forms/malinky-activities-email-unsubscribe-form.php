<?php
$form_fields = malinky_activities_set_email_unsubscribe_form_fields();

if (!empty($_POST['submit_email'])) {

	$form_fields = malinky_activities_get_posted_form_fields('malinky_activities_set_email_unsubscribe_form_fields');
	$errors = malinky_activities_validate_form_fields($form_fields);
	if (is_wp_error($errors)) {
		$error_messages = $errors->errors;
	} else {
		$unsubscribe_success = malinky_activities_email_unsubscribe_process();
		if (!is_wp_error($unsubscribe_success)) {
			get_header(); ?>
			<main role="main">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1><?php the_title(); ?></h1>
					<div class="col">
						<div class="col_item col_item_full">
							<p>You have now be unsubscribed from PLAAY emails.</p>				
						</div>
					</div><!-- .col -->							
				</article>
			</main>
			<?php get_footer(); exit();
		}
	}

}

if (isset($unsubscribe_success) && is_wp_error($unsubscribe_success))
	$process_error_messages = $unsubscribe_success->errors;

$unsubscribe_id = $_GET['unsubscribe'];
$form_action = site_url('email-unsubscribe?unsubscribe=' . $unsubscribe_id);

get_header(); ?>

<main role="main">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<h1><?php the_title(); ?></h1>
		<p class="form_intro">Please enter your email address to unsubscribe from PLAAY emails.</p>

		<?php if (isset($process_error_messages))
			echo '<p class="coach_message_error">' . $process_error_messages['email_unsubscribe_error'][0] . '</p>'; ?>		

		<div class="col">
			<div class="col_item col_item_full">					

				<form id="malinky_email_unsubscribe_form" action="<?php echo esc_url($form_action); ?>" method="post" role="form">

					<fieldset id="fieldset_voucher" class="voucher_fieldset">
						<legend>Email Address</legend>
						<?php foreach ($form_fields['parent_details'] as $key => $field) { ?>
						<div class="col">
							<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
								<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
							</div><!--
							--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
								 <?php malinky_activities_get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
								 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
							</div><!--
							--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
								<div class="field_error_icon"></div>					
							</div>
						</div><!-- .col -->
						<?php } ?>
					</fieldset>
					
					<input type="hidden" name="unsubscribe_code" value="<?php echo $unsubscribe_code ?>" />

					<?php wp_nonce_field( 'malinky_email_unsubscribe_form_nonce_action', 'malinky_email_unsubscribe_form_nonce' ); ?>
					<input type="submit" name="submit_email" class="button full_width" value="<?php esc_attr_e('Unsubscribe'); ?>" />

				</form>
			</div><!-- .col_item -->
		</div><!-- .col -->

	</article>
</main>

<?php get_footer(); ?>