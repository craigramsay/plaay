<?php
if (! defined('ABSPATH')) exit; // Exit if accessed directly

add_action('admin_menu', 'malinky_activities_admin_voucher_page');

function malinky_activities_admin_voucher_page()
{

	add_submenu_page(
		NULL,
		'Voucher',
		'Voucher',
		'manage_activities',
		'admin-voucher-page',
		'malinky_activities_admin_voucher_page_display'
	);

}

function malinky_activities_admin_voucher_page_display()
{
	?>
	<div class="wrap" id="profile-page">
	<?php

	if (!isset($_GET['voucher_id'])) {
		echo 'There has been a problem accessing the voucher information. Please try again.';
		exit();
	} 

	if (!absint($_GET['voucher_id'])) {
		echo 'There has been a problem accessing the voucher information. Please try again.';
		exit();
	}

	$voucher_id = absint($_GET['voucher_id']);

	$errors = new WP_Error();
	global $wpdb;

	$voucher = $wpdb->get_row(
		$wpdb->prepare(
		"SELECT * FROM malinky_voucher_codes WHERE id = %d AND voucher_code_activated = TRUE", $voucher_id
		)			
	);
	
	if (!isset($voucher)) {
		throw new Exception('There has been a problem accessing the voucher information. Please try again.');
	}

	$parent = $wpdb->get_row(
		$wpdb->prepare(
		"SELECT * FROM malinky_parents WHERE id = %d", $voucher->parent_id
		)			
	);	

	if (!isset($parent)) {
		throw new Exception('There has been a problem accessing the voucher information. Please try again.');
	}

	$stripe_charge = $wpdb->get_row(
		$wpdb->prepare(
		"SELECT * FROM malinky_stripe_payments WHERE voucher_id = %d", $voucher_id
		)			
	);

	?>
<div class="col">

		<div class="col_item col_item_full">
			<img src="<?php echo get_template_directory_uri() . '/img/logo_dark.png'; ?>" class="admin_invoice_logo" />
		</div><!--

		--><div class="col_item col_item_half medium-col_item_full small-col_item_full">
		
			<fieldset>
				<legend>Voucher Details</legend>
				<p><span class="bold_text">Activity</span> <?php esc_html_e($voucher->voucher_details_activity_name); ?></p>
				<p><span class="bold_text">Code</span> <?php esc_html_e($voucher->voucher_code) ;?></p>
				<p><span class="bold_text">Activated</span> <?php esc_html_e(date('d/m/Y', strtotime($voucher->voucher_code_activated_date)));?>
				<p><span class="bold_text">Format</span> <?php esc_html_e($voucher->voucher_details_format_term); ?></p>
				<p><span class="bold_text">Age Group of Child</span> <?php esc_html_e($voucher->voucher_details_age_group_term); ?></p>
				<p><span class="bold_text">Sport or Activity Type</span> <?php esc_html_e($voucher->voucher_details_category_term); ?></p>
				<p><span class="bold_text">Voucher</span> <?php esc_html_e($voucher->voucher_details_voucher_term); ?></p>
			</fieldset>

		</div><!--
		--><div class="col_item col_item_half medium-col_item_full small-col_item_full">
	
			<div class="col">
				<div class="col_item col_item_full">
			
					<fieldset>
						<legend>Voucher Holder Details</legend>
							<p><span class="bold_text">Name</span> <?php esc_html_e($parent->name);?></p>
							<p><span class="bold_text">Email</span> <?php esc_html_e($parent->email);?></p>
							<p><span class="bold_text">Phone</span> <?php esc_html_e($parent->phone);?></p>
					</fieldset>
	
				</div>
			</div>

		</div><!--
		--><div class="col_item col_item_full">

			<fieldset>
				<legend>Voucher Charges</legend>
			<p><span class="bold_text">Cost</span> &pound;<?php esc_html_e(number_format($voucher->voucher_cost/100, 2)); ?></p>
			<?php 
			if ($stripe_charge) {
				if ($stripe_charge->stripe_charge_id) { ?>
					<p><span class="bold_text">Invoice No.</span> <a href="<?php echo esc_url(add_query_arg(array('page' => 'admin-invoice-page', 'invoice_no' => $stripe_charge->stripe_charge_id, 'coach_id' => $voucher->coach_id), site_url('wp-admin/admin.php'))); ?>"><?php esc_html_e($stripe_charge->stripe_charge_id); ?></a></p>
					<p><span class="bold_text">Invoice Date</span> <?php esc_html_e(date('d/m/Y', strtotime($stripe_charge->stripe_charge_created))); ?></p>
				<?php }
			} ?>
			</fieldset>

		</div>

	</div><?php

}