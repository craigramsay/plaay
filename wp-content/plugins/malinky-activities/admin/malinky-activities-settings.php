<?php
if (! defined('ABSPATH')) exit; // Exit if accessed directly

add_action('admin_menu', 'malinky_activities_add_settings_page');
function malinky_activities_add_settings_page()
{
	add_submenu_page(	'edit.php?post_type=activities',
						'Format Charges',
						'Format Charges',
						'manage_options',
						'malinky_activities_format_settings',
						'malinky_activities_display_format_settings_page'
	);

	add_submenu_page(	'edit.php?post_type=activities',
						'Stripe Settings',
						'Stripe Settings',
						'manage_options',
						'malinky_activities_stripe_settings',
						'malinky_activities_display_stripe_settings_page'
	);	

	add_submenu_page(	'edit.php?post_type=activity_emails',
						'Email Settings',
						'Email Settings',
						'manage_options',
						'malinky_activities_email_settings',
						'malinky_activities_display_email_settings_page'
	);		
}

function malinky_activities_display_format_settings_page()
{
	?>
	<div class="wrap nosubsub">
		<h2>PLAAY Settings</h2>
		<form action="options.php" method="post">
			<?php settings_fields('malinky_activities_format_settings_options'); ?>
			<?php do_settings_sections('malinky_activities_format_settings'); ?>
			<input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save'); ?>" />
		</form>
	</div>	
	<?php
}

function malinky_activities_display_stripe_settings_page()
{
	?>
	<div class="wrap nosubsub">
		<h2>PLAAY Settings</h2>
		<form action="options.php" method="post">
			<?php settings_fields('malinky_activities_stripe_settings_options'); ?>
			<?php do_settings_sections('malinky_activities_stripe_settings'); ?>
			<input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save'); ?>" />
		</form>
	</div>	
	<?php
}

function malinky_activities_display_email_settings_page()
{
	?>
	<div class="wrap nosubsub">
		<h2>PLAAY Settings</h2>
		<form action="options.php" method="post">
			<?php settings_fields('malinky_activities_email_settings_options'); ?>
			<?php do_settings_sections('malinky_activities_email_settings'); ?>
			<input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save'); ?>" />
		</form>
	</div>	
	<?php
}

add_action('admin_init', 'plugin_admin_init');
function plugin_admin_init()
{
	//ACTIVITY FORMAT PRICE SECTION---------------------------
	//arg 1, group same as settings_fields. arg 2, name of options same as settings_fields. arg 3 name of validation callback
	register_setting(	'malinky_activities_format_settings_options',
						'malinky_activities_format_settings_options',
						'malinky_activities_format_settings_validate'
	);

	//create new sections, unique id, title, callback to display, page name slug same as add_submenu_page slug
	add_settings_section(	'malinky_activities_format_prices',
							'Session Format Charges',
							'format_prices_text',
							'malinky_activities_format_settings'
						);

	//create each field, unique id, title, callback to display, page name slug same as add_submenu_page slug, id of where settings section it lives in same as add_settings_section id, finally args for call back as an array
	//get all the activity formats first so settings page is automated then loop through using name and slug to create fields
	$activity_formats = get_terms('malinky_activities_format', 'hide_empty=0');
	foreach ($activity_formats as $key => $value) {
		//sample field below
		//add_settings_field('settings_format_education_establishment', 'Education Establishment', 'display_setting_format_prices', 'malinky_activities_format_settings', 'malinky_activities_format_prices', 'settings_format_education_establishment');
		add_settings_field(		'settings_format_' . str_replace('-', '_', $value->slug),
								$value->name,
								'display_setting_format_prices',
								'malinky_activities_format_settings',
								'malinky_activities_format_prices',
								array('format_slug' => 'settings_format_' . str_replace('-', '_', $value->slug))
							);
	}
	//ACTIVITY FORMAT PRICE SECTION---------------------------
	
	//STRIPE PAYMENTS SECTION---------------------------------
	register_setting(	'malinky_activities_stripe_settings_options',
						'malinky_activities_stripe_settings_options',
						'malinky_activities_stripe_settings_validate'
						);

	add_settings_section(	'malinky_activities_stripe_keys',
							'Stripe Keys',
							'stripe_keys_text',
							'malinky_activities_stripe_settings'
						);

	$stripe_keys = array('stripe_test_secret_key', 'stripe_test_published_key', 'stripe_live_secret_key', 'stripe_live_published_key');
	foreach ($stripe_keys as $key => $value) {
		//sample field below
		//add_settings_field('settings_stripe_test_secret_key', 'Stripe Test Secret Key', 'display_setting_stripe_keys', 'malinky_activities_stripe_settings', 'malinky_activities_stripe_keys', 'settings_stripe_test_secret_key');
		add_settings_field(		'settings_' . $value,
								ucwords(str_replace('_', ' ', $value)),
								'display_setting_stripe_keys',
								'malinky_activities_stripe_settings',
								'malinky_activities_stripe_keys',
								 array('stripe_slug' => 'settings_' . $value)
							);
	}
	//Add Live Stripe Checkbox
	add_settings_field(	'settings_stripe_live',
						'Stripe Live',
						'display_setting_stripe_live',
						'malinky_activities_stripe_settings',
						'malinky_activities_stripe_keys',
						 array('stripe_slug' => 'settings_stripe_live')
					);	
	//STRIPE PAYMENTS SECTION---------------------------------

	//SMTP SECTION---------------------------------
	register_setting(	'malinky_activities_email_settings_options',
						'malinky_activities_email_settings_options',
						'malinky_activities_email_settings_validate'
						);

	add_settings_section(	'malinky_activities_email_smtp_settings',
							'Email Settings',
							'email_settings_text',
							'malinky_activities_email_settings'
						);

	$email_smtp_settings = array('smtp_host', 'smtp_port', 'smtp_username', 'smtp_password');
	foreach ($email_smtp_settings as $key => $value) {
		//sample field below
		//add_settings_field('settings_stripe_test_secret_key', 'Stripe Test Secret Key', 'display_setting_stripe_keys', 'malinky_activities_email_settings', 'malinky_activities_email_smtp_settings', 'settings_stripe_test_secret_key');
		add_settings_field(		'settings_' . $value,
								ucwords(str_replace('_', ' ', $value)),
								'display_setting_email_smtp_settings', 'malinky_activities_email_settings',
								'malinky_activities_email_smtp_settings',
								 array('email_smtp_slug' => 'settings_' . $value)
							);
	}	
	//SMTP SECTION---------------------------------	

}

//ACTIVITY FORMAT PRICE SECTION---------------------------
function format_prices_text()
{
	echo '<p>Please enter the cost per downloaded voucher of each of the Session Formats. Use pence and no decimal points. Example £15.00 would be entered as 1500.</p>';
}

function display_setting_format_prices(array $args)
{
	$options = get_option('malinky_activities_format_settings_options');
	echo '<input id="' . $args['format_slug'] . '" name="malinky_activities_format_settings_options[' . $args['format_slug'] . ']" size="40" type="text" value="'; echo isset($options[$args['format_slug']]) ? $options[$args['format_slug']] : ''; echo '" />';
}

function malinky_activities_format_settings_validate($input)
{
	$options = get_option('malinky_activities_format_settings_options');
    $new_input = array();

	$activity_formats = get_terms('malinky_activities_format', 'hide_empty=0');
	foreach ($activity_formats as $key => $value) {
		$format_slug = 'settings_format_' . str_replace('-', '_', $value->slug);
	    if(isset($input[$format_slug])) {
	    	if (preg_match('/^[0-9]+$/', $input[$format_slug])) {
	        	$new_input[$format_slug] = $input[$format_slug];
	    	} else {
	    		$new_input[$format_slug] = $options[$format_slug];
	    	}
	    }
	}
    return $new_input;
}
//ACTIVITY FORMAT PRICE SECTION---------------------------

//STRIPE PAYMENTS SECTION---------------------------------
function stripe_keys_text()
{
	echo '<p>Please enter your Stripe test and published keys.</p>';
}

function display_setting_stripe_keys(array $args)
{
	$options = get_option('malinky_activities_stripe_settings_options');
	echo '<input id="' . $args['stripe_slug'] . '" name="malinky_activities_stripe_settings_options[' . $args['stripe_slug'] . ']" size="40" type="text" value="'; echo isset($options[$args['stripe_slug']]) ? $options[$args['stripe_slug']] : ''; echo '" />';
}

function display_setting_stripe_live(array $args)
{
	$options = get_option('malinky_activities_stripe_settings_options');
	
	if ( isset( $options[ $args['stripe_slug'] ] ) && $options[ $args['stripe_slug'] ] == 1 ) {
		$checked = 'checked';
	} else {
		$checked = '';
	}

	echo '<input type="checkbox" id="' . $args['stripe_slug'] . '" name="malinky_activities_stripe_settings_options[' . $args['stripe_slug'] . ']" value="1"' . $checked . '/>';
}

function malinky_activities_stripe_settings_validate($input)
{

	$options = get_option('malinky_activities_stripe_settings_options');
    $new_input = array();

	$stripe_keys = array('stripe_test_secret_key', 'stripe_test_published_key', 'stripe_live_secret_key', 'stripe_live_published_key', 'stripe_live');
	foreach ($stripe_keys as $key => $value) {
		$stripe_slug = 'settings_' . str_replace('-', '_', $value);

		if ( $stripe_slug == 'settings_stripe_live' ) {
			if ( array_key_exists( 'settings_stripe_live', $input ) ) {
				$new_input[$stripe_slug] = $input[$stripe_slug];
			} else {
				$new_input[$stripe_slug] = false;
			}
		} else {

		    if(isset($input[$stripe_slug])) {
		    	if (preg_match('/^([A-Za-z0-9_]{32})+$/', $input[$stripe_slug])) {
		        	$new_input[$stripe_slug] = $input[$stripe_slug];
		    	} else {
		    		$new_input[$stripe_slug] = $options[$stripe_slug];
		    	}
		    }

		}

	}
    return $new_input;
}
//STRIPE PAYMENTS SECTION---------------------------------

//SMTP SECTION---------------------------------
function email_settings_text()
{
	echo '<p>Please enter your SMTP settings.</p>';
}

function display_setting_email_smtp_settings(array $args)
{
	$options = get_option('malinky_activities_email_settings_options');
	echo '<input id="' . $args['email_smtp_slug'] . '" name="malinky_activities_email_settings_options[' . $args['email_smtp_slug'] . ']" size="40" type="text" value="'; echo isset($options[$args['email_smtp_slug']]) ? $options[$args['email_smtp_slug']] : ''; echo '" />';
}

function malinky_activities_email_settings_validate($input)
{
	$options = get_option('malinky_activities_email_settings_options');
    $new_input = array();

	$email_smtp_settings = array('smtp_host', 'smtp_port', 'smtp_username', 'smtp_password');
	foreach ($email_smtp_settings as $key => $value) {
		$email_smtp_slug = 'settings_' . str_replace('-', '_', $value);
	    if(isset($input[$email_smtp_slug])) {
	    	if (preg_match('/^.+$/', $input[$email_smtp_slug])) {
	        	$new_input[$email_smtp_slug] = $input[$email_smtp_slug];
	    	} else {
	    		$new_input[$email_smtp_slug] = $options[$email_smtp_slug];
	    	}
	    }
	}
    return $new_input;
}
//SMTP SECTION---------------------------------
?>