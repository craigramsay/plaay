<?php
if (! defined('ABSPATH')) exit; // Exit if accessed directly

class Malinky_Activities_Meta_Boxes {

	public function __construct()
	{
		add_action('add_meta_boxes_activities', array($this, 'malinky_activities_add_meta_boxes'));
		add_action('save_post', array($this, 'malinky_activities_save_post'), 1, 2);
		add_action('malinky_activities_save_job_listing', array($this, 'save_activity_data'), 1, 2);
		add_action('admin_notices', array($this, 'malinky_activities_admin_notice'), 0);
	}

	public function malinky_activities_set_meta_boxes()
	{
		$meta_boxes = array(
			'activity_description' => array(
				'type'				=> 'editor',
				'label'       		=> 'Description',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> ''
			),
			'activity_additional_information' => array(
				'type'				=> 'editor',
				'label'       		=> 'Additional Information',
				'label_type'		=> '(optional)',
				'validation_rules' 	=> array(),
				'placeholder' 		=> ''
			),
			'activity_distance_travelled' => array(
				'type'				=> 'dropdown',
				'label'       		=> 'Distance Travelled',
				'label_type'		=> '(required)',
				'placeholder' 		=> '',
				'options'			=> array('0' => 'Not Applicable Parent Travels', '5' => '5 Miles', '10' => '10 Miles', '15' => '15 Miles', '20' => '20 Miles', '25' => '25 Miles'),				
			),						
			'activity_address' => array(
				'type'				=> 'text',
				'label'       		=> 'Address',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> ''
			),
			'activity_town' => array(
				'type'				=> 'text',
				'label'       		=> 'Town',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> ''
			),
			'activity_postcode' => array(
				'type'				=> 'text',
				'label'       		=> 'Postcode',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> '',
			)											
		);
		return $meta_boxes;
	}

	public function malinky_activities_create_meta_boxes($post)
	{
		wp_nonce_field('malinky_activities_save_meta_data', 'malinky_activities_save_meta_data_nonce');

		foreach ($this->malinky_activities_set_meta_boxes() as $key => $field) {
			call_user_func(array($this, 'input_' . $field['type']), $key, $field);
		}
	}

	public function malinky_activities_add_meta_boxes()
	{
		add_meta_box('activity_data', 'Activity Data', array($this, 'malinky_activities_create_meta_boxes'), 'activities', 'normal', 'high');
	}

	private function input_text($key, $field)
	{
		global $post;

		if (empty($field['value']))
			$field['value'] = get_post_meta($post->ID, '_' . $key, true);
		?>
		<p class="form-field">
			<label for="<?php echo esc_attr($key); ?>"><?php esc_html_e($field['label']) ; ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
		</p>
		<input type="text" name="<?php echo esc_attr($key); ?>" id="<?php echo esc_attr($key); ?>" value="<?php echo esc_attr($field['value']); ?>" placeholder="<?php echo esc_attr($field['placeholder']); ?>"  />
		<?php
	}

	private function input_checkbox($key, $field)
	{
		global $post;

		if (empty($field['value']))
			$field['value'] = get_post_meta($post->ID, '_' . $key, true);
		?>
		<p class="form-field">
			<label for="<?php echo esc_attr($key); ?>"><?php esc_html_e($field['label']) ; ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
		</p>
		<input type="checkbox" name="<?php echo esc_attr($key); ?>" id="<?php echo esc_attr($key); ?>" value="1" <?php checked($field['value'], 1); ?> />
		<?php
	}

	private function input_dropdown($key, $field)
	{
		global $post;

		if (empty($field['value']))
		 	$field['value'] = get_post_meta($post->ID, '_' . $key, true);
		?>
		<p class="form-field">
			<label for="<?php echo esc_attr($key); ?>"><?php esc_html_e($field['label']) ; ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
		</p>
		<select name="<?php echo esc_attr($key); ?>" id="<?php echo esc_attr($key); ?>">
		<option value="please_choose">Please Choose</option>
		<?php
		 foreach($field['options'] as $option_key => $option_value) { ?>
			<option value="<?php echo esc_attr($option_key); ?>" <?php if (isset($field['value'])) { selected($field['value'], $option_key); } ?>><?php echo esc_html($option_value); ?></option>
		<?php } ?>
		</select>
		<?php
	}	

	private function input_dropdown_taxonomy($key, $field)
	{
		global $post;

		if (empty($field['value']))
		 	$field['value'] = get_post_meta($post->ID, '_' . $key, true);
		?>
		<p class="form-field">
			<label for="<?php echo esc_attr($key); ?>"><?php esc_html_e($field['label']) ; ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
		</p>
		<select name="<?php echo esc_attr($key); ?>" id="<?php echo esc_attr($key); ?>">
		<option value="please_choose">Please Choose</option>
		<?php
		foreach($field['options'] as $option_key => $option_value) { ?>
			<option value="<?php echo esc_attr($option_value->slug); ?>" <?php selected($field['value'], $option_value->slug); ?>><?php esc_html_e($option_value->name); ?></option>
		<?php } ?>
		</select>
		<?php
	}	

	private function input_editor($key, $field)
	{
		global $post;

		if (empty($field['value']))
		 	$field['value'] = get_post_meta($post->ID, '_' . $key, true);

		$editor = array(
			'media_buttons' => false,
			'textarea_rows' => 8,
			'quicktags' => false,
			'tinymce' => array(
				'plugins' => 'paste',
				'paste_auto_cleanup_on_paste' => true,
				'paste_remove_styles' => true,
				'paste_text_sticky' => true,
				'paste_text_sticky_default' => true,				
				'paste_retain_style_properties' => "none",
				'paste_strip_class_attributes' => true,
				'theme_advanced_buttons1' => 'bold,italic,|,bullist,numlist,|,link,|,undo,redo,|,|,code',
				'theme_advanced_buttons2' => '',
				'theme_advanced_buttons3' => '',
				'theme_advanced_buttons4' => ''
			),
		); ?>
		<p class="form-field">
			<label for="<?php echo esc_attr($key); ?>"><?php esc_html_e($field['label']) ; ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
		</p>
		<?php wp_editor(isset($field['value']) ? esc_textarea($field['value']) : '', $key, $editor);
	}

	public function malinky_activities_admin_notice()
	{
	    global $post;
	    //only display notices on an actual page screen
	    if (isset($post)) {
	    	//get notices specific to post
		    $notice = get_option('malinky_activity_notice_' . $post->ID);
		    if (empty($notice)) return '';
		    //loop through notices
		    foreach($notice as $key => $message) {
	            echo '<div id="message" class="error"><p>'.$message.'</p></div>';
	            //make sure to remove notice after its displayed so its only displayed when needed.
	            delete_option('malinky_activity_notice_' . $post->ID);
		    }
		}
	}

	public function malinky_activities_save_post($post_id, $post)
	{
		if (empty($post_id) || empty($post) || empty($_POST)) return;
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $post_id;
		if (is_int(wp_is_post_revision($post))) return $post_id;
		if (is_int(wp_is_post_autosave($post))) return $post_id;
		if (empty($_POST['malinky_activities_save_meta_data_nonce']) || ! wp_verify_nonce($_POST['malinky_activities_save_meta_data_nonce'], 'malinky_activities_save_meta_data')) return $post_id;
		if (!current_user_can('edit_post', $post_id)) return $post_id;
		if ($post->post_type != 'activities') return $post_id;

		//get all meta box details
		$meta_boxes = $this->malinky_activities_set_meta_boxes();
		foreach ($meta_boxes as $key => $field) {
			//if the meta box has a validation rule
			if (!empty($meta_boxes[$key]['validation_rules'])) {
				//for each validation test if its empty and add an error to the option array
				//THIS ONLY CURRENTLY WORKS FOR REQUIRED AS NO OTHER VALIDATION SET UP HERE
				foreach ($meta_boxes[$key]['validation_rules'] as $valid_key => $valid_rule) {
					if(!isset($_POST[$key]) || empty($_POST[$key])) {
						$activity_error = get_option('malinky_activity_notice_' . $post_id);
						$activity_error[$key] = 'You have left the ' . strtolower($meta_boxes[$key]['label']) . ' empty.';
						update_option('malinky_activity_notice_' . $post_id, $activity_error);
					}
				}
			}
		}

		$is_notice = get_option('malinky_activity_notice_' . $post->ID);
		//if the notice exists
		if ($is_notice !== false) {
			//need to remove save_post action due to wordpress bug that creates an infinite loop when using wp_update_post
			//see http://codex.wordpress.org/Plugin_API/Action_Reference/save_post
			remove_action( 'save_post', array($this, 'malinky_activities_save_post'), 1, 2);
			//make post draft if errors as it is always saved either way
		    $post = array( 'ID' => $post_id, 'post_status' => 'draft' );
			wp_update_post($post);
			//add save_post action back in
			add_action( 'save_post', array($this, 'malinky_activities_save_post'), 1, 2);
		}

		do_action('malinky_activities_save_job_listing', $post_id, $post);
	}

	public function save_activity_data($post_id, $post)
	{
		global $wpdb;

		foreach ($this->malinky_activities_set_meta_boxes() as $key => $field) {
			switch($field['type']) {
				case 'editor':
					if (isset($_POST[ $key ])) {		
						update_post_meta($post_id, '_' . $key, strip_tags($_POST[ $key ], '<strong><p><br><em><ul><ol><li>'));
					}
				break;
				case 'checkbox':
					if (empty($_POST[ $key ])) {
						update_post_meta($post_id, '_' . $key, 0);
					} else {
						update_post_meta($post_id, '_' . $key, sanitize_text_field($_POST[ $key ]));
					}
				break;				
				default:
					if (isset($_POST[ $key ])) {
						update_post_meta($post_id, '_' . $key, sanitize_text_field($_POST[ $key ]));
					}
			}
		}
	}
}
new Malinky_Activities_Meta_Boxes();