<?php
if (! defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * --------------------
 * REARRANGE ADMIN MENU
 * --------------------
 */
add_action('admin_menu', 'malinky_activities_admin_menu');
function malinky_activities_admin_menu()
{
    global $menu;
    //Move unwanted normal menus use 30 below as big gap of used here
    $menu[30] = $menu[5];
    $menu[31] = $menu[10];
    $menu[32] = $menu[65];
    $menu[33] = $menu[75];
    $menu[34] = $menu[80];
    unset($menu[5]);
    unset($menu[10]);
    unset($menu[65]);
    unset($menu[75]);
    unset($menu[80]);

    //Move separator
    $menu[3] = $menu[4];
    unset($menu[4]);
    
    //Move activities
    $menu[4] = $menu[26];
    unset($menu[26]);         
    
    //Move user
    $menu[6] = $menu[70];
    unset($menu[70]);        

    //Move parents
    //[7] Set in malinky-activities-admin-parents-page.php

    //Move vouchers
    //[9] Set in malinky-activities-admin-vouchers-page.php

    //Move invoices
    //[10] Set in malinky-activities-admin-invoices-page.php

    //Move unpaid-invoices
    //[11] Set in malinky-activities-admin-unpaid-invoices-page.php

    //Move activity emails
    $menu[12] = $menu[27];
    unset($menu[27]);

    //Move coach emails
    $menu[13] = $menu[28];
    unset($menu[28]);    

    //Move activity pages
    $menu[14] = $menu[20];
    unset($menu[20]);  

    //New separator
    $menu[15] = $menu[59];
    unset($menu[59]);

}

/**
 * -----------------------------
 * ADD COLUMNS TO WP-ADMIN USERS
 * -----------------------------
 */
add_filter('manage_users_columns', 'malinky_activities_user_columns');
function malinky_activities_user_columns($columns)
{
    $columns['activities']      = 'Activities';
    $columns['validated']       = 'Validated';
    $columns['payment_status']  = 'Payment Status';
    unset($columns['posts']);
    return $columns;
}

/**
 * --------------------------
 * ADD DATA TO WP-ADMIN USERS
 * --------------------------
 */
add_action('manage_users_custom_column', 'malinky_activities_user_column_data', 10, 3);
function malinky_activities_user_column_data($value, $column_name, $user_id)
{
    if ($column_name != 'activities' && $column_name != 'validated' && $column_name != 'payment_status')
        return $value;

    if ($column_name == 'activities') {
        global $wp_query;
        $posts = query_posts('post_type=activities&author=' . $user_id . '&order=ASC');
        $posts_count = count($posts);
        $posts_count = '<a href="' . esc_url( site_url('/wp-admin/edit.php?author=' . $user_id . '&post_type=activities') ) . '">' . $posts_count . '</a>';
        return $posts_count;    
    }

    if ($column_name == 'validated') {
        $validated = get_user_meta($user_id, '_validated_user', true);
        return $validated == true ? 'Yes' : 'No';
    }

    if ($column_name == 'payment_status') {
        $payment_status = get_user_meta($user_id, '_coach_stripe_payment_status', true);
        return $payment_status == true ? 'Active' : 'Inactive';
    }
    
}

/**
 * --------------------------------------
 * ADD SORTABLE COLUMNS TO WP-ADMIN USERS
 * --------------------------------------
 */
add_filter('manage_users_sortable_columns', 'malinky_activities_user_sortable_columns');
function malinky_activities_user_sortable_columns($columns)
{
    $columns['activities']      = 'activities';
    $columns['validated']       = 'validated';
    $columns['payment_status'] 	= 'payment_status';
    return $columns;
}

/**
 * -------------------------------------------------------
 * CHANGE TO WP-ADMIN USERS EDIT PAGE / COACH PROFILE PAGE
 * -------------------------------------------------------
 */
add_action( 'personal_options', 'my_show_extra_profile_fields' );
function my_show_extra_profile_fields($user) {

    global $user_id;
    global $pagenow;

    $caps = get_user_meta($user_id, 'wp_capabilities', true);
    $roles = array_keys((array)$caps);

    if (in_array('coach', $roles) && $pagenow == 'user-edit.php') {

    echo    '<table class="form-table">' . 
            '<tbody>' . 
            '<tr>' . 
            '<th>Name</th>' . 
            '<td>' . get_user_meta($user->ID, 'first_name', true) . ' ' . get_user_meta($user->ID, 'last_name', true) . '</td>' . 
            '</tr>' . 
            '</tbody>' . 
            '</table>' . 
            '<table class="form-table">' . 
            '<tbody>' . 
            '<tr>' . 
            '<th>Company Name</th>' . 
            '<td>' . get_user_meta($user->ID, '_coach_company_name', true) . '</td>' . 
            '</tr>' . 
            '</tbody>' . 
            '</table>' . 
            '<table class="form-table">' . 
            '<tbody>' . 
            '<tr>' . 
            '<th>Address</th>' . 
            '<td>' . get_user_meta($user->ID, '_coach_address', true) . '<br />' . get_user_meta($user->ID, '_coach_town', true) . '<br />' . ucwords(get_user_meta($user->ID, '_coach_county', true)) . '<br />' . get_user_meta($user->ID, '_coach_postcode', true) . '</td>' . 
            '</tr>' . 
            '</tbody>' . 
            '</table>' . 
            '<table class="form-table">' . 
            '<tbody>' . 
            '<tr>' . 
            '<th>Phone</th>' . 
            '<td>' . get_user_meta($user->ID, '_coach_phone', true) . '</td>' . 
            '</tr>' . 
            '</tbody>' . 
            '</table>' . 
            '<table class="form-table">' . 
            '<tbody>' . 
            '<tr>' . 
            '<th>Social Media</th>' . 
            '<td>';
            echo 'Website - ' . get_user_meta($user->ID, '_coach_website', true) . '<br />';
            echo 'Facebook - ' . get_user_meta($user->ID, '_coach_facebook', true) . '<br />';
            echo 'Twitter - ' . get_user_meta($user->ID, '_coach_twitter', true) . '<br />';
            echo 'Google Plus - ' . get_user_meta($user->ID, '_coach_googleplus', true) . '<br />';
    echo    '</td>' .                                     
            '</tr>' . 
            '</tbody>' . 
            '<table class="form-table">' . 
            '<tbody>' . 
            '<tr>' . 
            '<th>Payment Status</th>' . 
            '<td>';
    echo    get_user_meta($user->ID, '_coach_stripe_payment_status', true) == true ? 'Active' : 'Inactive';        
    echo    '</td>' .             
            '</tr>' . 
            '</tbody>' . 
            '</table>';

            remove_action('admin_color_scheme_picker', 'admin_color_scheme_picker');
            ?>
            <script type="text/javascript">
              jQuery(document).ready(function($){
                //remove personal options
                $("#your-profile .form-table:eq(0), #your-profile h3:first").remove();
                $("#your-profile .form-table:eq(6), #your-profile h3:eq(0)").remove();
                $("#your-profile .form-table:last, #your-profile h3:last").remove();
                $("#your-profile label[for=description]").parent().parent().remove();
                $("#your-profile label[for=url]").parent().parent().remove();
                $("#your-profile label[for=googleplus]").parent().parent().remove();
                $("#your-profile label[for=twitter]").parent().parent().remove();
                $("#your-profile label[for=facebook]").parent().parent().remove();
                $("h3").remove();
              });
            </script>

        <?php
    }

}

/**
 * --------------------------------------
 * ADD COLUMNS TO WP-ADMIN ACTIVITIES CPT
 * --------------------------------------
 */
add_filter('manage_activities_posts_columns', 'malinky_activities_activities_columns');
function malinky_activities_activities_columns($posts_columns)
{
	unset($posts_columns['author']);
	unset($posts_columns['date']);
    $posts_columns['coach'] 			= 'Coach';
    $posts_columns['activity_date'] 	= 'Date Added';
    $posts_columns['activity_format']   = 'Session Format';
    return $posts_columns;
}

/**
 * -----------------------------------
 * ADD DATA TO WP-ADMIN ACTIVITIES CPT
 * -----------------------------------
 */
add_action('manage_activities_posts_custom_column', 'malinky_activities_activities_columns_data', 10, 3);
function malinky_activities_activities_columns_data($column_name, $post_id)
{
    global $post;
    switch ($column_name) {
    case 'coach' :
        $coach_name = get_the_author_meta('display_name', $post->post_author);
        $coach_name = '<a href="' . esc_url( site_url('/wp-admin/edit.php?author=' . $post->post_author . '&post_type=activities') ) . '">' . $coach_name . '</a>';
        echo $coach_name;
        break;  
	case 'activity_date' :
		echo get_the_time('d/m/Y', $post_id);
        break;
    case 'activity_format' :
        the_activity_format();           
        break;                                 
    }
}

/**
 * -----------------------------------------------
 * ADD SORTABLE COLUMNS TO WP-ADMIN ACTIVITIES CPT
 * -----------------------------------------------
 */
add_filter('manage_edit-activities_sortable_columns', 'malinky_activities_activities_sortable_columns');
function malinky_activities_activities_sortable_columns($columns)
{
	$columns['coach'] 			= 'coach';
    $columns['activity_date'] 	= 'activity_date';
    $columns['activity_format'] = 'activity_format';
    return $columns;
}

/**
 * ---------------------------------------------------------------------
 * ACTUALLY PERFORM THE SORT OF COLUMNS WP-ADMIN ACTIVITIES TAXONOMY CPT
 * ---------------------------------------------------------------------
 */
add_filter('posts_clauses', 'malinky_activities_activities_taxonomy_sort', 10, 2);
function malinky_activities_activities_taxonomy_sort($clauses, $wp_query)
{
    global $wpdb;
    if(isset($wp_query->query['orderby']) && $wp_query->query['orderby'] == 'activity_format') {
        $clauses['join'] = "LEFT OUTER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id LEFT OUTER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id) LEFT OUTER JOIN {$wpdb->terms} USING (term_id)";
        $clauses['where'] .= "AND (taxonomy = 'malinky_activities_format' OR taxonomy IS NULL)";
        $clauses['groupby'] = "object_id";
        $clauses['orderby'] = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC)";
        if(strtoupper($wp_query->get('order')) == 'ASC'){
            $clauses['orderby'] .= 'ASC';
        } else{
            $clauses['orderby'] .= 'DESC';
        }
    }
    return $clauses;
}


/**
 * ------------------------------------------
 * ADD COLUMNS TO WP-ADMIN ACTIVITY EMAIL CPT
 * ------------------------------------------
 */
add_filter('manage_activity_emails_posts_columns', 'malinky_activities_activity_emails_columns');
function malinky_activities_activity_emails_columns($posts_columns)
{
    unset($posts_columns['date']);
    $posts_columns['email_type']        = 'Email Type';
    $posts_columns['email_date']        = 'Email Date';
    $posts_columns['email_days']        = 'Email Days';
    $posts_columns['activity_format']   = 'Activity Format';
    return $posts_columns;
}

/**
 * ---------------------------------------
 * ADD DATA TO WP-ADMIN ACTIVITY EMAIL CPT
 * ---------------------------------------
 */
add_action('manage_activity_emails_posts_custom_column', 'malinky_activities_activity_emails_columns_data', 10, 3);
function malinky_activities_activity_emails_columns_data($column_name, $post_id)
{
    switch ($column_name) {
    case 'email_type' :
        the_field('email_type', $post_id);
        break;
    case 'email_date' :
        if (get_field('email_date')) {
            echo date('d/m/Y', strtotime(get_field('email_date', $post_id)));
        } else {
            echo '-';
        }
        break;
    case 'email_days' :
        if (get_field('email_days')) {
            the_field('email_days');
        } else {
            echo '-';
        }
        break; 
    case 'activity_format' :
        if (get_field('activity_format')) {
            $format = get_term(get_field('activity_format'), 'malinky_activities_format');
            echo $format->name;
        } else {
            echo '-';
        }
        break;                
    }
}

/**
 * ------------------------------------------------
 * ADD SORTABLE COLUMNS WP-ADMIN ACTIVITY EMAIL CPT
 * ------------------------------------------------
 */
add_filter('manage_edit-activity_emails_sortable_columns', 'malinky_activities_activity_emails_sortable_columns');
function malinky_activities_activity_emails_sortable_columns($columns)
{
    $columns['email_type'] = 'email_type';
    return $columns;
}

/**
 * ----------------------------------------------------------------
 * ACTUALLY PERFORM THE SORT OF COLUMNS WP-ADMIN ACTIVITY EMAIL CPT
 * ----------------------------------------------------------------
 */
if ( is_admin() ) {
    add_filter('request', 'malinky_activities_activity_emails_sort');
}

function malinky_activities_activity_emails_sort( $vars ) {
 	
    //Dont do anything if we are not on the activity_emails CPT
    if ('activity_emails' != $vars['post_type']) return $vars;
     
    //Dont do anything if no orderby parameter is set
    if (!isset($vars['orderby'])) return $vars;
     
    // Check if the orderby parameter matches one of our sortable columns
    if ($vars['orderby'] == 'email_type') {
        // Add orderby meta_value and meta_key parameters to the query
        $vars = array_merge($vars, 
        	array(
            'meta_key' => $vars['orderby'],
            'orderby' => 'meta_value'
	        )
      	);
    }
    return $vars;
}

/**
 * ------------------------------------------
 * ADD COLUMNS TO WP-ADMIN COACH EMAIL CPT
 * ------------------------------------------
 */
add_filter('manage_coach_emails_posts_columns', 'malinky_activities_coach_emails_columns');
function malinky_activities_coach_emails_columns($posts_columns)
{
    unset($posts_columns['date']);
    $posts_columns['email_type']        = 'Email Type';
    $posts_columns['email_date']        = 'Email Date';
    $posts_columns['email_days']        = 'Email Days';
    return $posts_columns;
}

/**
 * ---------------------------------------
 * ADD DATA TO WP-ADMIN COACH EMAIL CPT
 * ---------------------------------------
 */
add_action('manage_coach_emails_posts_custom_column', 'malinky_activities_coach_emails_columns_data', 10, 3);
function malinky_activities_coach_emails_columns_data($column_name, $post_id)
{
    switch ($column_name) {
    case 'email_type' :
        the_field('email_type', $post_id);
        break;
    case 'email_date' :
        if (get_field('email_date')) {
            echo date('d/m/Y', strtotime(get_field('email_date', $post_id)));
        } else {
            echo '-';
        }
        break;
    case 'email_days' :
        if (get_field('email_days')) {
            the_field('email_days');
        } else {
            echo '-';
        }
        break;               
    }
}

/**
 * ------------------------------------------------
 * ADD SORTABLE COLUMNS WP-ADMIN COACH EMAIL CPT
 * ------------------------------------------------
 */
add_filter('manage_edit-coach_emails_sortable_columns', 'malinky_activities_coach_emails_sortable_columns');
function malinky_activities_coach_emails_sortable_columns($columns)
{
    $columns['email_type'] = 'email_type';
    return $columns;
}

/**
 * ----------------------------------------------------------------
 * ACTUALLY PERFORM THE SORT OF COLUMNS WP-ADMIN COACH EMAIL CPT
 * ----------------------------------------------------------------
 */
if ( is_admin() ) {
    add_filter('request', 'malinky_activities_coach_emails_sort');
}

function malinky_activities_coach_emails_sort( $vars ) {
    
    //Dont do anything if we are not on the coach_emails CPT
    if ('coach_emails' != $vars['post_type']) return $vars;
     
    //Dont do anything if no orderby parameter is set
    if (!isset($vars['orderby'])) return $vars;
     
    // Check if the orderby parameter matches one of our sortable columns
    if ($vars['orderby'] == 'email_type') {
        // Add orderby meta_value and meta_key parameters to the query
        $vars = array_merge($vars, 
            array(
            'meta_key' => $vars['orderby'],
            'orderby' => 'meta_value'
            )
        );
    }
    return $vars;
}

/**
 * -----------------------------------------
 * ADD AUTHOR TO WP-ADMIN ADD ACTIVITIES CPT
 * -----------------------------------------
 */
add_filter('wp_dropdown_users', 'malinky_activities_activities_add_cpt_author_list');
function malinky_activities_activities_add_cpt_author_list($output) 
{
    global $post;

	$current_coach = $post->post_author;

    if($post->post_type == 'activities')
    {
        $coaches = get_users(array('role'=>'coach'));        
        $coach_dropdown = '<select id="post_author_override" name="post_author_override" class="">';
    	foreach($coaches as $coach) {
    		if ($coach->ID == $current_coach) {
				$coach_dropdown .= '<option value="' . $coach->ID . '" selected>' . $coach->display_name . ' - ' . $coach->user_email . '</option>';
    		} else {
				$coach_dropdown .= '<option value="' . $coach->ID . '">' . $coach->display_name . ' - ' . $coach->user_email . '</option>';
    		}
        	
    	}
    	$coach_dropdown .= '</select>';
    	return $coach_dropdown;
 	}
 	return $output;
}