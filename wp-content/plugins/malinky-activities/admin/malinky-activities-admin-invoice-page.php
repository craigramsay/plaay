<?php
if (! defined('ABSPATH')) exit; // Exit if accessed directly

add_action('admin_menu', 'malinky_activities_admin_invoice_page');

function malinky_activities_admin_invoice_page()
{

	add_submenu_page(
		NULL,
		'Invoice',
		'Invoice',
		'manage_invoices',
		'admin-invoice-page',
		'malinky_activities_admin_invoice_page_display'
	);

}

function malinky_activities_admin_invoice_page_display()
{
	?>
	<div class="wrap" id="profile-page">
	<?php

	if (!isset($_GET['invoice_no'])) {
		echo 'There has been a problem accessing the invoice information. Please try again.';
		exit();
	} 

	if (!preg_match('/^(ch_)[a-zA-Z0-9]{24}+$/', $_GET['invoice_no'])) {
		echo 'There has been a problem accessing the invoice information. Please try again.';
		exit();
	}

	if (!isset($_GET['coach_id'])) {
		echo 'There has been a problem accessing the invoice information. Please try again.';
		exit();
	} 

	if (!absint($_GET['coach_id'])) {
		echo 'There has been a problem accessing the voucher information. Please try again.';
		exit();
	}

	

	$invoice_no = $_GET['invoice_no'];
	$coach_id = absint($_GET['coach_id']);

	$errors = new WP_Error();
	global $wpdb;

	$invoice = $wpdb->get_results(
		$wpdb->prepare(
		"SELECT malinky_stripe_payments.id, malinky_stripe_payments.voucher_id, malinky_stripe_payments.stripe_charge_id, malinky_stripe_payments.stripe_charge_amount, malinky_stripe_payments.stripe_charge_created, malinky_voucher_codes.voucher_code_activated_date, malinky_voucher_codes.voucher_code, malinky_voucher_codes.voucher_cost FROM malinky_stripe_payments LEFT JOIN malinky_voucher_codes ON malinky_stripe_payments.voucher_id = malinky_voucher_codes.id WHERE stripe_charge_id = %s", $invoice_no
		)			
	);
	
	?>

	<table class="coach_invoice">
		<tr><td colspan="3"><img src="<?php echo get_template_directory_uri() . '/img/logo_dark.png'; ?>" class="admin_invoice_logo" /></td></tr>
		<tr><td colspan="3"><h1 class="admin_invoice_h1">Invoice</h1></td></tr>
		<tr>
			<td class="extra_padding_bottom">
			PLAAY<br />
			Unit 11<br />
			Hove Business Centre<br />
			Fonthill Road<br />
			Hove<br />East Sussex<br />
			BN3 6HA
			</td>
			<td colspan="2" class="td_nested extra_padding_bottom">
				<table>
					<tr><td><span class="bold_text">Invoice No.</span></td> <td><?php esc_html_e($invoice[0]->stripe_charge_id); ?></td></tr>
					<tr><td><span class="bold_text">Date</span></td> <td><?php esc_html_e(date('d/m/Y', strtotime($invoice[0]->stripe_charge_created))); ?></td></tr>
				</table>
			</td>
		</tr>
		<tr><td class="bold_text">Invoice To</td></tr>
		<tr>
			<td class="extra_padding_bottom">
			<?php echo get_user_meta($coach_id, '_coach_company_name', true); ?><br />
			<?php echo get_user_meta($coach_id, '_coach_address', true); ?><br />
			<?php echo get_user_meta($coach_id, '_coach_town', true); ?><br />
			<?php echo ucwords(get_user_meta($coach_id, '_coach_county', true)); ?><br />
			<?php echo get_user_meta($coach_id, '_coach_postcode', true); ?>
			</td>
		</tr>
		<tr><td class="bold_text">Code</td><td class="bold_text">Date Activated</td><td class="bold_text">Cost</td></tr>
		<?php foreach ($invoice as $k => $v) { ?>
			<tr>
				<td><a href="<?php echo esc_url(add_query_arg(array('page' => 'admin-voucher-page', 'voucher_id' => $invoice[$k]->voucher_id), site_url('wp-admin/admin.php'))); ?>"><?php esc_html_e($invoice[$k]->voucher_code); ?></a></td>
				<td><?php esc_html_e(date('d/m/Y', strtotime($invoice[$k]->voucher_code_activated_date))); ?></td>
				<td>&pound;<?php esc_html_e(number_format($invoice[$k]->voucher_cost/100, 2)); ?></td>
			</tr>
		<?php } ?>
		<tr><td class="extra_padding_top"></td><td class="extra_padding_top"><span class="bold_text">Total:</span> </td><td class="coach_invoice_total extra_padding_top">&pound;<?php esc_html_e(number_format($invoice[0]->stripe_charge_amount/100, 2)); ?></td></tr>
	</table>
<?php
}