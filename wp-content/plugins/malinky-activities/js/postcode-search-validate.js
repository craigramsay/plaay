/**
 * --------------------------
 * POSTCODE SEARCH VALIDATION
 * --------------------------
 */
jQuery(document).ready(function($){

    jQuery.validator.addMethod("valid_postcode", function(value, element) {
        return this.optional(element) || /^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$/i.test(value);
    }, "Please enter a valid postcode.");                
 
    // validate contact form on keyup and submit
    $("#malinky_postcode_search_form").validate({
 
        //set the rules for the fild names
        rules: { 
            home_postcode_search: {
                required: true,
                valid_postcode: true
            }
        },
 
        //set error messages
        messages: {   
            home_postcode_search: {
                required: "Please enter a valid postcode.",
                valid_postcode: "Please enter a valid postcode."
            }                                                                         
        },
 
        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
            error.insertAfter(element);
            if ($(element).hasClass('error')) {
                $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');
            }
        },

        success: function(label) {
            $(label).parent().next().children('.field_error_icon').addClass('field_error_tick');
            $(label).parents().eq(1).next().children('.field_error_icon').addClass('field_error_tick');
            label.css('margin-top', '0');
            label.css('margin-bottom', '0');
        },

        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_tick');            
            $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');            
            $(element).next('p.error').css('margin-top', '1em');
            $(element).next('p.error').css('margin-bottom', '1em');
        },

        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_cross');
            $(element).parent().next().children('.field_error_icon').addClass('field_error_tick');
            $(element).next('p.error').css('margin-top', '0');
            $(element).next('p.error').css('margin-bottom', '0');          
        }    

    });

    // validate contact form on keyup and submit
    $("#postcode_search_form").validate({
 
        //set the rules for the fild names
        rules: { 
            home_postcode_search: {
                required: true,
                valid_postcode: true
            }
        },
 
        //set error messages
        messages: {   
            home_postcode_search: {
                required: "Please enter a valid postcode.",
                valid_postcode: "Please enter a valid postcode."
            }                                                                         
        },
 
        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
            $(element).addClass('home_postcode_search_error');
        }  

    });
    
    // validate contact form on keyup and submit
    $("#malinky_postcode_search_form_home").validate({
 
        //set the rules for the fild names
        rules: { 
            home_postcode_search: {
                required: true,
                valid_postcode: true
            }
        },
 
        //set error messages
        messages: {   
            home_postcode_search: {
                required: "Please enter a valid postcode.",
                valid_postcode: "Please enter a valid postcode."
            }                                                                         
        },
 
        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
            $(element).addClass('home_postcode_search_error');
        }  

    });

});