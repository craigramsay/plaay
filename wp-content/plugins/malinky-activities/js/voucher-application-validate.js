/**
 * ------------------------------
 * VOUCHER APPLICATION FORM VALIDATION
 * ------------------------------
 */
jQuery(document).ready(function($){

    //ensure any single options are already checked to save the user doing it
    var voucher_application_category = $("#fieldset_voucher_application_options").find("input[name='voucher_application_category[]']").length;
    if (voucher_application_category == 1) {
        $("input[name='voucher_application_category[]").attr('checked','checked');
    }        
    var voucher_application_voucher = $("#fieldset_voucher_application_options").find("input[name='voucher_application_voucher[]']").length;
    if (voucher_application_voucher == 1) {
        $("input[name='voucher_application_voucher[]").attr('checked','checked');
    }    
    var voucher_application_age_group = $("#fieldset_voucher_application_options").find("input[name='voucher_application_age_group[]']").length;
    if (voucher_application_age_group == 1) {
        $("input[name='voucher_application_age_group[]").attr('checked','checked');
    }        

    jQuery.validator.addMethod("valid_select", function(value, element) {
        return this.optional(element) || value != 'please_choose';
    }, "Please Choose.");

    jQuery.validator.addMethod("valid_county", function(value, element) {
        return this.optional(element) || /^[a-z\-]+$/.test(value);
    }, "Small letters and hyphen only please"); 

    jQuery.validator.addMethod("valid_phone", function(value, element) {
        return this.optional(element) || /^[0-9 ]+$/i.test(value);
    }, "Numbers and space only please");                    
 
    jQuery.validator.addMethod("valid_integer_or_please_choose", function(value, element) {
        return this.optional(element) || value == 'please_choose' || /^[0-9]+$/i.test(value);
    }, "Small letters and hyphen only please"); 

    // validate contact form on keyup and submit
    $("#malinky_voucher_application_form").validate({
 
        //set the rules for the field names
        rules: { 
            parent_name: {
                required: true
            },
            parent_county: {
                valid_select: true,
                valid_county: true
            },
            parent_email: {
                required: true,
                email: true
            },                                   
            parent_phone: {
                required: true,
                valid_phone: true  
            },
            'voucher_application_category[]': {
                required: true,
                maxlength: 1
            },
            'voucher_application_voucher[]': {
                required: true,
                maxlength: 1
            },
            'voucher_application_age_group[]': {
                required: true,
                maxlength: 1
            },                        
            child_birth_month: {
                valid_integer_or_please_choose: true
            },        
            terms_and_conditions: {
                required: true
            }
        },
 
        //set error messages
        messages: {   
            parent_name: "Please enter your name.", 
            parent_county: {
                valid_select: "Please choose a county.",
                valid_county: "Please choose a county."
            },
            parent_email: {
                required: "Please enter a valid email address.",
                email: "Please enter a valid email address."
            },
            parent_phone: {
                required: "Please enter a valid phone number.",
                valid_phone: "Please enter a valid phone number."
            },            
            'voucher_application_category[]': {
                required: "Please select one category.",
                maxlength: "Please select only one category."
            },
            'voucher_application_voucher[]': {
                required: "Please select one voucher.",
                maxlength: "Please select only one voucher."
            },
            'voucher_application_age_group[]': {
                required: "Please select one age group.",
                maxlength: "Please select only one age group."
            },
            child_birth_month: "Please choose a birth month.",
            terms_and_conditions: "Please accept our terms and conditions."
        },
 
        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
            if ($(element).is("input[type=checkbox]")) {
                error.insertAfter(element.parent().parent().children('.checkbox_block').last());
            } else {
                error.insertAfter(element);
            }
            if ( $(element).is("input[type=checkbox]") && $(element).hasClass('error') ) {  
                $(element).parent().parent().next().children('.field_error_icon').addClass('field_error_cross');
            } else if ($(element).hasClass('error')) {
                $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');
            }
        },

        success: function(label) {
            $(label).parent().next().children('.field_error_icon').addClass('field_error_tick');
            //label.css('margin-top', '0');
            //label.css('margin-bottom', '0');
        },

        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_tick');            
            $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');            
            //$(element).next('p.error').css('margin-top', '1em');
            //$(element).next('p.error').css('margin-bottom', '1em');
            if ($(element).is("input[type=checkbox]")) {
                $(element).parent().parent().next().children('.field_error_icon').removeClass('field_error_tick');            
                $(element).parent().parent().next().children('.field_error_icon').addClass('field_error_cross');             
            }            
        },

        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_cross');
            $(element).parent().next().children('.field_error_icon').addClass('field_error_tick');
            //$(element).next('p.error').css('margin-top', '0');
            //$(element).next('p.error').css('margin-bottom', '0');
            if ($(element).is("input[type=checkbox]")) {
                $(element).parent().parent().next().children('.field_error_icon').removeClass('field_error_cross');
                $(element).parent().parent().next().children('.field_error_icon').addClass('field_error_tick');
            }            
        }    

    });

});