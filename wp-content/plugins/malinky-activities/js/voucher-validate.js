/**
 * ------------------------------
 * CREATE VOUCHER FORM VALIDATION
 * ------------------------------
 */
jQuery(document).ready(function($){                   
 
    // validate contact form on keyup and submit
    $("#malinky_voucher_form").validate({
 
        //set the rules for the field names
        rules: { 
            parent_email: {
                required: true,
                email: true
            }
        },
 
        //set error messages
        messages: {   
            parent_email: {
                required: "Please enter a valid email address.",
                email: "Please enter a valid email address."
            }
        },
 
        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
            error.insertAfter(element);
            if ($(element).hasClass('error')) {
                $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');
            }
        },

        success: function(label) {
            $(label).parent().next().children('.field_error_icon').addClass('field_error_tick');
            //label.css('margin-top', '0');
            //label.css('margin-bottom', '0');
        },

        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_tick');            
            $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');            
            //$(element).next('p.error').css('margin-top', '1em');
            //$(element).next('p.error').css('margin-bottom', '1em');
        },

        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_cross');
            $(element).parent().next().children('.field_error_icon').addClass('field_error_tick');
            //$(element).next('p.error').css('margin-top', '0');
            //$(element).next('p.error').css('margin-bottom', '0');
        }    

    });

});