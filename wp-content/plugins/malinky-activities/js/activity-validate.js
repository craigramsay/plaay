/**
 * -----------------------------
 * ADD AND EDIT ACTIVITY VALIDATION
 * -----------------------------
 */
jQuery(document).ready(function($){
    
    //allows empty tiny mce to validate
    jQuery.validator.setDefaults({
        ignore: ''
    });

    jQuery.validator.addMethod("valid_select", function(value, element) {
        return this.optional(element) || value != 'please_choose';
    }, "Please Choose.");

    jQuery.validator.addMethod("valid_county", function(value, element) {
        return this.optional(element) || /^[a-z\-]+$/.test(value);
    }, "Small letters and hyphen only please"); 

    jQuery.validator.addMethod("valid_postcode", function(value, element) {
        return this.optional(element) || /^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$/i.test(value);
    }, "Valid postcode please");                
 
    jQuery.validator.addMethod("valid_integer", function(value, element) {
        return this.optional(element) || /^[0-9]+$/i.test(value);
    }, "Numbers only please"); 

    // validate contact form on keyup and submit
    $("#malinky_activity_form").validate({
 
        //set the rules for the fild names
        rules: { 
            activity_name: {
                required: true
            },
            activity_format: {
                valid_select: true,
                valid_county: true
            },            
            'activity_category[]': {
                required: true
            },
            'activity_voucher[]': {
                required: true
            },
            'activity_age_group[]': {
                required: true
            },                                   
            activity_description: {
                required: true  
            },
            activity_distance_travelled: {
                valid_select: true,
                valid_integer: true
            },            
            activity_address: {
                required: true
            },
            activity_town: {
                required: true
            },  
            activity_county: {
                valid_select: true,
                valid_county: true
            },
            activity_postcode: {
                required: true,
                valid_postcode: true
            }
        },
 
        //set error messages
        messages: {   
            activity_name: "Please enter an activity name.",
            activity_format: {
                valid_select: "Please choose a session format.",
                valid_county: "Please choose a session format."
            },              
            'activity_category[]': "Please choose atleast one category.",
            'activity_voucher[]': "Please choose atleast one voucher.",
            'activity_age_group[]': "Please choose atleast one age group.",
            activity_description: "Please enter an activity description.",                    
            activity_distance_travelled: {
                valid_select: "Please choose a distance travelled.",
                valid_integer: "Please choose a distance travelled."
            }, 
            activity_address: "Please enter your address.", 
            activity_town: "Please enter your town.",  
            activity_county: {
                valid_select: "Please choose a county.",
                valid_county: "Please choose a county."
            },  
            activity_postcode: {
                required: "Please enter a valid postcode.",
                valid_postcode: "Please enter a valid postcode."
            }                                                                         
        },
 
        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
            if ($(element).is("textarea")) {
                error.insertAfter(element.parent());    
            } else if ($(element).is("input[type=checkbox]")) {
                error.insertAfter(element.parent().parent().children('.checkbox_block').last());
            } else {
                error.insertAfter(element);
            }
            if ( $(element).is("textarea") && $(element).hasClass('error') ) {
                $(element).parents().eq(2).next().children('.field_error_icon').addClass('field_error_cross');
            } else if ( $(element).is("input[type=checkbox]") && $(element).hasClass('error') ) {  
                $(element).parent().parent().next().children('.field_error_icon').addClass('field_error_cross');
            } else if ($(element).hasClass('error')) {
                $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');
            }
        },

        success: function(label) {
            $(label).parent().next().children('.field_error_icon').addClass('field_error_tick');
            $(label).parents().eq(1).next().children('.field_error_icon').addClass('field_error_tick');
            $(label).parent().parent().next().children('.field_error_icon').addClass('field_error_tick');
            label.css('margin-top', '0');
            label.css('margin-bottom', '0');
        },

        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_tick');            
            $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');            
            //$(element).next('p.error').css('margin-top', '1em');
            //$(element).next('p.error').css('margin-bottom', '1em');
            if ($(element).is("textarea")) {
                $(element).parents().eq(2).next().children('.field_error_icon').removeClass('field_error_tick');            
                $(element).parents().eq(2).next().children('.field_error_icon').addClass('field_error_cross');            
                //$(element).parent().next('p.error').css('margin-top', '1em');
                //$(element).parent().next('p.error').css('margin-bottom', '1em');
            }
            if ($(element).is("input[type=checkbox]")) {
                $(element).parent().parent().next().children('.field_error_icon').removeClass('field_error_tick');            
                $(element).parent().parent().next().children('.field_error_icon').addClass('field_error_cross');             
            }            
        },

        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_cross');
            $(element).parent().next().children('.field_error_icon').addClass('field_error_tick');
            //(element).next('p.error').css('margin-top', '0');
            //$(element).next('p.error').css('margin-bottom', '0');
            if ($(element).is("textarea")) {
                $(element).parents().eq(2).next().children('.field_error_icon').removeClass('field_error_cross');            
                $(element).parents().eq(2).next().children('.field_error_icon').addClass('field_error_tick');            
                //$(element).parent().next('p.error').css('margin-top', '0');
                //$(element).parent().next('p.error').css('margin-bottom', '0');
            }  
            if ($(element).is("input[type=checkbox]")) {
                $(element).parent().parent().next().children('.field_error_icon').removeClass('field_error_cross');
                $(element).parent().parent().next().children('.field_error_icon').addClass('field_error_tick');
            }                        
        }    

    });

    //ensure tiny mce is saved before validation otherwise submit must be clicked twice to clear any errors
    $('#submit').click(function() {
        tinymce.triggerSave();       
    });

    //ensure tinymce is validated after each one is deactivated
    if (typeof(tinyMCE) != "undefined") {
        setTimeout(function () {        
            for (var i = 0; i < tinymce.editors.length; i++) {
                tinymce.editors[i].onChange.add(function (editor, e) {
                    tinymce.triggerSave();
                    $("#" + editor.id).valid();
                });
           }
        }, 1000);
    }

});