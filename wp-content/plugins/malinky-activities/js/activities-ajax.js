jQuery(document).ready(function($) {
	
	//Remove all checked on back button
	$('.activities_filter_checkbox_block input[type=checkbox]:checked').removeAttr('checked');

	$('#sidebar_content input[type=checkbox]').live('click', function() {

		var target 				= $('#activities');
		var orderby  			= target.data('orderby');
		var order    			= target.data('order');
		var posts_per_page 		= target.data('posts_per_page');
		var page 				= target.data('page');			
		var distance   			= $('#activity_distance select option:selected').val();	
		
		//set to new value
		var filter_format 		= $(':input[name="filter_format"]:checked').val();
		var filter_age_group 	= $(':input[name="filter_age_group"]:checked').val();		
		var filter_category 	= $(':input[name="filter_category"]:checked').val();
		var filter_voucher 		= $(':input[name="filter_voucher"]:checked').val();

		var data = {
			action: 			'malinky-activities-ajax-submit',
			orderby: 			orderby,
			order: 				order,
			posts_per_page: 	posts_per_page,
			page:				page,			
			distance: 			distance,
			filter_format: 		filter_format,
			filter_age_group: 	filter_age_group,			
			filter_category: 	filter_category,
			filter_voucher: 	filter_voucher
		};

		$.ajax({
			type: 		'POST',
			url: 		MalinkyActivitiesActivitiesAjax.ajaxurl,
			data: 		data,
			success: 	function(response) {
							var result = $.parseJSON(response);
							$('#sidebar_content').replaceWith(result.activities_filter);
							$('#activities').replaceWith(result.activities);
							$('#showing').html(result.showing);
							addthis.toolbox('.addthis_toolbox');							
						}
		});

	});

	$('#activity_sort select').live('change', function() {

		var target 				= $('#activities');
		var orderby  			= target.data('orderby');
		var order    			= target.data('order');
		var posts_per_page 		= target.data('posts_per_page');
		var page 				= target.data('page');			
		var distance   			= $('#activity_distance select option:selected').val();
		var filter_format 		= $(':input[name="filter_format"]:checked').val();
		var filter_age_group 	= $(':input[name="filter_age_group"]:checked').val();		
		var filter_category 	= $(':input[name="filter_category"]:checked').val();
		var filter_voucher 		= $(':input[name="filter_voucher"]:checked').val();

		//set to new value
		orderby 				= $("option:selected", this).val();
		order 					= orderby == 'activity_za' ? 'desc' : 'asc';

		var data = {
			action: 			'malinky-activities-ajax-submit',
			orderby: 			orderby,
			order: 				order,
			posts_per_page: 	posts_per_page,
			page:				page,			
			distance: 			distance,
			filter_format: 		filter_format,
			filter_age_group: 	filter_age_group,			
			filter_category: 	filter_category,
			filter_voucher: 	filter_voucher
		};

		$.ajax({
			type: 		'POST',
			url: 		MalinkyActivitiesActivitiesAjax.ajaxurl,
			data: 		data,
			success: 	function(response) {
							var result = $.parseJSON(response); 
							$('#sidebar_content').replaceWith(result.activities_filter);
							$('#activities').replaceWith(result.activities);
							addthis.toolbox('.addthis_toolbox');							
						}
		});

	});	

	$('#activity_distance select').live('change', function() {
		
		var target 				= $('#activities');
		var orderby  			= target.data('orderby');
		var order    			= target.data('order');
		var posts_per_page 		= target.data('posts_per_page');
		var page 				= target.data('page');			
		var filter_format 		= $(':input[name="filter_format"]:checked').val();
		var filter_age_group 	= $(':input[name="filter_age_group"]:checked').val();		
		var filter_category 	= $(':input[name="filter_category"]:checked').val();
		var filter_voucher 		= $(':input[name="filter_voucher"]:checked').val();		

		//set to new value
		var distance 			= $("option:selected", this).val();

		var data = {
			action: 			'malinky-activities-ajax-submit',
			orderby: 			orderby,
			order: 				order,
			posts_per_page: 	posts_per_page,
			page:				page,			
			distance: 			distance,
			filter_format: 		filter_format,
			filter_age_group: 	filter_age_group,
			filter_category: 	filter_category,
			filter_voucher: 	filter_voucher
		};

		$('.postcode_search_distance').text(distance);
		
		$.ajax({
			type: 		'POST',
			url: 		MalinkyActivitiesActivitiesAjax.ajaxurl,
			data: 		data,
			success: 	function(response) {
							var result = $.parseJSON(response); 
							$('#sidebar_content').replaceWith(result.activities_filter);
							$('#activities').replaceWith(result.activities);
							addthis.toolbox('.addthis_toolbox');
						}
		});

	});	

	$('#activity_per_page select').live('change', function() {

		var target 				= $('#activities');
		var orderby  			= target.data('orderby');
		var order    			= target.data('order');
		var page 				= target.data('page');			
		var distance   			= $('#activity_distance select option:selected').val();		
		var filter_format 		= $(':input[name="filter_format"]:checked').val();
		var filter_age_group 	= $(':input[name="filter_age_group"]:checked').val();
		var filter_category 	= $(':input[name="filter_category"]:checked').val();
		var filter_voucher 		= $(':input[name="filter_voucher"]:checked').val();

		//set to new value
		var posts_per_page 		= $("option:selected", this).val();

		var data = {
			action: 			'malinky-activities-ajax-submit',
			orderby: 			orderby,
			order: 				order,
			posts_per_page: 	posts_per_page,
			page:				page,			
			distance: 			distance,
			filter_format: 		filter_format,
			filter_age_group: 	filter_age_group,			
			filter_category: 	filter_category,
			filter_voucher: 	filter_voucher
		};

		$.ajax({
			type: 		'POST',
			url: 		MalinkyActivitiesActivitiesAjax.ajaxurl,
			data: 		data,
			success: 	function(response) {
							var result = $.parseJSON(response); 
							$('#sidebar_content').replaceWith(result.activities_filter);
							$('#activities').replaceWith(result.activities);
							addthis.toolbox('.addthis_toolbox');
						}
		});

	});	

	$('#activities_pagination a').live('click', function(event) {
		
		event.preventDefault();
		
		var target 				= $('#activities');
		var orderby  			= target.data('orderby');
		var order    			= target.data('order');
		var posts_per_page 		= target.data('posts_per_page');		
		var distance   			= $('#activity_distance select option:selected').val();		
		var filter_format 		= $(':input[name="filter_format"]:checked').val();
		var filter_age_group 	= $(':input[name="filter_age_group"]:checked').val();
		var filter_category 	= $(':input[name="filter_category"]:checked').val();
		var filter_voucher 		= $(':input[name="filter_voucher"]:checked').val();

		//set to new value
		var page 				= $(this).attr('href').substring($(this).attr('href').lastIndexOf('/') + 1);

		var data = {
			action: 			'malinky-activities-ajax-submit',
			orderby: 			orderby,
			order: 				order,
			posts_per_page: 	posts_per_page,
			page: 				page,
			distance: 			distance,
			filter_format: 		filter_format,
			filter_age_group: 	filter_age_group,
			filter_category: 	filter_category,
			filter_voucher: 	filter_voucher
		};

		$.ajax({
			type: 		'POST',
			url: 		MalinkyActivitiesActivitiesAjax.ajaxurl,
			data: 		data,
			success: 	function(response) {
							var result = $.parseJSON(response); 
							$('#sidebar_content').replaceWith(result.activities_filter);
							$('#activities').replaceWith(result.activities);
							addthis.toolbox('.addthis_toolbox');
						}
		});

	});	

	$('a#unset_postcode').live('click', function() {

		var data = {
			action: 'malinky-activities-ajax-unset-postcode'
		};

		$.ajax({
			type: 		'POST',
			url: 		MalinkyActivitiesActivitiesAjax.ajaxurl,
			data: 		data,
			success: 	function(response) {
							var result = $.parseJSON(response); 
							window.location = result.redirect;
						}
		});

	});	
	
});