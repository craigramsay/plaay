jQuery(document).ready(function($){

    /*-----SIDEBAR TOGGLE-----*/               
    $('a.sidebar_mobile_content_button').click(function(e) {
        $(this).next('#sidebar_content').slideToggle(function() {
        });
        e.preventDefault();
    }); 

    /*-----COACH CMS MESSAGES-----*/
    $('.coach_message_error, .coach_message_success').delay(2000).fadeOut(1000); 
    
    /*-----COACH HIDE VOUCHER TYPES BASED ON CHOSEN FORMAT IN ADD AND EDIT ACTIVITY PAGES-----*/
	//Page load
    if ($('#activity_format').val() == 'please_choose') {
    	$('.activity_voucher_parent_js').css('display', 'none');
	}
	//Edit activity page load
    if ($('#activity_format').val() != 'please_choose') {
		var coach_sel_format = $('#activity_format').val();
		$('.checkbox_block input:checkbox').each(function() {
    		if ( ($(this).attr('name') == 'activity_voucher[]') && ($(this).attr('id').indexOf(coach_sel_format) == -1) ) {
    			$(this).parent().css('display', 'none');
    		}
    	})
	}
	//Add activity change
    $('#activity_format').change(function() {
	    if ($('#activity_format').val() == 'please_choose') {
	    	$('.activity_voucher_parent_js').css('display', 'none');
		} else {
			$('.activity_voucher_parent_js').css('display', 'block');
	    	var coach_sel_format = $('#activity_format').val();
	    	$('.checkbox_block input[name="activity_voucher[]"]').parent().css('display', 'block');
	    	$('.checkbox_block input:checkbox').each(function() {
	    		if ( ($(this).attr('name') == 'activity_voucher[]') && ($(this).attr('id').indexOf(coach_sel_format) == -1) ) {
	    			$(this).parent().css('display', 'none');
	    		}
	    	})
    	}
    })

});