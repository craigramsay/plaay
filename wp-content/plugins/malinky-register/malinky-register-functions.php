<?php
/**
 * --------------------
 * REDIRECT AFTER LOGIN
 * --------------------
 */
add_filter('login_redirect','malinky_register_login_form_redirect', 10, 3);
function malinky_register_login_form_redirect($redirect_to, $request, $user)
{
    global $user;
    if (isset($user->roles) && is_array($user->roles)) {
        if(in_array('coach', (array) $user->roles)) {
            return site_url('coach');
        } else {
        	//doesn't work but should return to admin_url()
        	//return $redirect_to;
        	return admin_url();
        }
    }
}

/**
 * ----------------------------
 * REDIRECT THE REGISTER FORM
 * ----------------------------
 */
add_action('login_init','malinky_register_register_form_redirect');
function malinky_register_register_form_redirect()
{
	global $pagenow;
 	if ('wp-login.php' == $pagenow && isset($_GET['action']) && $_GET['action'] == 'register') {
  		wp_safe_redirect(site_url('register'));
  		exit();
  	}
}

/**
 * --------------------------------------------------------------------------------------------------
 * REDIRECT THE PROFILE.PHP FORM FOR A COACH
 * USED ADMIN_HEAD INSTEAD OF ADMIN_INIT BECAUSE OF PLUG IN CONFLICTS
 * --------------------------------------------------------------------------------------------------
 */
add_action('admin_head','malinky_register_profile_form_redirect');
function malinky_register_profile_form_redirect()
{
	global $pagenow;

	if (is_user_logged_in()) {
		global $current_user;
	    get_currentuserinfo();
	}

	//redirect coaches away from any wp_admin pages
 	if (is_admin() && in_array('coach', (array) $current_user->roles)) {
 		if (headers_sent()) {
        	echo '<meta http-equiv="refresh" content="0;url=' . site_url('coach') . '">';
            echo '<script type="text/javascript">document.location.href="' . site_url('coach') . '"</script>';
        } else {
        	wp_safe_redirect(site_url('coach'));
            exit();
		}
  	}
}

/**
 * ---------------------------------
 * GENERATE REGISTRATION FORM FIELDS
 * ---------------------------------
 */
function malinky_register_set_form_fields()
{
	$form_fields = array(
		'login_details' => array(
			'user_login' => array(
				'type'				=> 'email',
				'label'       		=> 'Email',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'email', 'email_confirm', 'user_email_exists', 'user_login_exists'),
				'placeholder' 		=> ''
			),
			'user_email' => array(
				'type'				=> 'email',
				'label'       		=> 'Confirm Email',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'email', 'email_confirm', 'user_email_exists'),
				'placeholder' 		=> ''
			),
			'password' => array(
				'type'				=> 'password',
				'label'       		=> 'Password',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'password_confirm', 'password_format'),
				'placeholder' 		=> ''
			),
			'confirm_password' => array(
				'type'				=> 'password',
				'label'       		=> 'Confirm Password',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'password_confirm', 'password_format'),
				'placeholder' 		=> ''
			)
		),
		'company_details' => array(
			'company_name' => array(
				'type'				=> 'text',
				'label'       		=> 'Company Name',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> ''
			),
			'first_name' => array(
				'type'				=> 'text',
				'label'       		=> 'First Name',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> ''
			),
			'last_name' => array(
				'type'				=> 'text',
				'label'       		=> 'Surname',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> ''
			),
			'address' => array(
				'type'				=> 'text',
				'label'       		=> 'Address',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> ''
			),
			'town' => array(
				'type'				=> 'text',
				'label'       		=> 'Town',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required'),
				'placeholder' 		=> ''
			),
			'county' => array(
				'type'				=> 'dropdown',
				'label'       		=> 'County',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('please_choose', 'county'),
				'placeholder' 		=> '',
				'options'			=> malinky_register_get_counties()
			),
			'postcode' => array(
				'type'				=> 'text',
				'label'       		=> 'Postcode',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'postcode'),
				'placeholder' 		=> ''
			),
			'phone' => array(
				'type'				=> 'text',
				'label'       		=> 'Phone',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('required', 'phone'),
				'placeholder' 		=> ''
			)
		),
		'social_media_details' => array(				
			'website' => array(
				'type'				=> 'text',
				'label'       		=> 'Website URL',
				'label_type'		=> '(optional)',
				'validation_rules' 	=> array('url'),
				'placeholder' 		=> '',
				'description'		=> 'Example: www.plaay.co.uk'
			),
			'twitter' => array(
				'type'				=> 'text',
				'label'       		=> 'Twitter Handle',
				'label_type'		=> '(optional)',
				'validation_rules' 	=> array('url_twitter_handle'),
				'placeholder' 		=> '',
				'description'		=> 'Example: plaay or @plaay'
			),
			'facebook' => array(
				'type'				=> 'text',
				'label'       		=> 'Facebook URL',
				'label_type'		=> '(optional)',
				'validation_rules' 	=> array('url'),
				'placeholder' 		=> '',
				'description'		=> 'Example: www.facebook.com/plaay'
			),
			'googleplus' => array(
				'type'				=> 'text',
				'label'       		=> 'Google Plus URL',
				'label_type'		=> '(optional)',
				'validation_rules' 	=> array('url'),
				'placeholder' 		=> '',
				'description'		=> 'Example: plus.google.com/+plaay'
			)		
		),
		'terms' => array(				
			'terms_and_conditions' => array(
				'type'				=> 'checkbox',
				'label'       		=> 'Terms &amp; Conditions',
				'label_type'		=> '(required)',
				'validation_rules' 	=> array('checkbox'),
				'placeholder' 		=> ''
			)
		)

	);
	return $form_fields;
}

/**
 * --------------------------------------
 * GENERATE THE CODE FOR EACH FORM FIELDS
 * --------------------------------------
 */
function get_form_field_template($template_name, $args = array()) {
	if ($args && is_array($args))
		extract($args);

	include(MALINKY_REGISTER_PLUGIN_DIR . 'form-fields/' . $template_name);
}

/**
 * ---------------------------------------------------------------------------------------------------------------
 * GET POSTED FORM FIELDS
 * IF RETURN_VALUES FALSE THESE VALUES ARE ADDED TO THE MAIN ARRAY GENERATED IN malinky_register_set_form_fields()
 * IF RETURN_VALUES TRUE THESE ARE RETURNED JUST AS VALUES AND USED FOR PROCESSING
 * ---------------------------------------------------------------------------------------------------------------
 */
function malinky_register_get_posted_form_fields($return_values = false, $edit_profile = false)
{
	$form_fields = malinky_register_set_form_fields();

	if ($edit_profile) {
		//remove terms form edit profile
		unset($form_fields['terms']);	
		//make password optional
		$form_fields['login_details']['password']['label_type'] = '(optional)';
		$form_fields['login_details']['password']['validation_rules'] = array('password_confirm', 'password_format');
		$form_fields['login_details']['confirm_password']['label_type'] = '(optional)';
		$form_fields['login_details']['confirm_password']['validation_rules'] = array('password_confirm', 'password_format');				
	}

	foreach ($form_fields as $fieldset => $fields) {
		foreach ($fields as $key => $field) {
			$values[$fieldset][$key] = isset($_POST[$key]) ? stripslashes($_POST[$key]) : '';
			switch ($key) {
				case 'user_login' :
					$values[$fieldset][$key] = sanitize_email($values[$fieldset][$key]);
				break;
				case 'user_email' :
					$values[$fieldset][$key] = sanitize_email($values[$fieldset][$key]);
				break;
				case 'password' :
					$values[$fieldset][$key] = $values[$fieldset][$key];
				break;
				case 'confirm_password' :
					$values[$fieldset][$key] = $values[$fieldset][$key];
				break;
				case 'website' :
					$values[$fieldset][$key] = sanitize_text_field($values[$fieldset][$key]);
					if (!empty($values[$fieldset][$key])) {
						if (!preg_match('/^https?:\/\//', $values[$fieldset][$key])) {
							$values[$fieldset][$key] = 'http://' . $values[$fieldset][$key];
						}
					}
				break;	
				case 'facebook' :
					$values[$fieldset][$key] = sanitize_text_field($values[$fieldset][$key]);
					if (!empty($values[$fieldset][$key])) {
						if (!preg_match('/^https?:\/\//', $values[$fieldset][$key])) {
							$values[$fieldset][$key] = 'http://' . $values[$fieldset][$key];
						}
					}
				break;	
				case 'googleplus' :
					$values[$fieldset][$key] = sanitize_text_field($values[$fieldset][$key]);
					if (!empty($values[$fieldset][$key])) {
						if (!preg_match('/^https?:\/\//', $values[$fieldset][$key])) {
							$values[$fieldset][$key] = 'http://' . $values[$fieldset][$key];
						}
					}
				break;				
				default:
					$values[$fieldset][$key] = sanitize_text_field($values[$fieldset][$key]);
				break;			
			}
			$form_fields[$fieldset][$key]['value'] = $values[$fieldset][$key];
		}
	}

	if ($return_values == true)
		return $values;

	return $form_fields;
}

/**
 * -----------------------------------
 * GET EXISTING COACH PROFILE FOR FORM
 * -----------------------------------
 */
function malinky_register_get_coach_profile_form_fields($profile_user)
{
	$form_fields = malinky_register_set_form_fields();

	//remove terms form edit profile
	unset($form_fields['terms']);	
	//make password optional
	$form_fields['login_details']['password']['label_type'] = '(optional)';
	$form_fields['login_details']['password']['validation_rules'] = array('password_confirm', 'password_format');
	$form_fields['login_details']['confirm_password']['label_type'] = '(optional)';
	$form_fields['login_details']['confirm_password']['validation_rules'] = array('password_confirm', 'password_format');				

	foreach ($form_fields as $fieldset => $fields) {
		foreach ($fields as $key => $field) {
			switch ($key) {
				case 'user_login' :
					$values[$fieldset][$key] = sanitize_email($profile_user->$key);
				break;
				case 'user_email' :
					$values[$fieldset][$key] = sanitize_email($profile_user->$key);
				break;
				case 'first_name' :
					$values[$fieldset][$key] = sanitize_text_field($profile_user->$key);
				break;
				case 'last_name' :
					$values[$fieldset][$key] = sanitize_text_field($profile_user->$key);
				break;
				default:
					$property = '_coach_' . $key;
					$values[$fieldset][$key] = sanitize_text_field($profile_user->$property);
				break;			
			}
			$form_fields[$fieldset][$key]['value'] = $values[$fieldset][$key];
		}
	}	

	return $form_fields;
}

/**
 * ---------------
 * FORM VALIDATION
 * ---------------
 */
function malinky_register_validate_form_fields($form_fields) {

	$errors = new WP_Error();

	foreach ($form_fields as $fieldset => $fields) {
		foreach ($fields as $key => $field) {
			if (is_array($field['validation_rules'])) {
				foreach ($field['validation_rules'] as $rule) {
					switch ($rule) {
						case 'required':
							if (empty($form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', error_messages($key . '_error'));
							}
						break;
						case 'email':
							if (!is_email($form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', error_messages($key . '_error'));
							}
						break;	
						case 'email_confirm':
							if ($form_fields[$fieldset]['user_login']['value'] != $form_fields[$fieldset]['user_email']['value']) {
								$errors->add('user_login_error', error_messages('user_login_error_2'));
								$errors->add('user_email_error', error_messages('user_email_error_2'));
							}
						break;
						case 'user_login_exists':
							if (is_user_logged_in()) {
								global $current_user;
      							get_currentuserinfo();
							}
							
							if (isset($current_user)) {
								if ($current_user->user_login != $form_fields[$fieldset][$key]['value']) {
									if (username_exists($form_fields[$fieldset][$key]['value'])) {
										$errors->add($key . '_error', error_messages($key . '_error_3'));
									}
								}								
							} else {
								if (username_exists($form_fields[$fieldset][$key]['value'])) {
									$errors->add($key . '_error', error_messages($key . '_error_3'));
								}
							}
						break;	
						case 'user_email_exists':
							if (is_user_logged_in()) {
								global $current_user;
      							get_currentuserinfo();
							}

							if (isset($current_user)) {
								if ($current_user->user_email != $form_fields[$fieldset][$key]['value']) {
									if (email_exists($form_fields[$fieldset][$key]['value'])) {
										$errors->add($key . '_error', error_messages($key . '_error_3'));
									}
								}
							 } else {
								if (email_exists($form_fields[$fieldset][$key]['value'])) {
									$errors->add($key . '_error', error_messages($key . '_error_3'));
								 }
							}						
						break;																			
						case 'password_confirm':
							if ((($form_fields[$fieldset]['password']['value'] != '') || ($form_fields[$fieldset]['confirm_password']['value'] != '')) && ($form_fields[$fieldset]['password']['value'] != $form_fields[$fieldset]['confirm_password']['value'])) {
								$errors->add('password_error', error_messages('password_error_2'));
								$errors->add('confirm_password_error', error_messages('confirm_password_error_2'));
							}
						break;
						case 'password_format':
							if (($form_fields[$fieldset]['password']['value'] != '') && ($form_fields[$fieldset]['confirm_password']['value'] != '') && ($form_fields[$fieldset]['password']['value'] == $form_fields[$fieldset]['confirm_password']['value'])) {
								if (!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[a-zA-Z0-9]+$/', $form_fields[$fieldset][$key]['value'])) {
									$errors->add($key . '_error', error_messages($key . '_error_3'));
								}
							}
						break;																		
						case 'please_choose':
							if ($form_fields[$fieldset][$key]['value'] == 'please_choose') {
								$errors->add($key . '_error', error_messages($key . '_error'));
							}	
						break;
						case 'county':
							if (!preg_match('/^[a-z\-]+$/', $form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', error_messages($key . '_error'));
							}	
						break;
						case 'postcode':
							if ($form_fields[$fieldset][$key]['value'] != '' && !preg_match("~^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$~i", $form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', error_messages($key . '_error'));
							}
						break;	
						case 'phone':
							if (!preg_match('/[0-9 ]+$/', $form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', error_messages($key . '_error'));
							}	
						break;																			
						case 'url':
							if ($form_fields[$fieldset][$key]['value'] != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', error_messages($key . '_error'));
							}
						break;
						case 'url_twitter_handle':
							if ($form_fields[$fieldset][$key]['value'] != '' && !preg_match("/^(\@)?[A-Za-z0-9_]+$/", $form_fields[$fieldset][$key]['value'])) {
								$errors->add($key . '_error', error_messages($key . '_error'));
							}
						break;																	
						case 'checkbox':							
							if ($form_fields[$fieldset][$key]['value'] != '1') {
								$errors->add($key . '_error', error_messages($key . '_error'));
							}
						break;																										
					}
				}					
			}
		}
	}

	if ($errors->get_error_code())
		return $errors;

}

/**
 * --------------
 * ERROR MESSAGES
 * --------------
 */
function error_messages($error_key)
{
	$error_messages = array(
		'user_login_error'				=> 'Please enter a valid email address.',
		'user_email_error'				=> 'Please enter a valid email address.',
		'user_login_error_2'			=> 'Please ensure your email addresses match.',
		'user_email_error_2'			=> 'Please ensure your email addresses match.',
		'user_login_error_3'			=> 'The email address already exists. Please enter another.',
		'user_email_error_3'			=> 'The email address already exists. Please enter another.',
		'password_error'				=> 'Please enter a password.',
		'confirm_password_error'		=> 'Please confirm your password.',
		'password_error_2'				=> 'Please ensure your passwords match.',
		'confirm_password_error_2'		=> 'Please ensure your passwords match.',
		'password_error_3'				=> 'Your password must contain atleast one letter and one number and no special characters.',
		'confirm_password_error_3'		=> 'Your password must contain atleast one letter and one number and no special characters.',
		'confirm_email_error'			=> 'Please enter a valid email address.',
		'company_name_error'			=> 'Please enter your company name.',
		'first_name_error'				=> 'Please enter your first name.',
		'first_name_error'				=> 'Please enter your first name.',
		'last_name_error'				=> 'Please enter your surname.',
		'address_error'					=> 'Please enter your address.',
		'town_error'					=> 'Please enter your town.',
		'county_error'					=> 'Please choose a county.',
		'postcode_error'				=> 'Please enter a valid postcode.',	
		'phone_error'					=> 'Please enter a valid phone number.',		
		'website_error'					=> 'Please enter a valid website URL.',
		'twitter_error'					=> 'Please enter a valid Twitter handle.',
		'facebook_error'				=> 'Please enter a valid Facebook URL.',
		'googleplus_error'				=> 'Please enter a valid Google Plus URL.',
		'terms_and_conditions_error'	=> 'Please accept our terms and conditions.'
	);
	return $error_messages[$error_key];
}

/**
 * -----------------------------------
 * PROCESS FORM AND REGISTER NEW COACH
 * -----------------------------------
 */
function malinky_register_new_coach()
{
	//check form submit and nonce
	if ((empty($_POST['submit_registration'])) || (!isset($_POST['malinky_register_register_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_register_register_form_nonce'], 'malinky_register_register_form')))
		wp_die(__('There was a fatal error with your form submission'));

	$values = malinky_register_get_posted_form_fields(true);

	//no errors create the user
	$new_user = array(
		'user_login'	=> $values['login_details']['user_login'],
		'user_pass'		=> $values['login_details']['password'],
		'user_email'	=> $values['login_details']['user_email'],
		'first_name'	=> $values['company_details']['first_name'],
		'last_name'		=> $values['company_details']['last_name'],
		'display_name'	=> $values['company_details']['first_name'] . ' ' . $values['company_details']['last_name']		
	);

	$user_id = wp_insert_user($new_user);

	//if new user isn't created send error at bottom of registration form
	if (!$user_id || is_wp_error($user_id)) {
		$errors->add('register_fail', __('There was a problem with your registration. Please try again.'));
		return $errors;
	}

	//generate email validation code
	$email_validation_code = malinky_register_generate_validation_code();

	//must have new user so lets insert custom meta values
	$custom_usermeta = array(
		'_validation_code'		=> $email_validation_code,
		'_validated_user' 		=> 0,
		'_coach_company_name'	=> $values['company_details']['company_name'],
		'_coach_phone'			=> $values['company_details']['phone'],
		'_coach_address'		=> $values['company_details']['address'],
		'_coach_town'			=> $values['company_details']['town'],
		'_coach_county'			=> $values['company_details']['county'],
		'_coach_postcode'		=> $values['company_details']['postcode'],
		'_coach_website'		=> $values['social_media_details']['website'],
		'_coach_twitter'		=> $values['social_media_details']['twitter'],
		'_coach_facebook'		=> $values['social_media_details']['facebook'],
		'_coach_googleplus'		=> $values['social_media_details']['googleplus'],
		'_coach_accept_tandcs'	=> $values['terms']['terms_and_conditions']
	);

	foreach ($custom_usermeta as $k => $v) {
		if ($custom_usermeta[$k]) {
			update_user_meta($user_id, $k, $v);	
		}
	}

	//send registration email
	malinky_register_send_registration_email($values['login_details']['user_login'], $email_validation_code);

	if ($user_id) {
		wp_safe_redirect('wp-login.php?action=registered');
		exit();
	}
}

/**
 * -----------------------------
 * PROCESS FORM AND UPDATE COACH
 * -----------------------------
 */
function malinky_register_update_coach($profile_user)
{
	if ((empty($_POST['update_profile'])) || (!isset($_POST['malinky_register_profile_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_register_profile_form_nonce'], 'malinky_register_profile_form')))
		wp_die(__('There was a fatal error with your form submission'));

	$values = malinky_register_get_posted_form_fields(true, true);

	//set flag for an email update
	$user_email_updated = false;

	//no errors update the user
	$user_data = array(
		'ID'			=> $profile_user->ID,
		'first_name'	=> $values['company_details']['first_name'],
		'last_name'		=> $values['company_details']['last_name'],
		'display_name'	=> $values['company_details']['first_name'] . ' ' . $values['company_details']['last_name']
	);

	//check for a password change
	if ($values['login_details']['password'] != '')
		$user_data['user_pass'] = $values['login_details']['password'];
	
	//check for a user_login change and update database manually
	if ($values['login_details']['user_login'] != $profile_user->user_login) {
		global $wpdb;
		$user_login_db_change = $wpdb->update($wpdb->users, array('user_login' => $values['login_details']['user_login']), array('ID' => $profile_user->ID));
		if (!$user_login_db_change) {
			$errors->add('register_fail', __('There was a problem updating your profile. Please try again.'));
			return $errors;
		}
		$user_data['user_email'] = $values['login_details']['user_email'];
		$user_email_updated = true;
	}

	$user_id = wp_update_user($user_data);

	//if user isn't updated send error at bottom of registration form
	if (!$user_id || is_wp_error($user_id)) {
		$errors->add('register_fail', __('There was a problem updating your profile. Please try again.'));
		return $errors;
	}

	//must have new user so lets insert custom meta values
	$custom_usermeta = array(
		'_coach_company_name'	=> $values['company_details']['company_name'],		
		'_coach_phone'			=> $values['company_details']['phone'],
		'_coach_address'		=> $values['company_details']['address'],
		'_coach_town'			=> $values['company_details']['town'],
		'_coach_county'			=> $values['company_details']['county'],
		'_coach_postcode'		=> $values['company_details']['postcode'],
		'_coach_website'		=> $values['social_media_details']['website'],
		'_coach_twitter'		=> $values['social_media_details']['twitter'],
		'_coach_facebook'		=> $values['social_media_details']['facebook'],
		'_coach_googleplus'		=> $values['social_media_details']['googleplus']			
	);

	foreach ($custom_usermeta as $k => $v) {
		if ($custom_usermeta[$k]) {
			update_user_meta($user_id, $k, $v);	
		}
	}

	//if email has been updated redirect to login as user is logged out anyway
	if ($user_email_updated && $user_id) {
		wp_safe_redirect(site_url('login'));
		exit();
	}

	if ($user_id) {
		wp_safe_redirect('coach?action=profile_updated');
		exit();
	}
}

/**
 * ----------------------------------
 * GENERATE THE VALIDATION EMAIL CODE
 * ----------------------------------
 */
function malinky_register_generate_validation_code()
{
	return md5(uniqid(rand(),1));
}

/**
 * ----------------------------------------------------
 * SEND THE REGISTRATION EMAIL AND VALIDATION CODE LINK
 * ----------------------------------------------------
 */
function malinky_register_send_registration_email($email, $validation_code)
{
	include_once(ABSPATH . 'wp-includes/class-phpmailer.php');

	$email_settings = get_option('malinky_activities_email_settings_options');

	if (!$email_settings) return;

	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host 		= $email_settings['settings_smtp_host'];
	$mail->SMTPAuth   	= true;
	$mail->Port       	= $email_settings['settings_smtp_port'];
	$mail->Username   	= $email_settings['settings_smtp_username'];
	$mail->Password   	= $email_settings['settings_smtp_password'];
	$mail->SMTPSecure 	= 'ssl';		

	$mail->AddAddress($email);

	$mail->SetFrom(get_option('admin_email'), "PLAAY");

	$mail->IsHTML(true);

	$mail->Subject = 'Please Activate Your ' . get_bloginfo('name') . ' Coach Account';
	$mail->Body    = '<p>Thank you for registering with ' . get_bloginfo('name') . '. Please click on the link below to activate your account.</p><p><a href="' . network_site_url('wp-login.php?action=validate_user&validation_code=' . $validation_code) . '">' . network_site_url('wp-login.php?action=validate_user&validation_code=' . $validation_code) . '</a></p><p>Thanks</p>';

	if(!$mail->Send()) {
		error_log($mail->ErrorInfo, 0);
    }
}

/**
 * ------------------------------------------------------
 * RESEND THE REGISTRATION EMAIL AND VALIDATION CODE LINK
 * ------------------------------------------------------
 */
add_action('login_init', 'malinky_register_resend_registration_email');
function malinky_register_resend_registration_email()
{
	if (isset($_GET['action']) && $_GET['action'] == 'resend_registration_email' && is_email($_GET['email'])) {
		global $wpdb;
		$email = $_GET['email'];
		$user_id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->users WHERE user_login = %s", $email));
		$email_validation_code = $wpdb->get_var($wpdb->prepare("SELECT meta_value FROM $wpdb->usermeta WHERE user_id = %d AND meta_key = '_validation_code'", $user_id));
		malinky_register_send_registration_email($email, $email_validation_code);
		wp_safe_redirect('?action=resent_registration_email');
		exit();
	}
}

/**
 * ---------------------------------------------
 * VALIDATE THE USER FROM THE REGISTRATION EMAIL
 * ---------------------------------------------
 */
add_action('login_init', 'malinky_register_validate_user');
function malinky_register_validate_user()
{
	if (basename($_SERVER['PHP_SELF']) == 'wp-login.php' && isset($_GET['action']) && $_GET['action'] == 'validate_user' && isset($_GET['validation_code'])) {
		$user_id = malinky_register_check_validation_code($_GET['validation_code']);
		if ($user_id) {
			update_user_meta($user_id, '_validated_user', 1);			
			delete_user_meta($user_id, '_validation_code');
			wp_safe_redirect('?action=user_validated');
			exit();
		} else {
			wp_safe_redirect('?action=user_validation_failed');
			exit();
		}
	}
}

/**
 * --------------------------------
 * CHECK THE VALIDATION CODE FORMAT
 * --------------------------------
 */
function malinky_register_check_validation_code($validation_code)
{
	global $wpdb;

	if (!preg_match('/[a-zA-Z0-9]{32}/', $validation_code))
		return false;

	$user_id = $wpdb->get_var($wpdb->prepare("SELECT user_id FROM $wpdb->usermeta WHERE meta_key = '_validation_code' AND meta_value = %s;", $validation_code));
	
	if (empty($user_id))
		return false;

	return $user_id;
}

/**
 * ------------------------------------
 * SETUP THE REGISTRATION PAGE MESSAGES
 * ------------------------------------
 */
if (basename($_SERVER['PHP_SELF']) == 'wp-login.php' && isset($_GET['action'])) {
	if ($_GET['action'] == 'registered' || $_GET['action'] == 'resent_registration_email' || $_GET['action'] == 'user_validated' || $_GET['action'] == 'user_validation_failed') {

		add_filter('login_message', 'malinky_register_login_messages');
		function malinky_register_login_messages($messages)
		{
			//change to switch with default message
			if (empty($messages)) {
				if ($_GET['action'] == 'registered') {
					return '<p class="message">You have now been registered. Please check your email to activate your account.</p>';
				} elseif ($_GET['action'] == 'resent_registration_email') {
					return '<p class="message">Your activation email has been resent.</p>';
				} elseif ($_GET['action'] == 'user_validated') {
					return '<p class="message">Your account has now been activated. Please login.</p>';
				} elseif ($_GET['action'] == 'user_validation_failed') {
		        	return '<div id="login_error">Your account activation has failed.</div>';
				}
		    } else {
		        return $messages;
		    }
		}

	}
}

/**
 * --------------------------------------------------------------------------------------
 * BLOCK UNVALIDATED USERS FROM LOGGING IN AND ENCOURAGE THE RESENDING OF VALIDATION EMAIL
 * --------------------------------------------------------------------------------------
 */
add_filter('wp_authenticate_user', 'malinky_register_is_validated_user', 10, 2);
function malinky_register_is_validated_user($user, $password)
{	
	$is_validated_user = get_user_meta($user->ID, '_validated_user', true);

	if (isset($user->roles) && is_array($user->roles)) {
        if(in_array('administrator', (array) $user->roles)) {
        	return $user;
        }
    }

	if ($is_validated_user == 0) {
		return new WP_Error('no_validated_user', __('You need to validate your account before logging in. Please click the link below to resend the validation email.<br /><br /><a href="' . site_url('/wp-login.php?action=resend_registration_email&email=' . $user->user_login) . '">Resend Validation Email</a>'));
	} else {
		return $user;
	}
}

/**
 * --------------------------------------
 * LIST OF COUNTIES FOR USER REGISTRATION
 * --------------------------------------
 */
function malinky_register_get_counties()
{
	$counties = array(
		'aberdeenshire' => 'Aberdeenshire',
		'angus' => 'Angus',
		'antrim' => 'Antrim',
		'argyll-and-bute' => 'Argyll and Bute',
		'armagh' => 'Armagh',
		'ayrshire-and-arran' => 'Ayrshire and Arran',
		'banffshire' => 'Banffshire',
		'bedfordshire' => 'Bedfordshire',
		'belfast-city' => 'Belfast City',
		'berkshire' => 'Berkshire',
		'berwickshire' => 'Berwickshire',
		'bristol' => 'Bristol',
		'buckinghamshire' => 'Buckinghamshire',
		'caithness' => 'Caithness',
		'cambridgeshire' => 'Cambridgeshire',
		'cheshire' => 'Cheshire',
		'city-of-london' => 'City of London',
		'clackmannan' => 'Clackmannan',
		'clwyd' => 'Clwyd',
		'cornwall' => 'Cornwall',
		'cumbria' => 'Cumbria',
		'derbyshire' => 'Derbyshire',
		'devon' => 'Devon',
		'dorset' => 'Dorset',
		'down' => 'Down',
		'dumfries' => 'Dumfries',
		'dunbartonshire' => 'Dunbartonshire',
		'durham' => 'Durham',
		'dyfed' => 'Dyfed',
		'east-lothian' => 'East Lothian',
		'east-riding-of-yorkshire' => 'East Riding of Yorkshire',
		'east-sussex' => 'East Sussex',
		'essex' => 'Essex',
		'fermanagh' => 'Fermanagh',
		'fife' => 'Fife',
		'gloucestershire' => 'Gloucestershire',
		'greater-london' => 'Greater London',
		'greater-manchester' => 'Greater Manchester',
		'gwent' => 'Gwent',
		'gwynedd' => 'Gwynedd',
		'hampshire' => 'Hampshire',
		'herefordshire' => 'Herefordshire',
		'hertfordshire' => 'Hertfordshire',
		'inverness' => 'Inverness',
		'isle-of-wight' => 'Isle of Wight',
		'kent' => 'Kent',
		'kincardineshire' => 'Kincardineshire',
		'lanarkshire' => 'Lanarkshire',
		'lancashire' => 'Lancashire',
		'leicestershire' => 'Leicestershire',
		'lincolnshire' => 'Lincolnshire',
		'londonderry' => 'Londonderry',
		'londonderry-city' => 'Londonderry City',
		'merseyside' => 'Merseyside',
		'mid-glamorgan' => 'Mid Glamorgan',
		'midlothian' => 'Midlothian',
		'moray' => 'Moray',
		'nairn' => 'Nairn',
		'norfolk' => 'Norfolk',
		'north-yorkshire' => 'North Yorkshire',
		'northamptonshire' => 'Northamptonshire',
		'northumberland' => 'Northumberland',
		'nottinghamshire' => 'Nottinghamshire',
		'orkney' => 'Orkney',
		'oxfordshire' => 'Oxfordshire',
		'perth-and-kinross' => 'Perth and Kinross',
		'powys' => 'Powys',
		'renfrewshire' => 'Renfrewshire',
		'ross-and-cromarty' => 'Ross and Cromarty',
		'roxburgh,-ettrick-and-lauderdale' => 'Roxburgh, Ettrick and Lauderdale',
		'rutland' => 'Rutland',
		'shetland' => 'Shetland',
		'shropshire' => 'Shropshire',
		'somerset' => 'Somerset',
		'south-glamorgan' => 'South Glamorgan',
		'south-yorkshire' => 'South Yorkshire',
		'staffordshire' => 'Staffordshire',
		'stirling-and-falkirk' => 'Stirling and Falkirk',
		'suffolk' => 'Suffolk',
		'surrey' => 'Surrey',
		'sutherland' => 'Sutherland',
		'the-stewartry-of-kirkcudbright' => 'The Stewartry of Kirkcudbright',
		'tweeddale' => 'Tweeddale',
		'tyne-and-wear' => 'Tyne and Wear',
		'tyrone' => 'Tyrone',
		'warwickshire' => 'Warwickshire',
		'west-glamorgan' => 'West Glamorgan',
		'west-lothian' => 'West Lothian',
		'west-midlands' => 'West Midlands',
		'west-sussex' => 'West Sussex',
		'west-yorkshire' => 'West Yorkshire',
		'western-isles' => 'Western Isles',
		'wigtown' => 'Wigtown',
		'wiltshire' => 'Wiltshire',
		'worcestershire' => 'Worcestershire'
	);
	return $counties;
}