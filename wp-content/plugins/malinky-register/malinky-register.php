<?php
/**
 * Plugin Name: Malinky Register
 * Plugin URI: 
 * Description: Front End Registration
 * Version: 1.1
 * Author: Malinky
 * Author URI: http://www.malinkymedia.com
 * Copyright: 2013 Craig Ramsay
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Malinky_Register {

	function __construct()
	{
		define( 'MALINKY_REGISTER_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		define( 'MALINKY_REGISTER_PLUGIN_URL', plugins_url( basename( plugin_dir_path( __FILE__ ) ) ) );

		include( 'malinky-register-functions.php' );			
		include( 'includes/malinky-register-shortcodes.php' );

		add_action( 'wp_enqueue_scripts', array( $this, 'malinky_register_frontend_scripts' ) );
	}

	function malinky_register_frontend_scripts()
	{	
		if ( is_page( 'register' ) ) {
			wp_register_script( 'malinky_validate_js', MALINKY_REGISTER_PLUGIN_URL . '/js/jquery.validate.min.js', array( 'jquery' ), NULL, true );		
			wp_enqueue_script( 'malinky_validate_js' );			
			wp_register_script( 'malinky_registration_validate_js', MALINKY_REGISTER_PLUGIN_URL . '/js/registration_validate.js', array( 'jquery', 'malinky_validate_js' ), NULL, true );
			wp_enqueue_script( 'malinky_registration_validate_js' );
		}
		if ( is_page('edit-profile') ) {
			wp_register_script( 'malinky_validate_js', MALINKY_REGISTER_PLUGIN_URL . '/js/jquery.validate.min.js', array( 'jquery' ), NULL, true );		
			wp_enqueue_script( 'malinky_validate_js' );			
			wp_register_script( 'malinky_profile_validate_js', MALINKY_REGISTER_PLUGIN_URL . '/js/profile_validate.js', array( 'jquery', 'malinky_validate_js' ), NULL, true );
			wp_enqueue_script( 'malinky_profile_validate_js' );
		}		
	}

}
$malinky_register = new Malinky_Register();