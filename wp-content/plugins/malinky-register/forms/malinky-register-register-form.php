<?php
//check if logged in and don't grant access again
if (is_user_logged_in()) {
	wp_die(__('You are already logged in.'));
}

$form_fields = malinky_register_set_form_fields();

if (!empty($_POST['submit_registration'])) {

	$form_fields = malinky_register_get_posted_form_fields();
	$errors = malinky_register_validate_form_fields($form_fields);
	if (is_wp_error($errors)) {
		$error_messages = $errors->errors;
	} else {
		malinky_register_new_coach();
	}

}
?>
<?php get_header(); ?>

<main role="main">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1><?php the_title(); ?></h1>
		<p class="form_content">Lets have some content here.</p>

		<div class="col">
			<div class="col_item col_item_full">
					
				<form id="register_form" method="post" role="form">

					<fieldset id="fieldset_register_login_details" class="register_fieldset">
						<legend>Login Details</legend>
						<p class="coach_message">Please enter your chosen login details. Your email address will be used as your username.</p>
						<?php foreach ($form_fields['login_details'] as $key => $field) { ?>
							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									 <?php get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
									 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->
						<?php } ?>
					</fieldset>
					<fieldset id="fieldset_register_company_details" class="register_fieldset">
						<legend>Company Details</legend>
						<?php foreach ($form_fields['company_details'] as $key => $field) { ?>
							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									 <?php get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
									 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div>
								<div class="col col_span_1_10">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->
						<?php } ?>
					</fieldset>
					<fieldset id="fieldset_register_social_media_details" class="register_fieldset">
						<legend>Social Media Details</legend>
						<?php foreach ($form_fields['social_media_details'] as $key => $field) { ?>
							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									 <?php get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
									 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->
						<?php } ?>
					</fieldset>	
					<fieldset id="fieldset_register_terms" class="register_fieldset">
						<legend>Terms and Conditions</legend>
						<?php foreach ($form_fields['terms'] as $key => $field) { ?>
							<div class="col">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">			
									<label for="<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $field['label'] ); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									 <?php get_form_field_template( $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
									 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->
						<?php } ?>
					</fieldset>

					<?php
					//wp registration error
					if (isset($error_messages['register_fail'][0])) echo '<p class="error">' . $error_messages['register_fail'][0] . '</p>';
					?>	

					<?php
					/**
					 * Fires following the 'E-mail' field in the user registration form.
					 *
					 * @since 2.1.0
					 */
					do_action( 'register_form' );
					?>
					<?php wp_nonce_field( 'malinky_register_register_form', 'malinky_register_register_form_nonce' ); ?>
					<input type="submit" name="submit_registration" class="button full_width" value="<?php esc_attr_e('Register'); ?>" />

				</form>
			</div><!-- .col_item -->
		</div><!-- .col -->

	</article>
</main>

<?php get_footer(); ?>