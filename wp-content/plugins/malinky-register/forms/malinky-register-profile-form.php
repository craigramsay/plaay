<?php
//check if a user is logged in
if (is_user_logged_in()) {
	global $current_user;
    get_currentuserinfo();
} else {
    wp_safe_redirect(site_url('wp-login.php'));
    exit();
}

//the below is used as coach/edit-profile is accessible also using coach/edit-profile?user_id=XX
if (isset($_GET['user_id'])) {
	 if (is_numeric($_GET['user_id'])) {
		//check if coach and if user_id matches logged in user and if they can edit their own profile if not die
		if (($current_user->ID != $_GET['user_id'] || !current_user_can('edit_user', $current_user->ID)) && (in_array('coach', (array) $current_user->roles))) {
			wp_die(__('You can\'t edit this profile.'));
		}	
		//check if admin and if trying to edit their own profile die they should access this through wp-admin
		if (($current_user->ID == $_GET['user_id'] && current_user_can('edit_user', $current_user->ID) && in_array('administrator', (array) $current_user->roles))) {
			wp_die(__('Please use the admin section to edit your profile.'));
		}
	} else {
		//die if user id isn't numeric
		wp_die(__('Invalid User.'));
	}
//if there is no user_id in url and it is an admin they need to access page through wp-admin	
} elseif (!isset($_GET['user_id']) && current_user_can('edit_users') && in_array('administrator', (array) $current_user->roles)) {
	wp_die(__('Please use the admin section to edit your profile.'));
}

//if it's a logged in coach tring to edit their own profile carry on
if (current_user_can('edit_user', $current_user->ID) && in_array('coach', (array) $current_user->roles)) {
	
	//get user profile data
	$profile_user = get_userdata($current_user->ID);

	if(!$profile_user)
		wp_die(__('There was an error accessing the user information.'));

	if (!empty($_POST['update_profile'])) {

		$form_fields = malinky_register_get_posted_form_fields(false, true);
		$errors = malinky_register_validate_form_fields($form_fields);
		if (is_wp_error($errors)) {
			$error_messages = $errors->errors;
		} else {
			malinky_register_update_coach($profile_user);
		}

	} else {

		$form_fields = malinky_register_get_coach_profile_form_fields($profile_user);

	}

	$form_action = site_url('coach/edit-profile');

	get_header('coach'); ?>

	<main role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<h1><?php the_title(); ?></h1>

			<div class="col">
				<div class="col_item col_item_full">

					<form id="profile_form" action="<?php echo esc_url($form_action); ?>" method="post" role="form">

						<fieldset id="fieldset_register_login_details" class="register_fieldset">
						<legend>Login Details</legend>
						<p class="coach_message">Please enter your chosen login details. Your email address will be used as your username.</p>
						<?php foreach ($form_fields['login_details'] as $key => $field) { ?>
							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
									<label for="<?php esc_attr_e($key); ?>"><?php esc_html_e($field['label']); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									 <?php get_form_field_template($field['type'] . '-field.php', array('key' => $key, 'field' => $field)); ?>
									 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div>
						<?php } ?>
						</fieldset>
						<fieldset id="fieldset_register_company_details" class="register_fieldset">
							<legend>Personal Details</legend>
							<?php foreach ($form_fields['company_details'] as $key => $field) { ?>
								<div class="col form_fields_2">
									<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
										<label for="<?php esc_attr_e($key); ?>"><?php esc_html_e($field['label']); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
									</div><!--
									--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
										 <?php get_form_field_template($field['type'] . '-field.php', array('key' => $key, 'field' => $field)); ?>
										 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
									</div><!--
									--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
											<div class="field_error_icon"></div>					
									</div>
								</div>
							<?php } ?>
						</fieldset>
						<fieldset id="fieldset_register_social_media_details" class="register_fieldset">
							<legend>Social Media Details</legend>
							<?php foreach ($form_fields['social_media_details'] as $key => $field) { ?>
								<div class="col form_fields_2">
									<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">				
										<label for="<?php esc_attr_e($key); ?>"><?php esc_html_e($field['label']); ?><?php if ($field['label_type']) echo ' <small>' . $field['label_type'] . '</small>'; ?></label>
									</div><!--
									--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
										 <?php get_form_field_template($field['type'] . '-field.php', array('key' => $key, 'field' => $field)); ?>
										 <?php if (isset($error_messages[$key . '_error'][0])) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
									</div><!--
									--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
										<div class="field_error_icon"></div>					
									</div>
								</div>
							<?php } ?>
						</fieldset>	

						<?php
						//wp registration error
						if (isset($error_messages['register_fail'][0])) echo '<p>' . $error_messages['register_fail'][0] . '</p>';
						?>	

						<?php wp_nonce_field('malinky_register_profile_form', 'malinky_register_profile_form_nonce'); ?>
						<input type="submit" name="update_profile" class="button full_width" value="<?php esc_attr_e('Update Profile'); ?>" />

					</form>
				</div><!-- .col_item -->
			</div><!-- .col -->

		</article>
	</main>
	
	<?php get_footer('coach');

} else { 
	wp_die(__('You can\'t edit this profile.'));
}
?>