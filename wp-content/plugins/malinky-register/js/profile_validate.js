/**
 * -----------------------------
 * REGISTRATION FORM VALIDATION
 * -----------------------------
 */
jQuery(document).ready(function($){
    
    jQuery.validator.addMethod("valid_select", function(value, element) {
        return this.optional(element) || value != 'please_choose';
    }, "Please Choose.");

    jQuery.validator.addMethod("valid_county", function(value, element) {
        return this.optional(element) || /^[a-z\-]+$/.test(value);
    }, "Small letters and hyphen only please"); 

    jQuery.validator.addMethod("valid_phone", function(value, element) {
        return this.optional(element) || /^[0-9 ]+$/i.test(value);
    }, "Numbers and space only please");   

    jQuery.validator.addMethod("valid_password", function(value, element) {
        return this.optional(element) || /^(?=.*\d)(?=.*[A-Za-z])[a-zA-Z0-9]+$/.test(value);
    }, "One letter one number please");  

    jQuery.validator.addMethod("valid_postcode", function(value, element) {
        return this.optional(element) || /^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$/i.test(value);
    }, "Valid postcode please");                
 
     jQuery.validator.addMethod("valid_url_opt_http", function(value, element) {
        return this.optional(element) || /^(?:(?:http|https):\/\/)?([-a-zA-Z0-9.]{2,256}\.[a-z]{2,4})\b(?:\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?$/.test(value);
    }, "One letter one number please"); 

    jQuery.validator.addMethod("valid_url_twitter_handle", function(value, element) {
        return this.optional(element) || /^(\@)?[A-Za-z0-9_]+$/.test(value);
    }, "One letter one number please");    

    // validate contact form on keyup and submit
    $("#profile_form").validate({
 
        //set the rules for the fild names
        rules: { 
            user_login: {
                required: true,
                email: true,
            },                  
            user_email: {
                required: true,
                email: true,
                equalTo: "#user_login"
            },
            password: {
                valid_password: true
            },                  
            confirm_password: {
                equalTo: "#password",
                valid_password: true
            },
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },             
            address: {
                required: true  
            },
            town: {
                required: true
            },  
            county: {
                valid_select: true,
                valid_county: true
            },
            postcode: {
                required: true,
                valid_postcode: true
            },
            phone: {
                required: true,
                valid_phone: true
            },  
            website: {
                valid_url_opt_http: true,
            },
            twitter: {
                valid_url_twitter_handle: true,
            },
            facebook: {
                valid_url_opt_http: true,
            },
            googleplus: {
                valid_url_opt_http: true,
            },
            terms_and_conditions: {
                required: true,
            }
        },
 
        //set error messages
        messages: {   
            user_login: {
                required: "Please enter a valid email address.",
                email: "Please enter a valid email address.",
            },
            user_email: {
                required: "Please enter a valid email address.",
                email: "Please enter a valid email address.",
                equalTo: "Please ensure your email addresses match."
            },
            password: {
                required: "Please enter a password.",
                valid_password: "Your password must contain atleast one letter and one number and no special characters."
            },
            confirm_password: {
                required: "Please confirm your password.",
                equalTo: "Please ensure your passwords match.",
                valid_password: "Your password must contain atleast one letter and one number and no special characters."
            },              
            first_name: "Please enter your first name.",
            last_name: "Please enter your surname.",                      
            address: "Please enter your address.", 
            town: "Please enter your town.",  
            county: {
                valid_select: "Please choose a county.",
                valid_county: "Please choose a county."
            },  
            postcode: {
                required: "Please enter your postcode.",
                valid_postcode: "Please enter a valid postcode."
            },
            phone: {
                required: "Please enter your phone number.",
                valid_phone: "Please enter a valid phone number."
            },            
            website: "Please enter a valid company website URL.",
            twitter: "Please enter a valid company Twitter handle.",
            facebook: "Please enter a valid company Facebook URL.",
            googleplus: "Please enter a valid company Google Plus URL.",
            terms_and_conditions: "Please accept our terms and conditions."                                                                           
        },
 
        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
            error.insertAfter(element);
            if ($(element).hasClass('error')) {
                $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');
            }
        },

        success: function(label) {
            $(label).parent().next().children('.field_error_icon').addClass('field_error_tick');
            label.css('margin-top', '0');
            label.css('margin-bottom', '0');
        },

        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_tick');            
            $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');            
            //$(element).next('p.error').css('margin-top', '1em');
            //$(element).next('p.error').css('margin-bottom', '1em');
        },

        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_cross');
            $(element).parent().next().children('.field_error_icon').addClass('field_error_tick');
            //$(element).next('p.error').css('margin-top', '0');
            //$(element).next('p.error').css('margin-bottom', '0');
        }        

    });
});