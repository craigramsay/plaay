<?php
/**
 * Default template for full width pages
 */
if (tree() == 'coach') {
	get_header('coach');
} else {
	get_header();
}
?>

<main role="main">
	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php if (!is_page('activities')) { ?>
				<h1><?php the_title(); ?></h1>
			<?php } ?>
			<?php the_content(); ?>
		</article><!-- #post -->
	<?php endwhile; // end of the loop. ?>
</main>

<?php get_footer();