<?php
/**
 * The template for displaying all archive pages.
 *
 * This can be overwritten with author.php, category.php, taxonomy.php, date.php, tag.php.
 */

get_header(); ?>

<main role="main">

	<div class="col">

		<div class="col_item col_item_7_10">
			
			<?php if ( have_posts() ) : ?>

				<div class="col">
					<div class="col_item col_item_full">
						<header class="content-introduction">
							<h1 class="content-introduction__title"><?php plaay_archive_title(); ?></h1>
							<p class="content-introduction__description"><?php plaay_archive_description(); ?></p>
						</header><!-- .content-header -->
					</div>
				</div>

				<?php while ( have_posts() ) : the_post(); ?>

					<div class="col">
						<div class="col_item col_item_full">
							<?php get_template_part( 'content' ); ?>
						</div>
					</div>	

				<?php endwhile;

				plaay_posts_pagination();

			else : ?>

				<div class="col">
					<div class="col_item col_item_full">
						<?php get_template_part( 'content', 'none' ); ?>
					</div>
				</div>				

			<?php endif; ?>
			
		</div><!--
	
		--><div class="col_item col_item_3_10">
		
			<?php get_sidebar(); ?>

		</div>

	</div><!-- .col -->

</main><!-- .main -->

<?php get_footer(); ?>