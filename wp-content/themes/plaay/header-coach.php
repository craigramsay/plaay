<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?> prefix="og: http://ogp.me/ns#"> <!--<![endif]--> 
<!--<![endif]-->
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>" />

    <!-- Use latest IE engine --> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile viewport -->    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icons for mobile for touch home screens and bookmarks. Remember to upload favicon.ico too! -->

    <!-- Chrome and Android (192 x 192) -->
    <meta name="application-name" content="<?php echo esc_attr( bloginfo( 'name' ) ); ?>">
    <link rel="icon" sizes="192x192" href="<?php echo esc_url( site_url( 'chrome-touch-icon-192x192.png' ) ); ?>">

    <!-- Safari on iOS (152 x 152) -->
    <meta name="apple-mobile-web-app-title" content="<?php echo esc_attr( bloginfo( 'name' ) ); ?>">
    <link rel="apple-touch-icon" href="<?php echo esc_url( site_url( 'apple-touch-icon.png' ) ); ?>">

    <!-- IE on Win8 (144 x 144 + tile color) -->
    <meta name="msapplication-TileImage" content="<?php echo esc_url( site_url( 'ms-touch-icon-144x144-precomposed.png' ) ); ?>">
    <meta name="msapplication-TileColor" content="#3372DF">

    <title><?php wp_title(); ?></title>

    <?php if ( WP_ENV == 'dev' ) { ?>
        <meta name="robots" content="noindex,nofollow">
    <?php } ?>
    
    <?php wp_head(); ?>

    <?php if ( ! is_front_page() ) { ?>
    <!--https://github.com/scottjehl/Respond-->
    <!--https://github.com/aFarkas/html5shiv-->
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/respond.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
    <![endif]-->
    <?php } ?>
    
</head>

<body <?php body_class(); ?>>
	<!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->

<?php
global $current_user;
get_currentuserinfo();
if (!is_user_logged_in()) {
    wp_safe_redirect(wp_login_url());
    exit();
}
if (!in_array('coach', (array) $current_user->roles))
    wp_die(__('You don\'t have access to this page.'));
?>

<div id="nav_full_coach">
    <nav id="nav_coach_content" role="navigation">
      <div class="col">
        <div class="col_item col_item_1_6">
          <a href="<?php echo esc_url( site_url('', 'http') ); ?>" id="logo"><img src="<?php echo get_template_directory_uri() . '/img/logo.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/logo@2x.png'; ?>" alt="PLAAY" class="non_responsive" /></a>
          <a href="<?php echo esc_url( site_url('', 'http') ); ?>" id="large-logo"><img src="<?php echo get_template_directory_uri() . '/img/logo_icon.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/logo_icon@2x.png'; ?>" alt="PLAAY" class="non_responsive" /></a>
        </div><!--
        --><div class="col_item col_item_5_6">
            <?php
            if (in_array('coach', (array) $current_user->roles)) {
                $args = array(
                'menu'              => 'Coach Menu',
                'container_id'      => 'menu_coach',
                );
                wp_nav_menu($args);
            } ?>
        </div>
      </div>
    </nav>
</div><!--END #nav_full-->

<div id="nav_mobile_coach">
    <nav id="nav_content_mobile" role="navigation">
        <div class="col">
            <div class="col_item col_item_6_8">
                <a href="<?php echo esc_url( site_url('', 'http') ); ?>" id="logo"><img src="<?php echo get_template_directory_uri() . '/img/logo.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/logo@2x.png'; ?>" alt="PLAAY" class="non_responsive" /></a>
            </div><!--
            --><div class="col_item col--align_right col_item_quarter" id="nav_mobile_menu">
                <a href=""><img src="<?php echo get_template_directory_uri() . '/img/mobile_navicon.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/mobile_navicon@2x.png'; ?>" alt="PLAAY Mobile Menu" /></a>
            </div>
        </div><!-- .col -->
        <div class="col">
            <div class="col_item col_item_full">
                <?php
                if (in_array('coach', (array) $current_user->roles)) {
                    $args = array(
                        'menu'              => 'Coach Menu',
                        'menu_id'           => 'nav_mobile_toggle',
                        'menu_class'        => 'list_no_style'                        
                    );
                    wp_nav_menu($args);
                } ?>
            </div>
        </div><!-- .col -->
    </nav>
</div><!--END #nav_full-->

<div id="wrap_coach">

    <nav id="nav_login_content" role="navigation">
        <div class="col">
            <div class="col_item col_item_full">
                <?php if (is_user_logged_in()) : ?>
                    <p>Logged in as <span><?php echo $current_user->user_login; ?></span></p>
                    <p><a href="<?php echo esc_url( wp_logout_url( home_url('', 'http') ) ); ?>">Logout</a></p>
                <?php endif; ?>
            </div>
        </div>
    </nav>