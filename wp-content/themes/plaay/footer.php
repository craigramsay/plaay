</div><!-- .wrap -->

<div class="wrap_home_content">
	<div id="footer">
		<a href="#" class="image-font back_top"><span class="image-font__sizing image-font__fontawesome fa-chevron-up image-font_home image-font__purple"></span></a>
		<footer id="footer_content" role="contentinfo">
			<div class="col">
				<div class="col_item medium-col_item--align_center small-col_item--align_center col_item_6_10 medium-col_item_full small-col_item_full">
					<nav id="nav_footer_content" role="navigation">
						<?php
						$args = array(
						'menu'              => 'Footer Menu',
						'container_id'      => 'menu_footer'
						);
						wp_nav_menu($args);
						?>
				    </nav>
				    <?php if (is_page('voucher-application') || is_page('voucher') || is_page('register')) { ?>
						<nav id="nav_footer_terms_content" role="navigation">
							<?php
							$args = array(
							'menu'              => 'Coach Footer Menu',
							'container_id'      => 'menu_terms_footer'
							);
							wp_nav_menu($args);
							?>
					    </nav>
				    <?php } else { ?>
						<nav id="nav_footer_terms_content" role="navigation">
							<?php
							$args = array(
							'menu'              => 'Footer Terms Menu',
							'container_id'      => 'menu_terms_footer'
							);
							wp_nav_menu($args);
							?>
					    </nav>					    	
				    <?php } ?>
				    <?php if (!is_page('voucher')) { ?>
				    <div class="nav_footer_social">
					    <a href="https://facebook.com/plaayuk" target="_blank" class="image-font"><span class="image-font__sizing image-font__fontawesome fa-facebook image-font_footer image-font__facebook"></span></a>
						<a href="https://twitter.com/plaayuk" target="_blank" class="image-font"><span class="image-font__sizing image-font__fontawesome fa-twitter image-font_footer image-font__twitter"></span></a>
						<a href="https://plus.google.com/111313808630392401548" target="_blank" class="image-font"><span class="image-font__sizing image-font__fontawesome fa-google image-font_footer image-font__google"></span></a>
					</div>
					<?php } ?>
				</div><!--
				--><div class="col_item col_item--align_right medium-col_item--align_center small-col_item--align_center col_item_4_10 medium-col_item_full small-col_item_full">
					<img src="<?php echo get_template_directory_uri() . '/img/logo_dark.png'; ?>" alt="PLAAY" class="footer-logo non_responsive" />
					<p class="footer_strapline">TRY IT. LOVE IT. PLAAY IT.</p>
					<p class="footer_registered">Copyright &copy; <?php echo date('Y'); ?></p>
					<p class="footer_registered">Registered in England and Wales No: 07312256</p>
					<p class="footer_registered">Registered Office: Unit 11, Hove Business Centre, Fonthill Road, Hove, East Sussex, BN3 6HA</p>
				</div>
			</div>
		</footer>
	</div>
</div><!-- .wrap_home_content -->

<?php wp_footer(); ?>

<!-- Add This Script -->
<?php if (is_singular('activities') || is_page('activities')) { ?>
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53f9f06c50178d9f"></script>
<?php } ?>

<?php if ( WP_ENV == 'prod' ) { ?>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-56721977-1');ga('send','pageview');
</script>

<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 943833609;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/943833609/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script>
  (function(){
    if(window.location.href.indexOf('/thanks?message=successful_voucher_application')!=-1){
      (new Image()).src="//www.googleadservices.com/pagead/conversion/943833609/?label=Ii0SCPmZ_2EQiYSHwgM&guid=ON&script=0";
    }
  })();
</script>

<?php } ?>

</body>
</html>