<?php
/**
 * Home Page
 */

get_header(); ?>

<main role="main">
    <?php while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php if (have_rows('home_info_repeater')) :
            $count_home_info = 1;
            $total_count_home_info = count(get_field('home_info_repeater'));
            //Need atleast same amount of images saved as sections
            $image_urls = get_field('home_images');
            shuffle($image_urls);

            $home_first_image = wp_get_attachment_image_src($image_urls[0]['image'], 'full');
            $home_first_image_touch = wp_get_attachment_image_src($image_urls[0]['image'], 'home-page-touch');
            ?>

            <div class="home_img home_img_touch_postcode_content" data-image="<?php echo esc_url($home_first_image[0]); ?>" data-image-mobile="<?php echo esc_url($home_first_image_touch[0]); ?>" data-no-retina>
                <div class="home_postcode_content">
                    <h3>PLAAY.CO.UK</h3>
                    <h1><?php the_field('home_strapline'); ?></h1>
                    <h4>TRY IT. LOVE IT. PLAAY IT.</h4>
                    <?php postcode_search_form_home(); ?>
                </div>
            </div>

            <?php while (have_rows('home_info_repeater')) : the_row(); ?>
            
                <div class="col">
                    <div class="col_item col_item_full">
                        <section id="home_section_<?php echo $count_home_info; ?>" class="home_section">
                            <?php if ($count_home_info < $total_count_home_info) { ?>
                                <a href="#home_section_<?php echo $count_home_info; ?>" class="image-font <?php echo $count_home_info == 1 ? 'home_scroll_first' : 'home_scroll'; ?>"><span class="image-font__sizing image-font__fontawesome fa-chevron-down image-font_home image-font__purple"></span></a>    
                            <?php } ?>                                    
                            <h3 class="home_section_heading"><?php strtoupper(the_sub_field('home_info_heading')); ?></h3>
                            <div class="col_item_4_5 medium-col_item_full small-col_item_full">
                                <?php the_sub_field('home_info_content'); ?>
                            </div>
                            <?php if ($count_home_info < $total_count_home_info) { ?>
                                <a href="#home_section_<?php echo $count_home_info + 1; ?>" class="image-font home_scroll"><span class="image-font__sizing image-font__fontawesome fa-chevron-down image-font_home image-font__purple"></span></a>
                            <?php } ?>
                        </section>
                    </div>
                  </div>

                <?php if ($count_home_info < $total_count_home_info) {
                    $home_image = wp_get_attachment_image_src($image_urls[$count_home_info]['image'], 'full');
                    $home_image_touch = wp_get_attachment_image_src($image_urls[$count_home_info]['image'], 'home-page-touch');
                    ?>
                    <div class="home_img" data-image="<?php echo esc_url($home_image[0]); ?>" data-image-mobile="<?php echo esc_url($home_image_touch[0]); ?>" data-no-retina></div>
                <?php } ?>

              <?php
            $count_home_info++;
            endwhile;
        endif; ?>

        </article>
    <?php endwhile; // end of the loop. ?>
</main>

<?php get_footer(); ?>