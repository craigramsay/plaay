<?php
/**
 * Template Name: Coach
 */

get_header('coach'); ?>

	<main role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if (is_page('invoice')) { ?>
					<img src="<?php echo get_template_directory_uri() . '/img/logo_dark.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/logo_dark@2x.png'; ?>" alt="PLAAY" class="non_responsive printed_invoice_logo" />
				<?php } ?>

				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</article>
		<?php endwhile; // end of the loop. ?>
	</main>

<?php get_footer('coach'); ?>