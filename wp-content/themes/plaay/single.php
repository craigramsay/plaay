<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

<main role="main">

	<div class="col">

		<div class="col_item col_item_7_10">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="col">
				<div class="col_item col_item_full">
					<?php get_template_part( 'content', 'single' ); ?>
				</div>
			</div>

			<?php plaay_post_pagination(); ?>

		<?php endwhile; //end loop. ?>

		</div><!--
	
		--><div class="col_item col_item_3_10">
		
			<?php get_sidebar(); ?>

		</div>

	</div><!-- .col -->

</main><!-- .main -->

<?php get_footer(); ?>