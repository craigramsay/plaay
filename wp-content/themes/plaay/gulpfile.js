/* ------------------------------------------------------------------------ *
 * Gulp Packages
 * ------------------------------------------------------------------------ */

var gulp            = require('gulp'); 

var autoprefixer    = require('gulp-autoprefixer');
var concat          = require('gulp-concat');
var cssUrlAdjuster  = require('gulp-css-url-adjuster');
var del             = require('del');
var minifyCSS       = require('gulp-minify-css');
var rename          = require('gulp-rename');
var replace         = require('gulp-replace');
var rsync           = require("rsyncwrapper").rsync;
var runSequence     = require('run-sequence');
var sass            = require('gulp-ruby-sass');
var sourcemaps      = require('gulp-sourcemaps');
var uglify          = require('gulp-uglify');

/*
https://github.com/sindresorhus/gulp-autoprefixer
https://www.npmjs.com/package/gulp-autoprefixer
Browser List for Autoprefixer https://github.com/ai/browserslist
https://github.com/wearefractal/gulp-concat
https://www.npmjs.com/package/gulp-concat
https://github.com/casualrelaxation/gulp-css-url-adjuster
https://www.npmjs.com/package/gulp-css-url-adjuster
https://github.com/sindresorhus/del
https://www.npmjs.com/package/del
https://github.com/jonathanepollack/gulp-minify-css
https://www.npmjs.com/package/gulp-minify-css
https://github.com/hparra/gulp-rename
https://www.npmjs.com/package/gulp-rename
https://github.com/jedrichards/rsyncwrapper
https://github.com/OverZealous/run-sequence
https://www.npmjs.com/package/run-sequence
https://github.com/sindresorhus/gulp-ruby-sass/tree/rw/1.0
https://www.npmjs.com/package/gulp-ruby-sass
https://github.com/floridoo/gulp-sourcemaps
https://www.npmjs.com/package/gulp-sourcemaps
https://github.com/terinjokes/gulp-uglify
https://www.npmjs.com/package/gulp-uglify
https://github.com/terinjokes/gulp-uglify/issues/56
*/


/* ------------------------------------------------------------------------ *
 * Critical CSS
 * 
 * gulp critical-render-css
 *
 * Loop through a selection of pages in urls object.
 * Extract critical css and store in style-critical.css.
 * When ready minify using clean-css. This is used instead of minify-css as
 * it is called directly by creating a new instance new cleanCSS().
 * Currently doesn't remove duplicate media queries so complete manually.
 * https://github.com/jakubpawlowicz/clean-css/issues/508.
 *
 * Media Queries to manually remove from style-critical.css
 *
 * @media screen and (max-width:986px){.main-navigation{font-size:.933333em}}@media screen and (max-width:942px){.main-navigation a{padding:0 10px}}@media screen and (max-width:876px){.main-navigation a{padding:0 8.5px}}@media screen and (max-width:846px){.main-navigation{font-size:.866666em}}@media screen and (max-width:816px){.main-navigation a{padding:0 7px}}@media screen and (max-width:790px){.main-navigation{font-size:.833333em}.main-navigation a{padding:0 7px}}
 * @media only screen and (min-width:481px)and (max-width:767px){.col-item-full--medium{width:100%}.col-item--align-center--medium{text-align:center}.col-item--margin-bottom-20--medium{margin-bottom:20px}}
 * @media screen and (max-width:767px){.wrap-mobile{margin-top:144px}.mobile-header{display:block}.main-header,.main-header-navigation{display:none}.nav-home a{text-indent:0}}
 * ------------------------------------------------------------------------ */


var penthouse       = require('penthouse'),
    fs              = require('fs'),
    cleanCSS        = require('clean-css'), 
    counter         = 1,
    urls            = {
        'home'                  : 'https://www.plaay.co.uk/',
        'activities'            : 'https://www.plaay.co.uk/activities',
    };
    /*
    https://github.com/pocketjoso/penthouse
    https://github.com/jakubpawlowicz/clean-css
    https://nodejs.org/api/fs.html

/**
 * Delete all contents of dev folder.
 */
gulp.task('critical-render-css-clean', function() {
    del('style-critical.css', {force:true});
});


for (var page in urls) {
    (function(page) {
        gulp.task(page, function () {
            penthouse({
                url     : urls[page],
                css     : 'style.css',
                width   : 1200,
                height  : 900
            }, function(err, criticalCss) {
                fs.appendFile('style-critical.css', criticalCss, function (err) {
                    if (err) throw err;
                    console.log(counter + ' URL complete - ' + urls[page]);
                    if (counter == Object.keys(urls).length) {
                        console.log('All Files Done');
                        fs.readFile('style-critical.css', function (err, data) {
                            if (err) throw err;
                            var minifiedCss = new cleanCSS().minify(data).styles;
                            fs.writeFile('style-critical.css', minifiedCss, function (err) {
                                if (err) throw err;
                                console.log('CSS Minified');
                            });
                        });
                    }
                    counter++;
                });
            });
        })
    })(page);
}


var urlTasks = [];
for (var page in urls) {
    urlTasks.push(page);
}


/**
 * Set up default (local) task.
 */
gulp.task('critical-render-css', function() {
    runSequence('critical-render-css-clean', 
                urlTasks
    );
})


/* ------------------------------------------------------------------------ *
 * Dev
 * 
 * gulp dev
 *
 * Move all applicable files and folders.
 * This includes all js for debugging with sourcemaps.
 * Compress CSS.
 * Concat and minify JS to scripts.js
 * ------------------------------------------------------------------------ */

/**
 * Delete all contents of dev folder.
 */
gulp.task('dev-clean', function () {
    del('../dev/*');
});


/**
  * Move root .php files, style.css as sass isn't used and screenshot.png for theme.
  */
gulp.task('dev-move-files', function() {
    return gulp.src(['*.php', 'screenshot.png', 'style-critical.css'])
        .pipe(gulp.dest('../dev'));
});


/**
  * Move fontawesome fonts.
  */
gulp.task('dev-move-fontawesome-fonts', function() {
    return gulp.src('bower_components/fontawesome/fonts/**')
        .pipe(gulp.dest('../dev/fonts'));
});


/**
  * Move root directories and their contents.
  * Move js and SASS to be used with root maps on dev.
  */
gulp.task('dev-move-dir', function() {
    return gulp.src(['css/**', 'fonts/**', 'img/**', 'js/**'], { base: './'} )
        .pipe(gulp.dest('../dev'));
});


/**
 * Minify CSS, Autoprefix.
 */
gulp.task('dev-styles', function() {
    return gulp.src('style.css')
    .pipe(autoprefixer({browsers: ['last 5 versions']}))
    .pipe(minifyCSS())
    .pipe(gulp.dest('../dev'));
});


/**
 * Pull in the plugin styles into main style.css.
 * Add to the compiled sass from dev-styles.
 * Sourcemap still works for SASS gnerated styles. The plugins don't use sourcemaps or SASS.
 * Also replace ..img/ path with img/.
 * Mainly uses loader.gif and is in the theme directory.
 */
gulp.task('dev-concat-plugin-styles', function() {
    return gulp.src([   '../dev/style.css',  
                        'bower_components/fontawesome/css/font-awesome.min.css'])
        .pipe(concat('style.css'))
        .pipe(cssUrlAdjuster({
            replace:  ['../','']
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest('../dev'));
});


/**
 * Cache buster for the inline and async references to styles.css in header.php
 */
gulp.task('dev-css-cache-buster', function() {
    return gulp.src('../dev/header.php')
        .pipe(replace(/style.css/g, function() {
            return 'style.' + Date.now() + '.css';        
        }))
        .pipe(gulp.dest('../dev'));
})


/**
 * Concat (rename) and minify our JS.
 *
 * sourceRoot sets the path where the source files are hosted relative to the source map.
 * This makes things appear in the correct folders when viewing through developer tools.
 *
 * Don't minimize or concat contact validate or jquery validate as minimized seperatley and only loaded on contact page.
 * Don't minimize or concat imagescroll as minimized seperatley and only loaded on home page for parallex.
 * Don't minimize modernizer seperatley as it is loaded in the header.
 * Don't minimize respond.js as it's only loaded in IE8 from the footer.
 */
gulp.task('dev-scripts', function() {
    return gulp.src(['js/*.js', '!js/contact-validate.js', '!js/html5shiv.js', '!js/jquery.imageScroll.js', '!js/jquery.validate.js', '!js/respond.js'])
		.pipe(sourcemaps.init())
		.pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('../sourcemaps', {includeContent: false, sourceRoot: '../js'}))
        .pipe(gulp.dest('../dev/js'));
});


/**
 * Minify and rename contact-validate.js, jquery.validate.js, jquery.imageScroll.js
 */
gulp.task('dev-scripts-others', function() {
    return gulp.src(['js/contact-validate.js', 'js/jquery.validate.js'])
        .pipe(sourcemaps.init())
        .pipe(rename(function (path) {
            path.extname = ".min.js"
        }))
        .pipe(uglify())
        .pipe(sourcemaps.write('../sourcemaps', {includeContent: false, sourceRoot: '../js'}))
        .pipe(gulp.dest('../dev/js'));
});


/**
 * Minify respond.js and htmlshiv.js.
 *
 * No concat just uglify and keep the same name.
 */
gulp.task('dev-scripts-respond-shiv', function() {
    return gulp.src(['js/respond.js', 'js/html5shiv.js'])
        .pipe(uglify())
        .pipe(gulp.dest('../dev/js'));
});


/**
 * Dev deploy
 *
 * Deploy the theme folder.
 */
gulp.task('dev-deploy', function() {
    rsync({
        src: "../dev/",
        dest: "devplaayco@plaay.co.uk:/home/devplaayco/public_html/wp-content/themes/plaay",
        ssh: true,
        recursive: true,
        deleteAll: true,
        exclude: ['.DS_Store'],
        args: ["--verbose"]
    },function (error,stdout,stderr,cmd) {
        if ( error ) {
            console.log(error.message);
        } else {
            console.log(stdout);
            console.log("Deployment Complete");
        }
    })
})


/**
 * Set up dev task.
 */
gulp.task('dev', function() {
  	runSequence('dev-clean', 
                'dev-move-files', 
                'dev-move-fontawesome-fonts',                 
                'dev-move-dir', 
                'dev-styles', 
                'dev-css-cache-buster', 
                'dev-concat-plugin-styles', 
                'dev-scripts', 
                'dev-scripts-others', 
                'dev-scripts-respond-shiv', 
                'dev-deploy'
            );
})


/* ------------------------------------------------------------------------ *
 * Prod
 * 
 * gulp prod
 *
 * Move all applicable files and folders.
 * Compress CSS.
 * Concat and minify JS to scripts.js
 * ------------------------------------------------------------------------ */

/**
 * Delete all contents of prod folder.
 */
gulp.task('prod-clean', function () {
    del('../prod/*');
});


/**
  * Move root .php files
  */
gulp.task('prod-move-files', function() {
    return gulp.src(['*.php', 'screenshot.png', 'style-critical.css'])
        .pipe(gulp.dest('../prod'));
});


/**
  * Move fontawesome fonts.
  */
gulp.task('prod-move-fontawesome-fonts', function() {
    return gulp.src('bower_components/fontawesome/fonts/**')
        .pipe(gulp.dest('../prod/fonts'));
});


/**
  * Move root directories and their contents.
  * Not js as we just need minified version as no sourcemaps are used in prod.
  */
gulp.task('prod-move-dir', function() {
    return gulp.src(['css/**', 'fonts/**', 'img/**'], { base: './'} )
        .pipe(gulp.dest('../prod'));
});


/**
 * Minify CSS, Autoprefix.
 */
gulp.task('prod-styles', function() {
    return gulp.src('style.css')
    .pipe(autoprefixer({browsers: ['last 5 versions']}))
    .pipe(minifyCSS())
    .pipe(gulp.dest('../prod'));
});


/**
 * Pull in the plugin styles into main style.css.
 * Add to the compiled sass from dev-styles.
 * Sourcemap still works for SASS gnerated styles. The plugins don't use sourcemaps or SASS.
 * Also replace ..img/ path with img/.
 * Mainly uses loader.gif and is in the theme directory.
 */
gulp.task('prod-concat-plugin-styles', function() {
    return gulp.src([   '../prod/style.css',  
                        'bower_components/fontawesome/css/font-awesome.min.css'])
        .pipe(concat('style.css'))
        .pipe(cssUrlAdjuster({
            replace:  ['../','']
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest('../prod'));
});


/**
 * Cache buster for the inline and async references to styles.css in header.php
 */
gulp.task('prod-css-cache-buster', function() {
    return gulp.src('../prod/header.php')
        .pipe(replace(/style.css/g, function() {
            return 'style.' + Date.now() + '.css';        
        }))
        .pipe(gulp.dest('../prod'));
})


/**
 * Concat (rename) and minify our JS.
 *
 * Don't minimize google maps as it's loaded on it's on and wp_localize_script with php settings if applicable.
 * Don't minimize moderinzer seperatley as it is loaded in the header.
 * Don't minimize respond.js as it's only loaded in IE8 from the footer.
 */
gulp.task('prod-scripts', function() {
    return gulp.src(['js/*.js', '!js/contact-validate.js', '!js/html5shiv.js', '!js/jquery.imageScroll.js', '!js/jquery.validate.js', '!js/modernizr-2.8.3.js', '!js/respond.js'])
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('../prod/js'));
});


/**
 * Minify and rename contact-validate.js, jquery.validate.js, jquery.imageScroll.js
 */
gulp.task('prod-scripts-others', function() {
    return gulp.src(['js/contact-validate.js', 'js/jquery.validate.js'])
        .pipe(rename(function (path) {
            path.extname = ".min.js"
        }))
        .pipe(uglify())
        .pipe(gulp.dest('../prod/js'));
});


/**
 * Minify respond.js and htmlshiv.js.
 *
 * No concat just uglify and keep the same name.
 */
gulp.task('prod-scripts-respond-shiv', function() {
    return gulp.src(['js/respond.js', 'js/html5shiv.js'])
        .pipe(uglify())
        .pipe(gulp.dest('../prod/js'));
});


/**
 * Dev deploy
 *
 * Deploy the theme folder.
 */
gulp.task('prod-deploy', function() {
    rsync({
        src: "../prod/",
        dest: "plaayco@plaay.co.uk:/home/plaayco/public_html/wp-content/themes/plaay",
        ssh: true,
        recursive: true,
        deleteAll: true,
        exclude: ['.DS_Store'],
        args: ["--verbose"]
    },function (error,stdout,stderr,cmd) {
        if ( error ) {
            console.log(error.message);
        } else {
            console.log(stdout);
            console.log("Deployment Complete");
        }
    })
})


/**
 * Set up prod task.
 */
gulp.task('prod', function() {
    runSequence('prod-clean', 
                'prod-move-files', 
                'prod-move-fontawesome-fonts',                 
                'prod-move-dir', 
                'prod-styles', 
                'prod-css-cache-buster', 
                'prod-concat-plugin-styles', 
                'prod-scripts', 
                'prod-scripts-others', 
                'prod-scripts-respond-shiv', 
                'prod-deploy'
            );
})