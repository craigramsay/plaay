<?php
/**
 * Template Name: Page Contact
 */
$error_messages = array();
$first_name 		= isset($_POST['first_name']) ? $_POST['first_name'] : '';
$email 				= isset($_POST['email']) ? $_POST['email'] : '';
$phone_number 		= isset($_POST['phone_number']) ? $_POST['phone_number'] : '';	
$comments 			= isset($_POST['comments']) ? $_POST['comments'] : '';

if (!empty($_POST['submit_contact'])):			
	$errors = malinky_process_contact($first_name , $phone_number, $email, $comments);
	if (is_wp_error($errors)) {
		$error_messages = $errors->errors;
	} else {
		$success = $errors;
	}
endif;
?>

<?php get_header(); ?>

<main role="main">

	<?php while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<h1><span><?php the_title(); ?></span></h1>		
		<div class="form_content"><?php the_content(); ?></div>

		<div class="col">
			<div class="col_item col_item_full">

					<form id="contact_form" action="" method="post" class="contact_form" role="form">

							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">		
									<label for="first_name"><?php _e('Name') ?><small> (required)</small></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
									<input type="text" class="input-text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr(wp_unslash($first_name)); ?>" />
									<?php if (isset($error_messages['first_name_error'][0])) echo '<p class="error">' . $error_messages['first_name_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->

							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">	
									<label for="email"><?php _e('Email Address') ?><small> (required)</small></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
								<input type="text" class="input-text" name="email" id="email" class="input" value="<?php echo esc_attr(wp_unslash($email)); ?>" />
								<?php if (isset($error_messages['email_error'][0])) echo '<p class="error">' . $error_messages['email_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->

							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">	
									<label for="phone_number"><?php _e('Phone Number') ?><small> (optional)</small></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
								<input type="text" class="input-text" name="phone_number" id="phone_number" class="input" value="<?php echo esc_attr(wp_unslash($phone_number)); ?>" />
								<?php if (isset($error_messages['phone_number_error'][0])) echo '<p class="error">' . $error_messages['phone_number_error'][0] . '</p>'; ?>			
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->

							<div class="col form_fields_2">
								<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">	
									<label for="comments"><?php _e('Comments') ?><small> (required)</small></label>
								</div><!--
								--><div class="col_item col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
								<textarea class="input-text" name="comments" id="comments" rows="8"><?php echo esc_textarea(wp_unslash($comments)); ?></textarea>
								<?php if (isset($error_messages['comments_error'][0])) echo '<p class="error">' . $error_messages['comments_error'][0] . '</p>'; ?>
								</div><!--
								--><div class="col_item col_item_1_10 medium-col_item_1_5 small-col_item_1_5">
									<div class="field_error_icon"></div>					
								</div>
							</div><!-- .col -->

						<?php
						//wp contact error
						if (isset($error_messages['contact_fail'][0])) echo '<p>' . $error_messages['contact_fail'][0] . '</p>';
						?>	

						<div class="col">
							<div class="col_item col_item_3_10 medium-col_item_full small-col_item_full">
							</div><!--
							--><div class="col_item col_item--align_right col_item_6_10 medium-col_item_4_5 small-col_item_4_5">
								<?php wp_nonce_field( 'malinky_process_contact_form', 'malinky_process_contact_form_nonce' ); ?>
								<input type="submit" name="submit_contact" class="button full_width" value="<?php esc_attr_e('Submit'); ?>" />
							</div>
						</div>

					</form>

			</div>
		</div><!-- .col -->
			
	</article>
	<?php endwhile; // end of the loop. ?>

</main>

<?php get_footer(); ?>