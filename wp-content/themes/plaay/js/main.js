jQuery(document).ready(function($){

    /*-----NAVIGATION-----*/
    //Ensure mobile nav is always hidden when back / forward cache is used 
    function pageShown(evt)
    {
        if (evt.persisted) {
            $('#nav_mobile_toggle').hide();
          }
    }

    if(window.addEventListener) {
      window.addEventListener("pageshow", pageShown, false);
    } else {
      window.attachEvent("pageshow", pageShown, false);
    }

    /*-----IE 7/8-----*/
    //if ($('html').hasClass('lt-ie10')) {  
      //$('#password').after('<small>Atleast one letter and one number and no special characters.</small>');
   // };
            
    /*-----NO CSS3 TRANSITIONS-----*/ 
    //if (!Modernizr.csstransitions) {
    //};
      
    /*-----FOOTER SCROLL TO TOP-----*/      
    /*$("#back_top").hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#back_top').fadeIn();
        } else {
            $('#back_top').fadeOut();
        }
    });*/

    // scroll body to 0px on click
    $('.back_top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    });
        
    /*-----INTERNAL LINKS SCROLL TO-----*/            
    $('.int_scroll').click(function () {
        var e = $(this).attr('href');
        $('body,html').animate({
            scrollTop: $(e).offset().top-70
        }, 600);
        return false;
    });

    /*-----INTERNAL LINKS SCROLL TO HOME -70 FOR NAV-----*/            
    $('.home_scroll, .home_scroll_first, .home_scroll a').click(function () {
        var e = $(this).attr('href');
        $('body,html').animate({
            scrollTop: $(e).offset().top-70
        }, 600);
        return false;
    });
    
    /*-----MOBILE NAV-----*/
    $('#nav_mobile_menu a').click(function(e) {
        $('#nav_mobile_toggle').slideToggle();
        e.preventDefault();
    });

    /*-----PARALLEX-----*/
    /*var touch       = Boolean(malinky_parallex_mobile.is_mobile_tablet);
    var isFrontPage = malinky_parallex_mobile.is_front_page;
    if (isFrontPage) {
        //Break parallex for ie8 and ie9
        if ( $('body').hasClass('lt-ie10') ) {
            touch = true;
        }
        $('.home_img').imageScroll({
            imageAttribute: (touch === true) ? 'image-mobile' : 'image',
            touch: touch,
            holderClass: 'home_img_holder',
            coverRatio: 0.8
        });
        if (touch === true) {
            $('html').addClass('touch');
            var $home_img_touch_first = $('.home_img_touch_postcode_content img');
            $home_img_touch_first.remove();
            $('.home_postcode_content').after($home_img_touch_first);
        }
    }*/

});