/**
 * -----------------------------
 * CONTACT FORM VALIDATION
 * -----------------------------
 */
jQuery(document).ready(function($){

    jQuery.validator.addMethod("valid_numbers_space", function(value, element) {
        return this.optional(element) || /^[0-9 ]+$/i.test(value);
    }, "Numbers and space only please");                 
 
    // validate contact form on keyup and submit
    $("#contact_form").validate({
 
        //set the rules for the fild names
        rules: {
            first_name: {
                required: true
            },                 
            email: {
                required: true,
                email: true
            },
            phone_number: {
                valid_numbers_space: true
            },            
            comments: {
                required: true
            }                              
        },
 
        //set error messages
        messages: {           
            first_name: "Please enter your name.",         
            email: {
                required: "Please enter a valid email address.",
                email: "Please enter a valid email address."
            },
            phone_number: {
                valid_numbers_space: "Please enter a valid phone number."
            },
            comments: "Please enter a comment.",                                                                         
        },
 
        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
            error.insertAfter(element);
            if ($(element).hasClass('error')) {
                $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');
            }
        },

        success: function(label) {
            $(label).parent().next().children('.field_error_icon').addClass('field_error_tick');
            label.css('margin-top', '0');
            label.css('margin-bottom', '0');
        },

        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_tick');            
            $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');            
            //$(element).next('p.error').css('margin-top', '1em');
            //$(element).next('p.error').css('margin-bottom', '1em');
        },

        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_cross');
            $(element).parent().next().children('.field_error_icon').addClass('field_error_tick');
            //$(element).next('p.error').css('margin-top', '0');
            //$(element).next('p.error').css('margin-bottom', '0');
        }

    });
});