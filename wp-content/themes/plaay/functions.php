<?php
/**
 * --------------------------
 * SETUP BASIC THEME SETTINGS
 * --------------------------
 */
add_action( 'after_setup_theme', 'plaay_setup' );
function plaay_setup() {

	/** ************************************************************************
	 * This theme styles the visual editor with editor-style.css to match the
	 * theme style.
	 **************************************************************************/
	add_editor_style();

	/** ************************************************************************
	 * Adds RSS feed links to <head> for posts and comments.
	 **************************************************************************/
	add_theme_support( 'automatic-feed-links' );

	/** ************************************************************************
	 * This theme supports a variety of post formats.
	 **************************************************************************/
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );

	/** ************************************************************************
	 * This theme uses wp_nav_menu() in one location.
	 **************************************************************************/
	register_nav_menu( 'front-end', 'Front End Menu' );
	register_nav_menu( 'footer', 'Footer Menu' );
	register_nav_menu( 'footer-terms', 'Footer Terms Menu' );
	register_nav_menu( 'coach', 'Coach Menu' );
	register_nav_menu( 'coach-footer', 'Coach Footer Menu' );

	/** ************************************************************************
	 * Set up crop size for coach logo
	 **************************************************************************/
	add_image_size( 'coach-logo', 277, 277 );
	add_image_size( 'home-page-touch', 600 );

	/**
	 * Include server side device detection.
	 *
	 * @link http://mobiledetect.net/
	 * @link https://github.com/serbanghita/Mobile-Detect/
	 */
	if ( ! is_admin() ) {

		if ( WP_ENV == 'local' ) {

		    require_once(ABSPATH . '/includes/Mobile_Detect.php');

		} elseif ( WP_ENV == 'dev' ) {

		    require_once(ABSPATH . '../includes/Mobile_Detect.php');    

		} else {

		    require_once(ABSPATH . '../includes/Mobile_Detect.php');

		}

		global $malinky_mobile_detect;
		$malinky_mobile_detect = new Mobile_Detect();

		function malinky_is_phone()
		{
			global $malinky_mobile_detect;
			if ( $malinky_mobile_detect->isMobile() && ! $malinky_mobile_detect->isTablet() )
				return true;
		}

		function malinky_is_phone_tablet()
		{
			global $malinky_mobile_detect;
			if ( $malinky_mobile_detect->isMobile() || $malinky_mobile_detect->isTablet() )
				return true;
		}	

		function malinky_is_phone_computer()
		{
			global $malinky_mobile_detect;
			if ( ! $malinky_mobile_detect->isTablet() )
				return true;
		}						

		function malinky_is_tablet()
		{
			global $malinky_mobile_detect;
			if ( $malinky_mobile_detect->isTablet() )
				return true;
		}

		function malinky_is_tablet_computer()
		{
			global $malinky_mobile_detect;
			if ( $malinky_mobile_detect->isTablet() || ! $malinky_mobile_detect->isMobile() )
				return true;
		}			

		function malinky_is_computer()
		{
			global $malinky_mobile_detect;
			if ( ! $malinky_mobile_detect->isMobile() && ! $malinky_mobile_detect->isTablet() )
				return true;
		}	

	}

}

/**
 * ------------------------
 * ENQUEUE FRONTEND SCRIPTS
 * ------------------------
 */
add_action( 'wp_enqueue_scripts', 'plaay_frontend_scripts');
function plaay_frontend_scripts()
{

	/**
	 * Load WP jQuery and jQuery migrate in the footer.
	 */
	if ( ! is_admin() && tree() != 'coach' ) {

		wp_deregister_script( 'jquery' );
		wp_deregister_script( 'jquery-migrate' );

		wp_register_script( 'jquery',
							'/wp-includes/js/jquery/jquery.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'jquery' );

		wp_register_script( 'jquery-migrate',
							'/wp-includes/js/jquery/jquery-migrate.min.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'jquery-migrate' );

	}


	if (is_page('invoice')) {
		wp_register_style( 'plaay-print', get_template_directory_uri() . '/css/print.css', '', NULL, 'print' );
		wp_enqueue_style( 'plaay-print' );
	}


	if ( WP_ENV == 'local' ) {

		/**
		 * Stylesheet which includes normalize.
		 * Remove this to test inline and async css.
		 */
		wp_enqueue_style( 'plaay-style', get_stylesheet_uri() );


		/**
		 * Font awesome font.
		 *
		 * @link http://fortawesome.github.io/Font-Awesome/
		 */		
		wp_register_style( 'plaay-font-awesome', 
						   '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css', 
						   false, 
						   NULL
		);
		wp_enqueue_style( 'plaay-font-awesome' );


		/**
		 * Malinky Media related javascript and jQuery.
		 */
		wp_register_script( 'plaay-main-js',
							get_template_directory_uri() . '/js/main.js',
							array( 'jquery' ),
							NULL,
							true
		);
		$malinky_parallex_mobile['is_mobile_tablet'] 	= malinky_is_phone_tablet();
		$malinky_parallex_mobile['is_front_page'] 		= is_front_page();
		wp_localize_script( 'plaay-main-js', 'malinky_parallex_mobile', $malinky_parallex_mobile );
		wp_enqueue_script( 'plaay-main-js' );


		/*if (is_front_page()) {
			wp_register_script( 'plaay-imagescroll-js', get_template_directory_uri() . '/js/jquery.imageScroll.js', array( 'jquery' ), NULL, true );
			wp_enqueue_script( 'plaay-imagescroll-js' );
		}*/


		if (is_page('contact')) {
			wp_register_script( 'plaay-validate-js', get_template_directory_uri() . '/js/jquery.validate.js', '', NULL, true );
			wp_register_script( 'plaay-contact', get_template_directory_uri() . '/js/contact-validate.js', array( 'plaay-validate-js' ), NULL, true );
			wp_enqueue_script( 'plaay-contact' );
		}

	}


	if ( WP_ENV == 'dev' || WP_ENV == 'prod' ) {

		/**
		 * Enqueue normal css for coach section rather than critical.
		 */
		if ( tree() == 'coach' ) {
			/**
			 * Stylesheet which includes normalize.
			 */
			wp_enqueue_style( 'plaay-style', get_stylesheet_uri() );
		}

		/**
		 * Modernizr which includes html5shiv.
		 *
		 * @link http://modernizr.com/
		 * @link https://github.com/aFarkas/html5shiv
		 */
		/*wp_register_script( 'malinky-modernizr-js',
							get_template_directory_uri() . '/js/modernizr-2.8.3.js',
							false,
							NULL
		);
		wp_enqueue_script( 'malinky-modernizr-js' );*/


		/*
		 * main.js
		 */
		wp_register_script( 'plaay-scripts-min-js',
							get_template_directory_uri() . '/js/scripts.min.js',
							array( 'jquery' ),
							NULL,
							true
		);
		/*$malinky_parallex_mobile['is_mobile_tablet'] 	= malinky_is_phone_tablet();
		$malinky_parallex_mobile['is_front_page'] 		= is_front_page();
		wp_localize_script( 'plaay-scripts-min-js', 'malinky_parallex_mobile', $malinky_parallex_mobile );*/		
		wp_enqueue_script( 'plaay-scripts-min-js' );

		/*if (is_front_page()) {
			wp_register_script( 'plaay-imagescroll-js', get_template_directory_uri() . '/js/jquery.imageScroll.min.js', array( 'jquery' ), NULL, true );
			wp_enqueue_script( 'plaay-imagescroll-js' );
		}*/

		if (is_page('contact')) {
			wp_register_script( 'plaay-validate-js', get_template_directory_uri() . '/js/jquery.validate.min.js', '', NULL, true );
			wp_register_script( 'plaay-contact', get_template_directory_uri() . '/js/contact-validate.min.js', array( 'plaay-validate-js' ), NULL, true );
			wp_enqueue_script( 'plaay-contact' );
		}

	}

}

/**
 * ----------------------
 * SETUP CUSTOM RSS FEEDS
 * ----------------------
 */
add_action('init', 'malinky_activities_rss');
function malinky_activities_rss()
{
	add_feed('feed-activities', 'malinky_activities_rss_template');
}

function malinky_activities_rss_template()
{
    get_template_part('rss', 'activity-listings');
}

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function plaay_widgets_init()
{

	register_sidebar( array(
		'name'          => __( 'Sidebar', 'malinky' ),
		'id'            => 'sidebar-1',
		'description'   => 'Main sidebar that appears on the left.',
		'before_widget' => '<aside id="%1$s" class="sidebar_block widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="sidebar_heading">',
		'after_title'   => '</h4>',
	) );

}

add_action( 'widgets_init', 'plaay_widgets_init' );

/**
 * ------------------------------------
 * CLIMB TO PARENT PAGE AND RETURN SLUG
 * @return string   
 * ------------------------------------
 */
function tree()
{
	$parent_post_name = '';
  	if( is_page() ) {
  		global $post;
      	/* Get an array of Ancestors and Parents if they exist */
  		$parents = get_post_ancestors( $post->ID );
      	/* Get the top Level page->ID count base 1, array base 0 so -1 */ 
  		$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
     	/* Get the parent and set the $parent_post_name with the page slug (post_name) */
  		$parent = get_page( $id );
  		$parent_post_name = $parent->post_name;
	}
	return $parent_post_name;
}

/**
 * --------------------
 * REMOVE GENERATOR TAG
 * @return string   
 * --------------------
 */
add_filter('the_generator', 'plaay_remove_version');
function plaay_remove_version()
{
	return '';
}

/**
 * ---------------------------------------
 * ADD SPECIAL CLASS TO ACTIVITY PAGES NAV
 * @return string   
 * ---------------------------------------
 */
add_filter( 'nav_menu_css_class', 'my_special_nav_class', 10, 2 );
function my_special_nav_class( $classes, $item )
{
	global $post;
	
	$post_type = get_post_type( $post );
    if( $post_type == 'activities' && $item->title == 'Activities' )
    {
        $classes[] = 'activities_menu_active_class';
    }
    return $classes;
}

/**
 * -------------------------------
 * CHANGE URL OF LOGIN SCREEN LOGO
 * @return string   
 * -------------------------------
 */
add_filter('login_headerurl', 'plaay_change_login_logo_link');
function plaay_change_login_logo_link()
{
	return esc_url ( site_url('', 'http') );
}

/**
 * -------------------------------------------
 * REPLACE USERNAME WITH EMAIL ON LOGIN SCREEN
 * -------------------------------------------
 */
if (in_array($GLOBALS['pagenow'], array('wp-login.php'))) {
	add_filter('gettext', 'login_add_email_text');
	function login_add_email_text($translated_text)
	{
	if ($translated_text === 'Username') {
	    $translated_text = 'Email';
	}

	return $translated_text;
	}
}

/**
 * -------------------------
 * REMOVE THE SHAKE ON LOGIN
 * -------------------------
 */
add_action('login_head', 'malinky_register_login_head');
function malinky_register_login_head()
{
    remove_action('login_head', 'wp_shake_js', 12);
}

/**
 * ----------------
 * STYLE LOGIN PAGE 
 * ----------------
 */
add_action( 'login_enqueue_scripts', 'plaay_login_logo' );
function plaay_login_logo()
{ ?>
    <style type="text/css">
    	body.login {
    		background-color: rgba(36,36,36,0.9);
    	}
        body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri() . '/img/logo.png'; ?>);
            background-size: 255px;
            width: auto;
        }
        .login h1 a {
        	background-size: auto;
        }
        html.ie8 body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri() . '/img/logo.png'; ?>);
        }
        .wp-core-ui .button.button-large {
        	height: auto;
        	line-height: normal;
		    padding: 10px;
        }        
        .wp-core-ui .button-primary {
		    width: auto;
		    height: auto;
		    display: inline-block;
		    padding: 10px;
		    background: #9e4381;
		    border: none;
		    color: #FFFFFF;
		    -webkit-border-radius: 5px;
		    -moz-border-radius: 5px;
		    border-radius: 5px;
		    font-weight: normal;   
		    line-height: normal;
		    margin-left: 4px;
		    text-shadow: none;
		    box-shadow: none;
		    font-size: 1.076923em;		
        }
        .wp-core-ui .button-primary:hover {
    		background: #852b68;
    		text-shadow: none;
        }        
        .login #nav {
        	text-shadow: none;
			padding: 26px 24px;
			background: #FFFFFF;
			border: 1px solid #e5e5e5;
			/*-webkit-box-shadow: rgba(200,200,200,.7) 0 4px 10px -1px;
			box-shadow: rgba(200,200,200,.7) 0 4px 10px -1px;*/
			-webkit-box-shadow: none;
			box-shadow: none;
			text-align: center;
		}
		.login #nav a {
		    width: auto;
		    height: auto;
		    display: block;
		    padding: 10px;
		    background: #9e4381;
		    border: none;
		    color: #FFFFFF !important;
		    -webkit-border-radius: 5px;
		    -moz-border-radius: 5px;
		    border-radius: 5px;
		    font-weight: normal;   
		    line-height: normal;
		    margin-left: 0px;
		    text-decoration: none;
		    font-size: 1.076923em;		    
		}
		.login #nav a:hover {
			color: #FFFFFF !important;
			/*background: #2072a1;*/
			background: #852b68;
			text-shadow: none;
		}
		.login #nav a:first-child {
			/*background: #f49316;*/
			background: #9e4381;
		}
		.login #nav a:first-child:hover {
			/*background: #e48306;*/
			background: #852b68;
		}
		.mobile #login #nav,
		.mobile #login #backtoblog {
			margin-left: 0;
		}
	
    </style>
<?php }

/**
 * --------------------------------------------------
 * PROCESS THE CUSTOM CONTACT FORM AND ERROR CHECKING
 * @param string $first_name  
 * @param string $phone_number  
 * @param string $email  
 * @param string $comments     
 * @return void    
 * --------------------------------------------------
 */
function malinky_process_contact($first_name, $phone_number, $email, $comments)
{
	//check form submit and nonce
	if ( (empty($_POST['submit_contact'])) || (!isset($_POST['malinky_process_contact_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_process_contact_form_nonce'], 'malinky_process_contact_form')) )
		wp_die(__('There was a fatal error with your form submission'));

	$errors = new WP_Error();
	$contact_error_messages = malinky_process_contact_error_messages();

	$sanitized_first_name 			= sanitize_text_field($first_name);
	$sanitized_phone_number 		= sanitize_text_field($phone_number);	
	$sanitized_email 				= sanitize_email($email);
	$sanitized_comments 			= sanitize_text_field($comments);		

	//check the first_name
	if ($sanitized_first_name == '') {
		$errors->add( 'first_name_error', __($contact_error_messages['first_name_error']));
	}

	//check the phone_number
	if ($sanitized_phone_number != '') {
		if (!preg_match('/[0-9 ]+$/', $sanitized_phone_number)) {		
			$errors->add( 'phone_number_error', __($contact_error_messages['phone_number_error_2']));
		}
	}	

	//check the email
	if ($sanitized_email == '') {
		$errors->add( 'email_error', __($contact_error_messages['email_error']));
	} elseif (!is_email($sanitized_email)) {
		$errors->add('email_error', __($contact_error_messages['email_error']));
	}

	//check the comments
	if ($sanitized_comments == '') {
		$errors->add( 'comments_error', __($contact_error_messages['comments_error']));
	}

	//if validation errors
	if ( $errors->get_error_code() )
		return $errors;

	//no errors send email

	//send registration email
	malinky_process_contact_email($sanitized_first_name, $sanitized_phone_number, $sanitized_email, $sanitized_comments);

	wp_safe_redirect('contacted');
	exit();
}

/**
 * -------------------
 * CONTACT FORM ERRORS
 * -------------------
 */
function malinky_process_contact_error_messages()
{
	return array(
		'first_name_error'			=> 'Please enter your name.',
		'phone_number_error'		=> 'Please enter your phone number.',
		'phone_number_error_2'		=> 'Please enter a valid phone number.',
		'email_error'				=> 'Please enter a valid email address.',
		'comments_error'			=> 'Please enter a comment.',
	);
}

/**
 * -------------------------------------
 * SEND THE CONTACT EMAIL
 * @param string $sanitized_first_name  
 * @param string $sanitized_phone_number  
 * @param string $sanitized_email  
 * @param string $sanitized_comments     
 * @return void   
 * -------------------------------------
 */
function malinky_process_contact_email($sanitized_first_name, $sanitized_phone_number, $sanitized_email, $sanitized_comments)
{
	include_once( ABSPATH . WPINC . '/class-phpmailer.php' );

	$email_settings = get_option('malinky_activities_email_settings_options');

	if (!$email_settings) return;
	
	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host 		= $email_settings['settings_smtp_host'];
	$mail->SMTPAuth   	= true;
	$mail->Port       	= $email_settings['settings_smtp_port'];
	$mail->Username   	= $email_settings['settings_smtp_username'];
	$mail->Password   	= $email_settings['settings_smtp_password'];
	$mail->SMTPSecure 	= 'ssl';	

	$mail->AddAddress(get_option('admin_email'), "PLAAY");

	$mail->SetFrom(get_option('admin_email'), "PLAAY");

	$mail->IsHTML(true);

	$mail->Subject = 'PLAAY Contact Form Message';
	$mail->Body    = '<p>Name: ' . $sanitized_first_name . '</p>';
	$mail->Body    .= '<p>Email: ' . $sanitized_email . '</p>';
	$mail->Body    .= '<p>Phone: ' . $sanitized_phone_number . '</p>';
	$mail->Body    .= '<p>Comments: ' . $sanitized_comments . '</p>';			

	if(!$mail->Send()) {
		error_log($mail->ErrorInfo, 0);
    }
}

/**
 * -----------------------------------
 * ADD LOGIN / LOGOUT TO NAV FOR COACH
 * -----------------------------------
 */
add_filter('wp_nav_menu_front-end-menu_items', 'malinky_add_loginout_front_end_menu');
function malinky_add_loginout_front_end_menu($items)
{
	//check if it's a user coach
	global $current_user;
	get_currentuserinfo();
	if (!in_array('coach', (array) $current_user->roles))
		return $items;

	//remove any https:// first incase we're on a secure page, ensures account link is added with https:// below
	$items = str_replace('https://', 'http://', $items);

	$my_account_link = '<li id="menu-item-myaccount" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-myaccount"><a href="' . esc_url(site_url('coach', 'https')) . '">Account</a></li>';
	$items = $my_account_link . $items;

	return $items;
}

/**
 * ----------------------------
 * ADD LOGIN / LOGOUT TO FOOTER
 * ----------------------------
 */
add_filter('wp_nav_menu_footer-menu_items', 'malinky_add_loginout_footer_menu');
function malinky_add_loginout_footer_menu($items)
{
	//remove any https:// first incase we're on a secure page, ensures account link is added with https:// below
	$items = str_replace('https://', 'http://', $items);

	$items .= '<li id="menu-item-loginout" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-loginout">' . wp_loginout('', false) . '</li>';
	return $items;
}

/**
 * ---------------------------------------------
 * REMOVE HTTPS FROM TERMS AND CONDITIONS FOOTER
 * ---------------------------------------------
 */
add_filter('wp_nav_menu_footer-terms-menu_items', 'malinky_remove_http_terms_footer_menu');
function malinky_remove_http_terms_footer_menu($items)
{
	//remove any https:// incase we're on a secure page
	$items = str_replace('https://', 'http://', $items);
	return $items;
}

/**
 * -----------------------------
 * MAKE login Login AND ONE WORD
 * -----------------------------
 */
add_filter('loginout', 'malinky_loginout_strtoupper_oneword');
function malinky_loginout_strtoupper_oneword($link)
{
	$link = str_replace('Log in', 'Login', $link);
	$link = str_replace('Log out', 'Logout', $link);
	return $link;
}

/**
 * ----------------------------------
 * HIDE ADMIN BAR FOR LOGGED IN USERS
 * ----------------------------------
 */
show_admin_bar(false);

/**
 * ---------------
 * STRING TRUNCATE
 * @param string $text
 * @param int $length
 * @return string
 * ---------------
 */
function truncate_words($text, $length) {
   $length = abs((int)$length);
   if(strlen($text) > $length) {
      $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
   }
   return $text;
}

/**
 * ----------------------------
 * BLOG POSTS PAGINATION
 * ----------------------------
 */
if ( ! function_exists( 'plaay_posts_pagination' ) ) {

	/**
	 * Display navigation to next/previous set of posts when applicable.
	 */
	function plaay_posts_pagination()
	{

		//Return if only 1 page
		if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
			return;
		} ?>

		<nav class="col posts-pagination col--gutterless" role="navigation">

			<div class="col_item col_item_half posts-pagination__link posts-pagination__link--older">
				<?php if ( get_next_posts_link() ) { ?>
						<?php next_posts_link( 'Older Posts' ); ?>
				<?php } ?>
			</div><!--

			--><div class="col_item col_item_half col_item--align_right posts-pagination__link posts-pagination__link--newer">
				<?php if ( get_previous_posts_link() ) { ?>
						<?php previous_posts_link( 'Newer Posts' ); ?>
				<?php } ?>			
			</div>

		</nav><!-- .posts-pagination -->

	<?php }

}

/**
 * ----------------------------
 * BLOG SINGLE POST PAGINATION
 * ----------------------------
 */
if ( ! function_exists( 'plaay_post_pagination' ) ) {

	/**
	 * Display navigation to next/previous post when applicable.
	 */
	function plaay_post_pagination()
	{

		//Return if no navigation.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) return; ?>

		<nav class="col post-pagination col--gutterless" role="navigation">

			<div class="col_item col_item_half post-pagination__link post-pagination__link--older">
				<?php previous_post_link( '%link', '%title' ); ?>
			</div><!--

			--><div class="col_item col_item_half col_item--align_right post-pagination__link post-pagination__link--newer">
				<?php next_post_link( '%link', '%title' ); ?>
			</div>

		</nav><!-- .post-pagination -->
		
	<?php }

}

/**
 * ----------------------------
 * BLOG META DATA
 * ----------------------------
 */
if ( ! function_exists( 'plaay_content_meta' ) ) {
	/**
	 * Posted and updated dates and author name / link.
	 *
	 * @param bool $show_author Set to false to hide author details.
	 */
	function plaay_content_meta( $show_author = true )
	{

		$posted_time = '';
		$updated_string = '';

		$posted_time = sprintf( 
			'<time class="content-header__meta__date--published" datetime="%1$s">%2$s</time>', 
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);

		$posted_string = 'Posted on ' . $posted_time;

		if ( ! $show_author )
			return '<span class="content-header__meta__date">' . $posted_string . '</span>';

		return '<span class="content-header__meta__date">' . $posted_string . '</span>';

	}

}

/**
 * Replace [...] from automatic excerpts.
 */
function plaay_excerpt( $more )
{

	return '...';

}

add_filter( 'excerpt_more', 'plaay_excerpt' );


if ( ! function_exists( 'plaay_read_more_text' ) )
{

	/**
	 * Return read more text for use in the_content and manually after the_excerpt().
	 */
	function plaay_read_more_text()
	{

		return '<span class="content-summary__more-link">Read More...</span>';

	}

}

if ( ! function_exists( 'plaay_archive_title' ) ) {

	/**
	 * Shim for `the_archive_title()`.
	 *
	 * Display the archive title based on the queried object.
	 *
	 * @todo Remove this function when WordPress 4.3 is released.
	 *
	 * @param string $before Optional. Content to prepend to the title. Default empty.
	 * @param string $after  Optional. Content to append to the title. Default empty.
	 */
	function plaay_archive_title( $before = '', $after = '' )
	{

		if ( is_category() ) {
			$title = sprintf( __( 'Category: %s', 'malinky' ), single_cat_title( '', false ) );
		} elseif ( is_tag() ) {
			$title = sprintf( __( 'Tag: %s', 'malinky' ), single_tag_title( '', false ) );
		} elseif ( is_author() ) {
			$title = sprintf( __( 'Author: %s', 'malinky' ), '<span class="vcard">' . get_the_author() . '</span>' );
		} elseif ( is_year() ) {
			$title = sprintf( __( 'Year: %s', 'malinky' ), get_the_date( _x( 'Y', 'yearly archives date format', 'malinky' ) ) );
		} elseif ( is_month() ) {
			$title = sprintf( __( 'Month: %s', 'malinky' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'malinky' ) ) );
		} elseif ( is_day() ) {
			$title = sprintf( __( 'Day: %s', 'malinky' ), get_the_date( _x( 'F j, Y', 'daily archives date format', 'malinky' ) ) );
		} elseif ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title', 'malinky' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title', 'malinky' );
		} elseif ( is_post_type_archive() ) {
			$title = sprintf( __( 'Archives: %s', 'malinky' ), post_type_archive_title( '', false ) );
		} elseif ( is_tax() ) {
			$tax = get_taxonomy( get_queried_object()->taxonomy );
			/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
			$title = sprintf( __( '%1$s: %2$s', 'malinky' ), $tax->labels->singular_name, single_term_title( '', false ) );
		} else {
			$title = __( 'Archives', 'malinky' );
		}

		/**
		 * Filter the archive title.
		 *
		 * @param string $title Archive title to be displayed.
		 */
		$title = apply_filters( 'get_the_archive_title', $title );

		if ( ! empty( $title ) ) {
			echo $before . $title . $after;
		}

	}

}

if ( ! function_exists( 'plaay_archive_description' ) ) {

	/**
	 * Shim for `the_archive_description()`.
	 *
	 * Display category, tag, or term description.
	 *
	 * @todo Remove this function when WordPress 4.3 is released.
	 *
	 * @param string $before Optional. Content to prepend to the description. Default empty.
	 * @param string $after  Optional. Content to append to the description. Default empty.
	 */
	function plaay_archive_description( $before = '', $after = '' )
	{

		$description = apply_filters( 'get_the_archive_description', term_description() );

		if ( ! empty( $description ) ) {
			/**
			 * Filter the archive description.
			 *
			 * @see term_description()
			 *
			 * @param string $description Archive description to be displayed.
			 */
			echo $before . $description . $after;
		}

	}

}