		<div id="footer_coach">
		<footer id="footer_content" role="contentinfo">
			<div class="col">
				<div class="col_item medium-col_item--align_center small-col_item--align_center col_item_6_10 medium-col_item_full small-col_item_full">
					<nav id="nav_footer_terms_content" role="navigation">
						<?php
						$args = array(
						'menu'              => 'Coach Footer Menu',
						'container_id'      => 'menu_terms_footer'
						);
						wp_nav_menu($args);
						?>
				    </nav>					
				</div><!--
				--><div class="col_item col_item--align_right medium-col_item--align_center small-col_item--align_center col_item_4_10 medium-col_item_full small-col_item_full">
					<img src="<?php echo get_template_directory_uri() . '/img/logo_dark.png'; ?>" data-at2x="<?php echo get_template_directory_uri() . '/img/logo_dark@2x.png'; ?>" alt="PLAAY" class="non_responsive" />
					<p class="footer_strapline">TRY IT. LOVE IT. PLAAY IT.</p>
					<p class="footer_registered">Copyright &copy; <?php echo date('Y'); ?></p>
					<p class="footer_registered">Registered in England and Wales No: 07312256</p>
					<p class="footer_registered">Registered Office: Unit 11, Hove Business Centre, Fonthill Road, Hove, East Sussex, BN3 6HA</p>
				</div>
			</div>
		</footer>
		</div>
		
</div><!-- .wrap -->

<?php wp_footer(); ?>

<?php if ( WP_ENV == 'prod' ) { ?>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-56721977-1');ga('send','pageview');
</script>

<?php } ?>

</body>
</html>