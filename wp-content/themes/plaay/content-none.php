<?php
/**
 * The template part for no posts to display in archives.php, index.php and search.php
 */
?>
<article>

	<h1>We're sorry the activity or page could not be found.</h1>
	<p>Please try again using the main navigation.</p>
	<p>Alternatively, search for activities by entering your postcode in the form below.</p>
	<div class="col">
        <div class="col_item col_item--align_left col_item_full">
            <?php postcode_search_form_404(); ?>
        </div>
    </div><!-- .col -->

</article>