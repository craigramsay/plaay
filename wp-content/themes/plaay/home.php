<?php
/**
 * Blog Landing
 */

get_header(); ?>

<main role="main">

	<div class="col">

		<div class="col_item col_item_7_10">

			<?php if ( have_posts() ) { ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<div class="col">
						<div class="col_item col_item_full">
							<?php get_template_part( 'content' ); ?>
						</div>
					</div>

				<?php endwhile; //end loop.

				plaay_posts_pagination();

			} else { ?>

					<div class="col">
						<div class="col_item col_item_full">
							<?php get_template_part( 'content', 'none' ); ?>
						</div>
					</div>				

			<?php } ?>

		</div><!--
			
		--><div class="col_item col_item_3_10">
		
			<?php get_sidebar(); ?>

		</div>

	</div><!-- .col -->

</main><!-- .main -->

<?php get_footer(); ?>								